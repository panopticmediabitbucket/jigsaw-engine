
#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "TestUtils.h"
#include "Orchestration/MockMessageQueue.h"
#include "Windows/ViewportWindow.h"
#include "MockCommandListExecutor.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	TEST_CLASS(ViewportWindowTest)
	{

		TEST_CLASS_INITIALIZE(Initialize) {
			MockRootProperties();
		}

		Jigsaw::Ref<Jigsaw::jsContext> context;

		TEST_METHOD_INITIALIZE(BeforeEach) {
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back({ "Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
			context = Jigsaw::jsContext::Create(config);
			std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();
			std::chrono::duration<float> s = end - start;
			float _s = s.count();
		}

		TEST_METHOD_CLEANUP(AfterEach) {
			context.reset();
		}

		TEST_METHOD(TestViewportWindowInitialize) {

			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();
			Jigsaw::orMessageQueue* message_queue = orchestrator->GetMessageQueue();
			Jigsaw::GraphicsContext* render_context = context->GetKnob<Jigsaw::GraphicsContext>();

			ViewportWindow viewport_window(1, render_context, orchestrator);

			{
				// the message is not handled because the application orchestrator is not started until the window is shown
				auto listener = message_queue->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, Jigsaw::QWORD());
				Sleep(1);
				Assert::IsFalse(listener->Check());

				viewport_window.HandleMessage(0, WM_SHOWWINDOW, 0, 0);

				// the message will be handled after the main application orchestrator is launched
				Sleep(1);
				Assert::IsTrue(listener->Check());
			}

			viewport_window.HandleMessage(0, WM_CLOSE, 0, 0);

			{
				// the message is not handled because the application orchestrator has been terminated
				auto listener = message_queue->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, Jigsaw::QWORD());
				Sleep(1);
				Assert::IsFalse(listener->Check());
			}
		}

#define _HIWORD(_long) *static_cast<unsigned short*>((void*)&_long)
#define _LOWORD(_long) *(static_cast<unsigned short*>((void*)&_long) + 1)

		TEST_METHOD(TestViewportWindowBlockOnWindowSizeChange) {
			Jigsaw::GraphicsContext* render_context = context->GetKnob<Jigsaw::GraphicsContext>();
			MockRenderContext* mock_context = static_cast<MockRenderContext*>(render_context);
			MockMessageQueue* _message_queue = static_cast<MockMessageQueue*>(context->GetKnob<Jigsaw::orMessageQueue>());
			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();

			ViewportWindow viewport_window(1, render_context, orchestrator);

			WPARAM size;
			_HIWORD(size) = 1280;
			_LOWORD(size) = 720;

			viewport_window.HandleMessage(0, WM_SHOWWINDOW, 0, 0);
			Assert::AreEqual(1, (int)mock_context->swapchain_call_count);
			viewport_window.HandleMessage(0, WM_SIZE, size, 0);
			Assert::AreEqual(2, (int)mock_context->swapchain_call_count);
			Sleep(3);
			Assert::IsTrue(_message_queue->message_received_listener->GetActivationTime() < _message_queue->block_event->GetActivationTime());
			viewport_window.HandleMessage(0, WM_CLOSE, 0, 0);

		}

		TEST_METHOD(TestViewportWindowSendsKeydownMessagesToOrchestratorOnKeydownTransitionFlagOnly) {
			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();
			Jigsaw::GraphicsContext* render_context = context->GetKnob<Jigsaw::GraphicsContext>();
			Jigsaw::orMessageQueue* message_queue = orchestrator->GetMessageQueue();

			ViewportWindow viewport_window(1, render_context, orchestrator);

			viewport_window.HandleMessage(0, WM_KEYDOWN, 'A', 0);

			Jigsaw::ORCHESTRATION_MESSAGE message;
			Assert::IsTrue(message_queue->DequeueMessage(&message));
			Assert::IsTrue(message.message_type == Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT);

			Jigsaw::QINPUT& input = reinterpret_cast<Jigsaw::QINPUT&>(message.message);
			Assert::IsTrue(input.GetInputChar() == 'A');
			Assert::IsTrue(input.GetInputType() == Jigsaw::QINPUT_TYPE::QKEY_DOWN);

			viewport_window.HandleMessage(0, WM_KEYDOWN, 'A', KEYDOWN_TRANSITION_FLAG);
			Assert::IsFalse(message_queue->DequeueMessage(&message));
		}

		TEST_METHOD(TestViewportWindowSendsKeyupMessagesToOrchestrator) {
			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();
			Jigsaw::GraphicsContext* render_context = context->GetKnob<Jigsaw::GraphicsContext>();
			Jigsaw::orMessageQueue* message_queue = orchestrator->GetMessageQueue();

			ViewportWindow viewport_window(1, render_context, orchestrator);

			viewport_window.HandleMessage(0, WM_KEYUP, 'A', 0);

			Jigsaw::ORCHESTRATION_MESSAGE message;

			Assert::IsTrue(message_queue->DequeueMessage(&message));
			Assert::IsTrue(message.message_type == Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT);

			Jigsaw::QINPUT& input = reinterpret_cast<Jigsaw::QINPUT&>(message.message);
			Assert::IsTrue(input.GetInputChar() == 'A');
			Assert::IsTrue(input.GetInputType() == Jigsaw::QINPUT_TYPE::QKEY_UP);
		}
	};
}
#endif
