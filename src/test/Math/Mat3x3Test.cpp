#ifdef _RUN_UNIT_TESTS_

#define _USE_MATH_DEFINES

#include "CppUnitTest.h"
#include "Math/LinAlg.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	TEST_CLASS(Mat3x3Test)
	{

		bool ApproximateVector(Vector3 a, Vector3 b) {
			return abs(a.x - b.x) < .0001f && abs(a.z - b.z) < .0001f && abs(a.y - b.y) < .0001f;
		}

		TEST_METHOD(TestDetermineAxisFromRotationMatrix) {
			Mat3x3 euler = Mat3x3::EulerAnglesYXZ(M_PI_2, M_PI_2, M_PI_2);
			Mat3x3 euler_o = Mat3x3::EulerAnglesYXZ(M_PI, 0, 0);

			Vector3 known_axis(0, 1, 1);
			Vector3 approx = euler * known_axis;

			Assert::IsTrue(ApproximateVector(known_axis, approx));

			Vector3 trace_vec(euler(0, 0), euler(1, 1), euler(2, 2));
			Vector3 trace_vec_o(euler_o(0, 0), euler_o(1, 1), euler_o(2, 2));

			Vector3 trace_vec_rot = euler * trace_vec;
			Vector3 axis_text = (trace_vec + trace_vec_rot).Normalized();

			Vector3 trace_vec_o_rot = euler_o * trace_vec_o;
			Vector3 axis_test_o = (trace_vec_o + trace_vec_o_rot).Normalized();

			Assert::IsTrue(ApproximateVector(axis_test_o, Vector3(0, 1, 0)));
		};

		TEST_METHOD(TestAngleAxisFormRotationMatrix) {
			Mat3x3 euler = Mat3x3::EulerAnglesYXZ(M_PI_2, M_PI_2, M_PI_2);
			Vector3 axis = Vector3(0, 1, 1);
			float angle = M_PI;
			Mat3x3 angle_axis = Mat3x3::FromAngleAxis(angle, axis);

			Assert::IsTrue(euler == angle_axis);
		}
	};
}
#endif
