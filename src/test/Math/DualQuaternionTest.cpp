#ifdef _RUN_UNIT_TESTS_

#define _USE_MATH_DEFINES
#include "CppUnitTest.h"
#include "Math/DualQuaternion.h"
#include "Math/LinAlg.h"
#include "TestUtils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	TEST_CLASS(DualQuaternionTest)
	{

		TEST_METHOD(TestDualQuaternionPointRotation) {
			Jigsaw::Math::DualQuaternion d = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 1, 0), PI_2, Vector3(5, 4, 3));

			Vector3 point(1, 0, 0);
			Vector3 point_2(1, 5, 4);
			Vector3 result = d.TransformPoint(point);
			Vector3 result_2 = d.TransformPoint(point_2);

			Assert::IsTrue(withinDelta(5, result.x));
			Assert::IsTrue(withinDelta(4, result.y));
			Assert::IsTrue(withinDelta(2, result.z));

			Jigsaw::Math::DualQuaternion r(Quaternion::FromAngleAxis(PI_2, Vector3(0, 1, 0)), Quaternion(0, 0, 0, 0));
			Jigsaw::Math::DualQuaternion t(Quaternion(1, 0, 0, 0), Quaternion(0, -5, -4, -3));
			Jigsaw::Math::DualQuaternion dual = r * t;
			Jigsaw::Math::DualQuaternion odd_conj = r.Conjugate() * t.Conjugate();
			Jigsaw::Math::DualQuaternion reg_conj = dual.Conjugate();
			Jigsaw::Math::DualQuaternion p_3 = Jigsaw::Math::DualQuaternion::FromPoint(Vector3(1, 5, 4));
			Jigsaw::Math::DualQuaternion p_3_res = dual * p_3 * r.Conjugate() * t.Conjugate();
			Jigsaw::Math::DualQuaternion p_3_transl = t * p_3 * t.Conjugate();
			Jigsaw::Math::DualQuaternion p_3_rot = r * p_3_transl * r.Conjugate();

		}

		bool ApproximateVector(const Vector3& a, const Vector3& b) {
			return abs(a.x - b.x) < .0001f && abs(a.z - b.z) < .0001f && abs(a.y - b.y) < .0001f;
		}

		TEST_METHOD(TestScLERP) {
			Jigsaw::Math::DualQuaternion d = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(1, 0, 0));
			Jigsaw::Math::DualQuaternion q = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 1), PI_2, Vector3(1, 0, 1));
			Vector3 d_t = d.GetTranslation();
			Vector3 q_t = q.GetTranslation();
			Assert::IsTrue(ApproximateVector(d_t, Vector3(1, 0, 0)));
			Assert::IsTrue(ApproximateVector(q_t, Vector3(1, 0, 1)));

			Jigsaw::Math::DualQuaternion sclerp = Jigsaw::Math::DualQuaternion::ScLERP(d, q, .5f);

			Vector3 sclerp_t = sclerp.GetTranslation();
			Assert::IsTrue(ApproximateVector(std::move(sclerp_t), Vector3(1, 0, .5f)));
		}

		TEST_METHOD(TestDLB) {
			Jigsaw::Math::DualQuaternion dqs[2];
			Jigsaw::Math::DualQuaternion root = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(0, 0, 0));
			dqs[0] = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(1, 0, 0), 0, Vector3(0, 3, 5));
			dqs[0] = root * dqs[0];
			dqs[1] = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(1, 0, 0), PI_2, Vector3(0, 7, 0));
			dqs[1] = dqs[0] * dqs[1];

			Vector3 point = Vector3(0, 10, 7);

			Vector3 offsets[2];
			offsets[0] = point - dqs[0].GetTranslation();
			offsets[1] = point - dqs[1].GetTranslation();

			float w_1[2];
			w_1[0] = 1.0f;
			w_1[1] = .0f;

			Vector3 weighted_point_a = Vector3::WeightedAverage(w_1, offsets, 2);
			Jigsaw::Math::DualQuaternion dlb_a = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_1, dqs, 2);

			w_1[0] = 0.0f;
			w_1[1] = 1.0f;

			Vector3 weighted_point_b = Vector3::WeightedAverage(w_1, offsets, 2);
			Jigsaw::Math::DualQuaternion dlb_b = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_1, dqs, 2);

			Vector3 result_a = dlb_a.TransformPoint(weighted_point_a);
			Vector3 result_b = dlb_b.TransformPoint(weighted_point_b);

			Assert::IsTrue(ApproximateVector(Vector3(0, 10, 7), std::move(result_a)));
			Assert::IsTrue(ApproximateVector(Vector3(0, 8, 5), std::move(result_b)));

			// remove the rotation
			dqs[1] = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(1, 0, 0), 0, Vector3(0, 7, 0));
			dqs[1] = dqs[0] * dqs[1];

			w_1[0] = .5f;
			w_1[1] = .5f;

			Vector3 weighted_point_c = Vector3::WeightedAverage(w_1, offsets, 2);
			Jigsaw::Math::DualQuaternion dlb_c = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_1, dqs, 2);

			Vector3 rot_point = dlb_c.TransformPoint(weighted_point_c);
			Assert::IsTrue(ApproximateVector(Vector3(0, 10, 7), std::move(rot_point)));
		}

		Vector3 CalculateWeightedJointOffset(Jigsaw::Math::DualQuaternion* bind_joints, float* w, const Vector3& vector, size_t count) {
			Vector3 out(0, 0, 0);
			for (int i = 0; i < count; i++) {
				out = out + w[i] * bind_joints[i].Conjugate().TransformPoint(vector);
			}
			
			return out;
		}

		TEST_METHOD(TestDLBPrecalculatedVertexWeightsVsTransformCalculatedWeights) {
			// building a series of 'bind' position dual quaternions
			Jigsaw::Math::DualQuaternion bind_dqs[3];
			bind_dqs[0] = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(0, 0, 0));
			bind_dqs[1] = bind_dqs[0] * Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(1, 0, 0));
			bind_dqs[2] = bind_dqs[1] * Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(3, 1, 0));

			// specifying the points and weights associated with them
			Vector3 point_a = Vector3(1, 1, 0);
			float w_a[3];
			w_a[0] = .5f; w_a[1] = .5f; w_a[2] = 0;

			Vector3 point_b = Vector3(2, 1, 0);
			float w_b[3];
			w_b[0] = 0.0f; w_b[1] = 1.0f; w_b[2] = 0;

			Vector3 point_c = Vector3(5, 0, 0);
			float w_c[3];
			w_c[0] = 0.0f; w_c[1] = 0.0f; w_c[2] = 1.0f;

			// articulating an initial pose with the first bone bent by 45 degrees
			Jigsaw::Math::DualQuaternion pose_dqs[3];
			pose_dqs[0] = Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 1), PI_4, Vector3(0, 0, 0));
			pose_dqs[1] = pose_dqs[0] * Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(1, 0, 0));
			pose_dqs[2] = pose_dqs[1] * Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(0, 0, 0), 0, Vector3(3, 1, 0));

			// appending a model-joint transformation to the end of each dq
			Jigsaw::Math::DualQuaternion blend_dqs[3];
			blend_dqs[0] = pose_dqs[0] * bind_dqs[0].Conjugate();
			blend_dqs[1] = pose_dqs[1] * bind_dqs[1].Conjugate();
			blend_dqs[2] = pose_dqs[2] * bind_dqs[2].Conjugate();

			// calculating the dual linear blend of each according to the weights
			Jigsaw::Math::DualQuaternion dlb_a = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_a, blend_dqs, 3);
			Jigsaw::Math::DualQuaternion dlb_b = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_b, blend_dqs, 3);
			Jigsaw::Math::DualQuaternion dlb_c = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_c, blend_dqs, 3);

			// transforming each point with the corresponding dq
			Vector3 trans_point_a_dlb_weighted = dlb_a.TransformPoint(point_a);
			Vector3 trans_point_b_dlb_weighted = dlb_b.TransformPoint(point_b);
			Vector3 trans_point_c_dlb_weighted = dlb_c.TransformPoint(point_c);

			// recalculating the dlb dual quaternions with only the poses, no model-joint transformations
			dlb_a = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_a, pose_dqs, 3);
			dlb_b = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_b, pose_dqs, 3);
			dlb_c = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_c, pose_dqs, 3);

			// pre-transforming the vertices according to the weighted model-joint transformations
			Vector3 weighed_a = CalculateWeightedJointOffset(bind_dqs, w_a, point_a, 3);
			Vector3 weighed_b = CalculateWeightedJointOffset(bind_dqs, w_b, point_b, 3);
			Vector3 weighed_c = CalculateWeightedJointOffset(bind_dqs, w_c, point_c, 3);

			// calculating the vertex positions in pose-space after pre-calculating the model-joint transformation
			Vector3 trans_point_a_vertex_weighted = dlb_a.TransformPoint(weighed_a);
			Vector3 trans_point_b_vertex_weighted = dlb_b.TransformPoint(weighed_b);
			Vector3 trans_point_c_vertex_weighted = dlb_c.TransformPoint(weighed_c);

			// asserting equality between the two approaches
			Assert::IsTrue(ApproximateVector(std::move(trans_point_a_dlb_weighted), std::move(trans_point_a_vertex_weighted)));
			Assert::IsTrue(ApproximateVector(Vector3(0, sqrtf(2), 0), trans_point_a_dlb_weighted));
			Assert::IsTrue(ApproximateVector(std::move(trans_point_b_dlb_weighted), std::move(trans_point_b_vertex_weighted)));
			Assert::IsTrue(ApproximateVector(std::move(trans_point_c_dlb_weighted), std::move(trans_point_c_vertex_weighted)));

			// altering the third joint for a new calculation
			pose_dqs[2] = pose_dqs[1] * Jigsaw::Math::DualQuaternion::FromAxisAngleTranslation(Vector3(1, 1, 0).Normalized(), PI_2, Vector3(3, 1, 0));
			blend_dqs[2] = pose_dqs[2] * bind_dqs[2].Conjugate();

			dlb_c = Jigsaw::Math::DualQuaternion::DualLinearBlend(w_c, blend_dqs, 3);
			Vector3 trans_point_c_alt = dlb_c.TransformPoint(point_c);

			// point d will use the same weights as point c
			Vector3 point_d = Vector3(3, 2, 0);
			Vector3 trans_point_d = dlb_c.TransformPoint(point_d);

			// pre-calculated transformed vertex position
			// both of the original points are on exact opposite sides of the axis of rotation, so they should be mirrored around the xy-plane
			Assert::IsTrue(ApproximateVector(Vector3(2.1213203f, 3.535533907f, sqrtf(2)), trans_point_d));

			Assert::IsTrue(withinDelta(trans_point_d.x, trans_point_c_alt.x));
			Assert::IsTrue(withinDelta(trans_point_d.y, trans_point_c_alt.y));
			Assert::IsTrue(withinDelta(trans_point_d.z, -trans_point_c_alt.z));

			Assert::IsTrue(ApproximateVector(Vector3(4, 1, 0), bind_dqs[2].GetTranslation()));

			return;
		}
	};
}
#endif
