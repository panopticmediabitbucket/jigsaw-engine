#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"

#include "Scene/jsSceneObserver.h"
#include "Entities/jsEntityService.h"
#include "Assets/DataAssets.h"
#include "Physics/TransformMachinePiece.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw;

#define FAB_COUNT 300

namespace ProjectTests {
	class MockObjectAsset : public Jigsaw::ObjectAsset {
	public:
		MockObjectAsset(Jigsaw::AssetDescriptor& desc, void* object) : Jigsaw::ObjectAsset(desc) {
			this->object = object;
		}
	};

	TEST_CLASS(JigsawSceneObserverTest)
	{

		static jsSceneObserver* observer;
		static AssetRef<Jigsaw::jsMachine> machine_ref;
		static jsEntityService* cluster_service;
		static AssetRef<jsScene> scene;

		static AssetRef<jsMachine> GetMachine() {
			AssetDescriptor desc;
			desc.fully_qualified_type_name = "Jigsaw::jsMachine";

			Jigsaw::jsMachine* machine = new Jigsaw::jsMachine;
			machine->machineId = Jigsaw::UID(0, 1);
			machine->name = "test_machine";
			machine->pieces.push_back(new Jigsaw::Physics::TransformMachinePiece);


			auto obj_asset = Ref<ObjectAsset>(new MockObjectAsset(desc, machine));
			return AssetRef<jsMachine>(obj_asset);
		}

		TEST_CLASS_INITIALIZE(Initialize) {
			// initializing services and the descriptors/assets
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back( {"Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

			cluster_service = context->GetKnob<jsEntityService>();
			Jigsaw::UID scene_id = Jigsaw::UID::Create();

			machine_ref = GetMachine();
			std::vector<AssetRef<jsMachine>> vector;
			vector.push_back(machine_ref);


			AssetDescriptor descriptor;
			descriptor.fully_qualified_type_name = "Jigsaw::jsScene";
			Ref<ObjectAsset> obj = MakeRef<ObjectAsset>(descriptor, new jsScene());
			scene = obj;
			scene->sceneId = scene_id;
			scene->machines = vector;
		}

		FabricationMap CreateMap()
		{
			// intializing fabrication arguments
			Jigsaw::MULTI_FABRICATION_ARGS args;
			Jigsaw::FABRICATION_ARGS fab_args_a, fab_args_b;
			Transform t_a, t_b;

			t_a.position = Vector3(1.5f, 2.2f, 3.7f);
			t_a.scale = Vector3(1.f, -1.f, 3.f);
			t_b.scale = Vector3(1.5f, 2.2f, 3.7f);

			fab_args_a.WithValue(std::move(t_a));
			fab_args_b.WithValue(std::move(t_b));

			args.entity_fabrication_args.push_back(std::move(fab_args_a));
			args.entity_fabrication_args.push_back(std::move(fab_args_b));

			FabricationMap scene_args;
			scene_args.insert(std::make_pair(machine_ref->machineId, args));

			return scene_args;
		}

		TEST_METHOD_INITIALIZE(MethodInitialize) {
			FabricationMap scene_args = CreateMap();

			observer = new jsSceneObserver(cluster_service, scene, std::move(scene_args));
		}

		TEST_CLASS_CLEANUP(Cleanup) {
			machine_ref = nullptr;
		}

		TEST_METHOD(TestActivatedSceneGeneratesMachineEntities) {
			observer->Awake();

			const Jigsaw::sysSignature signature = *machine_ref->GetSignature().get();
			Jigsaw::jsEntityCluster& cluster = (*cluster_service)[signature];
			Assert::AreEqual(2, (int)cluster.GetTotalCount());
			Jigsaw::jsMachineObserver* machine_observer = observer->GetMachineObserver(machine_ref->machineId);
			const std::vector<Jigsaw::UID>& entity_ids = machine_observer->GetEntityIds();

			jsEntity entity_a = cluster.FetchEntity(entity_ids.at(0));
			jsEntity entity_b = cluster.FetchEntity(entity_ids.at(1));

			Transform transform_a = entity_a.GetMemberData<Transform>();
			Transform transform_b = entity_b.GetMemberData<Transform>();

			Assert::IsTrue(Vector3(1.5f, 2.2f, 3.7f) == transform_a.position);
			Assert::IsTrue(Vector3(1, -1, 3) == transform_a.scale);
			Assert::IsTrue(Vector3(1.5f, 2.2f, 3.7f) == transform_b.scale);
			Assert::IsTrue(Vector3(0, 0, 0) == transform_b.position);

			observer->Destroy();
		}

		TEST_METHOD(TestDeactivatedSceneDeletesMachineEntities) {
			observer->Awake();
			observer->Destroy();

			const Jigsaw::sysSignature signature = *machine_ref->GetSignature().get();
			Jigsaw::jsEntityCluster& cluster = (*cluster_service)[signature];
			Assert::AreEqual(0, (int)cluster.GetTotalCount());
		}

		TEST_METHOD(TestFetchMachineObserverAndFabricateEntity) {
			observer->Awake();

			Jigsaw::jsMachineObserver* machine_observer = observer->GetMachineObserver(machine_ref->machineId);
			Jigsaw::FABRICATION_ARGS args;
			Transform t;
			t.position = Vector3(11, -222, 77);
			t.scale = Vector3(1.5, 1.5, 2.2);
			t.rotation = Quaternion(1, 1, 1, 1).Conjugate();
			args.WithValue(std::move(t));
			Jigsaw::jsEntity e = machine_observer->FabricateEntity(args);

			Jigsaw::jsEntityCluster& cluster = (*cluster_service)[machine_observer->GetMachineSignature()];

			// ensure that not only the original Entity data are loaded, but the fabricated entity as well
			Assert::AreEqual(3, (int)cluster.GetTotalCount());
			Assert::IsTrue(e.GetMemberData<Transform>() == t);

			for (int i = 0; i < FAB_COUNT; i++) {
				Jigsaw::jsEntity e_a = machine_observer->FabricateEntity(args);
			}

			Assert::AreEqual(3 + FAB_COUNT, (int)cluster.GetTotalCount());
			observer->Destroy();
			Assert::AreEqual(0, (int)cluster.GetTotalCount());
		}

		TEST_METHOD(TestOnlyOneMachineEntitiesRemovedOnSceneDeactivation) {
			auto m_ref = GetMachine();
			// activating the original scene
			observer->Awake();
			std::vector<Jigsaw::AssetRef<Jigsaw::jsMachine>> vector;
			vector.push_back(m_ref);
			Jigsaw::UID s_id = Jigsaw::UID::Create();

			AssetDescriptor descriptor;
			descriptor.fully_qualified_type_name = "Jigsaw::jsScene";
			Ref<ObjectAsset> obj = MakeRef<ObjectAsset>(descriptor, new jsScene());
			AssetRef<jsScene> scene = obj;
			scene->machines = vector;
			scene->sceneId = s_id;

			jsSceneObserver* other_observer = new jsSceneObserver(cluster_service, scene, CreateMap());
			other_observer->Awake();
			Jigsaw::jsMachineObserver* m_observer = other_observer->GetMachineObserver(m_ref->machineId);

			Jigsaw::jsEntity e = m_observer->FabricateEntity();
			jsEntityCluster& cluster = (*cluster_service)[m_observer->GetMachineSignature()];
			Assert::AreEqual(5, (int)cluster.GetTotalCount());

			observer->Destroy();
			Assert::AreEqual(3, (int)cluster.GetTotalCount());

			Jigsaw::jsEntity e_ = cluster.FetchEntity(e.GetUID());
			Transform t = e_.GetMemberData<Transform>();
			Assert::IsTrue(Transform() == t);
			other_observer->Destroy();
		}
	};

	jsSceneObserver* JigsawSceneObserverTest::observer;
	jsEntityService* JigsawSceneObserverTest::cluster_service;
	AssetRef<jsScene> JigsawSceneObserverTest::scene;
	AssetRef<Jigsaw::jsMachine> JigsawSceneObserverTest::machine_ref;
}
#endif
