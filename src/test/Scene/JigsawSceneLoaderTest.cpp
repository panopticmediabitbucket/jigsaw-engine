#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Scene/jsSceneLoader.h"
#include "Assets/AssetDatabase.h"
#include "Assets/dlAssetRegistrar.h"
#include "System/ASYNC.h"
#include "Ref.h"
#include "TestUtils.h"
#include "MockCommandListExecutor.h"
#include "Machines/jsMachine.h"
#include "Marshalling/MarshallingUtil.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw::Assets;
using namespace Jigsaw;

#define CHILD_MACHINE_COUNT 4
#define RESOURCE_POOL_COUNT 2

namespace ProjectTests {
	TEST_CLASS(JigsawSceneLoaderTest)
	{
		static Ref<dlAssetDatabaseReadWriter> db;
		static UID alt_scene_id;
		static UID scene_id;
		static Unique<AssetDescriptor[]> machine_descs;
		static Ref<Jigsaw::CommandListExecutor> executor;
		static THREAD_LOCAL_RESOURCE_POOL resource_pool;
		static Jigsaw::Ref<dlAssetRegistrar> asset_manager;

		TEST_CLASS_INITIALIZE(Setup) {
			resource_pool = THREAD_LOCAL_RESOURCE_POOL();
			asset_manager = Jigsaw::MakeRef<dlAssetRegistrar>();
			db = MockRootProperties();

			executor = Ref<Jigsaw::CommandListExecutor>(new MockCommandListExecutor);

			for (int i = 0; i < RESOURCE_POOL_COUNT; i++) {
				THREAD_LOCAL_SYSTEM_RESOURCES sys_resources;
				sys_resources.render_context = new MockRenderContext;
				sys_resources.cmd_list = Ref<Jigsaw::CommandList>(new MockCommandList);
				sys_resources.db = MakeRef<dlAssetDatabaseReader>("./unit_test.db");
				resource_pool.Enqueue(std::move(sys_resources));
			}
			resource_pool.cmd_list_exec = executor;
		}

		TEST_CLASS_CLEANUP(Cleanup) {
			DestroyTestDB(db);
			asset_manager.reset();
		}

		TEST_METHOD(TestLoadSceneAssets) {
			jsSceneLoader scene_l({ UIDFromString(T_SCENE_ID), resource_pool.Split(RESOURCE_POOL_COUNT) }, asset_manager.get());
			Ref<ASYNC_JOB<SCENE_LOAD_RESULT>> scene_load_job = scene_l.LoadScene();

			Assert::IsFalse(scene_load_job->Ready());
			while (!scene_load_job->Ready()) {}
			SCENE_LOAD_RESULT load_result = scene_load_job->Get();

			Assert::IsNotNull(load_result.loadedScene.Get());
			Assert::AreEqual(1, (int)load_result.loadedScene->GetMachines().size());
			resource_pool.Merge(std::move(load_result.recycledResources));
			Assert::AreEqual(RESOURCE_POOL_COUNT, (int)resource_pool.PoolSize());

			const std::vector<AssetRef<jsMachine>>& machines = load_result.loadedScene->GetMachines();
			for (const AssetRef<jsMachine>& machine : machines) {
				Assert::AreEqual("Test Cube Jigsaw machine", machine->name.c_str());
			}
		}

		TEST_METHOD(TestLoadSceneBringsSerializedMachineData) {
			Jigsaw::UID t_scene_id = Jigsaw::UIDFromString(T_SCENE_ID);
			jsSceneLoader scene_l({ t_scene_id, resource_pool.Split(RESOURCE_POOL_COUNT) }, asset_manager.get());
			Jigsaw::Ref<Jigsaw::ASYNC_JOB<Jigsaw::SCENE_LOAD_RESULT>> load_res = scene_l.LoadScene();
			Jigsaw::SCENE_LOAD_RESULT result = load_res->Await();
			
			auto& args = result.machineFabricationArgs;
			auto iter = args.find(Jigsaw::UIDFromString(T_MACHINE_ID));
			Assert::IsFalse(iter == args.end());
			resource_pool.Merge(std::move(result.recycledResources));
			Assert::AreEqual(RESOURCE_POOL_COUNT, (int)resource_pool.PoolSize());
		}

		TEST_METHOD(TestLoadGraphicsAssetsExecutesGraphicsCommandList) {
			unsigned int initial_invocations = static_cast<MockCommandListExecutor*>(executor.get())->list_invocation_count;

			jsSceneLoader scene_l({ UIDFromString(T_SCENE_ID), resource_pool.Split(RESOURCE_POOL_COUNT) }, asset_manager.get());
			auto load_job = scene_l.LoadScene();
			SCENE_LOAD_RESULT result = load_job->Await();

			Assert::AreEqual((int)(initial_invocations + 1), static_cast<MockCommandListExecutor*>(executor.get())->list_invocation_count);
			resource_pool.Merge(std::move(result.recycledResources));
			Assert::AreEqual(RESOURCE_POOL_COUNT, (int)resource_pool.PoolSize());

			auto queue = resource_pool.GetFilledResources();
			while (!queue.empty()) {
				auto front = queue.front();
				queue.pop();
				resource_pool.Enqueue(std::move(front));
			}
		}
	};

	Ref<dlAssetDatabaseReadWriter> JigsawSceneLoaderTest::db;
	UID JigsawSceneLoaderTest::scene_id;
	UID JigsawSceneLoaderTest::alt_scene_id;
	Unique<AssetDescriptor[]> JigsawSceneLoaderTest::machine_descs;
	Ref<Jigsaw::CommandListExecutor> JigsawSceneLoaderTest::executor;
	THREAD_LOCAL_RESOURCE_POOL JigsawSceneLoaderTest::resource_pool;
	Jigsaw::Ref<dlAssetRegistrar> JigsawSceneLoaderTest::asset_manager;
}
#endif
