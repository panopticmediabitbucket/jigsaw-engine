#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "System/sysHashString.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw;

namespace ProjectTests {
	TEST_CLASS(type_hash_Test)
	{
		TEST_METHOD(TestTypeHash) {
			static_assert(sysTypeHash<type_hash_Test>::Get().Value() == 1104863669u);
			unsigned int x = sysTypeHash<const type_hash_Test>::Get().Value();
			unsigned int y = sysTypeHash<type_hash_Test>::Get().Value();
			Assert::IsTrue(x == 1104863669u);
			Assert::IsTrue(sysTypeHash<const type_hash_Test>::Get().Value() == sysTypeHash<type_hash_Test>::Get().Value());
		}
	};
}
#endif
