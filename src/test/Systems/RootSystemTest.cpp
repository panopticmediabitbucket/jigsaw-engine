#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Systems/jsSystem.h"
#include "System/sysSignature.h"
#include "Physics/Transform.h"
#include "Time/TimeStep.h"
#include "Entities/jsEntityService.h"
#include "TransformSystemMock.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {

	TEST_CLASS(RootSystemTest)
	{
		TEST_METHOD(TestFindSystemInRegistry) {
			const Jigsaw::etype_info* t_info = &Jigsaw::etype_info::Id<TransformSystem>();
			auto vector = Jigsaw::Assembly::SystemRegistry::GetBeans();
			auto iter =std::find_if(vector.begin(), vector.end(), [=](Jigsaw::SYSTEM_REGISTRY_BEAN& bean) { return bean.typeInfo == t_info; });
			Assert::IsFalse(iter == vector.end());
		}

		TEST_METHOD(TestMultipleSignaturesMapToTransformSystem) {
			TransformSystem system;
			auto bean = TransformSystem::_Get_Registry_Bean();

			Jigsaw::SignatureBuilder builder_a, builder_b;
			builder_a.AddType(Jigsaw::etype_info::Id<Transform>()).AddType(Jigsaw::etype_info::Id<Jigsaw::UID>());
			builder_b.AddType(Jigsaw::etype_info::Id<Transform>()).AddType(Jigsaw::etype_info::Id<Jigsaw::Time::TimeStep>());
			Jigsaw::Unique<Jigsaw::sysSignature> sig_a = builder_a.Build();
			Jigsaw::Unique<Jigsaw::sysSignature> sig_b = builder_b.Build();

			Jigsaw::jsEntityService cluster_service;

			Jigsaw::jsEntityCluster& cluster_a = cluster_service[*sig_a.get()];
			Jigsaw::jsEntityCluster& cluster_b = cluster_service[*sig_b.get()];

			Jigsaw::jsEntity entity_a_1 = cluster_a.FabricateEntity(Jigsaw::jsEntity::SCOPE_EPHEMERAL);
			Jigsaw::jsEntity entity_a_2 = cluster_a.FabricateEntity(Jigsaw::jsEntity::SCOPE_EPHEMERAL);
			Jigsaw::jsEntity entity_b_1 = cluster_b.FabricateEntity(Jigsaw::jsEntity::SCOPE_EPHEMERAL);
			Jigsaw::jsEntity entity_b_2 = cluster_b.FabricateEntity(Jigsaw::jsEntity::SCOPE_EPHEMERAL);

			// a conversion can be produced for each of these
			Jigsaw::Unique<Jigsaw::sysSignatureMap> conversion_map_a;
			Jigsaw::Unique<Jigsaw::sysSignatureMap> conversion_map_b;
			Assert::IsTrue(sig_a->HasCompleteConversionMap(*bean.signature.get(), &conversion_map_a));
			Assert::IsTrue(sig_b->HasCompleteConversionMap(*bean.signature.get(), &conversion_map_b));

			Assert::AreEqual(entity_a_1.GetMemberData<Transform>().position.x, (float)0);
			Assert::AreEqual(entity_a_2.GetMemberData<Transform>().position.x, (float)0);
			Assert::AreEqual(entity_b_1.GetMemberData<Transform>().position.x, (float)0);
			Assert::AreEqual(entity_b_2.GetMemberData<Transform>().position.x, (float)0);

			// conversion map needs to be passed with the entity
			system.Update(entity_a_1, *conversion_map_a);
			system.Update(entity_a_2, *conversion_map_a);
			system.Update(entity_b_1, *conversion_map_b);
			system.Update(entity_b_2, *conversion_map_b);

			// System is executed on each of the members of the system
			Assert::AreEqual(entity_a_1.GetMemberData<Transform>().position.x, (float)5);
			Assert::AreEqual(entity_a_2.GetMemberData<Transform>().position.x, (float)5);
			Assert::AreEqual(entity_b_1.GetMemberData<Transform>().position.x, (float)5);
			Assert::AreEqual(entity_b_2.GetMemberData<Transform>().position.x, (float)5);
		}
	};
}
#endif
