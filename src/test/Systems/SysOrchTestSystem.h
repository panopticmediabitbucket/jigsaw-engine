#ifndef _SYS_ORCH_TEST_SYSTEM_
#define _SYS_ORCH_TEST_SYSTEM_

#include "Injection/JigsawInjection.h"
#include "Systems/jsSystem.h"

#define T_VAL 3.7f

	struct _data_entity {
		float x = 0;
	};

	struct TFloat
	{
		TFloat(float f) : f(f) {}
		float f = 0.0f;

		operator float() const { return f; }
		TFloat& operator=(float otherf) { f = otherf; }
	};

namespace ProjectTests {
	JGSW_KNOB_PROVIDER(__SysOrchTestProvider) {
public:
	KNOB(TFloat, test_val) {
		return new TFloat(T_VAL);
	};
	};

	JGSW_SYSTEM(SysOrchTestSystem) {
public:
	SLOT(slot_1, TFloat, test_val);

	JGSW_SYSTEM_METHOD(TestSystemMethod, _data_entity) {
		if (test_val) {
			a.x = *test_val + a.x;
		}
	};
	};

}


#endif
