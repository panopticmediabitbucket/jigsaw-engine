#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Systems/jsSystem.h"
#include "System/sysSignature.h"
#include "Physics/Transform.h"
#include "Time/TimeStep.h"
#include "Entities/jsEntityService.h"
#include "TransformSystemMock.h"
#include "Injection/jsContext.h"
#include "TestUtils.h"
#include "Systems/jsRuntimeSystemRegistry.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {

	class RuntimeSystemRegistryAccessor : public Jigsaw::jsRuntimeSystemRegistry {
	public:
		RuntimeSystemRegistryAccessor(const Jigsaw::SystemManagerArguments& arguments, const std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>& beans) :
			Jigsaw::jsRuntimeSystemRegistry(arguments, beans) {};

		void CallInit(const Jigsaw::Weak<Jigsaw::jsContext>& self) {
			Init();
		}

	};

	TEST_CLASS(RuntimeSystemRegistryTest) {
		static Jigsaw::Ref<RuntimeSystemRegistryAccessor> runtime_registry;

		TEST_CLASS_INITIALIZE(Setup) {
			MockRootProperties();

			Jigsaw::SystemManagerArguments arguments;
			Jigsaw::PriorityNode PRIORITY_LEVEL;
			Jigsaw::SystemInformation information;

			information.qualified_system_name = "ProjectTests::TransformSystem";
			PRIORITY_LEVEL.concurrent_systems.push_back(information);
			arguments.system_priority_levels.push_back(PRIORITY_LEVEL);

			auto beans = Jigsaw::Assembly::SystemRegistry::GetBeans();

			runtime_registry = Jigsaw::MakeRef<RuntimeSystemRegistryAccessor>(arguments, beans);
			Jigsaw::Ref<Jigsaw::jsContext> context = std::dynamic_pointer_cast<Jigsaw::jsContext>(runtime_registry);
			runtime_registry->CallInit(context);
		}

		TEST_METHOD(TestSystemsLoadedAndInstantiated) {
			Assert::AreEqual((int)runtime_registry->GetPriorityLevels(), 1);

			const Jigsaw::RUNTIME_PRIORITY_LEVEL& node = runtime_registry->GetPriorityLevel(0);
			Assert::IsNotNull(node.systems);
			Assert::IsTrue(node.system_count > 0);

			Assert::IsNotNull(node.systems[0].registry_data.typeInfo);
			const char* sysName = node.systems[0].registry_data.typeInfo->GetQualifiedName();
			Assert::AreEqual("ProjectTests::TransformSystem", sysName);
			Jigsaw::jsSystem* system = node.systems[0].system_instances[0];
			Assert::IsTrue((bool)system);
			TransformSystem* transform_system = dynamic_cast<TransformSystem*>(system);
			Assert::IsTrue((bool)transform_system);
		}

	};

	Jigsaw::Ref<RuntimeSystemRegistryAccessor> RuntimeSystemRegistryTest::runtime_registry;
}
#endif
