#include "SysOrchTestSystem.h"
namespace ProjectTests {
	START_JGSW_KNOB_PROVIDER_PROPERTIES(__SysOrchTestProvider)
		PROVIDER_NAMESPACE("SysOrchTestNamespace")
		END_JGSW_KNOB_PROVIDER_PROPERTIES(__SysOrchTestProvider)
		REGISTER_JGSW_KNOB_PROVIDER(__SysOrchTestProvider);

	START_REGISTER_SYSTEM(SysOrchTestSystem);
	END_REGISTER_SYSTEM(SysOrchTestSystem);
}

