#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"
#include "Entities/jsEntity.h"
#include "Entities/jsEntityCluster.h"
#include "RTTI/etype_info.h"
#include "System/sysSignature.h"
#include <vector>
#include "Math/LinAlg.h"
#include "TestUtils.h"

using namespace Jigsaw;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw::Util;

namespace ProjectTests {
	TEST_CLASS(JigsawEntityClusterTest)
	{
		SignatureBuilder GetBuilderA() {
			SignatureBuilder builder;
			builder.AddType(etype_info::Id<Vector2>());
			builder.AddType(etype_info::Id<Vector3>());
			return builder;
		}

		SignatureBuilder GetBuilderB() {
			SignatureBuilder builder;
			builder.AddType(etype_info::Id<Quaternion>());
			builder.AddType(etype_info::Id<Vector2>());
			builder.AddType(etype_info::Id<Vector3>());
			return builder;
		}

		Ref<jsEntityCluster> cluster_root_a;
		Ref<jsEntityCluster> cluster_root_b;
		Ref<sysSignature> signature_a;
		Ref<sysSignature> signature_b;

		TEST_METHOD_INITIALIZE(Setup) {
			SignatureBuilder builder_a = GetBuilderA();
			SignatureBuilder builder_b = GetBuilderB();
			Unique<sysSignature> sig_a = builder_a.Build();
			signature_a = MakeRef(sig_a);
			Unique<sysSignature> sig_b = builder_b.Build();
			signature_b = MakeRef(sig_b);
			cluster_root_a = MakeRef<jsEntityCluster>(signature_a);
			cluster_root_b = MakeRef<jsEntityCluster>(signature_b);
		}

		TEST_METHOD(TestEntityClusterPackAndFetch) {
			Assert::AreEqual(0, (int)cluster_root_a->GetClusterCount());
			jsEntity entity = cluster_root_a->FabricateEntity(jsEntity::SCOPE_PERSISTENT);
			Assert::AreEqual((int)jsEntity::SCOPE_PERSISTENT, (int)entity.GetScope());
			entity.SetMemberData<Vector2>(Vector2(0, 14));

			jsEntity fetched_entity = cluster_root_a->FetchEntity(entity.GetUID());
			Assert::AreEqual((int)jsEntity::SCOPE_PERSISTENT, (int)fetched_entity.GetScope());
			Vector2 member_vec = fetched_entity.GetMemberData<Vector2>();
			Assert::IsTrue(withinDelta(0, member_vec.x));
			Assert::IsTrue(withinDelta(14, member_vec.y));
		}

		TEST_METHOD(TestEntityClusterRootChangesRegisterInEntity) {
			jsEntity entity_a = cluster_root_a->FabricateEntity(jsEntity::SCOPE_PERSISTENT);
			jsEntity entity_b = cluster_root_a->FabricateEntity(jsEntity::SCOPE_PERSISTENT);

			entity_a.SetMemberData<Vector2>(Vector2(0, 0));
			entity_b.SetMemberData<Vector2>(Vector2(7, 8));

			jsEntity entity_a_fetch_1 = cluster_root_a->FetchEntity(entity_a.GetUID());
			// Alter member data
			entity_a_fetch_1.SetMemberData<Vector2>(Vector2(-1, 700));

			// and fetch again
			jsEntity entity_a_fetch_2 = cluster_root_a->FetchEntity(entity_a.GetUID());
			// check that changes processed
			Assert::IsTrue(withinDelta(entity_a_fetch_2.GetMemberData<Vector2>().x, -1));
			Assert::IsTrue(withinDelta(entity_a_fetch_2.GetMemberData<Vector2>().y, 700));

			jsEntity entity_b_fetch_1 = cluster_root_a->FetchEntity(entity_b.GetUID());
			Assert::IsTrue(withinDelta(entity_b_fetch_1.GetMemberData<Vector2>().x, 7));
			Assert::IsTrue(withinDelta(entity_b_fetch_1.GetMemberData<Vector2>().y, 8));
		}

		void BatchEntityAdd(int count) {
			for (int i = 0; i < count; i++) {
				jsEntity entity = cluster_root_a->FabricateEntity(jsEntity::SCOPE_PERSISTENT);
			}
		}

		TEST_METHOD(TestEntityClusterRootClusterCountExpands) {
			for (int i = 1; i <= 5; i++) {
				BatchEntityAdd(CLUSTER_SIZE);
				Assert::AreEqual(i, (int)cluster_root_a->GetClusterCount());
			}
		}

		TEST_METHOD(TestEntityClusterRemovalShrinksNodes) {
			BatchEntityAdd(CLUSTER_SIZE - 1);
			jsEntity entity_a = cluster_root_a->FabricateEntity(jsEntity::SCOPE_PERSISTENT);
			jsEntity entity_b = cluster_root_a->FabricateEntity(jsEntity::SCOPE_PERSISTENT);

			entity_b.SetMemberData<Vector2>(Vector2(12, -3));

			Assert::AreEqual(2, (int)cluster_root_a->GetClusterCount());
			cluster_root_a->RemoveEntity(entity_a.GetUID());
			Assert::AreEqual(1, (int)cluster_root_a->GetClusterCount());

			jsEntity fetch_entity_b = cluster_root_a->FetchEntity(entity_b.GetUID());
			Assert::IsTrue(withinDelta(fetch_entity_b.GetMemberData<Vector2>().x, 12));
			Assert::IsTrue(withinDelta(fetch_entity_b.GetMemberData<Vector2>().y, -3));
		}

		TEST_METHOD(TestConvertEntities) {
			jsEntity entity_a = cluster_root_a->FabricateEntity((jsEntity::SCOPE_PERSISTENT));

			entity_a.SetMemberData(Vector3(2.0f, 1.0f, -3.0f));
			entity_a.SetMemberData(Vector2(7.0f, -31.0f));

			jsEntityCluster::ConvertEntities(*cluster_root_a, *cluster_root_b, &entity_a.GetUID(), 1);
			jsEntity entity_b = cluster_root_b->FetchEntity(entity_a.GetUID());
			Assert::AreEqual(7.0f, entity_b.GetMemberData<Vector2>().x);
			Assert::AreEqual(-31.0f, entity_b.GetMemberData<Vector2>().y);
			Assert::AreEqual(2.0f, entity_b.GetMemberData<Vector3>().x);
			Assert::AreEqual(1.0f, entity_b.GetMemberData<Vector3>().y);
			Assert::AreEqual(-3.0f, entity_b.GetMemberData<Vector3>().z);

		}

	};
}
#endif
