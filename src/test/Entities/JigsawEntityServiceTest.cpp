#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"
#include "Entities/jsEntityService.h"
#include "System/sysSignature.h"
#include "Math/LinAlg.h"
#include "TestUtils.h"

using namespace Jigsaw;
using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw::Util;

namespace ProjectTests 
{
	TEST_CLASS(JigsawEntityServiceTest)
	{
		class MockConstruction : public jsEntityService 
		{
		public:
			MockConstruction() : jsEntityService() {}
		};

		TEST_METHOD_INITIALIZE(Setup) 
		{
			cluster_service = static_cast<jsEntityService*>(new MockConstruction());
			SignatureBuilder builder;
			builder.AddType(etype_info::Id<Vector2>());
			builder.AddType(etype_info::Id<Vector3>());
			Unique<sysSignature> u_sig = builder.Build();
			signature = MakeRef(u_sig);
		}

		jsEntityService* cluster_service;
		Ref<sysSignature> signature;

		TEST_METHOD(TestFetchClusterForSignature) 
		{
			jsEntityCluster& root = (*cluster_service)[*signature];
			Assert::IsTrue(root.GetSignature() == *signature);
		}

		TEST_METHOD(TestAddEntityToClusterAndFetch) 
		{
			jsEntityCluster& root = (*cluster_service)[*signature];
			jsEntity entity = root.FabricateEntity(jsEntity::SCOPE_EPHEMERAL);
			entity.SetMemberData<Vector2>(Vector2(-3, 7));

			jsEntityCluster& root_alt = (*cluster_service)[*signature];
			jsEntity fetched_entity = root_alt.FetchEntity(entity.GetUID());
			Assert::IsTrue(withinDelta(fetched_entity.GetMemberData<Vector2>().x, -3));
		}
	};
}
#endif
