#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Assembly/JGSW_ASSEMBLY.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	TEST_CLASS(JGSW_ASSEMBLY_Test)
	{
		TEST_METHOD(TestGetJigsawAssemblyInterface) {
			Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface> _interface;
			int ret = Jigsaw::Assembly::GetJigsawAssemblyInterface(&_interface);
			Assert::IsNotNull(_interface.get());

			{
				auto internal_maps = Jigsaw::Assembly::MarshallingRegistry::GetMaps();
				auto external_maps = _interface->GetMarshallingMaps();
				Assert::AreEqual((int)internal_maps.size(), (int)external_maps.size());
				auto int_iter = internal_maps.begin();
				auto ext_iter = external_maps.begin();
				while (int_iter != internal_maps.end()) {
					Assert::IsTrue((*int_iter++).GetType() == (*ext_iter++).GetType());
				}
			}

			unsigned int size = 0;
			auto map = Jigsaw::Assembly::JigsawStaticContext::GetProviderMap();
			for (auto pair : map) {
				size += pair.second.size();
			}

			Assert::AreEqual(size, (unsigned int)_interface->GetKnobProviderDefinitions().size());

			auto internal_beans = Jigsaw::Assembly::SystemRegistry::GetBeans();
			auto external_beans = _interface->GetJigsawSystemBeans();
			Assert::AreEqual((int)internal_beans.size(), (int)external_beans.size());
			auto int_iter = internal_beans.begin();
			auto ext_iter = external_beans.begin();
			while (int_iter != internal_beans.end()) {
				Assert::IsTrue((*int_iter++).typeInfo == (*ext_iter++).typeInfo);
			}
		}
	};
}
#endif
