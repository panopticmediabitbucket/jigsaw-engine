#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"
#include "Assembly/JigsawAssemblyInterface.h"
#include "Assembly/AssemblyImportUtil.h"
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Injection/jsContext.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	class JigsawAssemblyTestImpl : public Jigsaw::Assembly::JigsawAssemblyInterface {
	public:

		std::vector<Jigsaw::Marshalling::MarshallingMap> GetMarshallingMaps() const override {
			std::vector<Jigsaw::Marshalling::MarshallingMap> marshalling_maps;
			Jigsaw::Marshalling::MarshallingMap marshalling_map(Jigsaw::etype_info::Id<JigsawAssemblyTestImpl>(), std::function<void* (void*)>());
			marshalling_maps.push_back(marshalling_map);

			return marshalling_maps;
		}

		std::vector<Jigsaw::knob_provider_definition> GetKnobProviderDefinitions() const {
			std::vector<Jigsaw::knob_provider_definition> knob_provider_defs;

			Jigsaw::knob_provider_definition definition;
			definition.name = "test_definition";
			definition.properties.m_namespace = "___MOCK_NAMESPACE";
			definition.provider_type = &Jigsaw::etype_info::Id<JigsawAssemblyTestImpl>();
			knob_provider_defs.push_back(definition);

			return knob_provider_defs;
		}


		// Inherited via JigsawAssemblyInterface
		virtual std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> GetJigsawSystemBeans() const override {
			std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> beans;
			Jigsaw::SYSTEM_REGISTRY_BEAN bean;
			beans.push_back(bean);
			return beans;
		}

	};

	TEST_CLASS(AssemblyImportUtilTest)
	{

		TEST_METHOD(TestImportAssemblyComponentsTearUpTearDown) {
			// setup
			Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface> _interface = Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>(new JigsawAssemblyTestImpl);

			// finding initial values
			std::map<std::string, std::vector<Jigsaw::knob_provider_definition>>& knob_namespace_map = Jigsaw::Assembly::JigsawStaticContext::GetProviderMap();
			unsigned int provider_map_size = (u32)knob_namespace_map.size();

			std::unordered_map <Jigsaw::etype_index, Jigsaw::Marshalling::MarshallingMap>& m_map_map =  Jigsaw::Assembly::MarshallingRegistry::GetInstance().type_marshalling_map;
			unsigned int m_map_map_size = (u32)m_map_map.size();

			std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>& beans = Jigsaw::Assembly::SystemRegistry::GetBeans();
			unsigned int sys_beans_size = (u32)beans.size();

			// act
			Jigsaw::Assembly::ImportAssemblyComponents(_interface);

			// assert
			Assert::AreEqual(provider_map_size + 1, (unsigned int)knob_namespace_map.size());
			Assert::AreEqual(m_map_map_size + 1, (unsigned int)m_map_map.size());
			Assert::AreEqual(sys_beans_size + 1, (unsigned int)beans.size());

			// then act
			Jigsaw::Assembly::ReleaseAssemblyComponents(_interface);

			// then assert
			Assert::AreEqual(provider_map_size, (unsigned int)knob_namespace_map.size());
			Assert::AreEqual(m_map_map_size, (unsigned int)m_map_map.size());
			Assert::AreEqual(sys_beans_size, (unsigned int)beans.size());
		}
	};
}
#endif
