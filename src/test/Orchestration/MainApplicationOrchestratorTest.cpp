#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"
#include "Input/ctInputEvents.h"
#include "Orchestration/orOrchestrator.h"
#include "MockCommandListExecutor.h"
#include "Orchestration/orMessageQueue.h"
#include "Application/ApplicationRootProperties.h"
#include "TestUtils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw;

namespace ProjectTests {

	TEST_CLASS(ApplicationOrchestratorTest)
	{
		TEST_CLASS_INITIALIZE(Setup) {
			MockRootProperties();
		}

		TEST_METHOD(TestApplicationOrchestratorLoopBlocked) {
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back({ "Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

			Jigsaw::GraphicsContext* mock_render_context = context->GetKnob<Jigsaw::GraphicsContext>();
			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();

			{
				Jigsaw::Ref<Jigsaw::ASYNC_EVENT> event = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>("TEST_ASYNC_COMMUNICATION");
				Jigsaw::Ref<Jigsaw::EVENT_LISTENER> event_listener = Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(event);
				Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> block_event = orchestrator->GetMessageQueue()->BlockUntil(event_listener);
				Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> in_ev_listener = orchestrator->GetMessageQueue()->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, Jigsaw::QWORD());

				// events are not processed until the application is started
				Sleep(1);
				Assert::IsFalse(in_ev_listener->Check());
				Assert::IsFalse(block_event->Check());

				orchestrator->StartApplication();

				// events processed in order
				Sleep(2);
				Assert::IsTrue(block_event->Check());

				// the input event is later in the message queue. Since the queue is blocked, it's not processed yet
				Assert::IsFalse(in_ev_listener->Check());

				// after the blocking event is notified, the block is released
				event->Notify();
				Sleep(1);
				Assert::IsTrue(in_ev_listener->Check());
			}
			orchestrator->EndApplication();

			// After the application ends, no new events will be processed
			Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> in_ev_listener = orchestrator->GetMessageQueue()->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, Jigsaw::QWORD());
			Sleep(2);
			Assert::IsFalse(in_ev_listener->Check());
		}

		TEST_METHOD(TestApplicationOrchestratorReceivesInputMessages) {
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back({ "Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

			Jigsaw::ctInputEvents* input_events = context->GetKnob<Jigsaw::ctInputEvents>();
			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();
			Jigsaw::orMessageQueue* shared_state = orchestrator->GetMessageQueue();

			orchestrator->StartApplication();

			Jigsaw::QWORD word;
			Jigsaw::QINPUT& input = reinterpret_cast<Jigsaw::QINPUT&>(word);
			input.SetInputChar('Z');
			input.SetInputType(Jigsaw::QINPUT_TYPE::QKEY_DOWN);

			shared_state->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, word);
			Sleep(1);
			Assert::IsTrue(input_events->Key('Z'));

			input.SetInputType(Jigsaw::QINPUT_TYPE::QKEY_UP);
			shared_state->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, word);
			Sleep(1);
			Assert::IsFalse(input_events->Key('Z'));

			orchestrator->EndApplication();

		}

		TEST_METHOD(TestDeferredMessagesProcessedInNextFrame) {
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back({ "Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

			Jigsaw::ctInputEvents* input_events = context->GetKnob<Jigsaw::ctInputEvents>();
			Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>();
			Jigsaw::orMessageQueue* shared_state = orchestrator->GetMessageQueue();

			Ref<Jigsaw::ASYNC_EVENT> event_a = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>("TEST_EVENT");
			Ref<Jigsaw::ASYNC_EVENT> event_b = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>("TEST_EVENT_2");

			Ref<Jigsaw::EVENT_LISTENER> event_a_listener = Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(event_a);
			Ref<Jigsaw::EVENT_LISTENER> event_b_listener = Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(event_b);

			Ref<Jigsaw::EVENT_LISTENER> blocked_for_a = shared_state->BlockUntil(event_a_listener, Jigsaw::MESSAGE_TIMING::NEXT_FRAME);
			Ref<Jigsaw::EVENT_LISTENER> blocked_for_b = shared_state->BlockUntil(event_b_listener, Jigsaw::MESSAGE_TIMING::CURRENT_FRAME);

			orchestrator->StartApplication();
			Sleep(1);

			Assert::IsFalse(blocked_for_a->Check());
			Assert::IsTrue(blocked_for_b->Check());

			event_b->Notify();

			Sleep(1);
			Assert::IsTrue(blocked_for_a->Check());

			event_a->Notify();

			orchestrator->EndApplication();

		}
	};
}

#endif
