#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Orchestration/orSystemsOrchestrator.h"
#include "Injection/JigsawInjection.h"
#include "Systems/SysOrchTestSystem.h"
#include "Systems/TransformSystemMock.h"
#include "Time/TimeStep.h"
#include "Ref.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

#define T_ENTITY_COUNT 1000

namespace ProjectTests {


	TEST_CLASS(JigsawSystemsOrchestratorTest)
	{
		static Jigsaw::orSystemsOrchestrator* orchestrator;
		static Jigsaw::jsEntityService* cluster_service;
		static Jigsaw::UID* id_array_a;
		static Jigsaw::UID* id_array_b;
		static Jigsaw::jsEntityCluster* _cluster_a;
		static Jigsaw::jsEntityCluster* _cluster_b;

		static void FabricateMany(Jigsaw::jsEntityCluster& cluster, Jigsaw::UID* uids) {
			for (int i = 0; i < T_ENTITY_COUNT; i++) {
				uids[i] = cluster.FabricateEntity(Jigsaw::jsEntity::SCOPE_EPHEMERAL).GetUID();
			}
		};

		TEST_CLASS_INITIALIZE(Setup) {
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back({ "Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			config.m_packageNames.push_back({ "SysOrchTestNamespace", std::vector<const Jigsaw::etype_info*>() });
			Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

			cluster_service = context->GetKnob<Jigsaw::jsEntityService>();
			orchestrator = context->GetKnob<Jigsaw::orSystemsOrchestrator>();

			Jigsaw::SignatureBuilder builder_a, builder_b;
			builder_a.AddType(Jigsaw::etype_info::Id<Transform>()).AddType(Jigsaw::etype_info::Id<Jigsaw::UID>());
			builder_b.AddType(Jigsaw::etype_info::Id<Transform>()).AddType(Jigsaw::etype_info::Id<Jigsaw::Time::TimeStep>());
			Jigsaw::Unique<Jigsaw::sysSignature> sig_a = builder_a.Build();
			Jigsaw::Unique<Jigsaw::sysSignature> sig_b = builder_b.Build();

			Jigsaw::jsEntityCluster& cluster_a = (*cluster_service)[*sig_a.get()];
			Jigsaw::jsEntityCluster& cluster_b = (*cluster_service)[*sig_b.get()];

			id_array_a = new Jigsaw::UID[T_ENTITY_COUNT];
			id_array_b = new Jigsaw::UID[T_ENTITY_COUNT];

			FabricateMany(cluster_a, id_array_a);
			FabricateMany(cluster_b, id_array_b);

			_cluster_a = &cluster_a;
			_cluster_b = &cluster_b;
		}

		TEST_METHOD(TestEntitiesAlteredOnSystemsUpdate) {
			Transform a_init_transforms[T_ENTITY_COUNT];
			Transform b_init_transforms[T_ENTITY_COUNT];

			for (int i = 0; i < T_ENTITY_COUNT; i++) {
				a_init_transforms[i] = (*_cluster_a).FetchEntity(id_array_a[i]).GetMemberData<Transform>();
				b_init_transforms[i] = (*_cluster_b).FetchEntity(id_array_b[i]).GetMemberData<Transform>();
			}

			orchestrator->Update();

			for (int i = 0; i < T_ENTITY_COUNT; i++) {
				Assert::AreEqual(a_init_transforms[i].position.x + 5, (*_cluster_a).FetchEntity(id_array_a[i]).GetMemberData<Transform>().position.x);
				Assert::AreEqual(b_init_transforms[i].position.x + 5, (*_cluster_b).FetchEntity(id_array_b[i]).GetMemberData<Transform>().position.x);
			}
		}

		TEST_METHOD(TestSlotPickedUpOnSystemsUpdate) {
			Jigsaw::SignatureBuilder builder;
			builder.AddType(Jigsaw::etype_info::Id<_data_entity>());
			auto sig = builder.Build();
			Jigsaw::jsEntityCluster& cluster = (*cluster_service)[*sig.get()];
			Jigsaw::jsEntity entity = cluster.FabricateEntity(Jigsaw::jsEntity::SCOPE::SCOPE_EPHEMERAL);
			Assert::IsTrue(entity.GetMemberData<_data_entity>().x == 0);

			orchestrator->Update();

			_data_entity e = entity.GetMemberData<_data_entity>();
			Assert::IsTrue(e.x == T_VAL);
			
		}
	};

	Jigsaw::orSystemsOrchestrator* JigsawSystemsOrchestratorTest::orchestrator;
	Jigsaw::jsEntityService* JigsawSystemsOrchestratorTest::cluster_service;
	Jigsaw::UID* JigsawSystemsOrchestratorTest::id_array_a;
	Jigsaw::UID* JigsawSystemsOrchestratorTest::id_array_b;
	Jigsaw::jsEntityCluster* JigsawSystemsOrchestratorTest::_cluster_a;
	Jigsaw::jsEntityCluster* JigsawSystemsOrchestratorTest::_cluster_b;
}
#endif
