#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Ref.h"
#include "Graphics/GraphicsContext.h"
#include "MockCommandListExecutor.h"
#include "Orchestration/orSceneHierarchy.h"
#include "Entities/jsEntityService.h"
#include "Application/ApplicationRootProperties.h"
#include "TestUtils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	TEST_CLASS(SceneHierarchyTest)
	{
		static Jigsaw::GraphicsContext* render_context;
		static Jigsaw::jsEntityService* cluster_service;
		Jigsaw::orSceneHierarchy* orchestrator;

		TEST_CLASS_INITIALIZE(LoadDB) {
			MockRootProperties();
		}

		TEST_METHOD_INITIALIZE(Setup) {
			Jigsaw::jsContextConfig config;
			config.m_packageNames.push_back({ "Jigsaw::Test", std::vector<const Jigsaw::etype_info*>() });
			Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

			render_context = context->GetKnob<Jigsaw::GraphicsContext>();
			cluster_service = context->GetKnob<Jigsaw::jsEntityService>();
			orchestrator = context->GetKnob<Jigsaw::orSceneHierarchy>();
		}

		TEST_METHOD_CLEANUP(MethodCleanup) {
			for (auto cluster_it : *cluster_service) {
				auto cluster = cluster_it.second;
				while (cluster->GetTotalCount() > 0) {
					auto iter = cluster->GetNodeIterator(0);
					auto entity = *iter;
					cluster->RemoveEntity(entity.GetUID());
				}
			}
			delete orchestrator;
		}

		TEST_CLASS_CLEANUP(Cleanup) {
			delete cluster_service;
			delete render_context;
		}

		TEST_METHOD(TestInitializeSceneContext) {
			size_t array_size;
			Jigsaw::Unique<Jigsaw::UID[]> active_ids = orchestrator->GetActiveSceneIds(array_size);
			Assert::AreEqual(0, (int)array_size);
			orchestrator->LoadAndActivateScene(ApplicationRootProperties::Get().root_scene_id);

			active_ids = orchestrator->GetActiveSceneIds(array_size);
			Assert::AreEqual(1, (int)array_size);
		}

		BEGIN_TEST_METHOD_ATTRIBUTE(TestAwaitScene)
			TEST_PRIORITY(1)
		END_TEST_METHOD_ATTRIBUTE(TestAwaitScene)
		TEST_METHOD(TestAwaitScene) {
			Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> listener =
				orchestrator->LoadScene(ApplicationRootProperties::Get().root_scene_id);

			Assert::IsFalse(listener->Check());

			int i = 0;
			while (!listener->Check()) {
				std::this_thread::sleep_for(std::chrono::microseconds(16667));
				orchestrator->Update();
				i++;
				if (i > 50) {
					Assert::Fail(L"Infinite loop appears in Scene await operation");
				}
			}

			size_t array_size;
			Jigsaw::Unique<Jigsaw::UID[]> active_ids = orchestrator->GetActiveSceneIds(array_size);
			Assert::AreEqual(1, (int)array_size);
			Assert::IsTrue(listener->Check());
		}

	};
	Jigsaw::GraphicsContext* SceneHierarchyTest::render_context;
	Jigsaw::jsEntityService* SceneHierarchyTest::cluster_service;
}
#endif
