#if defined(_RUN_UNIT_TESTS_) || defined(_TEST_INJECTION_)
#ifndef _MOCK_MESSAGE_QUEUE_H_
#define _MOCK_MESSAGE_QUEUE_H_

#include "Orchestration/orMessageQueue.h"

namespace ProjectTests {

	class MockMessageQueue : public Jigsaw::orMessageQueue {
	public:
		MockMessageQueue() : Jigsaw::orMessageQueue() {}

		bool DequeueMessage(Jigsaw::ORCHESTRATION_MESSAGE* message) override {
			bool ret_val = Jigsaw::orMessageQueue::DequeueMessage(message);
			if ((*message).message_type == Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_BLOCK) {
				message_received_listener = Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(message->message_processed_handshake);
			}
			return ret_val;
		}

		Jigsaw::Ref<Jigsaw::EVENT_LISTENER> BlockUntil(const Jigsaw::Ref<Jigsaw::EVENT_LISTENER>& event, Jigsaw::MESSAGE_TIMING timing = Jigsaw::MESSAGE_TIMING::CURRENT_FRAME) override {
			block_event = event;
			return Jigsaw::orMessageQueue::BlockUntil(event, timing);
		}

		Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> message_received_listener;
		Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> block_event;

	};
}

#endif
#endif