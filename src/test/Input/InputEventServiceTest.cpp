#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Ref.h"
#include "Injection/jsContext.h"
#include "Orchestration/orOrchestrator.h"
#include "Input/ctInputEventWriter.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw;

namespace ProjectTests {
	TEST_CLASS(InputEventServiceTest)
	{
		Jigsaw::Ref<Jigsaw::jsContext> context;
		Jigsaw::ctInputEventWriter* input_event_update_service;
		Jigsaw::ctInputEvents* input_event_service;
		
		TEST_METHOD_INITIALIZE(BeforeEach) {
			Jigsaw::jsContextConfig config;
			Jigsaw::namespace_import import;
			import.m_namespace = "Jigsaw::Test";
			config.m_packageNames.push_back(import);
			context = Jigsaw::jsContext::Create(config);
			input_event_service = context->GetKnob<Jigsaw::ctInputEvents>();
			input_event_update_service = context->GetKnob<Jigsaw::ctInputEventWriter>();
		}

		TEST_METHOD_CLEANUP(AfterEach) {
			context.reset();
		}

		TEST_METHOD(TestInputEventReceived) {
			input_event_update_service->UpdateKey((short)'A', ACTIVITY_FLAGS::DOWN | ACTIVITY_FLAGS::PRESSED);
			Assert::IsTrue(input_event_service->Key('A'));
			Assert::IsTrue(input_event_service->KeyDown('A'));
			Assert::IsFalse(input_event_service->KeyUp('A'));
		}

		TEST_METHOD(TestKeyUpReceived) {
			input_event_update_service->UpdateKey((short)'A', ACTIVITY_FLAGS::UP);
			Assert::IsTrue(input_event_update_service->KeyUp('A'));
			Assert::IsFalse(input_event_service->KeyDown('A'));
			Assert::IsFalse(input_event_service->Key('A'));
		}

		TEST_METHOD(TestDownAndUpEventsCleared) {
			input_event_update_service->UpdateKey((short)'A', DOWN | PRESSED);
			input_event_update_service->UpdateKey((short)'B', DOWN | PRESSED);

			Assert::IsTrue(input_event_service->KeyDown('A'));
			Assert::IsTrue(input_event_service->KeyDown('B'));

			input_event_update_service->ClearFrameEvents();

			Assert::IsFalse(input_event_service->KeyDown('B'));
			Assert::IsTrue(input_event_service->Key('B'));
		}

		TEST_METHOD(TestKeyTappedSetsKeyUpInNextFrame) {
			input_event_update_service->PressKey('A');
			input_event_update_service->ReleaseKey('A');

			Assert::IsTrue(input_event_service->Key('A'));
			Assert::IsTrue(input_event_service->KeyDown('A'));
			Assert::IsFalse(input_event_service->KeyUp('A'));

			input_event_update_service->ClearFrameEvents();

			Assert::IsFalse(input_event_service->Key('A'));
			Assert::IsFalse(input_event_service->KeyDown('A'));
			Assert::IsTrue(input_event_service->KeyUp('A'));

			input_event_update_service->ClearFrameEvents();

			Assert::IsFalse(input_event_service->Key('A'));
			Assert::IsFalse(input_event_service->KeyDown('A'));
			Assert::IsFalse(input_event_service->KeyUp('A'));
		}

		TEST_METHOD(TestMousePositionReceived) {

		}
	};
}
#endif
