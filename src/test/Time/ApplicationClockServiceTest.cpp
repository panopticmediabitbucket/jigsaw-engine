#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Time/ApplicationClockService.h"
#include <thread>
#include <chrono>
#include "TestUtils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace std::literals::chrono_literals;

namespace ProjectTests {
	TEST_CLASS(ApplicationClockServiceTest) {

		TEST_METHOD(TestApplicationClockUpdate) {
			Jigsaw::Time::ApplicationClockService service;
			Assert::IsTrue(service.DeltaTime() == 0);

			std::this_thread::sleep_for(20000000ns);
			service.Update();
			Assert::IsTrue(service.DeltaTime() >= .020000000);
		}
	};
}

#endif
