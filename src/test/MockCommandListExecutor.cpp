#if defined(_RUN_UNIT_TESTS_) || defined(_TEST_INJECTION_)
#include "MockCommandListExecutor.h"

using namespace Jigsaw;

CommandListExecutor* ProjectTests::MockCommandListExecutor::SubmitCommandList(Jigsaw::Ref<Jigsaw::CommandList>& cmd_list)
{
	this->list_invocation_count += 1;
	return this;
}

CommandListExecutor* ProjectTests::MockCommandListExecutor::SubmitCommandLists(std::vector<Jigsaw::Ref<Jigsaw::CommandList>>& cmd_lists) 
{
	this->list_invocation_count += (int)cmd_lists.size();
	return this;
}

void ProjectTests::MockCommandListExecutor::SignalAndWait() {
}

void ProjectTests::MockCommandList::DrawIndexed(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_INDEXED_DATA& render_data) {
	has_commands = true;
}

void ProjectTests::MockCommandList::LoadBuffer(const void* t_arr, size_t t_size, size_t t_count, Jigsaw::GPUResource& buffer_dest) {
	has_commands = true;
}

bool ProjectTests::MockCommandList::HasCommands() {
	bool _has = has_commands;
	has_commands = false;
	return _has;
}

void ProjectTests::MockCommandList::SetRenderViews(Jigsaw::GPUResource& rtv_resource, Jigsaw::GPUResource& dsv_resource)
{
}

void ProjectTests::MockCommandList::SetToDisplay(Jigsaw::GPUResource& gpu_resource)
{
}

void ProjectTests::MockCommandList::SetBufferOutput(Jigsaw::GPUResource& gpu_resource)
{
}

void ProjectTests::MockCommandList::CopyBuffer(Jigsaw::GPUResource& source, Jigsaw::GPUResource& dest)
{
}

void ProjectTests::MockCommandList::ClearResource(Jigsaw::GPUResource& gpu_resource)
{
}

void ProjectTests::MockCommandList::SetToShaderView(Jigsaw::GPUResource& gpu_resource)
{
}

void ProjectTests::MockCommandList::LoadTexture(const u8* data, size_t bytes_per_row, Jigsaw::GPUResource& texture_dest)
{
}

std::vector<Jigsaw::GPUResource*>& ProjectTests::MockCommandList::GetLoadedBuffers()
{
	static std::vector<Jigsaw::GPUResource*> vec;
	return vec;
}

void ProjectTests::MockCommandList::UpdateResource(GPUResource& gpu_resource, const UPDATE_DATA_RAW& update_data)
{
}

void ProjectTests::MockCommandList::ResolveMultiSampledResource(GPUResource& ms_resource, GPUResource& dest_resource)
{
}

void ProjectTests::MockCommandList::DrawDirect(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_DIRECT_DATA& vertex_data)
{
}

void ProjectTests::MockRenderContext::CreateWindowSwapchain(const HWND hWnd, UINT width, UINT height)
{
	swapchain_call_count++;
}

Jigsaw::Ref<Jigsaw::CommandList> ProjectTests::MockRenderContext::GetCommandList(Jigsaw::JGSW_COMMAND_TYPE type)
{
	return Jigsaw::Ref<Jigsaw::CommandList>(new MockCommandList);
}

Jigsaw::Ref<Jigsaw::CommandListExecutor> ProjectTests::MockRenderContext::GetCommandListExecutor(Jigsaw::JGSW_COMMAND_TYPE type)
{
	return Jigsaw::Ref<Jigsaw::CommandListExecutor>(new MockCommandListExecutor);
}

void ProjectTests::MockRenderContext::DisplayFrame(){
}

Jigsaw::J_Viewport* ProjectTests::MockRenderContext::GetViewport() {
	return nullptr;
}
Jigsaw::GPUResource* ProjectTests::MockRenderContext::CreateGPUResource(JGSW_GPU_RESOURCE_DATA resource_args)
{
	return nullptr;
}
void ProjectTests::MockRenderContext::LoadShaderResources(const Jigsaw::Ref<Jigsaw::CommandList>& command_list)
{
}
#endif
