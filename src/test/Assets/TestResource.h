#ifdef _RUN_UNIT_TESTS_
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Assets/DataAssets.h"

class TestResource : public Jigsaw::DataAsset {
public:
	TestResource(const Jigsaw::AssetDescriptor& descriptor): DataAsset(descriptor) { }
	DECLARE_DATA_ASSET(TestResource);

	
protected:
	Jigsaw::ASSET_LOAD_RESULT Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) {
		Sleep(10);
		Jigsaw::ASSET_LOAD_RESULT result;
		result.result = Jigsaw::RESULT::COMPLETE;
		return result;
	}
};

#endif