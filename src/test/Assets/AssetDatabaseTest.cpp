#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"
#include "sqlite3.h"
#include "Assets/dlAssetRegistrar.h"
#include "Assets/AssetDatabase.h"
#include "TestUtils.h"
#include "Ref.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;
using namespace Jigsaw::Assets;
using namespace Jigsaw;

#define WRITE_COUNT 5
#define READ_COUNT 10
#define SCENE_DESCRIPTOR_COUNT 3

namespace ProjectTests {
	TEST_CLASS(AssetDatabaseTest)
	{

		static UID scene_id;
		static Ref<Jigsaw::dlAssetDatabaseReadWriter> db;
		static AssetDescriptor* test_desc;
		static Unique<AssetDescriptor[]> machine_descriptors;
		static Unique<UID[]> machine_ids;
		static std::vector<UID> read_machine_ids;

		TEST_CLASS_INITIALIZE(Setup) {
			db = GetTestDB();

			test_desc = new AssetDescriptor();
			test_desc->id = Jigsaw::UID::Create();
			test_desc->file_path = "./unit_test.db";
			test_desc->type = 32;
			db->WriteDescriptor(*test_desc);
		}

		TEST_CLASS_CLEANUP(Cleanup) {
			DestroyTestDB(db);
			db.reset();
			delete test_desc;
		}

		BEGIN_TEST_METHOD_ATTRIBUTE(AssetDatabaseTestReader)
			TEST_PRIORITY(2)
			END_TEST_METHOD_ATTRIBUTE()
			TEST_METHOD(AssetDatabaseTestReader) {
			AssetDescriptor t_desc = *test_desc;
			t_desc.id = UID::Create();
			db->WriteDescriptor(t_desc);

			dlAssetDatabaseReader reader("./unit_test.db");
			AssetDescriptor descriptor_b = reader.FetchDescriptor(t_desc.id);

			Assert::IsTrue(t_desc.id == descriptor_b.id);
			Assert::AreEqual(*t_desc.file_path, *descriptor_b.file_path);
			Assert::IsTrue(t_desc.type == descriptor_b.type);
			Assert::IsTrue(32 == descriptor_b.type);
		}
	};

	// static members
	UID AssetDatabaseTest::scene_id;
	Unique<AssetDescriptor[]> AssetDatabaseTest::machine_descriptors;
	std::vector<UID> AssetDatabaseTest::read_machine_ids;
	Unique<UID[]> AssetDatabaseTest::machine_ids;
	Ref<Jigsaw::dlAssetDatabaseReadWriter> AssetDatabaseTest::db;
	AssetDescriptor* AssetDatabaseTest::test_desc;
}
#endif
