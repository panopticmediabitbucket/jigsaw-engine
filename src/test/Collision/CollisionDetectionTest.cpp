#ifdef _RUN_UNIT_TESTS_
#include "CppUnitTest.h"

#define _USE_MATH_DEFINES

#include "Collision/CollisionDetection.h"
#include "Collision/CollisionPrimitives.h"
#include "TestUtils.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	TEST_CLASS(CollisionDetectionTest)
	{

		TEST_METHOD(TestCapsuleCreationAndCollision) {
			Jigsaw::Collision::Capsule c = Jigsaw::Collision::CreateCapsule(Quaternion::FromAngleAxis(PI_4, Vector3(-1, 1, 0)), Vector3(1, 1, 0), 1, .75f);

			Assert::IsTrue(withinDelta(c.up.GetLength(), 1));
			Assert::IsTrue(withinDelta(c.t.GetLength(), 1));
			Assert::IsTrue(withinDelta(c.bt.GetLength(), 1));

			Assert::IsTrue(withinDelta(c.bt.Dot(c.up), 0));
			Assert::IsTrue(withinDelta(c.bt.Dot(c.t), 0));
			Assert::IsTrue(withinDelta(c.up.Dot(c.t), 0));

			Jigsaw::Collision::Ray up{ Vector3(0, 0, 0), Vector3(0, 0, 1) };
			Jigsaw::Collision::Ray toward{ Vector3(0, 0, 0), Vector3(1, 1, 0).Normalized() };
			Jigsaw::Collision::Ray above_toward{ Vector3(0, 0, 5), Vector3(1, 1, 0).Normalized() };

			Assert::IsFalse(Jigsaw::Collision::RayCollidesCapsule(up, c));
			Assert::IsTrue(Jigsaw::Collision::RayCollidesCapsule(toward, c));
			Assert::IsFalse(Jigsaw::Collision::RayCollidesCapsule(above_toward, c));

			Jigsaw::Collision::Ray above_relative_toward{ 5 * c.up + c.c, -c.up };
			Jigsaw::Collision::Ray above_relative_away{ 5 * c.up + c.c, c.up };
			Assert::IsTrue(Jigsaw::Collision::RayCollidesCapsule(above_relative_toward, c));
			Assert::IsFalse(Jigsaw::Collision::RayCollidesCapsule(above_relative_away, c));
		}

		TEST_METHOD(TestCapsuleEdgeCollision) {
			Jigsaw::Collision::Capsule c = Jigsaw::Collision::CreateCapsule(Quaternion::FromAngleAxis(0, Vector3(0, 0, 0)), Vector3(1, -1, 0), 1, 1);
			Jigsaw::Collision::Ray edge_of_cylinder{ Vector3(5, 0, 0), Vector3(-1, 0, 0) };
			Assert::IsTrue(Jigsaw::Collision::RayCollidesCapsule(edge_of_cylinder, c));

			Jigsaw::Collision::Ray edge_of_top_sphere{ Vector3(5, -1, 2), Vector3(-1, 0, 0) };
			Assert::IsTrue(Jigsaw::Collision::RayCollidesCapsule(edge_of_top_sphere, c));

			Jigsaw::Collision::Ray edge_of_bottom_sphere{ Vector3(5, -1, -2), Vector3(-1, 0, 0) };
			Assert::IsTrue(Jigsaw::Collision::RayCollidesCapsule(edge_of_bottom_sphere, c));
		}

		TEST_METHOD(TestRayCollidesTriangle) {
			Quaternion rot = Quaternion::FromAngleAxis(PI_4, Vector3(0, 0, 1));
			Vector3 a(-3, 0, 0);
			Jigsaw::Collision::Triangle triangle{ a, rot.Rotate(Vector3(0, 3, 0)) + a, rot.Rotate(Vector3(0, 1, 4)) + a };

			Jigsaw::Collision::Ray to_corner{ Vector3(0, 0, 0), Vector3(-1, 0, 0) };
			Assert::IsTrue(Jigsaw::Collision::RayCollidesTriangle(to_corner, triangle));

			Jigsaw::Collision::Ray to_center{ Vector3(-3, 1, .5f), Vector3(-1, -1, 0) };
			Assert::IsTrue(Jigsaw::Collision::RayCollidesTriangle(to_center, triangle));

			Jigsaw::Collision::Ray miss{ Vector3(0, -.1f, 0), Vector3(-1, 0, 0) };
			Assert::IsFalse(Jigsaw::Collision::RayCollidesTriangle(miss, triangle));

			Vector3 dir = rot.Rotate(Vector3(0, 3, 0));
			Jigsaw::Collision::Ray inline_ray{ a - dir, dir };
			Assert::IsFalse(Jigsaw::Collision::RayCollidesTriangle(inline_ray, triangle));
		}
	};
}
#endif
