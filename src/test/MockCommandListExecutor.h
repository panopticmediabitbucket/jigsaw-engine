#if defined(_RUN_UNIT_TESTS_) || defined(_TEST_INJECTION_)
#ifndef _MOCK_COMMAND_LIST_EXECUTOR_H_
#define _MOCK_COMMAND_LIST_EXECUTOR_H_

#include "Graphics/CommandListExecutor.h"
#include "Graphics/GraphicsContext.h"
#include "Graphics/Pipeline/PipelineObject.h"

namespace ProjectTests 
{

	class MockCommandList : public Jigsaw::CommandList 
	{
	public:

		// Inherited via CommandList
		virtual void DrawIndexed(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const Jigsaw::VERTEX_INDEXED_DATA& render_data) override;
		virtual void LoadBuffer(const void* t_arr, size_t t_size, size_t t_count, Jigsaw::GPUResource& buffer_dest) override;
		virtual bool HasCommands() override;

		// Inherited via CommandList
		virtual void SetRenderViews(Jigsaw::GPUResource& rtv_resource, Jigsaw::GPUResource& dsv_resource) override;
		virtual void SetToDisplay(Jigsaw::GPUResource& gpu_resource) override;

		// Inherited via CommandList
		virtual void SetBufferOutput(Jigsaw::GPUResource& gpu_resource) override;

		virtual void CopyBuffer(Jigsaw::GPUResource& source, Jigsaw::GPUResource& dest) override;

		virtual void ClearResource(Jigsaw::GPUResource& gpu_resource) override;

		virtual void SetToShaderView(Jigsaw::GPUResource& gpu_resource) override;

		virtual void DrawDirect(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const Jigsaw::VERTEX_DIRECT_DATA& vertex_data) override;

		virtual void UpdateResource(Jigsaw::GPUResource& gpu_resource, const Jigsaw::UPDATE_DATA_RAW& update_data) override;

		virtual void ResolveMultiSampledResource(Jigsaw::GPUResource& ms_resource, Jigsaw::GPUResource& dest_resource) override;



		void LoadTexture(const u8* data, size_t bytes_per_row, Jigsaw::GPUResource& texture_dest) override;


		std::vector<Jigsaw::GPUResource*>& GetLoadedBuffers() override;

	private:
		bool has_commands = false;

	};

	class MockCommandListExecutor : public Jigsaw::CommandListExecutor 
	{
	public:

		// Inherited via CommandListExecutor
		virtual CommandListExecutor* SubmitCommandList(Jigsaw::Ref<Jigsaw::CommandList>& cmd_list) override;

		virtual CommandListExecutor* SubmitCommandLists(std::vector<Jigsaw::Ref<Jigsaw::CommandList>>& cmd_lists) override;

		virtual void SignalAndWait() override;

		int list_invocation_count = 0;
	};

	class MockRenderContext : public Jigsaw::GraphicsContext 
	{
	public:
		MockRenderContext() {}


		// Inherited via GraphicsContext
		virtual void CreateWindowSwapchain(const HWND hWnd, UINT width, UINT height) override;

		virtual Jigsaw::Ref<Jigsaw::CommandList> GetCommandList(Jigsaw::JGSW_COMMAND_TYPE type) override;

		virtual Jigsaw::Ref<Jigsaw::CommandListExecutor> GetCommandListExecutor(Jigsaw::JGSW_COMMAND_TYPE type) override;

		virtual void DisplayFrame() override;

		unsigned int swapchain_call_count = 0;
		std::chrono::system_clock::time_point last_swapchain_call_time;

		Jigsaw::J_Viewport* GetViewport() override;

		// Inherited via GraphicsContext
		virtual Jigsaw::GPUResource* CreateGPUResource(Jigsaw::JGSW_GPU_RESOURCE_DATA resource_args) override;
		virtual void LoadShaderResources(const Jigsaw::Ref<Jigsaw::CommandList>& command_list) override;
	};
}

#endif // !_MOCK_COMMAND_LIST_EXECUTOR_H_
#endif
