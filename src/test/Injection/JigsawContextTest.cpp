#ifdef _RUN_UNIT_TESTS_

#include "Injection/jsSlots.h"
#include "CppUnitTest.h"
#include "Injection/jsContextConfig.h"
#include "Injection/jsContext.h"
#include "Entities/jsEntityService.h"
#include "Graphics/GraphicsContext.h"
#include "Orchestration//orSystemsOrchestrator.h"
#include "Injection/TestKnobProvider.h"
#include "Injection/JigsawInjection.h"
#include "Systems/SysOrchTestSystem.h"
#include <vector>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {
	class MockSystemsOrchestrator : public Jigsaw::orSystemsOrchestrator {

	public:
		MockSystemsOrchestrator() {}

		Jigsaw::jsEntityService* GetServiceRef() {
			return s_clusterService;
		}

	};

	START_JGSW_COMPONENT_PROPERTIES(MockSystemsOrchestrator, orchestrator)
	PROVIDER_NAMESPACE("Jigsaw::Test::Alt")
	END_JGSW_COMPONENT_PROPERTIES(MockSystemsOrchestrator, orchestrator)
	JGSW_COMPONENT(MockSystemsOrchestrator, orchestrator);

	START_JGSW_COMPONENT_PROPERTIES(TestReceiver, receiver)
	PROVIDER_NAMESPACE("Jigsaw::Test::Alt2")
	END_JGSW_COMPONENT_PROPERTIES(TestReceiver, receiver)
	JGSW_COMPONENT(TestReceiver, receiver);

	TEST_CLASS(JigsawContextTest)
	{

		static Jigsaw::Ref<Jigsaw::jsContext> context;

		class T {
		public:
			class Q {
				int b;

			};
		};

		TEST_CLASS_INITIALIZE(Setup) {
			Jigsaw::jsContextConfig config;
			Jigsaw::namespace_import import;
			import.m_namespace = "Jigsaw::Test";
			config.m_packageNames.push_back(import);

			context = Jigsaw::jsContext::Create(config);
		}

		TEST_METHOD(TestContextInitialization) {
			Jigsaw::jsEntityService* cluster_service = context->GetKnob<Jigsaw::jsEntityService>();
			Assert::IsTrue((bool)cluster_service);
		}

		TEST_METHOD(TestTransitiveNamespace) {
			std::string* string = context->GetKnob<std::string>("string");
			Assert::IsTrue(*string == "test string value");
		}

		TEST_METHOD(TestInjectKnobsIntoJigsawSlots) {
			Jigsaw::Ref<TestReceiver> receiver = Jigsaw::MakeRef<TestReceiver>();
			context->FillSlots(*receiver);
			Assert::IsTrue(receiver->GetString() == "test string value");
		}

		TEST_METHOD(TestInjectKnobsIntoVectorSlots) {
			TestReceiver* receiver = new TestReceiver();
			context->FillSlots(*receiver);
			std::vector<std::string*>& strings = receiver->strings;
			Assert::IsFalse(strings.empty());
			Assert::IsTrue(strings.size() > 1);
			Assert::IsTrue(strings.size() == 3);
			std::vector<std::string*>& qual_strings = receiver->qual_strings;
			Assert::IsTrue(qual_strings.size() == 1);
		}

		TEST_METHOD(TestInjectKnobsIntoVectorSlotsAtContextBoot) {
			TestReceiver* receiver = context->GetKnob<TestReceiver>();
			std::vector<std::string*>& strings = receiver->strings;
			Assert::IsFalse(strings.empty());
			Assert::IsTrue(strings.size() > 1);
			std::vector<std::string*>& qual_strings = receiver->qual_strings;
			Assert::IsTrue(qual_strings.size() == 1);
		}

		TEST_METHOD(TestKnobComponentPickedUp) {
			MockSystemsOrchestrator* system_orchestrator = context->GetKnob<MockSystemsOrchestrator>();
			Jigsaw::jsEntityService* cluster_service = context->GetKnob<Jigsaw::jsEntityService>();
			Assert::IsTrue(cluster_service == system_orchestrator->GetServiceRef());
		}

		class dlLoadResourcesAccessor : public Jigsaw::dlLoadResources {
		public:
			Jigsaw::GraphicsContext* GetRenderContext() {
				return this->m_context;
			}
		};

		TEST_METHOD(TestSceneHierarachyBuiltWithParameters) {
			Jigsaw::dlLoadResources* load_resources = context->GetKnob<Jigsaw::dlLoadResources>();
			Jigsaw::GraphicsContext* render_context = context->GetKnob<Jigsaw::GraphicsContext>();
			dlLoadResourcesAccessor* accessor = static_cast<dlLoadResourcesAccessor*>(load_resources);

			Assert::IsTrue(load_resources);
			Assert::IsTrue(accessor->GetRenderContext() == render_context);
		}

		TEST_METHOD(TestContextInjected) {
			Jigsaw::Ref<TestReceiver> receiver = Jigsaw::MakeRef<TestReceiver>();
			context->FillSlots(*receiver);
			Jigsaw::jsContext* internal_context = receiver->jgsw_context;
			Assert::IsTrue(internal_context == context.get());
		}

		TEST_METHOD(TestIsAInheritance) {
			TestReceiver* receiver = new TestReceiver();
			context->FillSlots(*receiver);
			Assert::IsNotNull(receiver->clock_reader);
		}

		TEST_METHOD(TestIsAGraphResolution) {
			TestIsAInitializedClass* init_class = context->GetKnob<TestIsAInitializedClass>();
			Assert::IsNotNull(init_class->reader);
		}

		TEST_METHOD(TestLazyInitialization) {
			TestReceiver* receiver = context->GetKnob<TestReceiver>();
			Assert::IsTrue("lazyString" == *receiver->lazy_string);
		}

		template <class T> struct temp { typedef T type; };

		template <template <typename T> class _temp> struct Z { };

		TEST_METHOD(TestInjectKnobsTransitivelyInDependentContext) {
			// jsRuntimeSystemRegistry is in itself a jsContext
			Jigsaw::jsRuntimeSystemRegistry* system_registry = context->GetKnob<Jigsaw::jsRuntimeSystemRegistry>();

			// the RootSystems are constructed as a part of this context
			Jigsaw::jsSystem* root_system = system_registry->GetKnob<Jigsaw::jsSystem>("ProjectTests::SysOrchTestSystem");

			Assert::IsTrue((bool)root_system);
			SysOrchTestSystem* test_system = dynamic_cast<SysOrchTestSystem*>(root_system);
			Assert::IsTrue((bool)test_system);

			// the same 'test_val' knob defined in SysOrchTestKnobProvider is collected transitively from the root context
			// (jsContext) to the dependent context (jsRuntimeSystemRegistry) and injected into the jsSystem node native to that context 
			TFloat* test_val_from_context = context->GetKnob<TFloat>();
			float test_val_from_system = test_system->test_val->f;
			Assert::AreEqual(test_val_from_context->f, test_val_from_system);
		}


	};

	Jigsaw::Ref<Jigsaw::jsContext> JigsawContextTest::context;
}
#endif
