#ifdef _RUN_UNIT_TESTS_

#include "CppUnitTest.h"
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Injection/JigsawInjection.h"
#include "TestKnobProvider.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace ProjectTests {

	TEST_CLASS(JigsawKnobProviderTest) {

		TEST_METHOD(TestPropertiesRegistered) {
			std::vector<Jigsaw::knob_provider_definition> providers = Jigsaw::Assembly::JigsawStaticContext::GetNamespaceProviders("Jigsaw::Test");
			auto provider_def = providers.at(0);
			Jigsaw::jsKnobProvider* provider = provider_def.build_provider();

			TestKnobProvider* knob_provider = dynamic_cast<TestKnobProvider*>(provider);
			Assert::IsTrue((bool)knob_provider);

			Jigsaw::knob_definition knob_def = *provider->knobs.begin();
			Assert::AreEqual("cluster_service", knob_def.name.c_str());
			knob_def.customConstruct(knob_def);
			Jigsaw::jsEntityService* cluster_service = *static_cast<Jigsaw::jsEntityService**>(knob_def.referenceContainer);
			Assert::IsNotNull(cluster_service);
		}

		TEST_METHOD(TestKnobProviderConstructor) {
			std::vector<Jigsaw::knob_provider_definition> providers = Jigsaw::Assembly::JigsawStaticContext::GetNamespaceProviders("Jigsaw::Test");
			auto provider_def = providers.at(0);
			Jigsaw::jsKnobProvider* provider = provider_def.build_provider();

			TestKnobProvider* knob_provider = dynamic_cast<TestKnobProvider*>(provider);

			Jigsaw::knob_definition knob_def = *std::find_if(provider->knobs.begin(), provider->knobs.end(), [](Jigsaw::knob_definition& def) { return def.name == "load_resources"; });
			Assert::AreEqual(1, (int)knob_def.constructor_dependencies.size());

		}
	};
}

#endif
