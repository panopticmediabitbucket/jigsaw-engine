#if defined(_RUN_UNIT_TESTS_) || defined(_TEST_INJECTION_)
#ifndef _TEST_KNOB_PROVIDER_H_
#define _TEST_KNOB_PROVIDER_H_

#include "Debug/ScopedLoggingTimer.h"
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Injection/JigsawInjection.h"
#include "Entities/jsEntityService.h"
#include "Graphics/GPU_Objects/J_Viewport.h"
#include "Render/RenderService.h"
#include "Injection/jsSlots.h"
#include "Input/ctInputEventWriter.h"
#include "Orchestration/orSceneHierarchy.h"
#include "Orchestration/MockMessageQueue.h"
#include "Orchestration/orMainOrchestrator.h"
#include "Systems/jsRuntimeSystemRegistry.h"
#include "Systems/SystemArguments.h"
#include "Scene/jsSceneObserver.h"
#include "MockCommandListExecutor.h"
#include "Time/ApplicationClockService.h"
#include "Ref.h"
#include <string>

namespace ProjectTests {

	class TestIsAInitializedClass : public Jigsaw::jsSlots<TestIsAInitializedClass> {
	public:
		TestIsAInitializedClass(Jigsaw::Time::ApplicationClockReader* reader) : reader(reader) {}

		Jigsaw::Time::ApplicationClockReader* reader;

	};

	JGSW_KNOB_PROVIDER(TestKnobProvider) {

		KNOB(Jigsaw::jsEntityService, cluster_service) {
			return new Jigsaw::jsEntityService();
		};

		KNOB(Jigsaw::GraphicsContext, render_context) {
			return new MockRenderContext();
		}

		KNOB(Jigsaw::orSceneHierarchy, scene_hierarchy) {

			return new Jigsaw::orSceneHierarchy();
		}

		KNOB(Jigsaw::dlLoadResources, load_resources,
			SLOT_ARG(Jigsaw::GraphicsContext, context)) {
			return new Jigsaw::dlLoadResources(context, 5);
		}

		KNOB(Jigsaw::dlAssetRegistrar, asset_manager) {
			return new Jigsaw::dlAssetRegistrar();
		}

		KNOB(Jigsaw::orMainOrchestrator, orchestrator,
			IS_A(Jigsaw::orOrchestrator)) {

			return new Jigsaw::orMainOrchestrator();
		}

		KNOB(Jigsaw::Time::ApplicationClockService, clock_service,
			IS_AN(const Jigsaw::Time::ApplicationClockReader)) {
			return new Jigsaw::Time::ApplicationClockService();
		}

		KNOB(Jigsaw::ctInputEventWriter, input_service,
			IS_AN(Jigsaw::ctInputEvents)) {
			return new Jigsaw::ctInputEventWriter();
		}

		KNOB(Jigsaw::orSystemsOrchestrator, systems_orchestrator) {
			return new Jigsaw::orSystemsOrchestrator();
		}

		KNOB(Jigsaw::orMessageQueue, message_queue) {
			return new MockMessageQueue();
		}

		KNOB(TestIsAInitializedClass, is_a_initialized,
			SLOT_ARG(Jigsaw::Time::ApplicationClockReader, reader)) {
			return new TestIsAInitializedClass(reader);
		}

		KNOB(Jigsaw::J_Viewport, j_viewport, QUALIFIER("game_viewport"),
			SLOT_ARG(Jigsaw::GraphicsContext, render_context)) {
			return new Jigsaw::J_Viewport();
		}

		KNOB(Jigsaw::RenderService, render_service) {
			return new Jigsaw::RenderService();
		}

		KNOB(Jigsaw::jsRuntimeSystemRegistry, system_registry) {
			Jigsaw::SystemManagerArguments arguments;
			Jigsaw::PriorityNode PRIORITY_LEVEL;
			Jigsaw::SystemInformation information, test_system;

			information.qualified_system_name = "ProjectTests::TransformSystem";
			PRIORITY_LEVEL.concurrent_systems.push_back(information);

			test_system.qualified_system_name = "ProjectTests::SysOrchTestSystem";
			PRIORITY_LEVEL.concurrent_systems.push_back(test_system);

			arguments.system_priority_levels.push_back(PRIORITY_LEVEL);

			auto beans = Jigsaw::Assembly::SystemRegistry::GetBeans();

			return new Jigsaw::jsRuntimeSystemRegistry(arguments, beans);
		}
	};

	class TestReceiver : public Jigsaw::jsSlots<TestReceiver> {
public:
	std::string GetString();

	INJECT_CONTEXT(slot_1, jgsw_context);

	SLOT(slot_2, Jigsaw::Time::ApplicationClockReader, clock_reader);

	SLOT_COLLECTION(slot_3, std::vector, std::string, strings, LAZY);

	SLOT_COLLECTION(slot_4, std::vector, std::string, qual_strings, 
		QUALIFIER("testQualifier"));

	SLOT(slot_5, std::string, qual_string, QUALIFIER("testQualifier"));
	SLOT(slot_6, std::string, lazy_string, QUALIFIER("lazyString"), LAZY);

private:
	SLOT(slot_7, std::string, string);

	};

	JGSW_KNOB_PROVIDER(TestKnobProviderAlt) {

		KNOB(std::string, string) {
			return new std::string("test string value");
		};

		KNOB(std::string, other_string, QUALIFIER("testQualifier")) {
			return new std::string("test string value 2");
		}

		KNOB(std::string, lazy_string, QUALIFIER("lazyString"),
			SLOT_ARG(TestReceiver, test)) {
			return new std::string("lazyString");
		}

	};
}
#endif
#endif
