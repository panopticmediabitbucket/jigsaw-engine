#include "AssemblyImportUtil.h"
#include "Application/ApplicationRootProperties.h"
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Debug/j_debug.h"

namespace Jigsaw {
	namespace Assembly {

		//////////////////////////////////////////////////////////////////////////

		void JGSW_API ImportAssemblyComponents(const Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>& _interface) {
			// importing the knob provider definitions from the external interface/module
			std::vector<Jigsaw::knob_provider_definition> provider_defs = _interface->GetKnobProviderDefinitions();

			for (auto provider_def : provider_defs) {
				JigsawStaticContext::SubmitProvider(provider_def);
			}

			// importing the marshalling data from the external interface/module
			std::vector<Jigsaw::Marshalling::MarshallingMap> marshalling_maps = _interface->GetMarshallingMaps();
			for (auto map : marshalling_maps) {
				Assembly::MarshallingRegistry::RegisterMap(map.GetType(), map);
			}

			// importing the system registry beans from the external interface/module
			std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> beans = _interface->GetJigsawSystemBeans();
			for (auto bean : beans) {
				SystemRegistry::GetBeans().push_back(bean);
			}
		}


		//////////////////////////////////////////////////////////////////////////

		void JGSW_API ReleaseAssemblyComponents(const Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>& _interface) {
			// remove the knob providers from the knob provider definition map
			std::vector<Jigsaw::knob_provider_definition> provider_defs = _interface->GetKnobProviderDefinitions();

			for (auto provider_def : provider_defs) {
				std::map<std::string, std::vector<Jigsaw::knob_provider_definition>>& map = JigsawStaticContext::GetProviderMap();
				auto namespace_iter = std::find_if(map.begin(), map.end(), [&](const std::pair<std::string, std::vector<Jigsaw::knob_provider_definition>>& namespace_pair) ->
					bool {
						return provider_def.properties.m_namespace == namespace_pair.first;
					});

				if (namespace_iter != map.end()) {
					std::vector<Jigsaw::knob_provider_definition>& knob_defs = namespace_iter->second;
					auto knob_iter = std::find_if(knob_defs.begin(), knob_defs.end(), [&](const Jigsaw::knob_provider_definition& knob_def) -> bool {
						return knob_def.name == provider_def.name && knob_def.provider_type == provider_def.provider_type;

						});

					if (knob_iter != knob_defs.end()) {
						knob_defs.erase(knob_iter);
					}

					if (knob_defs.size() == 0) {
						map.erase(namespace_iter);
					}
				}
			}

			// remove the marshalling maps not local to this assembly
			std::vector<Jigsaw::Marshalling::MarshallingMap> m_maps = _interface->GetMarshallingMaps();
			std::unordered_map<Jigsaw::etype_index, Jigsaw::Marshalling::MarshallingMap>& m_type_map = MarshallingRegistry::GetInstance().type_marshalling_map;
			for (auto map : m_maps) {
				auto iter = m_type_map.find(map.GetType());
				if (iter != m_type_map.end()) {
					m_type_map.erase(iter);
				}
			}

			// remove the external system beans from the SystemRegistry
			std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> assembly_beans = _interface->GetJigsawSystemBeans();
			std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>& sys_beans = SystemRegistry::GetBeans();
			for (auto bean : assembly_beans) {
				auto iter = std::find_if(sys_beans.begin(), sys_beans.end(), [&](const Jigsaw::SYSTEM_REGISTRY_BEAN& sys_bean) -> bool { return sys_bean.typeInfo == bean.typeInfo; });
				if (iter != sys_beans.end()) {
					sys_beans.erase(iter);
				}
			}

		}

		//////////////////////////////////////////////////////////////////////////

		typedef int (*JGSW_ASSEMBLY_IMPORT_FUNCTION)(Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>*);

		//////////////////////////////////////////////////////////////////////////

		void JGSW_API ImportRootPropertiesAssemblies() {
			for (const std::string& path : ApplicationRootProperties::Get().extensions) {
				HMODULE lib_handle = LoadLibrary(path.c_str());
				J_D_ASSERT_LOG_ERROR(lib_handle != 0, ApplicationRootProperties, "Failed to import library {0}, please make sure it is along the project scan path", path);
				ImportModuleJigsawComponents(lib_handle, path);
			}
		}

		//////////////////////////////////////////////////////////////////////////

		std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>JGSW_API GetSystemBeans() {
			return SystemRegistry::GetBeans();
		}

		//////////////////////////////////////////////////////////////////////////

		void JGSW_API ImportModuleJigsawComponents(HMODULE module, const std::string& assembly_name) {
			JGSW_ASSEMBLY_IMPORT_FUNCTION import = reinterpret_cast<JGSW_ASSEMBLY_IMPORT_FUNCTION>(GetProcAddress(module, "GetJigsawAssemblyInterface"));
			if (import) {
				J_LOG_INFO(ApplicationRootProperties, "Successfully imported Jigsaw Assembly '{0}' and discovered import function", assembly_name);
				Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface> _interface;
				int import_result = import(&_interface);
				J_D_ASSERT_LOG_ERROR(import_result == 0, ApplicationRootProperties, "Something went wrong while attempting to import the Jigsaw Assembly {0}", assembly_name);
				ImportAssemblyComponents(_interface);
			}
			else {
				J_LOG_INFO(ApplicationRootProperties, "Successfully imported Unknown Assembly '{0}'", assembly_name);
			}
		}

		//////////////////////////////////////////////////////////////////////////
	}
}
