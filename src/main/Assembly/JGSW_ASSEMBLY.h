#ifndef _JGSW_ASSEMBLY_H_
#define _JGSW_ASSEMBLY_H_

#include "Injection/jsContext.h"
#include "Assembly/JigsawAssemblyInterface.h"
#include "Marshalling/MarshallingMap.h"
#include "Systems/SYSTEM_REGISTRY_BEAN.h"

namespace Jigsaw {
	namespace Assembly {
		/// <summary>
		/// The global context is used when a jigsaw context object is being constructed to fetch packages and providers
		/// of JigsawKnobs.
		/// </summary>
		class JGSW_API JigsawStaticContext {
		public:
			/// <summary>
			/// Used statically to submit a globally-scoped provider
			/// </summary>
			/// <param name="provider"></param>
			static void SubmitProvider(const Jigsaw::knob_provider_definition& provider);

			/// <summary>
			/// Returns the Knobs associated with a given namespace
			/// </summary>
			/// <param name="_namespace"></param>
			/// <returns></returns>
			static std::vector<Jigsaw::knob_provider_definition> GetNamespaceProviders(const std::string& _namespace);

			/// <summary>
			/// Returns the underlying namespace provider map
			/// </summary>
			/// <returns></returns>
			static std::map<std::string, std::vector<Jigsaw::knob_provider_definition>>& GetProviderMap();

		};

		/// <summary>
		/// The core registry where all MarshallingMaps for all serializable classes are stored. This registry is populated at run time
		/// while the data segment is being initialized. 
		/// </summary>
		class JGSW_API MarshallingRegistry {
		public:
			/// <summary>
			/// Do not call this method unless you're absolutely sure what you're doing. MarshallingMap's should be inserted using the 'REGISTER_SERIALIZABLE_..." methods,
			/// not through direct calls to 'RegisterMap' in consuming code. 
			/// </summary>
			/// <param name="type"></param>
			/// <param name="map"></param>
			static void RegisterMap(const Jigsaw::etype_info& type, const Jigsaw::Marshalling::MarshallingMap& map);

			/// <summary>
			/// Returns a MarshallingMap associated with the given 'type'
			/// </summary>
			/// <param name="type"></param>
			/// <returns></returns>
			static const Jigsaw::Marshalling::MarshallingMap& GetMarshallingMap(const Jigsaw::etype_info& type);

			/// <summary>
			/// Returns all of the maps that have been registered in the MarshallingRegistry
			/// </summary>
			/// <returns></returns>
			static std::vector<Jigsaw::Marshalling::MarshallingMap> GetMaps();

			/// <summary>
			/// The MarshallingRegistry needs to be specifically instantiated since the order of static instantiation of
			/// translation units is undefined. Hence, a singleton-retrieving method needs to be represented internally so that 
			/// the other static functions behave consistently. 
			/// </summary>
			/// <returns></returns>
			static MarshallingRegistry& GetInstance();

			std::unordered_map<Jigsaw::etype_index, Jigsaw::Marshalling::MarshallingMap> type_marshalling_map;

		};

		/// <summary>
		/// The SystemRegistry is where SYSTEM_REGISTRY_BEANs are passed and retrieved later
		/// </summary>
		class JGSW_API SystemRegistry {
		public:
			static std::vector<SYSTEM_REGISTRY_BEAN>& GetBeans();
		};

		extern "C" JGSW_API int GetJigsawAssemblyInterface(Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>* out_interface);

	}
}

#endif
