#ifndef _ASSEMBLY_IMPORT_UTIL_H_
#define _ASSEMBLY_IMPORT_UTIL_H_

#include <Windows.h>
#include "Ref.h"
#include "Assembly/JigsawAssemblyInterface.h"
#include "_jgsw_api.h"
#include "Systems/SYSTEM_REGISTRY_BEAN.h"
#include <vector>
#include "Ref.h"

namespace Jigsaw {
	namespace Assembly {

		/// <summary>
		/// Priviledged function to be used only by the editor and the application. Imports the assembly components associated with the _interface
		/// to the global contexts. This includes system beans, renderer beans, marshalling maps, and knob providers.
		/// </summary>
		/// <param name="_interface"></param>
		/// <returns></returns>
		void JGSW_API ImportAssemblyComponents(const Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>& _interface);

		/// <summary>
		/// Priviledged function to be used only by the editor and the application. Releases the assembly components associated with the _interface
		/// from the global contexts. This includes system beans, renderer beans, marshalling maps, and knob providers.
		/// </summary>
		/// <param name="_interface"></param>
		/// <returns></returns>
		void JGSW_API ReleaseAssemblyComponents(const Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>& _interface);

		/// <summary>
		/// Fetches the root properties from the file descriptor and releases them. 
		/// </summary>
		/// <returns></returns>
		void JGSW_API ImportRootPropertiesAssemblies();

		/// <summary>
		/// Gets systems.
		/// </summary>
		std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> JGSW_API GetSystemBeans();

		/// <summary>
		/// Imports any Jigsaw components associated with the module handle. 
		/// </summary>
		/// <param name="module"></param>
		/// <param name="assembly_name"></param>
		/// <returns></returns>
		void JGSW_API ImportModuleJigsawComponents(HMODULE module, const std::string& assembly_name);



	}
}
#endif