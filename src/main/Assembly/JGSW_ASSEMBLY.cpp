#include "JGSW_ASSEMBLY.h"
#include <map>

namespace Jigsaw {
	namespace Assembly {

		std::map<std::string, std::vector<Jigsaw::knob_provider_definition>>& JigsawStaticContext::GetProviderMap() {
			static std::map<std::string, std::vector<Jigsaw::knob_provider_definition>> map;
			return map;
		}

		void JigsawStaticContext::SubmitProvider(const Jigsaw::knob_provider_definition& provider_definition) {
			std::map<std::string, std::vector<Jigsaw::knob_provider_definition>>& map = GetProviderMap();
			auto find = map.find(provider_definition.properties.m_namespace);
			if (find == map.end()) {
				map.insert(std::make_pair(provider_definition.properties.m_namespace, std::vector<Jigsaw::knob_provider_definition>()));
			}
			map.at(provider_definition.properties.m_namespace).push_back(provider_definition);
		}

		std::vector<Jigsaw::knob_provider_definition> JigsawStaticContext::GetNamespaceProviders(const std::string& _namespace) {
			std::map<std::string, std::vector<Jigsaw::knob_provider_definition>>& map = GetProviderMap();
			auto find = map.find(_namespace);
			if (find != map.end()) {
				return (*find).second;
			}
			return std::vector<Jigsaw::knob_provider_definition>();
		}

		void MarshallingRegistry::RegisterMap(const Jigsaw::etype_info& type, const Jigsaw::Marshalling::MarshallingMap& map) {
			if (GetInstance().type_marshalling_map.find(type) == GetInstance().type_marshalling_map.end()) {
				Jigsaw::etype_index ind(type);
				GetInstance().type_marshalling_map.insert(std::make_pair(ind, map));
			} // else SOL
		}

		const Jigsaw::Marshalling::MarshallingMap& MarshallingRegistry::GetMarshallingMap(const Jigsaw::etype_info& type) {
			return GetInstance().type_marshalling_map.at(type);
		}

		std::vector<Jigsaw::Marshalling::MarshallingMap> MarshallingRegistry::GetMaps() {
			auto map = GetInstance().type_marshalling_map;
			std::vector<Jigsaw::Marshalling::MarshallingMap> map_vector;
			for (auto pair : map) {
				map_vector.push_back(pair.second);
			}
			return map_vector;
		}

		MarshallingRegistry& MarshallingRegistry::GetInstance() {
			static MarshallingRegistry registry;
			return registry;
		}

		// System Registry Impl

		std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>& SystemRegistry::GetBeans() {
			static std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> vector;
			return vector;
		}

		// Assembly implementation

		class JigsawAssemblyImpl : public JigsawAssemblyInterface {
		public:
			JigsawAssemblyImpl() {}

			std::vector<Jigsaw::Marshalling::MarshallingMap> GetMarshallingMaps() const override {
				return MarshallingRegistry::GetMaps();
			};

			std::vector<Jigsaw::knob_provider_definition> GetKnobProviderDefinitions() const override {
				auto map = JigsawStaticContext::GetProviderMap();
				std::vector<Jigsaw::knob_provider_definition> knob_vector;
				for (auto pair : map) {
					std::for_each(pair.second.begin(), pair.second.end(), [&](const Jigsaw::knob_provider_definition& def) { knob_vector.push_back(def); });
				}

				return knob_vector;
			}

			// Inherited via JigsawAssemblyInterface
			std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> GetJigsawSystemBeans() const override {
				return SystemRegistry::GetBeans();
			}


		};

		JGSW_API int GetJigsawAssemblyInterface(Jigsaw::Ref<Jigsaw::Assembly::JigsawAssemblyInterface>* out_interface) {
			*out_interface = Jigsaw::Ref<JigsawAssemblyInterface>(new JigsawAssemblyImpl);
			return 0;
		}
	}
}