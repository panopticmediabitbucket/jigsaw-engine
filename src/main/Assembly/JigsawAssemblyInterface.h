#ifndef _JIGSAW_ASSEMBLY_INTERFACE_
#define _JIGSAW_ASSEMBLY_INTERFACE_

#include "RTTI/etype_info.h"
#include "Injection/jsKnobProvider.h"
#include "Injection/jsContext.h"
#include "Marshalling/MarshallingMap.h"
#include <map>
#include "Systems/SYSTEM_REGISTRY_BEAN.h"

namespace Jigsaw {
	namespace Assembly {

		class JGSW_API JigsawAssemblyInterface {
		public:
			JigsawAssemblyInterface();

			virtual std::vector<Jigsaw::Marshalling::MarshallingMap> GetMarshallingMaps() const = 0;
			virtual std::vector<Jigsaw::knob_provider_definition> GetKnobProviderDefinitions() const = 0;
			virtual std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> GetJigsawSystemBeans() const = 0;

		};
	}
}
#endif