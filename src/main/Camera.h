#pragma once
#include "Graphics/DirectX/DX_Context.h"
#include "Graphics/DirectX/DirectXDataLayouts.h"
#include <DirectXMath.h>
#include "Math/LinAlg.h"

class JGSW_API Camera {

public:
	Vector3 viewing_dir;
	Vector3 position;
	float x_fov;
	float near_plane;
	float far_plane;

	float persp_ortho_interp = 0;

public:
	Camera(Vector3 position, Vector3 viewing_dir);

	Mat4x4 GetRotationMatrix() const;
	Mat4x4 GetTranslationMatrix() const;
	Mat4x4 GetViewMatrix() const;
	Mat4x4 GetProjectionMatrix(const float wh_ratio) const;

private:

};
