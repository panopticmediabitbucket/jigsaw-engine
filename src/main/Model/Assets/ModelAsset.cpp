#include "ModelAsset.h"
#include "Graphics/GPU_Objects/JGSW_GPU_RESOURCE_DATA.h"

using namespace Jigsaw;
using namespace Jigsaw::Assets;

//////////////////////////////////////////////////////////////////////////

IMPL_DATA_ASSET(ModelAsset);

//////////////////////////////////////////////////////////////////////////

Jigsaw::ModelAsset::ModelAsset(const AssetDescriptor& descriptor) : DataAsset(descriptor)
{
}

//////////////////////////////////////////////////////////////////////////

Jigsaw::ASSET_PACKAGE_RESULT ModelAsset::Package(const INDEXED_MESH* mesh)
{
	ASSET_PACKAGE_RESULT result;

	return result;
}

//////////////////////////////////////////////////////////////////////////

ASSET_LOAD_RESULT Jigsaw::ModelAsset::Init(const INDEXED_MESH& mesh, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources)
{
	ASSET_LOAD_RESULT result;

	// creating the vertex buffer resource
	JGSW_GPU_RESOURCE_DATA vertex_buffer_data;
	vertex_buffer_data.flags |= JGSW_GPU_RESOURCE_FLAG_SINGLE_UPLOAD;
	vertex_buffer_data.width = mesh.GetVertexCount() * (u32)mesh.CalculateVertexSize();
	vertex_buffer_data.resource_type = JGSW_GPU_RESOURCE_TYPE::BUFFER;
	m_vertices = sys_resources.render_context->CreateGPUResource(vertex_buffer_data);

	// creating the index buffer resource
	JGSW_GPU_RESOURCE_DATA index_buffer_data = vertex_buffer_data;
	index_buffer_data.width = mesh.GetIndexCount() * sizeof(u32);
	m_indices = sys_resources.render_context->CreateGPUResource(index_buffer_data);

	// Issuing the load command to the command list
	sys_resources.cmd_list->LoadBuffer(mesh.GetVertexData(), vertex_buffer_data.width, mesh.GetVertexCount(), *m_vertices);
	sys_resources.cmd_list->LoadBuffer(mesh.GetIndexArray(), (u32)sizeof(u32), mesh.GetIndexCount(), *m_indices);

	result.result = RESULT::COMPLETE;
	return result;
}

//////////////////////////////////////////////////////////////////////////

ASSET_LOAD_RESULT Jigsaw::ModelAsset::Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources)
{
	u8* file_read = new u8[file_data.file_size];
	fread_s(file_read, file_data.file_size, file_data.file_size, 1, file_data.file);
	INDEXED_MESH& mesh = *reinterpret_cast<INDEXED_MESH*>(file_read);
	ASSET_LOAD_RESULT result = Init(mesh, sys_resources);
	return result;
}

//////////////////////////////////////////////////////////////////////////
