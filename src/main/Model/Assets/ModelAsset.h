/*********************************************************************
 * ModelAsset.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: August 2021
 *********************************************************************/
#ifndef _GRAPHICS_ASSETS_H_
#define _GRAPHICS_ASSETS_H_

#include "Assets/DataAssets.h"
#include "Model/INDEXED_MESH.h"

namespace Jigsaw {

	/// <summary>
	/// ModelResource contains the information necessary for rendering a mesh. 
	/// </summary>
	class JGSW_API ModelAsset final : public DataAsset {
	public:
		ModelAsset(const AssetDescriptor& descriptor);

		DECLARE_DATA_ASSET(ModelAsset);

#if JGSW_DEV
		/// <summary>
		/// Packages the mesh for file writing such that, on a subsequent load, a valid ModelAsset will be created.
		/// </summary>
		/// <param name="mesh"></param>
		/// <returns></returns>
		static ASSET_PACKAGE_RESULT Package(const INDEXED_MESH* mesh);
#endif

		/// <summary>
		/// Get the vertices associated with the model. 
		/// </summary>
		/// <returns></returns>
		inline Jigsaw::GPUResource* GetVertices() const { return m_vertices; }

		/// <summary>
		/// Get the indices associated with the model. 
		/// </summary>
		/// <returns></returns>
		inline Jigsaw::GPUResource* GetIndices() const { return m_indices; }

	protected:
		INDEXED_MESH* m_mesh;
		Jigsaw::GPUResource* m_vertices;
		Jigsaw::GPUResource* m_indices;

		/// <summary>
		/// Initializes the model resource directly with an Indexed Mesh rather than through a load operation. 
		/// </summary>
		/// <param name="mesh"></param>
		/// <param name="sys_resources"></param>
		/// <returns></returns>
		ASSET_LOAD_RESULT Init(const INDEXED_MESH& mesh, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources);

		/// <summary>
		/// Load function called by Asset manager for supported Jigsaw file types. Fbx/obj files will be handled first by their corresponding translators.
		///
		/// The translators are not included in the Jigsaw Engine or any related projects because they are not part of the actual engine runtime.
		/// They are meant to exist only in the Development environment and be removed later.
		/// </summary>
		/// <param name="file_data"></param>
		/// <param name="sys_resources"></param>
		/// <returns></returns>
		ASSET_LOAD_RESULT Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) override;
	};
}
#endif