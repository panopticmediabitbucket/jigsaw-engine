#include "INDEXED_MESH.h"

namespace Jigsaw {

	//////////////////////////////////////////////////////////////////////////

	INDEXED_MESH* INDEXED_MESH::Create(size_t vertex_count, size_t index_count, const JGSW_VERT_TUPLE* vertex_args, size_t num_vertex_args) {
		size_t vertex_size = 0;
		for (u32 i = 0; i < num_vertex_args; i++) {
			vertex_size += GetSize(vertex_args[i].output_type);
		}

		size_t running_size = sizeof(mesh_header) + sizeof(JGSW_FORMAT) * num_vertex_args;
		Util::NextAlignN(running_size, 0x10);
		running_size += vertex_size * vertex_count;
		running_size += index_count * sizeof(u32);

		INDEXED_MESH* mesh = reinterpret_cast<INDEXED_MESH*>(_aligned_malloc(running_size, alignof(max_align_t)));
		mesh->m_header.m_indexCount = index_count;
		mesh->m_header.m_vertexCount = vertex_count;
		mesh->m_header.m_numVertexArgs = num_vertex_args;
		{
			JGSW_VERT_TUPLE* args = reinterpret_cast<JGSW_VERT_TUPLE*>(&mesh->m_header + 1);
			for (u32 i = 0; i < num_vertex_args; i++) {
				args[i] = vertex_args[i];
			}
		}

		return mesh;
	}

	//////////////////////////////////////////////////////////////////////////

	inline const JGSW_VERT_TUPLE* INDEXED_MESH::GetVertexArgFormats() const {
		return reinterpret_cast<const JGSW_VERT_TUPLE*>(&m_header + 1);
	}

	//////////////////////////////////////////////////////////////////////////

	inline const u8* INDEXED_MESH::GetVertexData() const {
		const u8* endArgFormats = reinterpret_cast<const u8*>(GetVertexArgFormats() + m_header.m_numVertexArgs);
		Util::NextAlignN((u64&)endArgFormats, 0x10);
		return endArgFormats;
	}

	//////////////////////////////////////////////////////////////////////////

	inline u32 INDEXED_MESH::CalculateVertexSize() const {
		const JGSW_VERT_TUPLE* formats = GetVertexArgFormats();
		u32 out_size = 0;
		for (u32 i = 0; i < m_header.m_numVertexArgs; i++) {
			out_size += GetSize(formats[i].output_type);
		}
		return out_size;
	}

	//////////////////////////////////////////////////////////////////////////

	inline const u32* INDEXED_MESH::GetIndexArray() const {
		const u32* verticesEnd = reinterpret_cast<const u32*>(GetVertexData() + (CalculateVertexSize() * m_header.m_vertexCount));
		Util::NextAlign<u32>((u64&)verticesEnd);
		return verticesEnd;
	}

	//////////////////////////////////////////////////////////////////////////
}