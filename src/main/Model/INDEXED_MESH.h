#ifndef _INDEXED_MESH_H_
#define _INDEXED_MESH_H_

#include <cstddef>
#include "corecrt_malloc.h"
#include "Graphics/GPU_Objects/JGSW_GPU_RESOURCE_DATA.h"

namespace Jigsaw {
	/// <summary>
	/// 
	/// </summary>
	struct mesh_header {
		// This must always come first
		u32 m_resourceVersion;

		u32 m_vertexCount = 0;
		u32 m_indexCount = 0;
		u32 m_numVertexArgs = 0;
	};

	/// <summary>
	/// INDEXED_MESH provides accessor methods for a very specific data layout:
	/// 
	/// 4 Bytes <<< unsigned integer containing the number of vertices in the mesh
	/// vertex_size * number of vertices Bytes <<< vertex data
	/// 4 Bytes <<< unsigned integer containing the number of indices
	/// 4 * number of indices <<< array of unsigned integers for indices
	/// </summary>
	class JGSW_API INDEXED_MESH {
	public:
		INDEXED_MESH() = delete;

		mesh_header m_header;

		inline void Free() { _aligned_free(this); }

		/// <summary>
		/// Creates a properly-aligned mesh object with the committed counts and size. The data are still uninitialized.
		/// </summary>
		/// <param name="vertex_count"></param>
		/// <param name="index_count"></param>
		/// <param name="vertex_size"></param>
		/// <returns></returns>
		static INDEXED_MESH* Create(size_t vertex_count, size_t index_count, const JGSW_VERT_TUPLE* vertex_args, size_t num_vertex_args);

		/// <returns>The vertex arguments</returns>
		const JGSW_VERT_TUPLE* GetVertexArgFormats() const;

		/// <returns>The vertex data in unsigned char form</returns>
		const u8* GetVertexData() const;

		/// <returns>The number of vertices associated with the mesh</returns>
		inline unsigned int GetVertexCount() const { return m_header.m_vertexCount; }

		/// <returns>The number of indices associated with the data array</returns>
		inline u32 GetIndexCount() const { return m_header.m_indexCount; }

		/// <returns>The size in bytes of each Vertex</returns>
		u32 CalculateVertexSize() const;

		/// <returns>The array of indices</returns>
		const u32* GetIndexArray() const;
	};
}
#endif
