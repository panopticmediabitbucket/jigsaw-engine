#include "xd3d12.h"
#include "Util/DTO.h"

using namespace Jigsaw;

JGSW_API D3D12_RESOURCE_BARRIER DX_ResourceTransition(ID3D12Resource* resource, D3D12_RESOURCE_STATES before, D3D12_RESOURCE_STATES after) {
	D3D12_RESOURCE_BARRIER barrier;
	barrier.Transition.pResource = resource;
	barrier.Transition.StateAfter = after;
	barrier.Transition.StateBefore = before;
	barrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	barrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	barrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	return barrier;
}

Jigsaw::AutoRegister register_topology(&D3D12_CONVERSIONS::__PopulateD3D12Conversions);

void D3D12_CONVERSIONS::__PopulateD3D12Conversions() {
	D3D12_CONVERSIONS& conversions = Instance();

	// PRIMITIVE TOPOLOGY
	for (int i = 0; i < (int)JGSW_TOPOLOGY_TYPE::__NUM_JGSW_TOPOLOGY_TYPE; i++) {
		conversions.primitive_topology[i] = D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
	}
	conversions.primitive_topology[(int)JGSW_TOPOLOGY_TYPE::JGSW_TOPOLOGY_TYPE_LINE] = D3D_PRIMITIVE_TOPOLOGY_LINELIST;
	conversions.primitive_topology[(int)JGSW_TOPOLOGY_TYPE::JGSW_TOPOLOGY_TYPE_TRIANGLE] = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	// FORMAT
	for (int i = 0; i < (int)JGSW_FORMAT::__NUM_JGSW_FORMATS; i++) {
		conversions.formats[i] = DXGI_FORMAT_UNKNOWN;
	}

	conversions.formats[(int)JGSW_FORMAT::R8G8B8A8_UNORM] = DXGI_FORMAT_R8G8B8A8_UNORM;
	conversions.formats[(int)JGSW_FORMAT::R16_UINT] = DXGI_FORMAT_R16_UINT;
	conversions.formats[(int)JGSW_FORMAT::R32_UINT] = DXGI_FORMAT_R32_UINT;
	conversions.formats[(int)JGSW_FORMAT::R32_SINT] = DXGI_FORMAT_R32_SINT;
	conversions.formats[(int)JGSW_FORMAT::R32_FLOAT] = DXGI_FORMAT_R32_FLOAT;
	conversions.formats[(int)JGSW_FORMAT::R16G16_UINT] = DXGI_FORMAT_R16G16_UINT;
	conversions.formats[(int)JGSW_FORMAT::R32G32_FLOAT] = DXGI_FORMAT_R32G32_FLOAT;
	conversions.formats[(int)JGSW_FORMAT::R32G32B32_FLOAT] = DXGI_FORMAT_R32G32B32_FLOAT;
	conversions.formats[(int)JGSW_FORMAT::R16G16B16A16_UINT] = DXGI_FORMAT_R16G16B16A16_UINT;
	conversions.formats[(int)JGSW_FORMAT::R32G32B32A32_FLOAT] = DXGI_FORMAT_R32G32B32A32_FLOAT;

	conversions.formats[(int)JGSW_FORMAT::D24_UNORM_S8_UINT] = DXGI_FORMAT_D24_UNORM_S8_UINT;
	conversions.formats[(int)JGSW_FORMAT::BC7_SRGB] = DXGI_FORMAT_BC7_UNORM_SRGB;

	// SHADER VISIBILITY ~for dx12, this is a 1-1 mapping~
	for (int i = 0; i < (int)JGSW_SHADER::__NUM_VISIBILITIES; i++) 
	{
		conversions.shader_visibilities[i] = (D3D12_SHADER_VISIBILITY)i;
	}

	for (u32 i = 1; i < (int)JGSW_FORMAT::__NUM_JGSW_FORMATS; i++)
	{
		J_D_ASSERT_LOG_ERROR(conversions.formats[i], D3D12_CONVERSIONS, "Unspecified format conversion: {0}", i);
	}
}

