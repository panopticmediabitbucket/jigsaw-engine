#ifndef _APPLICATION_ROOT_PROPERTIES_H_
#define _APPLICATION_ROOT_PROPERTIES_H_

#include "System/UID.h"
#include "ApplicationRoot.h"
#include "Systems/SystemArguments.h"
#include <vector>
#include "_jgsw_api.h"

struct JGSW_API ApplicationRootProperties {
public:

	Jigsaw::UID root_scene_id;

	std::string db_connection;

	std::string lib_path = "./Lib/metadata/";

	std::vector<std::string> extensions;

	Jigsaw::SystemManagerArguments system_arguments;

	static const ApplicationRootProperties& Get();

};
#endif