#include "Windows/window.h"
#include "Windows/ViewportWindow.h"
#include "ApplicationRoot.h"
#include "ApplicationRootProperties.h"
#include "Assembly/AssemblyImportUtil.h"
#include "Marshalling/MarshallingUtil.h"
#include "Ref.h"
#include "Graphics/GraphicsContext.h"
#include "Orchestration/orOrchestrator.h"
#include <map>

// Global variables
std::map<HWND, Jigsaw::Ref<Window>> window_map;
HINSTANCE main_programInstance;

HWND main_hWnd;
WNDCLASS main_wndClass;

/**
 * Main loop for translating and dispatching messages
 */
void MainLoop() {
	MSG message;
	PeekMessage(&message, NULL, 0, 0, PM_NOREMOVE);
	while(message.message != WM_QUIT) {
		if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE)) {
			TranslateMessage(&message);
			DispatchMessage(&message);
		} 
	}
}

LRESULT TerminateWindow(HWND hWnd, HRESULT error) {
	DestroyWindow(hWnd);
	UnregisterClass(main_wndClass.lpszClassName, main_programInstance);
	return error;
}

LRESULT MessageHandler(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	if (window_map.find(hWnd) == window_map.end()) {
		std::cout << "Unhandled window message: " << uMsg << std::endl;
		return DefWindowProc(hWnd, uMsg, wParam, lParam);
	} else {
		if ( WM_SIZE == uMsg) {
			std::cout << "resize event";
		}
		return window_map.at(hWnd)->HandleMessage(hWnd, uMsg, wParam, lParam);
	}
}

JGSW_API HRESULT Run(int argc, const char* argv[]) {
	main_programInstance = (HINSTANCE)GetModuleHandle(NULL);

	Jigsaw::FILE_DATA f_data = Jigsaw::FileUtil::GetFileRead("application_root_properties.json");
	const_cast<ApplicationRootProperties&>(ApplicationRootProperties::Get()) = *Jigsaw::Marshalling::MarshallingUtil::Unmarshal<ApplicationRootProperties>(f_data).get();
	std::fclose(f_data.file);

	Jigsaw::Assembly::ImportRootPropertiesAssemblies();

	// Initialize values for window creation
	int xPos = 0;
	int yPos = 0;
	int width = 1280;
	int height = 720;

	// application initialization
	Jigsaw::jsContextConfig config;
	config.m_packageNames.push_back({ "GLOBAL", std::vector<const Jigsaw::etype_info*>() });
	config.m_packageNames.push_back({ "Jigsaw_Engine", std::vector<const Jigsaw::etype_info*>() });
	Jigsaw::Ref<Jigsaw::jsContext> context = Jigsaw::jsContext::Create(config);

	Jigsaw::orOrchestrator* orchestrator = context->GetKnob<Jigsaw::orOrchestrator>("main_app_orchestrator");
	Jigsaw::GraphicsContext* render_context = context->GetKnob<Jigsaw::GraphicsContext>();
	Jigsaw::Ref<ViewportWindow> viewport_window = Jigsaw::MakeRef<ViewportWindow>(static_cast<double>(width) / static_cast<double>(height), render_context, orchestrator);

	// Registering the window class with the operating system
	viewport_window->PopulateWindowClass(&main_wndClass, main_programInstance);
	main_wndClass.lpfnWndProc = (WNDPROC)MessageHandler;
	ATOM class_registry = RegisterClass(&main_wndClass);
	if (!class_registry) {
		return HRESULT_FROM_WIN32(GetLastError());
	}

	HWND parent = NULL;
	HMENU hMenu = NULL;

	main_hWnd = CreateWindow(main_wndClass.lpszClassName, "Main_Window", WS_DLGFRAME | WS_SYSMENU | WS_OVERLAPPEDWINDOW,
		xPos, yPos, width, height, parent, hMenu, main_programInstance, NULL);

	// if failed to create window, thrown an error
	if (main_hWnd == 0) {
		return HRESULT_FROM_WIN32(GetLastError());
	}

	// insert viewport into application window_map and finalize/show the window 
	Jigsaw::Ref<Window> window = std::dynamic_pointer_cast<Window>(viewport_window);
	window_map.insert(std::make_pair(main_hWnd, window));
	ShowWindow(main_hWnd, SW_SHOWNORMAL);
	std::cout << "Window created with handle: " << main_hWnd << std::endl;

	MainLoop();

	return 0;
}
