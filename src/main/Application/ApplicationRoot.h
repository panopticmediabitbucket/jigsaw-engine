#include <Windows.h>
#include "_jgsw_api.h"

/// <summary>
/// The main call used by any 'main method' implementation. Essential OS resources are initialized, the ApplicationRootProperties are loaded,
/// the JigsawContext is resolved, dynamic libraries are loaded, and the event loop for the operating system is entered. 
/// </summary>
/// <returns></returns>
JGSW_API HRESULT Run();

