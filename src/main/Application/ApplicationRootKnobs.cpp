/*********************************************************************
 * ApplicationRootKnobs.ipp
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/

// std
#include <functional>

// Jigsaw_Engine
#include "Assets/dlAssetRegistrar.h"
#include "Assets/dlLoadResources.h"
#include "Application/ApplicationRootProperties.h"
#include "Injection/JigsawInjection.h"
#include "Input/ctInputEventWriter.h"
#include "Entities/jsEntityService.h"
#include "Graphics/GraphicsContext.h"
#include "Render/RenderService.h"
#include "Graphics/__graphics_api_context.h"
#include "Orchestration/orSystemsOrchestrator.h"
#include "Orchestration/orSceneHierarchy.h"
#include "Orchestration/orMainOrchestrator.h"

namespace Jigsaw 
{
	/// <summary>
	/// Provides the root knobs for the Jigsaw Engine runtime. These are in the GLOBAL namespace.
	/// </summary>
	JGSW_KNOB_PROVIDER(ApplicationRootKnobProvider) 
	{
	public:
		//////////////////////////////////////////////////////////
	
		// JGSW ROOT SYSTEM KNOBS

		//////////////////////////////////////////////////////////

		// The shared jsEntityService provides the universal entry point for interacting with jsEntities
		KNOB(jsEntityService, cluster_service) 
		{
			return new jsEntityService;
		};

		// The application clock.
		KNOB(Jigsaw::Time::ApplicationClockService, clock_service, IS_AN(Jigsaw::Time::ApplicationClockReader)) 
		{
			return new Time::ApplicationClockService;
		}

		// Provides a view into all pending input events.
		KNOB(Jigsaw::ctInputEventWriter, input_service,
			IS_AN(Jigsaw::ctInputEvents), QUALIFIER("GameInput")) 
		{
			return new ctInputEventWriter;
		}

		//////////////////////////////////////////////////////////

		// DATA LAYER KNOBS

		//////////////////////////////////////////////////////////

		KNOB(dlAssetRegistrar, asset_manager) {
			return new dlAssetRegistrar;
		}

		KNOB(dlLoadResources, loadResources,
			SLOT_ARG(GraphicsContext, context)) {
			return new dlLoadResources(context, 4);
		}

		//////////////////////////////////////////////////////////

		// ORCHESTRATION LAYER KNOBS

		//////////////////////////////////////////////////////////


		// The primary Orchestration element. Handles the game loop and message processing.
		KNOB(orMainOrchestrator, mainOrchestrator, QUALIFIER("MainOrchestrator"),
			IS_AN(orOrchestrator)) 
		{
			return new orMainOrchestrator;
		}

		// The orMessageQueue for the application. This is how the main thread is informed of asynchronous events, such as input.
		KNOB(orMessageQueue, messageQueue, QUALIFIER("MainMessageQueue")) 
		{
			return new orMessageQueue;
		}

		// Handles all the dispatching for Jigsaw Systems.
		KNOB(orSystemsOrchestrator, systemsOrchestrator) 
		{
			return new orSystemsOrchestrator;
		}

	};


	/// <summary>
	/// These are bundled in the Jigsaw_Engine namespace. These knobs are not used in Dev, for unit testing, or in the Editor. 
	/// They are engine-level implementations of these services. 
	/// </summary>
	JGSW_KNOB_PROVIDER(JigsawEngineRuntimeProvider) 
	{
	public:
		//////////////////////////////////////////////////////////

		// GRAPHICS

		//////////////////////////////////////////////////////////

#ifndef _RUN_UNIT_TESTS_
		KNOB(GraphicsContext, gfx) 
		{
#if J_WIN_10
			return DX_Context::Init();
#endif // J_WIN_10
		};
#endif // _RUN_UNIT_TESTS_

		// The primary Viewport, fetched from the one allocated in the render context
		KNOB(const J_Viewport, j_viewport,
			SLOT_ARG(GraphicsContext, gfx), QUALIFIER("GameViewport"))
		{
			return gfx->GetViewport();
		}

		//////////////////////////////////////////////////////////

		// RENDERING PIPELINE KNOBS

		//////////////////////////////////////////////////////////

		KNOB(RenderService, render_service, QUALIFIER("game_render_service"))
		{
			return new RenderService();
		}

		//////////////////////////////////////////////////////////

		// JIGSAW ENGINE SYSTEMS

		//////////////////////////////////////////////////////////

		// Handles the lifetime of scenes in the world. Also handles requests for loading/unloading scenes. 
		KNOB(orSceneHierarchy, scene_hierarchy)
		{
			return new orSceneHierarchy;
		}

		// The underlying registry used by the orSystemsOrchestrator to track all of the active systems and their properties. 
		KNOB(jsRuntimeSystemRegistry, system_registry)
		{
			return new jsRuntimeSystemRegistry(ApplicationRootProperties::Get().system_arguments, Assembly::SystemRegistry::GetBeans());
		}
	};

	START_JGSW_KNOB_PROVIDER_PROPERTIES(ApplicationRootKnobProvider);
	END_JGSW_KNOB_PROVIDER_PROPERTIES(ApplicationRootKnobProvider);
	REGISTER_JGSW_KNOB_PROVIDER(ApplicationRootKnobProvider);

	START_JGSW_KNOB_PROVIDER_PROPERTIES(JigsawEngineRuntimeProvider);
	PROVIDER_NAMESPACE("JGSW_RUNTIME")
		END_JGSW_KNOB_PROVIDER_PROPERTIES(JigsawEngineRuntimeProvider);
	REGISTER_JGSW_KNOB_PROVIDER(JigsawEngineRuntimeProvider);
}