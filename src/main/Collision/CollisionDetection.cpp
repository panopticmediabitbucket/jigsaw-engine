#include "CollisionDetection.h"
#include "Math/Alg.h"

bool Jigsaw::Collision::RayCollidesSphere(const Ray& ray, const Sphere& sphere) {
	float a = ray.direction.Dot(ray.direction);
	Vector3 toward_vec = ray.origin - sphere.c;
	float b = 2 * (toward_vec.Dot(ray.direction));
	float c = toward_vec.Dot(toward_vec) - (sphere.r * sphere.r);

	float ret_1, ret_2;

	bool has_solution = Jigsaw::Math::QuadraticFormula(a, b, c, &ret_1, &ret_2);
	return has_solution && (ret_1 >= 0 || ret_2 >= 0);
}

bool Jigsaw::Collision::RayCollidesCapsule(const Ray& ray, const Capsule& capsule) {
	Vector3 d = ray.direction;
	Vector3 s = ray.origin;

	Vector3 s_proj_up = (s.Dot(capsule.up) * s);
	Vector3 d_proj_t_bt = d - (d.Dot(capsule.up) * d);
	Vector3 s_proj_t_bt = s - s_proj_up;
	Vector3 c_proj_up = (capsule.c.Dot(capsule.up) * capsule.up);
	Vector3 c_proj_t_bt = capsule.c - c_proj_up;

	Ray coplanar_ray{ s_proj_t_bt, d_proj_t_bt };
	Circle coplanar_circle{ c_proj_t_bt, capsule.up, capsule.r };
	float t_1, t_2;

	if (RayCollidesCircle2D(coplanar_ray, coplanar_circle, &t_1, &t_2)) {
		Vector3 d_t = t_1 * ray.direction;
		Vector3 intersect_point = d_t + s_proj_up;
		Vector3 ip_sub_c_proj_up = intersect_point + c_proj_up;
		Vector3 ip_sub_c_proj_up_proj_up = ip_sub_c_proj_up.Dot(capsule.up) * capsule.up;
		float ip_sub_c_proj_up_proj_up_l_sqrd = ip_sub_c_proj_up_proj_up.Dot(ip_sub_c_proj_up_proj_up);
		if (ip_sub_c_proj_up_proj_up_l_sqrd < capsule.h * capsule.h) {
			return true;
		}
	}

	return RayCollidesSphere(ray, { capsule.c + capsule.h * capsule.up, capsule.r }) || RayCollidesSphere(ray, { capsule.c - capsule.h * capsule.up, capsule.r });
}

bool Jigsaw::Collision::RayCollidesCircle2D(const Ray& ray, const Circle& circle, float* out_t_1, float* out_t_2) {
	Vector3 cs = circle.c - ray.origin;
	float toward = cs.Dot(ray.direction);

	if (toward > 0) {
		Vector3 toward_v = toward * ray.direction;

		Vector3 internal_vec = toward_v - cs;
		float int_vec_l_sqrd = internal_vec.Dot(internal_vec);
		float r_sqrd = circle.r * circle.r;

		if (int_vec_l_sqrd <= r_sqrd) {
			float b_sqrd = r_sqrd - int_vec_l_sqrd;

			// need to handle single collisions
			float b = sqrtf(b_sqrd);
			*out_t_1 = toward - b;
			*out_t_2 = toward + b;
			return true;
		}
	}

	return false;
}

bool Jigsaw::Collision::RayCollidesTriangle(const Ray& ray, const Triangle& triangle) {
	Vector3 ba = triangle.b - triangle.a;
	Vector3 ca = triangle.c - triangle.a;
	Vector3 sv = ray.origin - triangle.a;

	Vector3 d_cross_ca = ray.direction.Cross(ca);

	float mat_det = d_cross_ca.Dot(ba);
	if (mat_det != 0) {
		Vector3 res_cross_ba = sv.Cross(ba);

		float one_over_mat_det = 1 / mat_det;
		float t = res_cross_ba.Dot(ca) * one_over_mat_det;
		if (t >= 0) {
			float u = d_cross_ca.Dot(sv) * one_over_mat_det;
			if (u >= 0 && u <= 1) {
				float v = res_cross_ba.Dot(ray.direction) * one_over_mat_det;
				return v >= 0 && v <= 1 && (u + v) <= 1;
			}
		}
	}
	return false;
}

