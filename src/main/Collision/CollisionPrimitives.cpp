#include "CollisionPrimitives.h"

namespace Jigsaw {
	namespace Collision {
		JGSW_API Capsule Jigsaw::Collision::CreateCapsule(const Quaternion& rotation, const Vector3& c, float h, float r) {
			Capsule ret;
			ret.up = rotation.Rotate(Vector3(0, 0, 1));
			ret.bt = rotation.Rotate(Vector3(0, 1, 0));
			ret.t = ret.bt.Cross(ret.up);

			ret.c = c;
			ret.h = h;
			ret.r = r;
			return ret;
		}
	}
}
