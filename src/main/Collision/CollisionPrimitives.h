#ifndef _COLLISION_PRIMITIVES_H_
#define _COLLISION_PRIMITIVES_H_

#include "Math/LinAlg.h"
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Collision {

		struct Sphere {
			Vector3 c;
			float r;

		};

		struct Capsule {
			Vector3 up, t, bt;
			Vector3 c;
			float h;
			float r;

		};

		struct Ray {
			Vector3 origin;
			Vector3 direction;

		};

		struct Circle {
			Vector3 c;
			Vector3 n;
			float r;
		};

		struct Triangle {
			Vector3 a, b, c;
		};

		JGSW_API Capsule CreateCapsule(const Quaternion& rotation, const Vector3& c, float h, float r);

	}
}

#endif