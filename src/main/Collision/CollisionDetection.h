#ifndef _COLLISION_DETECTION_H_
#define _COLLISION_DETECTION_H_

#include "CollisionPrimitives.h"

namespace Jigsaw {
	namespace Collision {

		bool RayCollidesSphere(const Ray& ray, const Sphere& sphere);

		bool RayCollidesCapsule(const Ray& ray, const Capsule& capsule);

		bool RayCollidesCircle2D(const Ray& ray, const Circle& circle, float* out_t_1, float* out_t_2);

		bool RayCollidesTriangle(const Ray& ray, const Triangle& triangle);
	}
}
#endif
