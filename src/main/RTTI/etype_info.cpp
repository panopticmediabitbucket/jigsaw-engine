#include "etype_info.h"
#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw
{
	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::BuildArray(unsigned int n, u8** out_array) const
	{
		if (m_array_builder)
		{
			*out_array = m_array_builder(n);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::BuildPointerArray(unsigned int n, u8** out_ptr_array) const
	{
		if (m_pointer_array_builder)
		{
			*out_ptr_array = static_cast<u8*>(m_pointer_array_builder(n));
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::BuildVector(unsigned int n, void** out_vector) const
	{
		if (m_vector_builder)
		{
			*out_vector = m_vector_builder(n);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::BuildPointerVector(unsigned int n, void** out_vector) const
	{
		if (m_pointer_vector_builder)
		{
			*out_vector = m_pointer_vector_builder(n);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::GetVectorRawData(void* raw_vector, bool is_pointer, void** out_data) const
	{
		if (m_vector_data_getter)
		{
			*out_data = m_vector_data_getter(raw_vector, is_pointer);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::BuildUnique(void* arg, void** out_ptr) const
	{
		if (m_unique_pointer_builder)
		{
			*out_ptr = m_unique_pointer_builder(arg);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::GetElementCount(void* raw_vector, bool is_pointer, size_t* out_number) const
	{
		if (m_vector_count_function)
		{
			*out_number = m_vector_count_function(raw_vector, is_pointer);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::MovePointerIntoPointerArray(void* array, void* element, unsigned int i) const
	{
		if (m_move_pointer_into_pointer_array_function)
		{
			m_move_pointer_into_pointer_array_function(array, element, i);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::Move(u8* dst, u8* src) const
	{
		if (m_mover_function)
		{
			m_mover_function(dst, src);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::Copy(u8* dst, const u8* src) const
	{
		if (m_copier)
		{
			m_copier(dst, src);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::ConstructInPlace(void* dst) const
	{
		if (m_construct_in_place_function)
		{
			m_construct_in_place_function(dst);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::DestructInPlace(void* dst) const
	{
		if (m_destruct_in_place_function)
		{
			m_destruct_in_place_function(dst);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::New(void** dst) const
	{
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool etype_unsafe_functions::Delete(void* dst) const
	{
		if (m_delete_function)
		{
			m_delete_function(dst);
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	etype_info::etype_info(const type_info& t_i, const char* name, const size_t size, const size_t align, const etype_properties& properties, etype_unsafe_functions&& unsafe_functions, const std::function<const Jigsaw::etype_info* (void*)>&& instanceTypeFunction)
		: base_inf(t_i), name(name), size(size), align(align), properties(properties), unsafe_functions(std::move(unsafe_functions)), instanceTypeFunction(instanceTypeFunction) { }

	//////////////////////////////////////////////////////////////////////////

	std::mutex& etype_info::GetLock()
	{
		static std::mutex mut;
		return mut;
	}

	std::unordered_map<sysHashString, const etype_info*>& etype_info::GetQualifiedNameMap()
	{
		static std::unordered_map<sysHashString, const etype_info*> map;
		return map;
	}

	std::unordered_map<std::type_index, const etype_info*>& etype_info::GetMap()
	{
		static std::unordered_map<std::type_index, const etype_info*> map;
		return map;
	};

	const etype_info* etype_info::GetByQualifiedName(const char* name)
	{
		// Removing concurrency for now
		//std::scoped_lock<std::mutex> scoped_lock(etype_info::GetLock());
		sysHashString hash = sysHashString::Create(name);
		auto iter = GetQualifiedNameMap().find(hash);
		if (iter != GetQualifiedNameMap().end())
		{
			return iter->second;
		}
		return nullptr;
	}

	const Jigsaw::etype_info* etype_info::GetInstanceType(void* instance) const
	{
		return instanceTypeFunction(instance);
	}

	const const char* etype_info::GetQualifiedName() const
	{
		return name;
	}

	/// <summary>
	/// It was decided to leave the etype_index in an unsafe state if a proper constructor is not called. 
	/// It will throw an exception earlier, which will make it easier to detect the problem
	/// </summary>
	/// <returns></returns>
	etype_index::etype_index() noexcept : _Tptr(nullptr) { }

	std::string* etype_index_ToString(etype_index* index)
	{
		return new std::string(index->Get().GetQualifiedName());
	}

	void etype_index_FromString(etype_index* index, std::string* string)
	{
		*index = etype_index(*etype_info::GetByQualifiedName(string->c_str()));
	}

	START_REGISTER_SERIALIZABLE_CLASS(etype_index)
		REGISTER_CUSTOM_FIELD_MAPPER(etype_index, std::string, type_name, etype_index_ToString, etype_index_FromString)
		END_REGISTER_SERIALIZABLE_CLASS(etype_index)
}

