/*********************************************************************
 * etype_info.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2020
 *********************************************************************/
#ifndef _ETYPE_INFO_H_
#define _ETYPE_INFO_H_

 // External
#include <typeinfo>
#include <typeindex>
#include <functional>
#include <mutex>
#include <unordered_map>

// Jigsaw_Engine
#include "etype_link.h"
#include "System/sysHashString.h"
#include "Util/BYTE.h"

namespace Jigsaw 
{
	class JGSW_API etype_unsafe_functions
	{
	public:
		/// <summary>
		/// Creates an etype_unsafe_fucntions object that implements each of the functions with respect to a specific type. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		template<typename T>
		static etype_unsafe_functions CreateFromType()
		{

			etype_unsafe_functions unsafe_type_functions;

			unsafe_type_functions.m_move_pointer_into_pointer_array_function = &_MovePointerIntoPointerArray<T>;

			unsafe_type_functions.m_unique_pointer_builder = &_BuildUnique<T>;

			if constexpr (std::is_destructible<T>::value)
			{
				unsafe_type_functions.m_destruct_in_place_function = &_DestructInPlace<T>;

				unsafe_type_functions.m_delete_function = &_Delete<T>;
			}

			if constexpr (std::is_default_constructible<T>::value)
			{
				unsafe_type_functions.m_new_function = &_New<T>;

				unsafe_type_functions.m_construct_in_place_function = &_ConstructInPlace<T>;

				unsafe_type_functions.m_array_builder = &_ArrayBuilder<T>;

				unsafe_type_functions.m_pointer_array_builder = &_PointerArrayBuilder<T>;

				unsafe_type_functions.m_pointer_vector_builder = &_PointerVectorBuilder<T>;

				if constexpr (!std::is_same<bool, T>::value)
				{
					unsafe_type_functions.m_vector_data_getter = &_VectorDataGetter<T>;
				}

				if constexpr (std::is_copy_assignable<T>::value)
				{
					unsafe_type_functions.m_copier = &_Copy<T>;
				}

				if constexpr (std::is_copy_constructible<T>::value)
				{

					unsafe_type_functions.m_vector_builder = &_VectorBuilder<T>;

					unsafe_type_functions.m_vector_count_function = &_VectorCount<T>;
				} // END copy constructible
			} // END move assignable

			if constexpr (std::is_move_assignable<T>::value)
			{
				unsafe_type_functions.m_mover_function = &_Move<T>;
			}

			return unsafe_type_functions;
		}

		/// <summary>
		/// Builds an array from the enclosed type.
		/// </summary>
		/// <param name="n">The number of elements in the array</param>
		/// <param name="out_array"></param>
		/// <returns>False if the enclosed type is not default constructible. True if a pointer to an array n items long was stored in out_array</returns>
		bool BuildArray(unsigned int n, u8** out_array) const;

		/// <summary>
		/// Builds an array of Unique pointers from the enclosed type.
		/// </summary>
		/// <param name="n">The number of elements in the array</param>
		/// <param name="out_array"></param>
		/// <returns>False if the enclosed type is not default constructible. True if a pointer to an array n items long was stored in out_array</returns>
		bool BuildPointerArray(unsigned int n, u8** out_ptr_array) const;

		/// <summary>
		/// Builds an std::vector out of an existing array, emplacing the content of that array in a new vector object. 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="n"></param>
		/// <param name="out_vector"></param>
		/// <returns></returns>
		bool BuildVector(unsigned int n, void** out_vector) const;

		/// <summary>
		/// Builds an std::vector out of an existing array of pointers, emplacing the content of those unique pointers in a new vector object. 
		/// </summary>
		/// <param name="array"></param>
		/// <param name="n"></param>
		/// <param name="out_vector"></param>
		/// <returns></returns>
		bool BuildPointerVector(unsigned int n, void** out_vector) const;

		/// <summary>
		/// Returns a pointer to the raw data of an std::vector object matching this etype
		/// </summary>
		/// <param name="raw_vector"></param>
		/// <param name="is_pointer"></param>
		/// <param name="out_data"></param>
		/// <returns></returns>
		bool GetVectorRawData(void* raw_vector, bool is_pointer, void** out_data) const;

		/// <summary>
		/// Builds an array from the enclosed type.
		/// </summary>
		/// <param name="n">The number of elements in the array</param>
		/// <param name="out_array"></param>
		/// <returns>False if the enclosed type is not default constructible. True if a pointer to an array n items long was stored in out_array</returns>
		bool BuildUnique(void* arg, void** out_ptr) const;

		/// <summary>
		/// Counts the elements of a vector of elements of the specified etype or a vector of pointers to that etype
		/// </summary>
		/// <param name="raw_vector"></param>
		/// <param name="is_pointer"></param>
		/// <param name="out_number"></param>
		/// <returns>False if the etype is not copy constructible and therefore cannot be an std::vector type</returns>
		bool GetElementCount(void* raw_vector, bool is_pointer, size_t* out_number) const;

		/// <summary>
		/// Moves a Jigsaw::Unique<T> relevant to this etype_info into an array of these pointers at index i
		/// This is an unsafe operation and should only be called in cases for marshalling/unmarshalling
		/// </summary>
		/// <param name="array"></param>
		/// <param name="element"></param>
		/// <param name="i"></param>
		/// <returns></returns>
		bool MovePointerIntoPointerArray(void* array, void* element, unsigned int i) const;

		/// <summary>
		/// If the given type is movable, it is moved from src to dst. It is assumed that the passed in dst and src are pointers to valid instantiations of this type.
		/// </summary>
		/// <param name="dst"></param>
		/// <param name="src"></param>
		/// <returns>True if move was successful, false if the type is not move assignable</returns>
		bool Move(u8* dst, u8* src) const;

		/// <summary>
		/// Copies the value at 'src' for the given type into the allocated destination 'dst'
		/// </summary>
		/// <param name="dst"></param>
		/// <param name="src"></param>
		/// <returns>False if the type is not copy assignable</returns>
		bool Copy(u8* dst, const u8* src) const;

		/// <summary>
		/// If the given type is move assignable and default constructible, then a default construction will happen at the destination
		/// </summary>
		/// <param name="dst"></param>
		/// <returns>Returns false if the given type is not default constructible or move assignable</returns>
		bool ConstructInPlace(void* dst) const;

		/// <summary>
		/// Calls the destructor for the given type on the passed in destination. THIS DOES NOT DEALLOCATE MEMORY
		/// </summary>
		/// <param name="dst"></param>
		/// <returns>Returns false if the given type is not destructible</returns>
		bool DestructInPlace(void* dst) const;

		/// <summary>
		/// Allocates memory and calls the default constructor for the type, storing a pointer to the result in the destination.
		/// </summary>
		/// <param name="dst"></param>
		/// <returns></returns>
		bool New(void** dst) const;

		/// <summary>
		/// Calls the destructor for the given type on the passed in destination. Deallocates memory in the process
		/// </summary>
		/// <param name="dst"></param>
		/// <returns>Returns false if the given type is not destructible</returns>
		bool Delete(void* dst) const;

	private:
		etype_unsafe_functions() {}

		void (*m_new_function)(void**) = nullptr;
		void (*m_delete_function)(void*) = nullptr;
		void (*m_destruct_in_place_function)(void*) = nullptr;
		u8* (*m_array_builder)(unsigned int n) = nullptr;
		void* (*m_vector_builder)(unsigned int) = nullptr;
		void* (*m_pointer_vector_builder)(unsigned int) = nullptr;
		void (*m_mover_function)(void*, void*) = nullptr;
		void (*m_copier)(void*, const void*) = nullptr;
		void* (*m_unique_pointer_builder)(void*) = nullptr;
		void* (*m_pointer_array_builder)(unsigned int) = nullptr;
		void (*m_move_pointer_into_pointer_array_function)(void*, void*, unsigned int) = nullptr;
		size_t(*m_vector_count_function)(void*, bool) = nullptr;
		void* (*m_vector_data_getter)(void*, bool) = nullptr;
		void (*m_construct_in_place_function) (void*) = nullptr;

		template<typename T>
		static void _New(void** dst)
		{
			*dst = static_cast<void*>(new T);
		}

		template<typename T>
		static void _Delete(void* v)
		{
			delete static_cast<T*>(v);
		}

		template<typename T>
		static void _DestructInPlace(void* v)
		{
			T& t = *static_cast<T*>(v);
			t.~T();
		}

		template<typename T>
		static void _ConstructInPlace(void* v)
		{
			new(v) T;
		}

		template<typename T>
		static u8* _ArrayBuilder(unsigned int n)
		{
			return static_cast<u8*>(static_cast<void*>(new T[n]));
		}

		template<typename T>
		static void* _PointerArrayBuilder(unsigned int n)
		{
			return new T * [n];
		}

		template<typename T>
		static void* _VectorBuilder(unsigned int n)
		{
			std::vector<T>* vector = new std::vector<T>(n);
			return vector;
		}

		template<typename T>
		static void* _PointerVectorBuilder(unsigned int n)
		{
			std::vector<T*>* vector = new std::vector<T*>(n);
			return vector;
		}

		template<typename T>
		static void _Move(void* dst, void* src)
		{
			*static_cast<T*>(dst) = std::move(*static_cast<T*>(src));
		}

		template<typename T>
		static void _Copy(void* dst, const void* src)
		{
			T& _dst = *static_cast<T*>(dst);
			const T& _src = *static_cast<const T*>(src);
			_dst = _src;
		}

		template<typename T>
		static void* _BuildUnique(void* data)
		{
			T* t = static_cast<T*>(data);
			return static_cast<void*>(new Jigsaw::Unique<T>(static_cast<T*>(t)));
		};

		template<typename T>
		static void _MovePointerIntoPointerArray(void* dst_array, void* element, unsigned int n)
		{
			T** pointer_array = static_cast<T**>(dst_array);
			T* unique_element = static_cast<T*>(element);
			T** position = pointer_array + n;
			*position = unique_element;
		}

		template<typename T>
		static size_t _VectorCount(void* raw_vector, bool pointer_vector)
		{
			if (pointer_vector)
			{
				std::vector<T*>* vector = static_cast<std::vector<T*>*>(raw_vector);
				return vector->size();
			}
			else
			{
				std::vector<T>* vector = static_cast<std::vector<T>*>(raw_vector);
				return vector->size();
			}
		}

		template <typename T>
		static void* _VectorDataGetter(void* raw_vector, bool pointer_vector)
		{
			if (pointer_vector)
			{
				std::vector<T*>& vector = *static_cast<std::vector<T*>*>(raw_vector);
				T** data = vector.data();
				void* ret_val = static_cast<void*>(data);
				return ret_val;
			}
			else
			{
				std::vector<T>& vector = *static_cast<std::vector<T>*>(raw_vector);
				T* data = vector.data();
				void* ret_val = static_cast<void*>(data);
				return ret_val;
			}
		}
	};

	struct JGSW_API etype_properties
	{
		bool is_enum = false;
		sysHashString hashName;
	};


	/// <summary>
	/// An extension of C++'s "type_info." etype_info encapsulates additional data unique to one runtime type. This can include the type's
	/// flat size in memory. Default construction and move assignment of the given type are included if those functions are available for 
	/// the given type. 
	/// </summary>
	class JGSW_API etype_info
	{
	private:
		etype_info(const type_info& t_i, const char* name, const size_t size, const size_t align, const etype_properties& properties, etype_unsafe_functions&& unsafe_functions, const std::function<const Jigsaw::etype_info* (void*)>&& instanceTypeFunction);

		etype_info(const etype_info& other) = delete;

		static std::mutex& GetLock();

		static std::unordered_map<sysHashString, const etype_info*>& GetQualifiedNameMap();

		static std::unordered_map<std::type_index, const etype_info*>& GetMap();

		/// <summary>
		/// Returns a function that will examine an instance and return the inherited or non-inherited type of the instance.
		/// It is assumed that the passed in 'instance' argument IS an instance of the given type argument 'T' associated with that
		/// base etype_info object. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		template<typename T>
		static constexpr std::function<const Jigsaw::etype_info* (void*)> GetInstanceTypeFunction()
		{
			return [](void* instance) -> const Jigsaw::etype_info*
			{
				const Jigsaw::etype_info* et_i = nullptr;
				T* t_instance = static_cast<T*>(instance);
				if (t_instance)
				{
					std::type_index t_index = typeid(*t_instance);
					auto iter = GetMap().find(t_index);
					et_i = iter == GetMap().end() ? et_i : (*iter).second;
				}
				return et_i;
			};
		}

		/// <summary>
		/// This internal method assigns a series of type-dependent functions to the 'unsafe_functions' facet of the etype_info object
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="unsafe_type_functions"></param>

		etype_properties properties;
		const char* name;
		std::function<const Jigsaw::etype_info* (void*)> instanceTypeFunction;
		etype_unsafe_functions unsafe_functions;
		const type_info& base_inf;
		std::vector<const etype_info*> ancestors;
		std::vector<const etype_info*> decendents;

	public:

#pragma warning(disable:4307)
		/// <summary>
		/// The 'Id' function returns the etype_info of the type parameter. Unlike reflection/introspection in other languages,
		/// this is done at the moment that the given type is requested. That means that the 'etype_info' objects stored in the Map
		/// can and do change throughout the execution lifetime. Once a type is registered, though, it is registered forever, and it's immutable. 
		/// 
		/// Since retrieving types by fully-qualified name is restricted to types that have already been registered through the execution
		/// of this function, it is recommended that any type you are intending to retrieve by qualified name be registered statically by calling
		/// this method at the boot time of your program. 
		/// 
		/// Retrieving by fully-qualified name (GetByQualifiedName) during the static initialization of the program yields completely undefined behavior.
		///
		/// Default-constructible types are given an array_builder function. Move-assignable types are given a move_function. 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		template<typename T>
		static const etype_info& Id()
		{
			typedef std::remove_const<T>::type T_;
			// Removing concurrency for the time being. It shouldn't really be here. 
			//std::scoped_lock<std::mutex> lock(etype_info::GetLock());

			const type_info& inf = typeid(T_);
			std::type_index ind(inf);

			auto iter = GetMap().find(ind);
			if (iter == GetMap().end())
			{
				etype_unsafe_functions unsafe_type_functions = etype_unsafe_functions::CreateFromType<T_>();
				constexpr sysHashString hash = sysTypeHash<T_>::Get();

				const char* full_name = inf.name();
				full_name = strchr(full_name, ' ') + 1;

				etype_properties properties;
				properties.is_enum = std::is_enum_v<T>;
				properties.hashName = hash;

				auto func = GetInstanceTypeFunction<T_>();
				etype_info* et_i = new etype_info(inf, full_name, sizeof(T_), alignof(T_), properties, std::move(unsafe_type_functions), std::move(func));

				// If the parent type has a type link, we may want the ability to cast back and forth
				if constexpr (has_child_link<T>::value)
				{
					if (!std::is_same<T, typename T::__childlinker::BaseType>().value)
					{
						static_assert(std::is_base_of<typename T::__childlinker::BaseType, T>().value, "Illegal use of RTTI parent/child linking");
						etype_info* ancestor = &const_cast<etype_info&>(etype_info::Id<typename T::__childlinker::BaseType>());
						ancestor->decendents.push_back(et_i);
						et_i->ancestors.push_back(ancestor);
					}
				}

				GetMap().insert(std::make_pair(ind, et_i));
				GetQualifiedNameMap().insert(std::make_pair(hash, et_i));
			}

			return *GetMap().at(inf);
		};

		template <typename T, typename...rest>
		static inline void GetTypes(std::vector<const etype_info*>& vector)
		{
			vector.push_back(&etype_info::Id<T>());
			if constexpr (sizeof...(rest) > 0)
			{
				GetTypes<rest...>(vector);
			}
		}

		/// <summary>
		/// Variadic type retrieval
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		template <typename...Tparams>
		static inline std::vector <const etype_info*> GetTypes()
		{
			std::vector<const etype_info*> types;
			GetTypes<Tparams...>(types);
			return types;
		}

		/// <summary>
		/// This function is available after boot time. Any classes you are expecting to fetch by fully-qualified name need to have
		/// registrations that takes place at boot time, otherwise, the function will return null. 
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		static const etype_info* GetByQualifiedName(const char* name);

#pragma warning(default:4307)

		/// <summary>
		/// Checks if this type is downstream in the inheritance hierarchy from another type 'T' (or 'T' itself).
		/// 'T' must be publicly-decorated with the RTTI_LINK_CHILDREN(T) macro. 
		/// </summary>
		template <typename T>
		inline bool DescendsFrom() const
		{
			static const etype_info* candidate = &Id<T>();
			static_assert(has_child_link<T>::value, "The template parameter is invalid because it is not publicly-decorated with RTTI_LINK_CHILDREN. This is necessary for performing hierarchy checks.");
			bool found = this == candidate;
			for (u32 i = 0; i < ancestors.size() && !found; i++)
			{
				found |= ancestors[i] == candidate;
			}
			return found;
		}

		/// <summary>
		/// Attempts to create a new 'T' from this type info. This will only work if this type is or descends from the type argument
		/// and this type is trivially-constructible. Otherwise returns null.
		/// </summary>
		template <typename T>
		inline T* New() const
		{
			static const etype_info* candidate = &Id<T>();
			if (this == candidate || DescendsFrom<T>())
			{
				void* dst = nullptr;
				unsafe_functions.New(&dst);
				return static_cast<T*>(dst);
			}
			return nullptr;
		}

		/// <summary>
		/// Checks if the passed in object is an instance of this type.
		/// </summary>
		/// <typeparam name="T">The base type of the object.</typeparam>
		/// <param name="object"></param>
		template <typename T>
		inline bool IsType(T* object) const
		{
			static const etype_info* rootType = &Id<T>();
			static_assert(has_child_link<T>::value, "'IsType' function can only be called with a template parameter that has child link information.");
			return this->DescendsFrom<T>() && rootType->GetInstanceType(static_cast<void*>(object)) == this;
		}

		/// <summary>
		/// Returns the structured object for calling unsafe initialization, movement, and array building functions
		/// </summary>
		/// <returns></returns>
		inline const etype_unsafe_functions& GetUnsafeFunctions() const { return unsafe_functions; };

		/// <summary>
		/// Fetches the properties inherent to this type
		/// </summary>
		/// <returns></returns>
		inline const etype_properties& GetProperties() const { return properties; };

		/// <summary>
		/// Returns the etype_info associated with an object instance. If it is an inherited type, it will be returned
		/// </summary>
		/// <param name="instance"></param>
		/// <returns></returns>
		const Jigsaw::etype_info* GetInstanceType(void* instance) const;

		/// <summary>
		/// Return the qualified name of this etype_info
		/// </summary>
		/// <returns></returns>
		const char* GetQualifiedName() const;

		bool operator<(const etype_info& other) const noexcept
		{
			return base_inf.before(other.base_inf);
		}

		const type_info& GetBase() const noexcept
		{
			return base_inf;
		}

		const bool operator==(const etype_info& other) const
		{
			return base_inf == other.base_inf;
		}

		const bool operator!=(const etype_info& other) const
		{
			return base_inf != other.base_inf;
		}

		const size_t size;
		const size_t align;

	};



	// typeindex standard header

	// Copyright (c) Microsoft Corporation.
	// SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception
			/**
			* This implementation is an adaptation of Microsoft's type_index implementation to wrap etype_info which contains additional
			* details relevant to the type. I take no credit for the majority of the work done here; it is just a small extension.
			*/
#include <yvals_core.h>

#if _HAS_CXX20
#include <compare>
#endif // _HAS_CXX20

	class JGSW_API etype_index
	{ // wraps a typeinfo for indexing
	public:
		/// <summary>
		/// Default assignment yields an invalid type.
		/// </summary>
		/// <returns></returns>
		etype_index() noexcept;

		etype_index(const etype_info& _Tinfo) noexcept : _Tptr(&_Tinfo) {}

		etype_index(const etype_index& other) noexcept : _Tptr(other._Tptr) {}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		etype_index& operator=(const etype_index& other)
		{
			_Tptr = other._Tptr;
			return *this;
		}

		_NODISCARD size_t hash_code() const noexcept
		{
			return (_Tptr->GetBase()).hash_code();
		}

		_NODISCARD const char* name() const noexcept
		{
			return (_Tptr->GetBase()).name();
		}

		_NODISCARD bool operator==(const etype_index& _Right) const noexcept
		{
			return (_Tptr->GetBase()) == (_Right._Tptr->GetBase());
		}

		_NODISCARD bool operator!=(const etype_index& _Right) const noexcept
		{
			return !(*this == _Right);
		}

		_NODISCARD bool operator<(const etype_index& _Right) const noexcept
		{
			return *_Tptr < *_Right._Tptr;
		}

		_NODISCARD bool operator>=(const etype_index& _Right) const noexcept
		{
			return !(*this < _Right);
		}

		_NODISCARD bool operator>(const etype_index& _Right) const noexcept
		{
			return _Right < *this;
		}

		_NODISCARD bool operator<=(const etype_index& _Right) const noexcept
		{
			return !(_Right < *this);
		}

		inline operator const etype_info& ()
		{
			return Get();
		}

		const etype_info& Get() const
		{
			return *_Tptr;
		}

		operator const etype_info& () const
		{
			return Get();
		}

	private:
		const etype_info* _Tptr;
	};

	std::string* etype_index_ToString(etype_index* index);
	void etype_index_FromString(etype_index* index, std::string* string);

	_STL_RESTORE_CLANG_WARNINGS
}

// STRUCT TEMPLATE SPECIALIZATION hash
_STD_BEGIN
template <>
struct JGSW_API hash<Jigsaw::etype_index>
{
	_CXX17_DEPRECATE_ADAPTOR_TYPEDEFS typedef Jigsaw::etype_index _ARGUMENT_TYPE_NAME;
	_CXX17_DEPRECATE_ADAPTOR_TYPEDEFS typedef size_t _RESULT_TYPE_NAME;

	_NODISCARD size_t operator()(const Jigsaw::etype_index& _Keyval) const noexcept
	{
		return _Keyval.hash_code();
	}
};
_STD_END


#endif // !_ETYPE_INFO_H_
