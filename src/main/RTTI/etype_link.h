/*********************************************************************
 * etype_link.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2020
 *********************************************************************/
#ifndef _ETYPE_LINK_H_
#define _ETYPE_LINK_H_

namespace Jigsaw
{

	struct __base_childlinker { };

	template<typename T>
	struct has_child_link 
	{
	private:
		template <typename U> static u8 check(typename U::__childlinker*);
		template <typename U> static u16 check(...);
	public:
		static const bool value = sizeof(u8) == sizeof(check<T>(0));
	};

	class etype_info;

#define RTTI_LINK_CHILDREN(this_type) \
		struct __childlinker : public Jigsaw::__base_childlinker { typedef this_type BaseType; };

}
#endif // _ETYPE_LINK_H_