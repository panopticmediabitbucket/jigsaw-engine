/*********************************************************************
 * jsMachine.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: October 2020
 *********************************************************************/
#ifndef _JIGSAW_MACHINE_H_
#define _JIGSAW_MACHINE_H_
#include "jsPiece.h"
#include "Ref.h"
#include "System/sysSignature.h"
#include "Machines/FABRICATION_ARGS.h"
#include "_jgsw_api.h"

namespace Jigsaw {
	/// <summary>
	/// JigsawMachine is the core scene element in the JigsawEngine. Primarily, it maintains a list of JigsawMachinePieces that can be used
	/// to infuse JigsawEntities with component data. 
	/// </summary>
	class JGSW_API jsMachine {
	public:
		CANT_COPY(jsMachine);

		/// <summary>
		/// Constructor
		/// </summary>
		jsMachine();

		/// <summary>
		/// Move constructor.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		jsMachine(jsMachine&& other) noexcept;

		/// <summary>
		/// Move assignment operator
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		jsMachine& operator=(jsMachine&& other) noexcept;

		// Delete releases the machine pieces
		~jsMachine();

		Jigsaw::jsEntity FabricateEntity(Jigsaw::jsEntityCluster& cluster, Jigsaw::FABRICATION_ARGS& args);

		/// <summary>
		/// Returns the signature formed by all of the individual Machine Pieces.
		/// </summary>
		/// <returns></returns>
		const Jigsaw::Unique<Jigsaw::sysSignature> GetSignature() const;

		std::string name;
		Jigsaw::Unique<jsMachine[]> children;
		std::vector<jsPiece*> pieces;
		Jigsaw::UID machineId;

	};
}

#endif