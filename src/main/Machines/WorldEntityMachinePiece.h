#ifndef _WORLD_ENTITY_MACHINE_PIECE_H_
#define _WORLD_ENTITY_MACHINE_PIECE_H_

#include "jsPiece.h"
#include "Physics/Transform.h"
#include "Time/TimeStep.h"
#include "_jgsw_api.h"

namespace Jigsaw
{
	/// <summary>
	/// WorldEntityMachinePiece 
	/// </summary>
	JGSW_MACHINE_PIECE(JGSW_API WorldEntityMachinePiece) {
public:
	JGSW_COMPONENT_DATA(Transform, transform);
	JGSW_COMPONENT_DATA(Jigsaw::Time::TimeStep, time_step);
	};
}
#endif