#ifndef _FABRICATION_ARGS_H_
#define _FABRICATION_ARGS_H_

#include "Assembly/JGSW_ASSEMBLY.h"
#include "RTTI/etype_info.h"
#include "Machines/SerializableMachineData.h"
#include "Debug/j_debug.h"
#include "Marshalling/JSONNode.h"
#include "Entities/jsEntity.h"
#include "_jgsw_api.h"

namespace Jigsaw {
	/// <summary>
	/// FABRICATION_ARGS are created to override some of the values associated with a given jsEntity fabrication
	/// operation. Values can be overriden with either Serialized Data or with a direct Rvalue object override. 
	/// 
	/// The JigsawMachineObserver will validate the passed in parameters at the time of jsEntity fabrication. 
	/// In Debug mode, an exception should be thrown. In release code, invalid parameters yield undefined behavior. 
	/// </summary>
	class JGSW_API FABRICATION_ARGS {
	public:
		FABRICATION_ARGS() : scope(Jigsaw::jsEntity::SCOPE_EPHEMERAL) {}

		/// <summary>
		/// Pushes a value override object associated with the FABRICATION_ARGS. If an object of the same type is passed,
		/// then the already-allocated memory will be overwritten
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="value"></param>
		/// <returns></returns>
		template<typename T>
		inline FABRICATION_ARGS& WithValue(T&& value) {

			const Jigsaw::etype_info& t_info = Jigsaw::etype_info::Id<T>();
			auto iter = std::find_if(value_overrides.begin(), value_overrides.end(), [&t_info](FABRICATION_VALUE_OVERRIDE& ovrd) { return ovrd.type_index == t_info; });
			if (iter == value_overrides.end()) {
				FABRICATION_VALUE_OVERRIDE value_override;
				value_override.raw_data = Jigsaw::Ref<u8[]>(new u8[t_info.size]);
				t_info.GetUnsafeFunctions().Move(value_override.raw_data.get(), static_cast<u8*>((void*)&value));
				value_override.type_index = t_info;
				value_overrides.push_back(value_override);
			}
			else {
				FABRICATION_VALUE_OVERRIDE & override = *iter;
				t_info.GetUnsafeFunctions().Move(override.raw_data.get(), static_cast<u8*>((void*)&value));
			}

			return *this;
		}

		// this implementation may need to change
		FABRICATION_ARGS& WithSerializedData(const Jigsaw::SerializableEntityData& entity_data);

		/// <summary>
		/// Adds the given 'scope' to the FABRICATION_ARGS
		/// </summary>
		/// <param name="scope"></param>
		/// <returns></returns>
		FABRICATION_ARGS& WithScope(Jigsaw::jsEntity::SCOPE scope);

		/// <summary>
		/// Populates the 'destination' with the relevant type data if the relevant type is present in the
		/// FABRICATION_VALUE_OVERRIDE vector
		/// </summary>
		/// <param name="t_info"></param>
		/// <param name="destination"></param>
		/// <returns>True if the passed in 't_info' object is represented in the fabrication data</returns>
		bool GetData(const Jigsaw::etype_info& t_info, void* destination);

		/// <summary>
		/// Return the entity id to be fabricated
		/// </summary>
		/// <returns></returns>
		Jigsaw::UID GetEntityUID() const;

	private:

		struct FABRICATION_VALUE_OVERRIDE {
			Jigsaw::etype_index type_index;
			Jigsaw::Ref<u8[]> raw_data;

			u8* GetData();
		};

		std::vector<FABRICATION_VALUE_OVERRIDE> value_overrides;
		Jigsaw::jsEntity::jsEntity::SCOPE scope;
		Jigsaw::UID entity_id;

	};

	/// <summary>
	/// This simple wrapper holds multiple FABRICATION_ARGS instances for use with a single machine. 
	/// </summary>
	class JGSW_API MULTI_FABRICATION_ARGS {
	public:
		std::vector<FABRICATION_ARGS> entity_fabrication_args;

	};

	inline FABRICATION_ARGS& FABRICATION_ARGS::WithSerializedData(const Jigsaw::SerializableEntityData& entity_data) {
		FABRICATION_ARGS args;
		scope = entity_data.scope;
		entity_id = entity_data.entity_id;
		for (UINT i = 0; i < entity_data.aligned_types.size(); i++) {
			std::istringstream ss(entity_data.string_vector.strings.at(i));
			Jigsaw::Marshalling::JSONNodeReader node(ss, Jigsaw::Assembly::MarshallingRegistry::GetMarshallingMap(entity_data.aligned_types.at(i).Get()));
			auto result = node.BuildNode();

			FABRICATION_VALUE_OVERRIDE override;
			override.raw_data = Jigsaw::Ref<u8[]>(static_cast<u8*>(result.raw_data));
			override.type_index = entity_data.aligned_types.at(i);
			value_overrides.push_back(override);
		}
		return *this;
	}

	inline FABRICATION_ARGS& FABRICATION_ARGS::WithScope(Jigsaw::jsEntity::SCOPE scope) {
		this->scope = scope;
		return *this;
	}

	inline bool FABRICATION_ARGS::GetData(const Jigsaw::etype_info& t_info, void* destination) {
		auto iter = std::find_if(value_overrides.begin(), value_overrides.end(), [&t_info](const FABRICATION_VALUE_OVERRIDE & override) { return override.type_index == t_info; });
		if (iter != value_overrides.end()) {
			t_info.GetUnsafeFunctions().Move((u8*)destination, iter->GetData());
			value_overrides.erase(iter);
			return true;
		}
		return false;
	}

	inline Jigsaw::UID FABRICATION_ARGS::GetEntityUID() const {
		return entity_id;
	}

	inline u8* FABRICATION_ARGS::FABRICATION_VALUE_OVERRIDE::GetData() {
		return raw_data.get();
	}
}
#endif