#include "SerializableMachineData.h"
#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw 
{
		START_REGISTER_SERIALIZABLE_CLASS(SerializableMachineData)
		REGISTER_SERIALIZABLE_VECTOR(SerializableMachineData, SerializableEntityData, entity_data)
		END_REGISTER_SERIALIZABLE_CLASS(SerializableMachineData)
}