#include "jsPiece.h"
#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw {
	void Jigsaw::jsPiece::ComposeSignature(Jigsaw::SignatureBuilder& signature_builder) const {
		for (const jsPiece::component_def& comp_def : component_definitions) {
			J_D_ASSERT_LOG_ERROR(comp_def.type_info, jsPiece, "A component definition is missing type information.");
			signature_builder.AddType(*comp_def.type_info);
		}
	}

	void jsPiece::Turn(Jigsaw::jsEntity& entity) const {
		for (const jsPiece::component_def& comp_def : component_definitions) {
			J_D_ASSERT_LOG_ERROR(comp_def.e_init, jsPiece, "The Machine Piece attempted to be initialized with an invalid initializer.");
			comp_def.e_init(entity);
		}
	}

	void jsPiece::Hammer(Jigsaw::jsEntity& entity, Jigsaw::FABRICATION_ARGS& args) const {
		for (const jsPiece::component_def& comp_def : component_definitions) {
			J_D_ASSERT_LOG_ERROR(comp_def.type_info && comp_def.e_aug, jsPiece, "The Machine Piece attempted to be augmented with an invalid augmentor.");
			comp_def.e_aug(entity, args, *comp_def.type_info);
		}
	}

	START_REGISTER_SERIALIZABLE_CLASS(jsPiece)
		END_REGISTER_SERIALIZABLE_CLASS(jsPiece)
}

