#include "jsMachineLoader.h"

#include "Assets/dlAssetRegistrar.h"
#include "Debug/j_log.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	static unsigned int sm_loadIdx = 0;
	jsMachineLoader::_MACHINE_ASYNC_JOB::_MACHINE_ASYNC_JOB(MACHINE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager) : m_args(std::move(args)), m_assetManager(asset_manager), ASYNC_JOB("MACHINE_LOAD_JOB_" + sm_loadIdx) {
	}

	//////////////////////////////////////////////////////////////////////////

	MACHINE_LOAD_RESULT jsMachineLoader::_MACHINE_ASYNC_JOB::Execute() 
	{
		using namespace Jigsaw::Assets;

		J_LOG_INFO(jsMachineLoader, "Dispatching thread to load machines");
		MACHINE_LOAD_RESULT result;

		THREAD_LOCAL_SYSTEM_RESOURCES resources = m_args.resourcePool.Get();
		const UID* id = m_args.machineIds.data();
		Unique<Ref<DataAsset>[]> machines = m_assetManager->FetchAssets(id, m_args.machineIds.size() , resources);
		Ref<DataAsset>* machines_ = machines.get();

		J_LOG_INFO(jsMachineLoader, "Loading {0:d} machines", m_args.machineIds.size());
		result.jgswMachines.reserve(m_args.machineIds.size());
		for (int i = 0; i < m_args.machineIds.size(); i++)
		{

			Ref<ObjectAsset> machine_obj = std::dynamic_pointer_cast<ObjectAsset>(machines_[i]);
			result.jgswMachines.push_back(AssetRef<jsMachine>(machine_obj));
		}

		if (resources.cmd_list->HasCommands())
		{
			m_args.resourcePool.cmd_list_exec->SubmitCommandList(resources.cmd_list);
		}

		result.recycledResources.Enqueue(std::move(resources));

		return result;
	}

	//////////////////////////////////////////////////////////////////////////

	jsMachineLoader::jsMachineLoader(MACHINE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager) : m_args(std::move(args)), m_assetManager(asset_manager) { }

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<ASYNC_JOB<MACHINE_LOAD_RESULT>> Jigsaw::jsMachineLoader::LoadMachine()
	{
		return Jigsaw::Ref<ASYNC_JOB<MACHINE_LOAD_RESULT>>(new _MACHINE_ASYNC_JOB(std::move(m_args), m_assetManager));
	}

	//////////////////////////////////////////////////////////////////////////
}
