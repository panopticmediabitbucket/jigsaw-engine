/*********************************************************************
 * jsPiece.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _JIGSAW_MACHINE_PIECE_H_
#define _JIGSAW_MACHINE_PIECE_H_

// Jigsaw_Engine
#include "Entities/jsEntity.h"
#include "Machines/FABRICATION_ARGS.h"
#include "System/sysSignature.h"
#include "Util/DTO.h"
#include "RTTI/etype_link.h"

namespace Jigsaw
{

	/// <summary>
	/// JigsawMachinePiece is the root class for all inheriting pieces that apply component data to JigsawEntities during fabrication
	/// </summary>
	class JGSW_API jsPiece
	{
	public:
		RTTI_LINK_CHILDREN(jsPiece);

		jsPiece() = default;

		virtual ~jsPiece() = default;

		/// <summary>
		/// Modifies the 'signature_builder' to include the component data native to this jsPiece
		/// </summary>
		/// <param name="signature_builder"></param>
		void ComposeSignature(Jigsaw::SignatureBuilder& signature_builder) const;

		/// <summary>
		/// 'Turns' the fabricated Entity, assigning the default ComponentData from this MachinePiece to the jsEntity
		/// </summary>
		/// <param name="signature_builder"></param>
		void Turn(Jigsaw::jsEntity& entity) const;

		/// <summary>
		/// 'Hammers' the fabricated Entity, assigning values corresponding to from the FABRICATION_ARGS to the newly-minted entity
		/// </summary>
		/// <param name="entity"></param>
		/// <param name="args"></param>
		void Hammer(Jigsaw::jsEntity& entity, Jigsaw::FABRICATION_ARGS& args) const;

	protected:
		typedef Jigsaw::Functor<void, jsPiece*, Jigsaw::jsEntity&> ENTITY_INIT_FUNCTOR;
		typedef void (*ENTITY_AUG_FUNC)(Jigsaw::jsEntity&, Jigsaw::FABRICATION_ARGS&, const Jigsaw::etype_info&);

		struct component_def
		{
			const Jigsaw::etype_info* type_info = nullptr;

			ENTITY_INIT_FUNCTOR e_init;
			ENTITY_AUG_FUNC e_aug;
		};

		std::vector<component_def> component_definitions;
	};
}

#define EXPANDER(fieldname) fieldname
#define D_EXPANDER(classname, field_name) EXPANDER(classname)EXPANDER(_)EXPANDER(field_name)

/// <summary>
/// Template function definition for Augmenting entity component data with fabrication args.
/// </summary>
/// <typeparam name="T"></typeparam>
/// <param name="e"></param>
/// <param name="args"></param>
/// <param name="t_info"></param>
template <typename T>
void AugmentEntity(Jigsaw::jsEntity& e, Jigsaw::FABRICATION_ARGS& args, const Jigsaw::etype_info& t_info)
{
	T t;
	if (args.GetData(t_info, &t))
	{
		e.SetMemberData(std::move(t));
	}
}

/// Macro for defining a Jigsaw Machine Piece
#define JGSW_MACHINE_PIECE(piece_type)\
class piece_type : public Jigsaw::jsPiece, protected Jigsaw::DTO<piece_type>

#define JGSW_COMPONENT_DATA(type, field_name) \
type field_name; \
private: struct EXPANDER(field_name)_component_def  {\
	static void ComponentInit(Jigsaw::jsPiece* piece, Jigsaw::jsEntity& e) {\
		_THIS_TYPE* this_piece = static_cast<_THIS_TYPE*>(piece); \
		e.SetMemberData<type>(this_piece->field_name); \
	}; \
	EXPANDER(field_name)_component_def(_THIS_TYPE& piece) { \
		static const Jigsaw::etype_info& t_info = Jigsaw::etype_info::Id<type>(); \
		Jigsaw::jsPiece::component_def def;\
		def.type_info = &t_info; \
		def.e_init = Jigsaw::jsPiece::ENTITY_INIT_FUNCTOR(&ComponentInit, &piece); \
		def.e_aug = &AugmentEntity<type>; \
		piece.component_definitions.push_back(def); \
	}; \
}; \
EXPANDER(field_name)_component_def EXPANDER(field_name)_component_def_register = EXPANDER(field_name)_component_def(*this); \
public:

#endif

