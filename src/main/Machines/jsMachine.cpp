#include "jsMachine.h"
#include "Entities/jsEntityCluster.h"
#include "Marshalling/JigsawMarshalling.h"


namespace Jigsaw {
	START_REGISTER_SERIALIZABLE_CLASS(jsMachine)
		REGISTER_SERIALIZABLE_FIELD(jsMachine, std::string, name)
		REGISTER_SERIALIZABLE_POINTER_VECTOR(jsMachine, jsPiece, pieces)
		REGISTER_SERIALIZABLE_FIELD(jsMachine, Jigsaw::UID, machineId)
		END_REGISTER_SERIALIZABLE_CLASS(jsMachine);

	//////////////////////////////////////////////////////////////////////////

	// jsMachine Implementations

	//////////////////////////////////////////////////////////////////////////

	const Jigsaw::Unique<Jigsaw::sysSignature> Jigsaw::jsMachine::GetSignature() const {
		Jigsaw::SignatureBuilder builder;
		for (const Jigsaw::jsPiece* piece : this->pieces) {
			piece->ComposeSignature(builder);
		}
		return builder.Build();
	}

	//////////////////////////////////////////////////////////////////////////

	jsMachine::jsMachine()
	{
	}

	//////////////////////////////////////////////////////////////////////////

	jsMachine::jsMachine(jsMachine&& other) noexcept : name(std::move(other.name)),
		pieces(std::move(other.pieces)), children(std::move(other.children)), machineId(std::move(other.machineId))
	{
	}

	//////////////////////////////////////////////////////////////////////////

	jsMachine& jsMachine::operator=(jsMachine&& other) noexcept
	{
		pieces = std::move(other.pieces);
		children = std::move(other.children);
		machineId = std::move(other.machineId);
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	jsMachine::~jsMachine() {
		for (jsPiece* piece : pieces) {
			delete piece;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::jsEntity jsMachine::FabricateEntity(Jigsaw::jsEntityCluster& cluster, Jigsaw::FABRICATION_ARGS& args) {
		Jigsaw::jsEntity entity(cluster.FabricateEntity(Jigsaw::jsEntity::SCOPE_EPHEMERAL));
		for (jsPiece* piece : pieces) {
			piece->Turn(entity);
			piece->Hammer(entity, args);
		}
		return entity;
	}
}