#ifndef _SERIALIZABLE_MACHINE_ENTITY_DATA_H_
#define _SERIALIZABLE_MACHINE_ENTITY_DATA_H_

#include <vector>
#include "Entities/SerializableEntityData.h"

namespace Jigsaw 
{
	class SerializableMachineData 
	{
	public:
		std::vector<Jigsaw::SerializableEntityData> entity_data;

	};
}
#endif