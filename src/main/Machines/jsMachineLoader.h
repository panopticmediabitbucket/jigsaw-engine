#ifndef _JIGSAW_MACHINE_LOADER_H_
#define _JIGSAW_MACHINE_LOADER_H_

#include "Assets/ASSET_LOAD_RESULT.h"
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Graphics/CommandListExecutor.h"
#include "Ref.h"
#include "jsMachine.h"
#include "System/ASYNC.h"
#include "Assets/DataAssets.h"

namespace Jigsaw {
	/// <summary>
	/// Arguments needed to launch a jsMachineLoader. resourcePool must not be empty
	/// </summary>
	struct MACHINE_LOAD_ARGS 
	{
		std::vector<Jigsaw::UID> machineIds;
		Jigsaw::THREAD_LOCAL_RESOURCE_POOL resourcePool;
	};

	/// <summary>
	/// The result produced contains a Unique array of SerializedRef<jsMachine> objects. The output 'count'
	/// will match the input 'count'
	/// </summary>
	struct MACHINE_LOAD_RESULT
	{
		std::vector<Jigsaw::AssetRef<jsMachine>> jgswMachines;
		Jigsaw::THREAD_LOCAL_RESOURCE_POOL recycledResources;
	};

	/// <summary>
	/// The jsMachineLoader launches a ASYNC_JOB that produces a MACHINE_LOAD_RESULT upon completion
	/// </summary>
	class jsMachineLoader
	{
	public:
		jsMachineLoader(MACHINE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager);

		/// <summary>
		/// Once the call to LoadMachine is made, the underlying jsMachineLoader is no longer valid.
		/// </summary>
		/// <returns></returns>
		Jigsaw::Ref<Jigsaw::ASYNC_JOB<MACHINE_LOAD_RESULT>> LoadMachine();

	private:
		/// <summary>
		/// Private class used to override the 'Load' action of the ASYNC_JOB
		/// </summary>
		class _MACHINE_ASYNC_JOB : public Jigsaw::ASYNC_JOB<MACHINE_LOAD_RESULT>
		{
		public:
			_MACHINE_ASYNC_JOB(MACHINE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager);

		protected:
			MACHINE_LOAD_RESULT Execute() override;

		private:
			MACHINE_LOAD_ARGS m_args;
			Jigsaw::dlAssetRegistrar* m_assetManager;

		};

		MACHINE_LOAD_ARGS m_args;
		Jigsaw::dlAssetRegistrar* m_assetManager;
	};
}
#endif // !_JIGSAW_MACHINE_LOADER_H_
