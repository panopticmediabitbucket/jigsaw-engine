#include "jsMachineObserver.h"
#include "Debug/j_debug.h"
#include "Machines/FABRICATION_ARGS.h"

namespace Jigsaw
{

		//////////////////////////////////////////////////////////////////////////

		jsMachineObserver::jsMachineObserver(const Jigsaw::AssetRef<jsMachine>& machine,
			Jigsaw::jsEntityCluster& cluster,
			Jigsaw::jsSceneObserver* scene)
			: m_machine(machine), m_cluster(&cluster), m_scene(scene) {}

		//////////////////////////////////////////////////////////////////////////

		Jigsaw::UID jsMachineObserver::GetUID() const
		{
			return m_machine->machineId;
		}

		//////////////////////////////////////////////////////////////////////////

		Jigsaw::jsEntity jsMachineObserver::FabricateEntity(FABRICATION_ARGS& args)
		{
			Jigsaw::jsEntity e = m_machine->FabricateEntity(*m_cluster, args);
			m_entityIds.push_back(e.GetUID());
			return e;
		}

		//////////////////////////////////////////////////////////////////////////

		Jigsaw::jsEntity jsMachineObserver::FabricateEntity()
		{
			FABRICATION_ARGS args;
			Jigsaw::jsEntity e = m_machine->FabricateEntity(*m_cluster, args);
			m_entityIds.push_back(e.GetUID());
			return e;
		}

		//////////////////////////////////////////////////////////////////////////

		const Jigsaw::sysSignature& jsMachineObserver::GetMachineSignature()
		{
			return m_cluster->GetSignature();
		}

		//////////////////////////////////////////////////////////////////////////

		const std::vector<Jigsaw::UID>& jsMachineObserver::GetEntityIds() const
		{
			return m_entityIds;
		}

		//////////////////////////////////////////////////////////////////////////

		void jsMachineObserver::Init(Jigsaw::MULTI_FABRICATION_ARGS& machine_fabrication_data)
		{
			for (Jigsaw::FABRICATION_ARGS& entity_fabrication_args : machine_fabrication_data.entity_fabrication_args)
			{
				Jigsaw::jsEntity entity = m_machine->FabricateEntity(*m_cluster, entity_fabrication_args);
				m_entityIds.push_back(entity.GetUID());
			}
		}

		//////////////////////////////////////////////////////////////////////////

		void jsMachineObserver::Destroy()
		{
			for (Jigsaw::UID& id : m_entityIds)
			{
				m_cluster->RemoveEntity(id);
			}
		}

		//////////////////////////////////////////////////////////////////////////
}

