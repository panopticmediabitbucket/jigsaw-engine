#include "Machines/WorldEntityMachinePiece.h"

#include "Marshalling/JigsawMarshalling.h"

using namespace Jigsaw::Time;

namespace Jigsaw
{

	START_REGISTER_SERIALIZABLE_CLASS(WorldEntityMachinePiece)
		REGISTER_SERIALIZABLE_CHILD_OF(WorldEntityMachinePiece, jsPiece)
		REGISTER_SERIALIZABLE_FIELD(WorldEntityMachinePiece, Transform, transform)
		END_REGISTER_SERIALIZABLE_CLASS(WorldEntityMachinePiece)
}