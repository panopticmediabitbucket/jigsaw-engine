/*********************************************************************
 * jsMachineObserver.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: April 2021
 *********************************************************************/
#ifndef _JIGSAW_MACHINE_OBSERVER_H_
#define _JIGSAW_MACHINE_OBSERVER_H_

#include "Entities/jsEntityCluster.h"
#include "Machines/jsMachine.h"
#include "Machines/SerializableMachineData.h"
#include "Assets/DataAssets.h"

namespace Jigsaw {
	class jsSceneObserver;

	/// <summary>
	/// JigsawMachineObserver is an interface used for Fabricating entities primarily, but it can also provide insight
	/// into the Entities themselves. 
	/// </summary>
	class JGSW_API jsMachineObserver {
	public:
		virtual ~jsMachineObserver() {}

		/// <summary>
		/// Fetch the entity UID's associated with this machine
		/// </summary>
		/// <returns></returns>
		const std::vector<Jigsaw::UID>& GetEntityIds() const;

		/// <returns>The machine Id</returns>
		Jigsaw::UID GetUID() const;

		/// <returns>The name of the machine</returns>
		inline const char* GetName() const { return m_machine->name.c_str(); };

		/// <summary>
		/// This override allows the caller to specify FABRICATION_ARGS associated with the fabrication operation
		/// </summary>
		/// <param name="args"></param>
		/// <returns></returns>
		Jigsaw::jsEntity FabricateEntity(FABRICATION_ARGS& args);

		/// <summary>
		/// Fabricates a single jsEntity with the default parameters of the jsMachine that this object observes
		/// </summary>
		/// <returns></returns>
		Jigsaw::jsEntity FabricateEntity();

		/// <summary>
		/// Returns a signature associated with this jsMachine
		/// </summary>
		/// <returns></returns>
		const Jigsaw::sysSignature& GetMachineSignature();

		/// <summary>
		/// Fabricate a batch of JigsawEntities. 
		/// </summary>
		/// <param name="machine_fabrication_data"></param>
		void Init(Jigsaw::MULTI_FABRICATION_ARGS& machine_fabrication_data);

	protected:
		jsMachineObserver(const Jigsaw::AssetRef<jsMachine>& machine,
			Jigsaw::jsEntityCluster& cluster,
			Jigsaw::jsSceneObserver* scene);

		friend class Jigsaw::jsSceneObserver;

		void Destroy();

		std::vector<Jigsaw::UID> m_entityIds;
		const Jigsaw::UID m_uid;
		const Jigsaw::AssetRef<jsMachine> m_machine;
		Jigsaw::jsEntityCluster* m_cluster;
		Jigsaw::jsSceneObserver* m_scene;

	};
}
#endif