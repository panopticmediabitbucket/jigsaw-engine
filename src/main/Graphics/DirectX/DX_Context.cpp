#include "DX_Context.h"
#include "DX_CommandList.h"
#include "Debug/j_debug.h"
#include "Graphics/DirectX/DX_CommandQueue.h"
#include "Graphics/Pipeline/PipelineService.h"

const D3D_FEATURE_LEVEL feature_levels[] = { D3D_FEATURE_LEVEL_11_1, D3D_FEATURE_LEVEL_11_0 };

#define ADDITIONAL_RTV_DESCS 6
#define ADDITIONAL_DSV_DESCS 6
#define SRV_DESCRIPTOR_COUNT 1024

using namespace Microsoft::WRL;

namespace Jigsaw {

	//////////////////////////////////////////////////////////////////////////

	// DX_Context implementations

	//////////////////////////////////////////////////////////////////////////

	DX_Context* DX_Context::instance;

	//////////////////////////////////////////////////////////////////////////

	DX_Context::DX_Context(
		const ComPtr<ID3D12Device2> dx_device,
		Jigsaw::Ref<Jigsaw::DX_CommandQueue>* const cmd_queues,
		const ComPtr<IDXGIFactory4> dx_factory
	) : dx_device(dx_device),
		cmd_queues(cmd_queues),
		dx_factory(dx_factory),
		dx_srv_heap_ring_buffer(CreateDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV, SRV_DESCRIPTOR_COUNT, D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE)),
		dx_rtv_descriptor_ring(CreateDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE_RTV, SWAP_CHAIN_COUNT + ADDITIONAL_RTV_DESCS)),
		dx_dsv_descriptor_ring(CreateDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE_DSV, SWAP_CHAIN_COUNT + ADDITIONAL_DSV_DESCS)),
		GraphicsContext() {
		instance = this;
	}

	//////////////////////////////////////////////////////////////////////////

	/**
	* Basic utility for printing error/debug messages to the console when something fails
	*/
	void LocalPrintDebugMessages(ComPtr <ID3D12Device2> dx_device) {
		ComPtr<ID3D12InfoQueue> info_queue;
		HRESULT cast_result = dx_device.As(&info_queue);
		PRINT_HRESULT_ERRORS_THROW("Failed to cast device to info queue: ", cast_result);

		UINT64 count = info_queue->GetNumStoredMessages();
		for (unsigned int i = 0; i < count; i++) {
			SIZE_T length = 0;
			HRESULT h = info_queue->GetMessage(i, NULL, &length);

			D3D12_MESSAGE* message = (D3D12_MESSAGE*)malloc(length);
			h = info_queue->GetMessage(i, message, &length);

			J_LOG_INFO(DX_Context, message->pDescription);
			free(message);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_Context::PrintDebugMessages() {
		if (instance == nullptr) {
			return;
		}
		else {
			LocalPrintDebugMessages(instance->dx_device);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	/**
	* Creates a swap chain with standard settings for the target window and returns a ComPtr to that SwapChain's interface.
	*/
	void DX_Context::CreateWindowSwapchain(const HWND hWnd, UINT width, UINT height) {
		auto render_queue = cmd_queues[JGSW_COMMAND_TYPE_RENDER];
		bool rebind = false;
		if (!dx_swap_chain) {
			rebind = false;
			// initial swap chain creation routine
			DXGI_SWAP_CHAIN_DESC1 swap_chain_description;
			ZeroMemory(&swap_chain_description, sizeof(DXGI_SWAP_CHAIN_DESC1));

			RECT r;
			GetWindowRect(hWnd, &r);

			swap_chain_description.Width = r.right - r.left; // use the width value of the associated HWND 
			swap_chain_description.Height = r.bottom - r.top; // use the height value of the associated HWND 
			swap_chain_description.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
			swap_chain_description.SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
			swap_chain_description.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
			swap_chain_description.BufferCount = SWAP_CHAIN_COUNT;
			swap_chain_description.Stereo = FALSE;
			swap_chain_description.SampleDesc.Count = 1;
			swap_chain_description.SampleDesc.Quality = 0;
			swap_chain_description.Scaling = DXGI_SCALING_STRETCH;
			swap_chain_description.AlphaMode = DXGI_ALPHA_MODE_UNSPECIFIED;
			swap_chain_description.Flags = 0;

			// building the actual swap chain
			ComPtr<IDXGISwapChain1> swap_chain_1;

			HRESULT swap_chain_creation_result = dx_factory->CreateSwapChainForHwnd(render_queue->Get(), hWnd, &swap_chain_description, nullptr, nullptr, &swap_chain_1);
			PRINT_HRESULT_ERRORS_THROW("Failed to create Swap Chain: ", swap_chain_creation_result);

			HRESULT cast_result = swap_chain_1.As(&dx_swap_chain);

			J_LOG_INFO(DX_Context, "Swap chain created");
		}
		else
		{
			rebind = true;

			render_queue->SignalAndWait();

			// The ResizeBuffers1 call needs arrays the size of the # of buffers in the swapchain populated with the command queues
			// and the node mask for each. Since they use the same queue and node, they are just repeated
			IUnknown* raw_queues[SWAP_CHAIN_COUNT];
			UINT node_masks[SWAP_CHAIN_COUNT];
			GPUResource** rtv_resources_swap = &j_viewport.GetRtvResource(0);
			GPUResource** dsv_resources_swap = &j_viewport.GetDsvResource(0);

			for (int i = 0; i < SWAP_CHAIN_COUNT; i++) {
				raw_queues[i] = render_queue->GetD3D12CommandQueue().Get();
				node_masks[i] = FRAME_NODE;
				DX_GPUResource* dx_rtv_swap = dynamic_cast<DX_GPUResource*>(rtv_resources_swap[i]);
				DX_GPUResource* dx_dsv_swap = dynamic_cast<DX_GPUResource*>(dsv_resources_swap[i]);
				dx_rtv_swap->Reset();
				dx_dsv_swap->Reset();
				continue;
			}

			HRESULT result = dx_swap_chain->ResizeBuffers1(SWAP_CHAIN_COUNT, width, height, DXGI_FORMAT_R8G8B8A8_UNORM, 0, node_masks, raw_queues);

			J_D_ASSERT_LOG_ERROR((S_OK == result), DX_Context, "Failed to resize the display buffers");
			PrintDebugMessages();
		}

		CreateSwapChainResourcesAndRTVs(dx_swap_chain, rebind);

		DX_GPUResource* rtv_backbuffer = dynamic_cast<DX_GPUResource*>(j_viewport.GetRtvResource(0));
		GPUResource** target_dsv_resources = &j_viewport.GetDsvResource(0);
		CreateDSResourcesAndViews(rtv_backbuffer->GetResource(), dx_dsv_descriptor_ring, target_dsv_resources, rebind);

		j_viewport.ResetFrame();
	}

	//////////////////////////////////////////////////////////////////////////

	GPUResource* DX_Context::CreateGPUResource(JGSW_GPU_RESOURCE_DATA resource_args)
	{

		J_D_ASSERT_LOG_ERROR(!((resource_args.flags & JGSW_GPU_RESOURCE_FLAG_UPDATABLE) && (resource_args.visibility & JGSW_VISIBILITY::CPU_READABLE)),
			DX_Context, "GPUResource can not both be flagged 'UPDATABLE' and 'CPU_READABLE'.");

		CD3DX12_HEAP_PROPERTIES heap((JGSW_VISIBILITY::CPU_READABLE & resource_args.visibility) ? D3D12_HEAP_TYPE_READBACK : D3D12_HEAP_TYPE_DEFAULT);

		if (resource_args.cpu_mappable)
		{
			heap.Type = D3D12_HEAP_TYPE_CUSTOM;
			heap.MemoryPoolPreference = D3D12_MEMORY_POOL_L0;
			heap.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_WRITE_BACK;
		}

		// If no format is passed in, we'll default to R8G8B8A8
		DXGI_FORMAT format = resource_args.format == JGSW_FORMAT::UNDEFINED ? DXGI_FORMAT_R8G8B8A8_UNORM : D3D12_CONVERSIONS::ConvertFormat(resource_args.format);

		D3D12_RESOURCE_DESC resource_desc = resource_args.resource_type == JGSW_GPU_RESOURCE_TYPE::TEXTURE
			? CD3DX12_RESOURCE_DESC::Tex2D(format, resource_args.width, resource_args.height, 1, 1, resource_args.sample_desc.count, resource_args.sample_desc.quality)
			: CD3DX12_RESOURCE_DESC::Buffer(resource_args.width);

		D3D12_CLEAR_VALUE clear_value;
		clear_value.Format = resource_desc.Format;
		clear_value.Color[0] = resource_args.default_color[0];
		clear_value.Color[1] = resource_args.default_color[1];
		clear_value.Color[2] = resource_args.default_color[2];
		clear_value.Color[3] = resource_args.default_color[3];

		if (resource_args.visibility & JGSW_VISIBILITY::RENDER_TARGET)
		{
			resource_desc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET;
			resource_desc.Format = (resource_args.flags & (int)JGSW_GPU_RESOURCE_FLAGS::JGSW_GPU_RESOURCE_FLAG_32_BIT_FLOAT)
				? DXGI_FORMAT_R32G32B32A32_FLOAT : DXGI_FORMAT_R8G8B8A8_UNORM;
			clear_value.Format = resource_desc.Format;
			clear_value.Color[0] = resource_args.default_color[0];
			clear_value.Color[1] = resource_args.default_color[1];
			clear_value.Color[2] = resource_args.default_color[2];
			clear_value.Color[3] = resource_args.default_color[3];
		}
		else if (resource_args.visibility & JGSW_VISIBILITY::DEPTH_STENCIL)
		{
			resource_desc.Flags |= D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;
			resource_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			clear_value.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
			clear_value.DepthStencil.Depth = resource_args.ds.depth;
			clear_value.DepthStencil.Stencil = resource_args.ds.stencil;
		}
		else if (resource_args.visibility & JGSW_VISIBILITY::STREAM_OUTPUT)
		{
			resource_desc.Format = DXGI_FORMAT_UNKNOWN;
		}

		// No clear value for buffers or non-rtv/dsv-visible textures
		D3D12_CLEAR_VALUE* clear_ref =
			resource_args.resource_type == JGSW_GPU_RESOURCE_TYPE::BUFFER || !(resource_args.visibility & (JGSW_VISIBILITY::RENDER_TARGET | JGSW_VISIBILITY::DEPTH_STENCIL))
			? nullptr : &clear_value;

		D3D12_RESOURCE_STATES initial_state = JGSW_VISIBILITY::CPU_READABLE & resource_args.visibility ? D3D12_RESOURCE_STATE_COPY_DEST : D3D12_RESOURCE_STATE_COMMON;

		Microsoft::WRL::ComPtr<ID3D12Resource> dx_resource;

		HRESULT result = dx_device->CreateCommittedResource(&heap, D3D12_HEAP_FLAG_NONE, &resource_desc, initial_state, clear_ref, IID_PPV_ARGS(&dx_resource));

		PRINT_HRESULT_ERRORS_THROW("Failed to create GPU resource: ", result);
		DX_GPUResource* gpu_resource = new DX_GPUResource(dx_resource, clear_value, resource_args, initial_state);

		// If the resource is updatable, then a copy resource needs to be made as well
		if (resource_args.flags & JGSW_GPU_RESOURCE_FLAG_UPDATABLE || resource_args.flags & JGSW_GPU_RESOURCE_FLAG_SINGLE_UPLOAD)
		{
			Microsoft::WRL::ComPtr<ID3D12Resource> copy_resource;
			CD3DX12_HEAP_PROPERTIES copy_heap(D3D12_HEAP_TYPE_UPLOAD);
			if (resource_args.resource_type == JGSW_GPU_RESOURCE_TYPE::BUFFER)
			{
				D3D12_RESOURCE_DESC update_desc = CD3DX12_RESOURCE_DESC::Buffer(resource_desc.Width);
				HRESULT copy_result = dx_device->CreateCommittedResource(&copy_heap, D3D12_HEAP_FLAG_NONE, &update_desc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&copy_resource));
				PRINT_HRESULT_ERRORS_THROW("Failed to create GPU resource: ", copy_result);
			}
			// For textures, among others. We get the copyable footprint first
			else
			{
				UINT64 required_size;
				UINT64 row_size_bytes;
				UINT num_rows;
				D3D12_PLACED_SUBRESOURCE_FOOTPRINT footprint;
				dx_device->GetCopyableFootprints(&resource_desc, 0, 1, 0, &footprint, &num_rows, &row_size_bytes, &required_size);

				D3D12_RESOURCE_DESC update_desc = CD3DX12_RESOURCE_DESC::Buffer(required_size);
				HRESULT copy_result = dx_device->CreateCommittedResource(&copy_heap, D3D12_HEAP_FLAG_NONE, &update_desc, D3D12_RESOURCE_STATE_GENERIC_READ, nullptr, IID_PPV_ARGS(&copy_resource));
				PRINT_HRESULT_ERRORS_THROW("Failed to create GPU resource: ", copy_result);
			}
			gpu_resource->SetCopyResource(copy_resource);
		}

		// Setting the corresponding views in the descriptor rings
		if (resource_args.visibility & JGSW_VISIBILITY::RENDER_TARGET)
		{
			dx_rtv_descriptor_ring->WriteResource(gpu_resource, { nullptr });
		}

		if (resource_args.visibility & JGSW_VISIBILITY::DEPTH_STENCIL)
		{
			dx_dsv_descriptor_ring->WriteResource(gpu_resource, { nullptr });
		}

		if (resource_args.visibility & JGSW_VISIBILITY::SHADER)
		{
			D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc;
			ZeroMemory(&srvDesc, sizeof(srvDesc));

			// for depth-stencil, we are interested in the stencil for the shader resource view. 
			if (resource_desc.Format == DXGI_FORMAT_D24_UNORM_S8_UINT)
			{
				srvDesc.Format = DXGI_FORMAT_X24_TYPELESS_G8_UINT;
				srvDesc.Texture2D.PlaneSlice = 1;
			}
			else
			{
				srvDesc.Format = resource_desc.Format;
			}

			srvDesc.ViewDimension = resource_args.sample_desc.count > 1 ? D3D12_SRV_DIMENSION_TEXTURE2DMS : D3D12_SRV_DIMENSION_TEXTURE2D;
			srvDesc.Texture2D.MipLevels = 1;
			srvDesc.Texture2D.MostDetailedMip = 0;
			srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

			dx_srv_heap_ring_buffer->WriteResource(gpu_resource, { &srvDesc });
		}

		return gpu_resource;
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::CommandList> DX_Context::GetCommandList(JGSW_COMMAND_TYPE type)
	{
		DX_CommandQueue* dx_command_queue = static_cast<DX_CommandQueue*>(cmd_queues[type].get());
		return dx_command_queue->GetCommandList();
	}

	//////////////////////////////////////////////////////////////////////////

	ID3D12Device2* DX_Context::GetDevice() {
		return dx_device.Get();
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::J_Viewport* DX_Context::GetViewport()
	{
		return &j_viewport;
	}

	//////////////////////////////////////////////////////////////////////////

	/**
	* A utility for creating a basic descriptor heap and storing it in the destination
	*/
	Ref<DX_DescriptorHeapRing> DX_Context::CreateDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE heap_type, int count, D3D12_DESCRIPTOR_HEAP_FLAGS flags) 
	{
		D3D12_DESCRIPTOR_HEAP_DESC descriptor_heap_desc;
		descriptor_heap_desc.Type = heap_type;
		descriptor_heap_desc.NumDescriptors = count;
		descriptor_heap_desc.Flags = flags;
		descriptor_heap_desc.NodeMask = 0;

		return MakeRef<DX_DescriptorHeapRing>(descriptor_heap_desc, count, dx_device);
	}

	//////////////////////////////////////////////////////////////////////////

	/**
	* uses the specified swap chain to create a descriptor heap and then initialize the render target views
	*/
	void DX_Context::CreateSwapChainResourcesAndRTVs(const ComPtr<IDXGISwapChain4>& swap_chain, bool rebind) {

		GPUResource** rtv_array = &j_viewport.GetRtvResource(0);

		// bind the back buffers to the corresponding descriptor handles one by one and create the render target views
		for (int i = 0; i < SWAP_CHAIN_COUNT; i++) {
			Microsoft::WRL::ComPtr<ID3D12Resource> resource;
			swap_chain->GetBuffer(i, IID_PPV_ARGS(&resource));

			if (!rebind) 
			{
				D3D12_CLEAR_VALUE clear_value;
				clear_value.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
				clear_value.Color[0] = .2f;
				clear_value.Color[1] = .2f;
				clear_value.Color[2] = .22f;
				clear_value.Color[3] = 1.0f;

				JGSW_GPU_RESOURCE_DATA resource_data;
				resource_data.height = (unsigned int)resource->GetDesc().Height;
				resource_data.width = (unsigned int)resource->GetDesc().Width;
				resource_data.resource_type = JGSW_GPU_RESOURCE_TYPE::TEXTURE;
				DX_GPUResource* dx_resource = new DX_GPUResource(resource, clear_value, resource_data, D3D12_RESOURCE_STATE_PRESENT);
				dx_rtv_descriptor_ring->WriteResource(dx_resource, { nullptr });
				rtv_array[i] = dx_resource;
			}
			else
			{
				DX_GPUResource* dx_resource = dynamic_cast<DX_GPUResource*>(rtv_array[i]);
				dx_resource->Rebind(dx_device, resource);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_Context::CreateDSResourcesAndViews(ID3D12Resource* rtv_backbuffer,
		const Ref<DX_DescriptorHeapRing>& dsv_descriptor_heap,
		GPUResource** target_dsv_resource_array, bool rebind)
	{

		D3D12_CLEAR_VALUE clear_val;
		clear_val.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		clear_val.DepthStencil.Depth = 1.0f;
		clear_val.DepthStencil.Stencil = 0;

		D3D12_RESOURCE_DESC rtv_buffer_desc = rtv_backbuffer->GetDesc();
		D3D12_RESOURCE_DESC dsv_buffer_desc = rtv_buffer_desc;
		dsv_buffer_desc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		dsv_buffer_desc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

		// Create the buffers and map the views to the descriptor heap
		for (int i = 0; i < SWAP_CHAIN_COUNT; i++)
		{
			CD3DX12_HEAP_PROPERTIES heap(D3D12_HEAP_TYPE_DEFAULT);
			Microsoft::WRL::ComPtr<ID3D12Resource> ds_buffer;
			HRESULT result = dx_device->CreateCommittedResource(&heap, D3D12_HEAP_FLAG_NONE,
				&dsv_buffer_desc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &clear_val, IID_PPV_ARGS(&ds_buffer));
			PRINT_HRESULT_ERRORS_THROW("Failed to create depth stencil: ", result);

			if (!rebind)
			{
				JGSW_GPU_RESOURCE_DATA resource_data;
				resource_data.resource_type = JGSW_GPU_RESOURCE_TYPE::TEXTURE;
				resource_data.height = ds_buffer->GetDesc().Height;
				resource_data.width = (u32)ds_buffer->GetDesc().Width;
				DX_GPUResource* dx_resource = new DX_GPUResource(ds_buffer, clear_val, resource_data, D3D12_RESOURCE_STATE_DEPTH_WRITE);
				dsv_descriptor_heap->WriteResource(dx_resource, { nullptr });
				target_dsv_resource_array[i] = dx_resource;
			}
			else
			{
				DX_GPUResource* dx_resource = dynamic_cast<DX_GPUResource*>(target_dsv_resource_array[i]);
				dx_resource->Rebind(dx_device, ds_buffer);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	HRESULT DX_Context::CreateViewport(D3D12_VIEWPORT& viewport, const ComPtr <ID3D12Resource>& back_buffer) const {
		D3D12_RESOURCE_DESC back_buffer_desc = back_buffer->GetDesc();

		ZeroMemory(&viewport, sizeof(D3D12_VIEWPORT));
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 00;
		viewport.Height = (FLOAT)back_buffer_desc.Height;
		viewport.Width = (FLOAT)back_buffer_desc.Width;
		viewport.MinDepth = 0;
		viewport.MaxDepth = 1;

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::CommandListExecutor> DX_Context::GetCommandListExecutor(JGSW_COMMAND_TYPE type) {
		return std::dynamic_pointer_cast<CommandListExecutor>(cmd_queues[type]);
	}

	//////////////////////////////////////////////////////////////////////////

	/**
	* The Device, command queue, and target factory are 'const' values and need to be initialized before constructing the actual
	* instance of the DirectX 12 manager. This private, static method handles the initialization of those immutable things so that
	* if the display properties or swap chain need to change, they may, but it will not result in a reconstruction of the DX_Context
	* or the pipeline itself.
	*/
	DX_Context* DX_Context::Init() {
		if (instance != nullptr) // this should only ever be called one time by ApplicationRoot 
			throw - 1;

		ComPtr<ID3D12Device2> device;
		Jigsaw::Ref<Jigsaw::DX_CommandQueue>* cmd_queues;
		Microsoft::WRL::ComPtr<IDXGIFactory4> dx_factory;
		InitializeRootObjects(&device, &cmd_queues, &dx_factory, false);

		instance = new DX_Context(device, cmd_queues, dx_factory);

		return instance;
	}

	//////////////////////////////////////////////////////////////////////////

	BOOL DX_Context::InitializeRootObjects(Microsoft::WRL::ComPtr<ID3D12Device2>* dx_device, Jigsaw::Ref<Jigsaw::DX_CommandQueue>** out_cmd_queue_array,
		Microsoft::WRL::ComPtr<IDXGIFactory4>* dx_factory, bool init_editor) {

		// creating the initial factory for adapters, etc.
		HRESULT factory_creation_result = CreateDXGIFactory2(DXGI_CREATE_FACTORY_DEBUG, IID_PPV_ARGS(&(*dx_factory)));
		PRINT_HRESULT_ERRORS_THROW("Factory creation failure: ", factory_creation_result)

			// loading the adapter and casting it to version 4
			ComPtr<IDXGIAdapter> adapter;
		ComPtr<IDXGIAdapter4> adapter_4;

		HRESULT retrieve_adapter = (*dx_factory)->EnumAdapters(0, &adapter);
		PRINT_HRESULT_ERRORS_THROW("Adapter retrieve failure: ", retrieve_adapter);
		HRESULT cast_adapter = adapter.As(&adapter_4);
		PRINT_HRESULT_ERRORS_THROW("Adapter cast failure: ", cast_adapter);

		//enabling debug layer for D3D12 prior to creating the device
		ComPtr<ID3D12Debug> debug;
		D3D12GetDebugInterface(__uuidof(ID3D12Debug), &debug);
		debug->EnableDebugLayer();

		// creating the main device
		HRESULT create_device = D3D12CreateDevice(nullptr, D3D_FEATURE_LEVEL_11_0, IID_PPV_ARGS(&(*dx_device)));
		PRINT_HRESULT_ERRORS_THROW("Device creation failure code: ", create_device);

		*out_cmd_queue_array = new Ref<Jigsaw::DX_CommandQueue>[__JGSW_COMMAND_TYPE_SIZE];
		ComPtr<ID3D12CommandQueue> frame_queue, load_queue;

		// creating the frame-render command queue
		D3D12_COMMAND_QUEUE_DESC frame_queue_desc = XD3D12_COMMAND_QUEUE_DESC::FrameQueue(FRAME_NODE);
		HRESULT frame_queue_result = (*dx_device)->CreateCommandQueue(&frame_queue_desc, __uuidof(ID3D12CommandQueue), &frame_queue);
		PRINT_HRESULT_ERRORS_THROW("Command queue creation failure: ", frame_queue_result);
		(*out_cmd_queue_array)[JGSW_COMMAND_TYPE_RENDER] = MakeRef<DX_CommandQueue>(frame_queue, ACTIVE_FRAME_ALLOCATORS);

		// creating the loading command queue
		D3D12_COMMAND_QUEUE_DESC load_queue_desc = XD3D12_COMMAND_QUEUE_DESC::LoadQueue(FRAME_NODE);
		HRESULT load_queue_result = (*dx_device)->CreateCommandQueue(&load_queue_desc, __uuidof(ID3D12CommandQueue), &load_queue);
		PRINT_HRESULT_ERRORS_THROW("Command queue creation failure: ", load_queue_result);
		(*out_cmd_queue_array)[JGSW_COMMAND_TYPE_LOAD] = MakeRef<DX_CommandQueue>(load_queue, ACTIVE_LOAD_ALLOCATORS);

		J_LOG_INFO(DX_Context, "Device creation successful");
		return TRUE;
	}

	//////////////////////////////////////////////////////////////////////////

	// f_i = frame index
	void DX_Context::DisplayFrame() {
		this->dx_swap_chain->Present(1, 0);

		J_LOG_TRACE(DX_Context, "Displayed frame on DX_CONTEXT")
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_Context::LoadShaderResources(const Jigsaw::Ref<Jigsaw::CommandList>& command_list) {
		DX_CommandList* dx_list = dynamic_cast<DX_CommandList*>(command_list.get());
		ID3D12DescriptorHeap* const heap = this->dx_srv_heap_ring_buffer->GetHeap();
		dx_list->GetDXCMDList()->SetDescriptorHeaps(1, &heap);
	}

	//////////////////////////////////////////////////////////////////////////
}
