/*********************************************************************
 * DX_Context.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: May 2021
 *********************************************************************/
#ifndef _DX_CONTEXT_H_
#define _DX_CONTEXT_H_

// External
#include <iostream>
#include <queue>
#include <map>
#include <vector>
#include <mutex>
#include <dxgi1_6.h>
#include "xd3d12.h"

// Jigsaw_Engine
#include "Graphics/CommandListExecutor.h"
#include "Graphics/__graphics_api.h"
#include "Graphics/CommandListExecutor.h"
#include "Graphics/CommandList.h"
#include "Graphics/DirectX/DX_CommandQueue.h"
#include "Graphics/DirectX/GPU_Objects/DX_DescriptorHeapRing.h"
#include "Graphics/GraphicsContext.h"
#include "Graphics/DirectX/GPU_Objects/DX_GPUResource.h"

#define SWAP_CHAIN_COUNT 2

#define FRAME_NODE 0b001
#define LOAD_NODE 0b010
#define RT_NODE 0b100

#define ACTIVE_FRAME_ALLOCATORS 15 
#define ACTIVE_LOAD_ALLOCATORS 8

#define PRINT_HRESULT_ERRORS_THROW(prestring, hresult) \
	if(FAILED(hresult)) { \
		DX_Context::PrintDebugMessages(); \
		std::string output = prestring + std::string(_com_error(hresult).ErrorMessage());\
		J_LOG_ERROR(Jigsaw::DX_Context, output.c_str());\
		throw hresult; \
	} 

namespace Jigsaw {
	/// <summary>
	/// The DX_Context is one of the primary services exposed to the ApplicationOrchestrator. It handles the creation of the device for interfacing
	/// with the GPU, can alter RenderTargets in response to changes in the ViewportWindow, and manages the creation, allocation, and submission of ID3D12GraphicsCommandLists.
	/// 
	/// DX_Context is also the final stop point for synchronization with the GPU.
	/// </summary>
	/// <see>
	/// Jigsaw::GraphicsContext
	/// </see>
	/// <author>
	/// Luke Randazzo
	/// </author>
	class JGSW_API DX_Context : public Jigsaw::GraphicsContext {
	public:

		/// <summary>
		/// PrintDebugMessages is a simple, static debugging tool that logs the queue of debug messages
		/// </summary>
		static void PrintDebugMessages();

		/// <summary>
		/// An instance of DX_Context is required to call the CreateWindowSwapchain function. In response to resizing events or upon creation of the program,
		/// there is a need to define the swapchain and the depth stencil backbuffers
		/// </summary>
		/// <param name="hWnd"></param>
		virtual void CreateWindowSwapchain(const HWND hWnd, UINT width, UINT height) override;

		/// <summary>
		/// Used for creating a new GPUResource from scratch, perhaps for a RenderTarget or something else.
		/// <param name="resource_args"></param>
		/// <returns></returns>
		/// </summary>
		Jigsaw::GPUResource* CreateGPUResource(JGSW_GPU_RESOURCE_DATA resource_args) override;

		/// <summary>
		/// An instance of DX_Context is required to call the GetCommandList function. The passed in 'type' indicates to the DX_Context if we're looking for
		/// a command list that's optimized for loading or direct execution of draw commands.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		Jigsaw::Ref<Jigsaw::CommandList> GetCommandList(Jigsaw::JGSW_COMMAND_TYPE type) override;

		/// <summary>
		/// Returns the underlying device to the caller.
		/// </summary>
		/// <returns>A ID3D12Device</returns>
		ID3D12Device2* GetDevice();

		/// <summary>
		/// Returns the primary viewport associated with the underlying GraphicsContext. The GraphicsContext owns the only non-const version of this, allowing it to make modifications
		/// when necessary.
		/// <returns></returns>
		/// </summary>
		Jigsaw::J_Viewport* GetViewport();

		/// <summary>
		/// Returns a DX_CommandQueue object which handles command list submission, execution, and synchronization. 
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		Jigsaw::Ref<Jigsaw::CommandListExecutor> GetCommandListExecutor(JGSW_COMMAND_TYPE type) override;

		/// <summary>
		/// Does the underlying transition operations needed to present the current frame
		/// </summary>
		void DisplayFrame() override;

		/// <summary>
		/// Enqueues a load operation on the given command list to use the designated shader resources.
		/// <param name="command_list"></param>
		/// </summary>
		void LoadShaderResources(const Jigsaw::Ref<Jigsaw::CommandList>& command_list) override;

		/// <summary>
		/// This method handles the initial construction of the DX_Context and the corresponding Device.
		/// It is intended to be called only by the GraphicsContext after the target architecture and graphics API (DX12) has been selected
		/// </summary>
		/// <returns></returns>
		static DX_Context* Init() noexcept(false);

	protected:
		static DX_Context* instance;

		/// <summary>
		/// Protected constructor. All of these parameters are defined statically in the Init function.
		/// </summary>
		/// <param name="dx_device"></param>
		/// <param name="cmd_queues"></param>
		/// <param name="dx_factory"></param>
		DX_Context(const Microsoft::WRL::ComPtr<ID3D12Device2> dx_device,
			Jigsaw::Ref<Jigsaw::DX_CommandQueue>* const cmd_queues,
			const Microsoft::WRL::ComPtr<IDXGIFactory4> dx_factory);

		/// <summary>
		/// Returns true if object initialization succeeds. The pointers are populated with complete objects upon completion.
		/// </summary>
		/// <param name="dx_device"></param>
		/// <param name="out_cmd_queue_array"></param>
		/// <param name="dx_factory"></param>
		static BOOL InitializeRootObjects(Microsoft::WRL::ComPtr<ID3D12Device2>* dx_device, Jigsaw::Ref<Jigsaw::DX_CommandQueue>** out_cmd_queue_array,
			Microsoft::WRL::ComPtr<IDXGIFactory4>* dx_factory, bool init_editor);

		/// <summary>
		/// Used internally to create descriptor heaps for a view of a particular 'heap_type'
		/// </summary>
		/// <param name="heap_type"></param>
		/// <param name="count"></param>
		/// <param name="destination"></param>
		Ref<DX_DescriptorHeapRing> CreateDescriptorHeap(D3D12_DESCRIPTOR_HEAP_TYPE heap_type, int count, D3D12_DESCRIPTOR_HEAP_FLAGS flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE);

		/// <summary>
		/// Used internally to create a new render target view after the swap_chain was created for the current details of the view window
		/// </summary>
		/// <param name="swap_chain"></param>
		void CreateSwapChainResourcesAndRTVs(const Microsoft::WRL::ComPtr<IDXGISwapChain4>& swap_chain, bool rebind);

		/// <summary>
		/// Creates Depth-stencil buffers and corresponding views based on the dimensions and information contained on the rtv_backbuffer input
		/// and stores the results in the dsv_backbuffers array (assumed to be initialized but empty) and the dsv_descriptor_heap (assumed to be initialized but empty)
		/// </summary>
		/// <param name="rtv_backbuffer"></param>
		/// <param name="dsv_descriptor_heap"></param>
		/// <param name="dsv_backbuffers"></param>
		/// <param name="count"></param>
		void CreateDSResourcesAndViews(ID3D12Resource* rtv_backbuffer,
			const Jigsaw::Ref<Jigsaw::DX_DescriptorHeapRing>& dsv_descriptor_heap,
			Jigsaw::GPUResource** target_dsv_resource_array, bool rebind);

		/// <summary>
		/// Outs a D3D12_VIEWPORT describing the details of the rtv back buffer
		/// </summary>
		/// <param name="viewport"></param>
		/// <param name="back_buffer"></param>
		/// <returns></returns>
		HRESULT CreateViewport(D3D12_VIEWPORT& viewport, const Microsoft::WRL::ComPtr<ID3D12Resource>& back_buffer) const;

		// immutable instance variables
		const Microsoft::WRL::ComPtr<ID3D12Device2> dx_device;
		const Microsoft::WRL::ComPtr<IDXGIFactory4> dx_factory;

		// mutable instance variables
		Jigsaw::Ref<Jigsaw::DX_CommandQueue>* const cmd_queues;

		D3D12_VIEWPORT viewport;

		Microsoft::WRL::ComPtr<IDXGISwapChain4> dx_swap_chain;
		Jigsaw::Ref<Jigsaw::DX_DescriptorHeapRing> dx_rtv_descriptor_ring;
		Jigsaw::Ref<Jigsaw::DX_DescriptorHeapRing> dx_dsv_descriptor_ring;
		Jigsaw::Ref<Jigsaw::DX_DescriptorHeapRing> dx_srv_heap_ring_buffer;
		Jigsaw::J_Viewport j_viewport;

	};
}
#endif // _DX_CONTEXT_H_
