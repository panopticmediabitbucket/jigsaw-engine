#include "Debug/j_debug.h"
#include "Graphics/DirectX/DirectXDataLayouts.h"
#include "Graphics/DirectX/DX_CommandList.h"
#include "Graphics/DirectX/GPU_Objects/DX_GPUResource.h"
#include "Graphics/Pipeline/DirectX/DX_PipelineObject.h"
#include "Graphics/Pipeline/DirectX/DX_RootSignatureObject.h"
#include "Debug/j_debug.h"
#include "Util/DTO.h" 

using namespace Microsoft::WRL;

namespace Jigsaw {

	////////////////////////////////////////////////////////////////////////////////////

	// DX_CommandList implementations

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::LoadBuffer(const void* t_arr, size_t t_size, size_t t_count, Jigsaw::GPUResource& buffer_dest) 
	{
		command_list_dirty = true;

		J_D_ASSERT_LOG_ERROR(buffer_dest.GetResourceData().flags & (JGSW_GPU_RESOURCE_FLAG_UPDATABLE | JGSW_GPU_RESOURCE_FLAG_SINGLE_UPLOAD),
			DX_CommandList, "The resource targeted for copying is not updatable");
		J_D_ASSERT_LOG_ERROR(buffer_dest.GetResourceData().resource_type == JGSW_GPU_RESOURCE_TYPE::BUFFER,
			DX_CommandList, "The resource targeted for copying is not a buffer");

		DX_GPUResource& dx_gpu_resource = static_cast<DX_GPUResource&>(buffer_dest);
		D3D12_SUBRESOURCE_DATA vertex_data;
		vertex_data.pData = t_arr;
		vertex_data.RowPitch = t_count;
		vertex_data.SlicePitch = t_count;

		UINT64 updateRes = UpdateSubresources(dx_cmd_list.Get(), dx_gpu_resource.GetResource(), dx_gpu_resource.GetCopyResource(), 0, 0, 1, &vertex_data);

		loading_buffers.push_back(&buffer_dest);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::LoadTexture(const u8* tex_data, size_t bytes_per_row, Jigsaw::GPUResource& texture_dest)
	{
		command_list_dirty = true;

		J_D_ASSERT_LOG_ERROR(texture_dest.GetResourceData().flags & (JGSW_GPU_RESOURCE_FLAG_UPDATABLE | JGSW_GPU_RESOURCE_FLAG_SINGLE_UPLOAD),
			DX_CommandList, "The resource targeted for copying is not updatable");
		J_D_ASSERT_LOG_ERROR(texture_dest.GetResourceData().resource_type == JGSW_GPU_RESOURCE_TYPE::TEXTURE,
			DX_CommandList, "The resource targeted for copying is not a buffer");

		DX_GPUResource& dx_gpu_resource = static_cast<DX_GPUResource&>(texture_dest);
		D3D12_SUBRESOURCE_DATA vertex_data;
		vertex_data.pData = tex_data;
		vertex_data.RowPitch = bytes_per_row;
		vertex_data.SlicePitch = 1;

		UINT64 updateRes = UpdateSubresources(dx_cmd_list.Get(), dx_gpu_resource.GetResource(), dx_gpu_resource.GetCopyResource(), 0, 0, 1, &vertex_data);

		loading_buffers.push_back(&texture_dest);
	}

	////////////////////////////////////////////////////////////////////////////////////

	Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> DX_CommandList::GetDXCMDList() 
	{
		return dx_cmd_list;
	}

	////////////////////////////////////////////////////////////////////////////////////

	ID3D12GraphicsCommandList* DX_CommandList::operator->() 
	{
		return dx_cmd_list.Get();
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::UpdateResource(GPUResource& gpu_resource, const UPDATE_DATA_RAW& update_data) 
	{
		J_D_ASSERT_LOG_ERROR(dynamic_cast<DX_GPUResource*>(&gpu_resource), DX_CommandList, "Failed to cast the GPUResource to target type");

		DX_GPUResource& dx_gpu_resource = static_cast<DX_GPUResource&>(gpu_resource);
		dx_gpu_resource.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_COPY_DEST);
		ID3D12Resource* dx_resource = dx_gpu_resource.GetResource();
		ID3D12Resource* copy_resource = dx_gpu_resource.GetCopyResource();

		D3D12_SUBRESOURCE_DATA vertex_data;
		vertex_data.pData = update_data.data;
		vertex_data.RowPitch = update_data.size;
		vertex_data.SlicePitch = update_data.size;

		UpdateSubresources(dx_cmd_list.Get(), dx_resource, copy_resource, 0, 0, 1, &vertex_data);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::DrawDirect(Ref<Pipeline::PipelineObject>& pl_object, const VERTEX_DIRECT_DATA& vertex_data) 
	{
		Pipeline::DX_PipelineObject* dx_plo = static_cast<Pipeline::DX_PipelineObject*>(pl_object.get());
		Pipeline::DX_RootSignatureObject* dx_rso = static_cast<Pipeline::DX_RootSignatureObject*>(pl_object->GetRootSignature().get());

		dx_cmd_list->SetPipelineState(dx_plo->GetPipelineState().Get());
		dx_cmd_list->SetGraphicsRootSignature(dx_rso->GetRootSignature().Get());
		dx_cmd_list->IASetPrimitiveTopology(D3D12_CONVERSIONS::ConvertPrimitiveTopology(pl_object->GetTopologyType()));

		J_D_ASSERT_LOG_ERROR(dynamic_cast<DX_GPUResource*>(vertex_data.verticesResource),
			DX_CommandList, "An invalid instance of a GPUResource was passed.");

		DX_GPUResource* dx_gpu_resource = static_cast<DX_GPUResource*>(vertex_data.verticesResource);
		ID3D12Resource* v_resource = dx_gpu_resource->GetResource();
		dx_gpu_resource->Transition(dx_cmd_list, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);

		D3D12_VERTEX_BUFFER_VIEW vbv;
		vbv.BufferLocation = v_resource->GetGPUVirtualAddress();
		vbv.SizeInBytes = (UINT)v_resource->GetDesc().Width;
		vbv.StrideInBytes = (UINT)vertex_data.elementSize;
		dx_cmd_list->IASetVertexBuffers(0, 1, &vbv);

		dx_cmd_list->SetGraphicsRoot32BitConstants(0, sizeof(vertex_data.dataBuffer) / 4, &vertex_data.dataBuffer, 0);

		dx_cmd_list->DrawInstanced((UINT)vertex_data.count, 1, (UINT)vertex_data.startLocation, 0);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::DrawIndexed(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_INDEXED_DATA& render_data) 
	{
		Pipeline::DX_PipelineObject* dx_plo = static_cast<Pipeline::DX_PipelineObject*>(pl_object.get());
		Pipeline::DX_RootSignatureObject* dx_rso = static_cast<Pipeline::DX_RootSignatureObject*>(pl_object->GetRootSignature().get());
		command_list_dirty = true;

		dx_cmd_list->SetPipelineState(dx_plo->GetPipelineState().Get());
		dx_cmd_list->SetGraphicsRootSignature(dx_rso->GetRootSignature().Get());
		dx_cmd_list->IASetPrimitiveTopology(D3D12_CONVERSIONS::ConvertPrimitiveTopology(pl_object->GetTopologyType()));

		DX_GPUResource* dx_vertices = static_cast<DX_GPUResource*>(render_data.vertices_resource);
		D3D12_VERTEX_BUFFER_VIEW vbv =
		{ dx_vertices->GetResource()->GetGPUVirtualAddress(), dx_vertices->GetResourceData().width, (UINT)render_data.vertex_element_size };
		dx_cmd_list->IASetVertexBuffers(0, 1, &vbv);

		DX_GPUResource* dx_indices = static_cast<DX_GPUResource*>(render_data.indices_resource);
		D3D12_INDEX_BUFFER_VIEW ibv =
		{ dx_indices->GetResource()->GetGPUVirtualAddress(), dx_indices->GetResourceData().width, DXGI_FORMAT_R32_UINT };
		dx_cmd_list->IASetIndexBuffer(&ibv);

		MVPMatrixConstant mvpUpdateValue;
		Mat4x4 pvmMat = render_data.pvMat * render_data.t.getTRSMatrix();

		mvpUpdateValue.modelRotMatrix = render_data.t.getRotationMatrix();
		mvpUpdateValue.mvpMatrix = pvmMat;

		u32 param_idx = 0;
		dx_cmd_list->SetGraphicsRoot32BitConstants(param_idx++, sizeof(MVPMatrixConstant) / 4, &mvpUpdateValue, 0);
		if (pl_object->GetPipeline() == Pipeline::JIGSAW_PIPELINE::JIGSAW_PIPELINE_COLOR_ID) 
		{
			dx_cmd_list->SetGraphicsRoot32BitConstants(param_idx++, sizeof(RGBA) / 4, &render_data.color, 0);
		}

		for (u32 i = 0; i < ARRAYSIZE(render_data.buffer_refs); i++) 
		{
			if (render_data.buffer_refs[i]) 
			{
				DX_GPUResource* dx_resource = static_cast<DX_GPUResource*>(render_data.buffer_refs[i]);
				dx_resource->Transition(dx_cmd_list, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
				dx_cmd_list->SetGraphicsRootDescriptorTable(param_idx++, dx_resource->GetGPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV));
			}
		}

		Vector4 res = pvmMat * Vector4(render_data.t.position.x, render_data.t.position.y, render_data.t.position.z, 1);

		static size_t index = 1;
		if (render_data.so_buffer) 
		{
			SetBufferOutput(*render_data.so_buffer);
		}

		// Pushing the draw call to the cmd list
		dx_cmd_list->DrawIndexedInstanced(ibv.SizeInBytes / 4, 1, 0, 0, 0);

		if (render_data.so_cpu_copy) 
		{
			this->CopyBuffer(*render_data.so_buffer, *render_data.so_cpu_copy);
			index--;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////

	bool DX_CommandList::HasCommands() 
	{
		return command_list_dirty;
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::ResolveMultiSampledResource(GPUResource& ms_resource, GPUResource& dest_resource) 
	{
		DX_GPUResource& ms_dx_r = static_cast<DX_GPUResource&>(ms_resource);
		DX_GPUResource& dest_dx_r = static_cast<DX_GPUResource&>(dest_resource);

		dest_dx_r.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_RESOLVE_DEST);
		ms_dx_r.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_RESOLVE_SOURCE);
		dx_cmd_list->ResolveSubresource(dest_dx_r.GetResource(), 0, ms_dx_r.GetResource(), 0, ms_dx_r.GetResource()->GetDesc().Format);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::SetRenderViews(Jigsaw::GPUResource& rtv_resource, Jigsaw::GPUResource& dsv_resource) 
	{
		DX_GPUResource& dx_rtv_resource = dynamic_cast<DX_GPUResource&>(rtv_resource);
		DX_GPUResource& dx_dsv_resource = dynamic_cast<DX_GPUResource&>(dsv_resource);

		D3D12_CPU_DESCRIPTOR_HANDLE rtv_handle = dx_rtv_resource.GetCPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
		D3D12_CPU_DESCRIPTOR_HANDLE dsv_handle = dx_dsv_resource.GetCPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);

		dx_cmd_list->OMSetRenderTargets(1, &rtv_handle, FALSE, &dsv_handle);

		dx_rtv_resource.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_RENDER_TARGET);
		dx_dsv_resource.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_DEPTH_WRITE);

		CD3DX12_RECT scissor(0, 0, LONG_MAX, LONG_MAX);
		dx_cmd_list->RSSetScissorRects(1, &scissor);

		D3D12_VIEWPORT viewport;
		viewport.MaxDepth = 1.0f;
		viewport.MinDepth = -1.0f;
		viewport.TopLeftX = 0;
		viewport.TopLeftY = 0;
		viewport.Width = (FLOAT)dx_rtv_resource.GetResourceData().width;
		viewport.Height = (FLOAT)dx_rtv_resource.GetResourceData().height;
		dx_cmd_list->RSSetViewports(1, &viewport);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::SetBufferOutput(Jigsaw::GPUResource& gpu_resource)
	{
		DX_GPUResource& dx_resource = static_cast<DX_GPUResource&>(gpu_resource);
		dx_resource.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_STREAM_OUT);

		ID3D12Resource* dx_r = dx_resource.GetResource();
		D3D12_STREAM_OUTPUT_BUFFER_VIEW so_view;
		so_view.BufferLocation = dx_r->GetGPUVirtualAddress() + 4;
		so_view.BufferFilledSizeLocation = dx_r->GetGPUVirtualAddress();
		so_view.SizeInBytes = dx_r->GetDesc().Width - 4;
		dx_cmd_list->SOSetTargets(0, 1, &so_view);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::CopyBuffer(Jigsaw::GPUResource& source, Jigsaw::GPUResource& dest) 
	{
		DX_GPUResource& dx_source = dynamic_cast<DX_GPUResource&>(source);
		DX_GPUResource& dx_dest = dynamic_cast<DX_GPUResource&>(dest);
		dx_source.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_COPY_SOURCE);
		dx_dest.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_COPY_DEST);
		dx_cmd_list->CopyBufferRegion(dx_dest.GetResource(), 0, dx_source.GetResource(), 0, dx_source.GetResource()->GetDesc().Width);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::ClearResource(Jigsaw::GPUResource& gpu_resource) 
	{
		DX_GPUResource& dx_resource = dynamic_cast<DX_GPUResource&>(gpu_resource);
		switch (dx_resource.clear_value.Format) 
		{
		case DXGI_FORMAT_R32G32B32A32_FLOAT:
		case DXGI_FORMAT_R8G8B8A8_UNORM:
			dx_cmd_list->ClearRenderTargetView(dx_resource.GetCPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE_RTV), dx_resource.clear_value.Color, 0, nullptr);
			break;
		case DXGI_FORMAT_D24_UNORM_S8_UINT:
			dx_cmd_list->ClearDepthStencilView(dx_resource.GetCPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE_DSV), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, dx_resource.clear_value.DepthStencil.Depth, dx_resource.clear_value.DepthStencil.Stencil, 0, nullptr);
			break;
		default:
			J_D_ASSERT_LOG_ERROR(false, DX_CommandList, "Unknown resource format type for clear operation: {0}", dx_resource.clear_value.Format);
			break;
		}
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::SetToDisplay(Jigsaw::GPUResource& gpu_resource) 
	{
		DX_GPUResource& dx_resource = dynamic_cast<DX_GPUResource&>(gpu_resource);

		CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(dx_resource.GetResource(), dx_resource.current_state, D3D12_RESOURCE_STATE_COMMON);
		dx_resource.current_state = D3D12_RESOURCE_STATE_COMMON;

		dx_cmd_list->ResourceBarrier(1, &barrier);
	}

	////////////////////////////////////////////////////////////////////////////////////

	void DX_CommandList::SetToShaderView(Jigsaw::GPUResource& gpu_resource)
	{
		DX_GPUResource& dx_resource = static_cast<DX_GPUResource&>(gpu_resource);

		dx_resource.Transition(dx_cmd_list, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
	}

	////////////////////////////////////////////////////////////////////////////////////
}