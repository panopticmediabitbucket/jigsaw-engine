#ifndef _DX_COMMAND_LIST_H_
#define _DX_COMMAND_LIST_H_

#include "Graphics/CommandList.h"
#include "_jgsw_api.h"

namespace Jigsaw {
	/// <summary>
	/// DX_CommandList converts the Jigsaw-level API components into DX12 command list submissions which must be subsequently 
	/// passed to a command list executor.
	/// </summary>
	class JGSW_API DX_CommandList final : public Jigsaw::CommandList{
	public:
		/// <summary>
		/// Sole constructor. 
		/// </summary>
		/// <param name="dx_cmd_list"></param>
		DX_CommandList(Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>&dx_cmd_list) : dx_cmd_list(dx_cmd_list) {}

		/// <summary>
		/// Implementation for the Jigsaw CommandList interface's "LoadBuffer" method
		/// </summary>
		/// <param name="t_arr"></param>
		/// <param name="t_size"></param>
		/// <param name="t_count"></param>
		/// <param name="buffer_dest"></param>
		void LoadBuffer(const void* t_arr, size_t t_size, size_t t_count, Jigsaw::GPUResource& buffer_dest) override final;

		/// <summary>
		/// Loads the texture from the raw buffer into the destination texture
		/// </summary>
		/// <param name="data"></param>
		/// <param name="num_bytes"></param>
		/// <param name="texture_dest"></param>
		void LoadTexture(const u8* tex_data, size_t bytes_per_row, Jigsaw::GPUResource& texture_dest) override final;

		/// <summary>
		/// files commands for drawing the 'render_data' on the current cmd list
		/// </summary>
		/// <param name="pl_object"></param>
		/// <param name="render_data"></param>
		void DrawIndexed(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_INDEXED_DATA& render_data) override final;

		/// <summary>
		/// DrawDirect does not use an index buffer. The vertices will be drawn from the start index to the count. 
		/// </summary>
		/// <param name="pl_object"></param>
		/// <param name="vertex_data"></param>
		void DrawDirect(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_DIRECT_DATA& vertex_data) override final;

		/// <summary>
		/// 
		/// </summary>
		/// <returns></returns>
		bool HasCommands() override final;

		/// <summary>
		/// Resolves the resource data from the ms_resource into a single-sampled dest_resource
		/// </summary>
		/// <param name="ms_resource"></param>
		/// <param name="dest_resource"></param>
		void ResolveMultiSampledResource(GPUResource& ms_resource, GPUResource& dest_resource) override final;

		/// <summary>
		/// Sets the GPUResources as the render resources.
		/// <param name="rtv_resource"></param>
		/// <param name="dsv_resource"></param>
		/// </summary>
		void SetRenderViews(Jigsaw::GPUResource& rtv_resource, Jigsaw::GPUResource& dsv_resource) override final;

		/// <summary>
		/// Designates the buffer as an output target for the following draw operation.
		/// <param name="gpu_resource"></param>
		/// </summary>
		void SetBufferOutput(Jigsaw::GPUResource& gpu_resource) override;

		/// <summary>
		/// Copies the contents of the source buffer into the destination buffer. 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		void CopyBuffer(Jigsaw::GPUResource& source, Jigsaw::GPUResource& dest) override;

		/// <summary>
		/// Resets the passed in gpu_resource to its original, clear value.
		/// <param name="gpu_resource"></param>
		/// </summary>
		void ClearResource(Jigsaw::GPUResource& gpu_resource) override;

		/// <summary>
		/// Sets the GPUResources into display mode.
		/// <param name="rtv_resource"></param>
		/// <param name="dsv_resource"></param>
		/// </summary>
		void SetToDisplay(Jigsaw::GPUResource& gpu_resource) override;

		/// <summary>
		///  Transitions the resource for use in Shaders
		/// </summary>
		/// <param name="gpu_resource"></param>
		void SetToShaderView(Jigsaw::GPUResource& gpu_resource) override;

		/// <summary>
		/// Returns a ComPtr to the underlying ID3D12GraphicsCommandList interface, but destroys the local reference to the
		/// dx_cmd_list. This command effectively nullifies the underlying DX_CommandList object
		/// </summary>
		/// <returns></returns>
		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> GetDXCMDList();

		ID3D12GraphicsCommandList* operator->();

		/// <summary>
		/// Updates the resource with the 'update_data' passed in
		/// </summary>
		/// <param name="gpu_resource"></param>
		/// <param name="update_data"></param>
		void UpdateResource(GPUResource& gpu_resource, const UPDATE_DATA_RAW& update_data) override final;

	private:

		bool command_list_dirty = false;

		Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList> dx_cmd_list;

	};
}

#endif // !_DX_COMMAND_LIST_H_
