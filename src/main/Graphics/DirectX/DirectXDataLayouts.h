#pragma once
#include <Directxmath.h>
#include "Math/LinAlg.h"

typedef struct _mvpMatrixConstant {
	Mat4x4 modelRotMatrix;
	Mat4x4 mvpMatrix;
} MVPMatrixConstant;

/// <summary>
/// Used in cases where there is no 'model' to transition--the vertices are already expressed in world space
/// </summary>
typedef struct _primitiveProjectionDataBuffer {
	Mat4x4 pvMat;
//-----------------------------
	Vector3 vertOffset;
	float globalOpacityMultiplier = 1.0f;
//-----------------------------
	Vector3 cameraPosition;
	float fullVisibleRSqrd = 0.0f;
//-----------------------------
	float alphaCoefficients[3]{ 0.0f, 0.0f, 0.0f };
	float invisibleRSqrd = 0.0f;
//-----------------------------
} PrimitiveProjectionDataBuffer;

typedef struct _vertexPositionAndColorAndNormal {
	Vector3 position;
	Vector3 color;
	Vector3 normal;
} PositionColorNormal;

typedef struct _worldLight {
	int type;
	float intensity;
	float angle;
	const float padding = 0;

	Vector4 position;
	Vector4 direction;
	Vector4 color;
} WorldLight;

typedef struct _worldLightDefBlock {
	WorldLight lights[4];
} WorldLightDefBlock;
