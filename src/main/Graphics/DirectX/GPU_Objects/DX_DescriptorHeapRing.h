/*********************************************************************
 * DX_DescriptorHeapRing.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: May 2021
 *********************************************************************/
#ifndef _DX_DESCRIPTOR_HEAP_RING_H_
#define _DX_DESCRIPTOR_HEAP_RING_H_

#include "Graphics/__graphics_api.h"
#include "Graphics/DirectX/GPU_Objects/DX_GPUResource.h"
#include "_jgsw_api.h"

namespace Jigsaw {

	/// <summary>
	/// Basic utility for maintaining a pointer to an available space in a descriptor heap. The actual descriptors are written to the Resources themselves.
	/// </summary>
	class JGSW_API DX_DescriptorHeapRing {
	public:
		/// <summary>
		/// The constructor will take the dx_device as an argument so that the underlying heap can be constructed.
		/// <param name="heap_type"></param>
		/// <param name="heap_size"></param>
		/// <param name="dx_device"></param>
		/// </summary>
		DX_DescriptorHeapRing(D3D12_DESCRIPTOR_HEAP_DESC heap_desc, size_t heap_size, const Microsoft::WRL::ComPtr<ID3D12Device2>& dx_device);

		/// <summary>
		/// Not copyable.
		/// <param name="other"></param>
		/// </summary>
		DX_DescriptorHeapRing(const DX_DescriptorHeapRing& other) = delete;

		/// <summary>
		/// Not movable.
		/// <param name="other"></param>
		/// </summary>
		DX_DescriptorHeapRing(DX_DescriptorHeapRing&& other) = delete;

		~DX_DescriptorHeapRing();

		/// <summary>
		/// Returns the index of the view that was overwritten in the Ring buffer. If the write fails, returns -1
		/// <param name="dx_resource"></param>
		/// <returns></returns>
		/// </summary>
		int WriteResource(Jigsaw::DX_GPUResource* dx_resource, const XD3D12_VIEW_DESC& view_desc);

		/// <summary>
		/// Iterates the underlying pointer without writing anything.
		/// </summary>
		void ClaimNext();

		/// <summary>
		/// Returns the underlying heap for more specific manipulations.
		/// <returns></returns>
		/// </summary>
		ID3D12DescriptorHeap* GetHeap();

	private:
		/// <summary>
		/// Iterates the underlying pointer to the next heap position.
		/// <returns></returns>
		/// </summary>
		u32 Iterate();

		Microsoft::WRL::ComPtr<ID3D12DescriptorHeap> heap;
		const Microsoft::WRL::ComPtr<ID3D12Device2> dx_device;

		Jigsaw::GPUResource** heap_positions;
		D3D12_DESCRIPTOR_HEAP_DESC heap_desc;
		u32 size = 0;
		u32 index = 0;

	};
}
#endif