#include "DX_DescriptorHeapRing.h"
#include <assert.h>
#include "Debug/j_debug.h"
#include <string>
#include <comdef.h>

#define PRINT_HRESULT_ERRORS_THROW(prestring, hresult) \
	if(FAILED(hresult)) { \
		std::string output = prestring + std::string(_com_error(hresult).ErrorMessage());\
		J_LOG_ERROR(Jigsaw::DX_DescriptorHeapRing, output.c_str());\
		throw hresult; \
	} 

namespace Jigsaw 
{

	//////////////////////////////////////////////////////////////////////////

	DX_DescriptorHeapRing::DX_DescriptorHeapRing(D3D12_DESCRIPTOR_HEAP_DESC heap_desc, size_t heap_size, const Microsoft::WRL::ComPtr<ID3D12Device2>& dx_device)
		: heap_desc(heap_desc), size((u32)heap_size), dx_device(dx_device), heap_positions(new GPUResource*[heap_size])
	{
		memset(heap_positions, 0, sizeof(GPUResource*) * heap_size);
		HRESULT result = dx_device->CreateDescriptorHeap(&this->heap_desc, IID_PPV_ARGS(&heap));
		PRINT_HRESULT_ERRORS_THROW("Failed to create descriptor heap: ", result);
	}

	//////////////////////////////////////////////////////////////////////////

	DX_DescriptorHeapRing::~DX_DescriptorHeapRing()
	{
		delete[] heap_positions;
	}

	//////////////////////////////////////////////////////////////////////////

	int DX_DescriptorHeapRing::WriteResource(Jigsaw::DX_GPUResource* dx_resource, const XD3D12_VIEW_DESC& view_desc)
	{
		XD3D12_CPU_DESCRIPTOR_HANDLE cpu_handle(heap->GetCPUDescriptorHandleForHeapStart(), dx_device, heap_desc.Type);
		XD3D12_GPU_DESCRIPTOR_HANDLE gpu_handle(heap->GetGPUDescriptorHandleForHeapStart(), dx_device, heap_desc.Type);

		u32 next_index = Iterate();
		dx_resource->CreateResourceView(dx_device, view_desc, cpu_handle.get(next_index), gpu_handle.get(next_index));
		heap_positions[(u32)next_index] = dx_resource;
		return (int)next_index;
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_DescriptorHeapRing::ClaimNext()
	{
		Iterate();
	}

	//////////////////////////////////////////////////////////////////////////

	ID3D12DescriptorHeap* DX_DescriptorHeapRing::GetHeap()
	{
		return this->heap.Get();
	}

	//////////////////////////////////////////////////////////////////////////

	u32 DX_DescriptorHeapRing::Iterate()
	{
		u32 ret = index++;
		if (index >= size)
		{
			index = 0;
			J_LOG_WARN(DX_DescriptorHeapRing, "Capacity reached on ring buffer");
		}
		return ret;
	}

	//////////////////////////////////////////////////////////////////////////
}
