/*********************************************************************
 * DX_GPUResource.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: May 2021
 *********************************************************************/
#ifndef _DX_GPU_RESOURCE_H_
#define _DX_GPU_RESOURCE_H_

#include "Graphics/GPU_Objects/GPUResource.h"
#include "Graphics/DirectX/__graphics_dx.h"
#include "Debug/j_debug.h"
#include "_jgsw_api.h"
#include "xd3d12.h"
#include <map>

namespace Jigsaw {

	/// <summary>
	/// Direct X implementation of the GPUResource.
	/// </summary>
	class JGSW_API DX_GPUResource final : public GPUResource
	{
	public:
		/// <summary>
		/// The DX_GPUResource is constructed with already-established information on the DX_GPUResource. 
		/// </summary>
		/// <param name="dx_resource"></param>
		/// <param name="clear_value"></param>
		/// <param name="args"></param>
		/// <param name="initial_state"></param>
		DX_GPUResource(const Microsoft::WRL::ComPtr<ID3D12Resource>&dx_resource, D3D12_CLEAR_VALUE clear_value, JGSW_GPU_RESOURCE_DATA args, D3D12_RESOURCE_STATES initial_state);

		/// <summary>
		/// Samples the underlying resource with the 'JGSW_BOX' parameters and outputs to 'dest' in row-column-depth order. Calls the 'ReadFromSubresource' method.
		/// 
		/// It must be ensured that the the resource was constructed in a CPU-readable state initially (L1 memory pool, etc.)
		/// </summary>
		/// <param name="dest"></param>
		/// <param name="src_subresource"></param>
		/// <param name="box"></param>
		void SampleSubresource(void* dest, UINT src_subresource, JGSW_BOX box) override;

		/// <returns>A GPU-visible pointer for the specified visibility (generally SHADER)</returns>
		virtual GpuPtr GetGpuPointer(JGSW_VISIBILITY_ visibility = JGSW_VISIBILITY::SHADER) const;

		/// <summary>
		/// Returns the underlying resource for manipulation.
		/// <returns></returns>
		/// </summary>
		ID3D12Resource* GetResource();

		/// <summary>
		/// Returns the copy resource assigned to this DX_GPUResource. 
		/// </summary>
		/// <returns></returns>
		ID3D12Resource* GetCopyResource();

		/// <summary>
		/// Sets the copy resource internally
		/// </summary>
		void SetCopyResource(const Microsoft::WRL::ComPtr<ID3D12Resource>& copy_resource);

		/// <summary>
		/// Releases the underlying resource. 
		/// </summary>
		void Reset();

		/// <summary>
		/// Rebinds the DX_GPUResource to this D3D12 Resource. This will attempt to recreate any associated views. 
		/// </summary>
		/// <param name="resource"></param>
		void Rebind(const Microsoft::WRL::ComPtr<ID3D12Device2>& dx_device, const Microsoft::WRL::ComPtr<ID3D12Resource>& resource);

		void Map(size_t size, float** dest);

		/// <summary>
		/// Transitions the resource from its current state to the 'after_state' if the states do not match
		/// </summary>
		/// <note> NOT THREAD SAFE </note>
		/// <param name="dx_cmd_list"></param>
		/// <param name="after_state"></param>
		void Transition(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& dx_cmd_list, D3D12_RESOURCE_STATES after_state);

		/// <summary>
		/// Returns the underlying resource for manipulation.
		/// <returns></returns>
		/// </summary>
		ID3D12Resource* operator->();

		/// <summary>
		/// Assigns a view of the resource to the passed in 'descriptor_handle' arguments. The caller must ensure there are no collisions at the incoming handles. 
		/// </summary>
		/// <param name="dx_device"></param>
		/// <param name="view_desc"></param>
		/// <param name="target_descriptor_handle"></param>
		/// <param name="gpu_descriptor_handle"></param>
		void CreateResourceView(Microsoft::WRL::ComPtr<ID3D12Device2> dx_device, const XD3D12_VIEW_DESC& view_desc, XD3D12_CPU_DESCRIPTOR_HANDLE& target_descriptor_handle, XD3D12_GPU_DESCRIPTOR_HANDLE& gpu_descriptor_handle);

		/// <summary>
		/// Returns a CPU-accessible view associated with the heap_type. Does not transition the resource. 
		/// </summary>
		/// <param name="heap_type"></param>
		/// <returns></returns>
		D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE heap_type);

		/// <summary>
		/// Returns a GPU-accessible view associated with the heap_type. Does not transition the resource. 
		/// </summary>
		/// <param name="heap_type"></param>
		/// <returns></returns>
		D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE heap_type);

		D3D12_RESOURCE_STATES current_state;

		const D3D12_CLEAR_VALUE clear_value;

	private:
		std::string resource_name;
		std::map<D3D12_DESCRIPTOR_HEAP_TYPE, D3D12_CPU_DESCRIPTOR_HANDLE> cpu_view_map;
		std::map<D3D12_DESCRIPTOR_HEAP_TYPE, D3D12_GPU_DESCRIPTOR_HANDLE> gpu_view_map;

		Microsoft::WRL::ComPtr<ID3D12Resource> dx_resource;

		// optional resource for GPU_Resources with the JGSW_GPU_RESOURCE_FLAG_UPDATABLE
		Microsoft::WRL::ComPtr<ID3D12Resource> copy_resource;

	};

	//////////////////////////////////////////////////////////////////////////

	inline ID3D12Resource* DX_GPUResource::GetResource() {
		J_D_ASSERT_LOG_ERROR(dx_resource.Get(), DX_GPUResource, "The dx resource does not exist on the requested GPUResource");
		return dx_resource.Get();
	}

	//////////////////////////////////////////////////////////////////////////

	inline ID3D12Resource* DX_GPUResource::GetCopyResource() {
		J_D_ASSERT_LOG_ERROR(copy_resource.Get(), DX_GPUResource, "The copy resource does not exist on the requested GPUResource");
		return copy_resource.Get();
	}

	//////////////////////////////////////////////////////////////////////////

	inline void DX_GPUResource::SetCopyResource(const Microsoft::WRL::ComPtr<ID3D12Resource>& copy_resource) {
		this->copy_resource = copy_resource;
	}

	//////////////////////////////////////////////////////////////////////////
}
#endif