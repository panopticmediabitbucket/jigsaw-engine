#include "DX_GPUResource.h"
#include "Debug/j_debug.h"
#include "xd3d12.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	DX_GPUResource::DX_GPUResource(const Microsoft::WRL::ComPtr<ID3D12Resource>& dx_resource, D3D12_CLEAR_VALUE clear_value, JGSW_GPU_RESOURCE_DATA data, D3D12_RESOURCE_STATES initial_state)
		: dx_resource(dx_resource), clear_value(clear_value), current_state(initial_state), GPUResource(data)
	{
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_GPUResource::SampleSubresource(void* dest, UINT src_subresource, JGSW_BOX box)
	{
		D3D12_RESOURCE_DESC resource_desc = dx_resource->GetDesc();
		dx_resource->ReadFromSubresource(dest, (UINT)resource_desc.Width, (UINT)resource_desc.DepthOrArraySize, (UINT)src_subresource, reinterpret_cast<D3D12_BOX*>(&box));
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::GpuPtr DX_GPUResource::GetGpuPointer(JGSW_VISIBILITY_ visibility /*= JGSW_VISIBILITY::SHADER*/) const
	{
		if (visibility == JGSW_VISIBILITY::SHADER)
		{
			return gpu_view_map.at(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV).ptr;
		}

		J_LOG_ERROR(DX_GPUResource, "Requested a GpuPointer for unknown visibility: {0}", visibility);

		return 0;
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_GPUResource::Reset()
	{
		dx_resource.Reset();
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_GPUResource::Rebind(const Microsoft::WRL::ComPtr<ID3D12Device2>& dx_device, const Microsoft::WRL::ComPtr<ID3D12Resource>& resource)
	{
		this->dx_resource = resource;

		std::map<D3D12_DESCRIPTOR_HEAP_TYPE, D3D12_CPU_DESCRIPTOR_HANDLE> cpu_view_map = this->cpu_view_map;
		std::map<D3D12_DESCRIPTOR_HEAP_TYPE, D3D12_GPU_DESCRIPTOR_HANDLE> gpu_view_map = this->gpu_view_map;
		auto cpu_iter = cpu_view_map.begin();
		auto gpu_iter = gpu_view_map.begin();
		this->cpu_view_map.clear();
		this->gpu_view_map.clear();
		while (cpu_iter != cpu_view_map.end())
		{
			J_D_ASSERT_LOG_ERROR(cpu_iter->first == gpu_iter->first, DX_GPUResource, "The entries of the cpu_view_map and gpu_view_map do not match during Rebind operation");
			XD3D12_CPU_DESCRIPTOR_HANDLE cpu_handle(cpu_iter->second, dx_device, cpu_iter->first);
			XD3D12_GPU_DESCRIPTOR_HANDLE gpu_handle(gpu_iter->second, dx_device, gpu_iter->first);
			CreateResourceView(dx_device, { nullptr }, cpu_handle, gpu_handle);
			cpu_iter++;
			gpu_iter++;
		}

		D3D12_RESOURCE_DESC desc = resource->GetDesc();
		resource_args.width = desc.Width;
		resource_args.height = desc.Height;
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_GPUResource::Map(size_t size, float** dest)
	{
		D3D12_RANGE range;
		range.Begin = 0;
		range.End = 3000;
		dx_resource->Map(0, &range, (void**)dest);
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_GPUResource::Transition(const Microsoft::WRL::ComPtr<ID3D12GraphicsCommandList>& dx_cmd_list, D3D12_RESOURCE_STATES after_state) {
		if (current_state != after_state) {
			CD3DX12_RESOURCE_BARRIER barrier = CD3DX12_RESOURCE_BARRIER::Transition(this->dx_resource.Get(), current_state, after_state);
			this->current_state = after_state;
			dx_cmd_list->ResourceBarrier(1, &barrier);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	ID3D12Resource* DX_GPUResource::operator->() {
		return GetResource();
	}

	//////////////////////////////////////////////////////////////////////////

	void DX_GPUResource::CreateResourceView(Microsoft::WRL::ComPtr<ID3D12Device2> dx_device, const XD3D12_VIEW_DESC& view_desc, XD3D12_CPU_DESCRIPTOR_HANDLE& target_descriptor_handle, XD3D12_GPU_DESCRIPTOR_HANDLE& gpu_descriptor_handle)
	{
		J_D_ASSERT_LOG_ERROR(cpu_view_map.find(target_descriptor_handle.GetHeapType()) == cpu_view_map.end(), DX_GPUResource, "Attempted to Create Resource View for GPU Resource {0} of descriptor heap type {1}, which already exists in the view map", "", target_descriptor_handle.GetHeapType());

		cpu_view_map.insert(std::make_pair(target_descriptor_handle.GetHeapType(), target_descriptor_handle));
		gpu_view_map.insert(std::make_pair(target_descriptor_handle.GetHeapType(), gpu_descriptor_handle));

		switch (target_descriptor_handle.GetHeapType()) 
		{
		case D3D12_DESCRIPTOR_HEAP_TYPE_RTV:
			dx_device->CreateRenderTargetView(dx_resource.Get(), view_desc.rtv_desc, target_descriptor_handle);
			break;
		case D3D12_DESCRIPTOR_HEAP_TYPE_DSV:
			dx_device->CreateDepthStencilView(dx_resource.Get(), view_desc.dsv_desc, target_descriptor_handle);
			break;
		case D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV:
			dx_device->CreateShaderResourceView(dx_resource.Get(), view_desc.srv_desc, target_descriptor_handle);
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	D3D12_CPU_DESCRIPTOR_HANDLE DX_GPUResource::GetCPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE heap_type)
	{
		J_D_ASSERT_LOG_ERROR(cpu_view_map.find(heap_type) != cpu_view_map.end(), DX_GPUResource, "Attempted to fetch CPU Resource View for GPU Resource {0} of descriptor heap type {1}, which does not exist in the view map", "", heap_type);
		return cpu_view_map.at(heap_type);
	}

	//////////////////////////////////////////////////////////////////////////

	D3D12_GPU_DESCRIPTOR_HANDLE DX_GPUResource::GetGPUDescriptorHandleForHeapType(D3D12_DESCRIPTOR_HEAP_TYPE heap_type)
	{
		J_D_ASSERT_LOG_ERROR(gpu_view_map.find(heap_type) != gpu_view_map.end(), DX_GPUResource, "Attempted to fetch GPU Resource View for GPU Resource {0} of descriptor heap type {1}, which does not exist in the view map", "", heap_type);
		return gpu_view_map.at(heap_type);
	}

	//////////////////////////////////////////////////////////////////////////

}