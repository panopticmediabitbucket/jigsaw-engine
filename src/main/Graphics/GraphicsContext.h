/*********************************************************************
 * RenderContext.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2020
 *********************************************************************/
#ifndef _RENDER_CONTEXT_H_
#define _RENDER_CONTEXT_H_

#include "Graphics/__graphics_api.h"
#include "Graphics/GPU_Objects/J_Viewport.h"
#include "Graphics/GPU_Objects/JGSW_GPU_RESOURCE_DATA.h"
#include "Ref.h"
#include "CommandList.h"
#include "CommandListExecutor.h"

namespace Jigsaw 
{
	/// <summary>
	/// A non-API specific GraphicsContext that provides virtual handles to all API-related functions. Inheriting classes provide
	/// API-specific implementations of the GraphicsContext (DX_RenderContext, etc.)
	/// </summary>
	class JGSW_API GraphicsContext 
	{
	public:
		GraphicsContext();

		/// <summary>
		/// An instance of the GraphicsContext is required to call the CreateWindowSwapchain function. In response to resizing events or upon creation of the Application,
		/// there is a need to define the swapchain and the depth stencil backbuffers.
		/// </summary>
		/// <param name="hWnd"></param>
		virtual void CreateWindowSwapchain(const HWND hWnd, UINT width, UINT height) = 0;

		/// <summary>
		/// Used for creating a new GPUResource from scratch, perhaps for a RenderTarget or something else.
		/// <param name="resource_args"></param>
		/// <returns></returns>
		/// </summary>
		virtual Jigsaw::GPUResource* CreateGPUResource(JGSW_GPU_RESOURCE_DATA resource_args) = 0;

		/// <summary>
		/// An instance of GraphicsContext is required to call the GetCommandList function. The passed in 'type' indicates to the GraphicsContext if we're looking for
		/// a command list that's optimized for loading or direct execution of draw commands, among other operations.
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		virtual Jigsaw::Ref<Jigsaw::CommandList> GetCommandList(JGSW_COMMAND_TYPE type) = 0;

		/// <summary>
		/// An instance is required. Returns a CommandListExecutor object that can be used to submit commands of the target 'type.'
		/// </summary>
		/// <param name=""></param>
		/// <returns></returns>
		virtual Jigsaw::Ref<Jigsaw::CommandListExecutor> GetCommandListExecutor(JGSW_COMMAND_TYPE type) = 0;

		/// <summary>
		/// Returns the primary viewport associated with the underlying GraphicsContext. The GraphicsContext owns the only non-const version of this, allowing it to make modifications
		/// when necessary.
		/// <returns></returns>
		/// </summary>
		virtual Jigsaw::J_Viewport* GetViewport() = 0;

		/// <summary>
		/// The final call at the end of any given frame is to 'DisplayFrame()' after all of the CommandLists have been passed to their
		/// Respective CommandListExecutors.
		/// </summary>
		virtual void DisplayFrame() = 0;

		/// <summary>
		/// Enqueues a load operation on the given command list to use the designated shader resources.
		/// <param name="command_list"></param>
		/// </summary>
		virtual void LoadShaderResources(const Jigsaw::Ref<Jigsaw::CommandList>& command_list) = 0;

	};
}
#endif