/*********************************************************************
 * VERTEX_INDEXED_DATA.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: June 2021
 *********************************************************************/
#ifndef _RENDER_DATA_H_
#define _RENDER_DATA_H_

// Jigsaw_Engine
#include "DirectX/DirectXDataLayouts.h"
#include "GPU_Objects/GPUResource.h"
#include "Physics/Transform.h"
#include "Util/COLOR.h"

namespace Jigsaw 
{
	struct JGSW_API VERTEX_INDEXED_DATA 
	{
		GPUResource* vertices_resource;
		GPUResource* indices_resource;
		GPUResource* so_buffer;
		GPUResource* so_cpu_copy;
		size_t vertex_element_size = 0;

		Transform t;
		Mat4x4 pvMat;
		RGBA color;
		GPUResource* buffer_refs[5];
	};

	struct JGSW_API VERTEX_DIRECT_DATA 
	{
		GPUResource* verticesResource;
		size_t count = 0;
		size_t startLocation = 0;
		size_t elementSize = 0;

		PrimitiveProjectionDataBuffer dataBuffer;
	};

	/// <summary>
	/// Arguments for updating a Resource on a render pipeline/RenderJob
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template <typename T>
	struct JGSW_API UPDATE_DATA 
	{
		T* data = nullptr;
		size_t size = 0;
	};

	/// <summary>
	/// A struct that encapsulates an untyped data for transition
	/// </summary>
	struct JGSW_API UPDATE_DATA_RAW 
	{
		void* data = nullptr;
		size_t size = 0;
	};
}
#endif