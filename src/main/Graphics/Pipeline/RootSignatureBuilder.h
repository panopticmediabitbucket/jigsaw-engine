#pragma once
#include "Assets/dlAssetRegistrar.h"
#include "Graphics/__graphics_api_context.h"

namespace Jigsaw {
	namespace Pipeline {
		/// <summary>
		/// Builder interface for compiling RootSignatures. Unlike some other builders, the order in which some of these calls are made is important.
		/// The 'SetNextShaderVisibility' call will have an impact on subsequent 'Push' calls. 
		/// </summary>
		class RootSignatureBuilder {
		public:
			RootSignatureBuilder(); 

			CANT_MOVE(RootSignatureBuilder);
			CANT_COPY(RootSignatureBuilder);

			/// <summary>
			/// Indicates the visibility of the subsequent 'Push' calls to the root signature description. 
			/// 
			/// Any encountered Shader visibilities will automatically adjust the Root Signature flags to enable access to the root signature for the corresponding shader type
			/// </summary>
			/// <param name="visibility"></param>
			/// <returns></returns>
			virtual RootSignatureBuilder* SetNextShaderVisiblity(const JGSW_SHADER visibility) = 0;

			/// <summary>
			/// Pushes a 32-bit constant buffer description at the specified buffer index. 
			/// </summary>
			/// <param name="register_number"></param>
			/// <param name="no_32_bit_params"></param>
			/// <returns></returns>
			virtual RootSignatureBuilder* Push32BitConstantArgument(u32 register_number, size_t no_32_bit_params) = 0;

			/// <summary>
			/// Pushes a resource-reference argument for the specified register number. This could be a sampler, texture, or a buffer.
			/// </summary>
			/// <param name="register_number"></param>
			/// <returns></returns>
			virtual RootSignatureBuilder* PushTextureReferenceArgument(u32 register_number) = 0;

			/// <summary>
			/// Includes the specified property in the RootSignature definition
			/// </summary>
			/// <param name="property"></param>
			/// <returns></returns>
			virtual RootSignatureBuilder* AddProperty(const JGSW_ROOT_SIGNATURE_PROPERTY property) = 0;

			/// <summary>
			/// Registers the size of the given template parameter as the next value to be interpreted in the root signature as a series of 32-bit constants
			/// The visibility of the given parameter will be determined by the most recent call to 'SetNextShaderVisibility'
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <param name="register_number"></param>
			/// <returns></returns>
			template <typename T>
			inline RootSignatureBuilder* PushStructAs32BitConstant(const UINT register_number) {
				size_t obj_size = sizeof(T);
				size_t no_32_bit_constants = obj_size / 4;
				return Push32BitConstantArgument(register_number, no_32_bit_constants);
			}

			/// <summary>
			/// Completes the construction and builds a RootSignatureResource for use in the pipeline
			/// </summary>
			/// <param name="render_context"></param>
			/// <returns></returns>
			virtual Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject> Build(Jigsaw::GraphicsContext* render_context) = 0;

		};
	}
}
