#ifndef _PIPELINE_BUILDER_PROVIDER_H_
#define _PIPELINE_BUILDER_PROVIDER_H_

#include "Ref.h"
#include "Graphics/Pipeline/PipelineObjectBuilder.h"
#include "Graphics/Pipeline/RootSignatureBuilder.h"

namespace Jigsaw {
	namespace Pipeline{

		/// <summary>
		/// A simple interface that will provide a builder that is specific to the active graphics API for PipelineObjects and RootSignatureObjects.
		/// </summary>
		class PipelineBuilderProvider {
		public:

			/// <summary>
			/// Fetch a scoped PipelineObjectBuilder specific to the active graphics API (DX, GL, etc.)
			/// </summary>
			/// <param name="pipeline"></param>
			/// <returns></returns>
			Jigsaw::Unique<Jigsaw::Pipeline::PipelineObjectBuilder> GetPipelineBuilder(JIGSAW_PIPELINE pipeline);

			/// <summary>
			/// Fetch a scoped RootSignatureObjectBuilder specific to the active graphics API (DX, GL, etc.)
			/// </summary>
			/// <param name="pipeline"></param>
			/// <returns></returns>
			Jigsaw::Unique<Jigsaw::Pipeline::RootSignatureBuilder> GetRootSignatureBuilder();

		};

	}
}

#endif