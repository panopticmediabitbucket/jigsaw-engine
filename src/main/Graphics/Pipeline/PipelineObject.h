#ifndef _PIPELINE_OBJECT_H_
#define _PIPELINE_OBJECT_H_

#include "Graphics/__graphics_api.h"
#include "Assets/RuntimeAssets.h"
#include "_jgsw_api.h"
#include "Ref.h"
#include "Graphics/GPU_Objects/JGSW_GPU_RESOURCE_DATA.h"
#include "Graphics/Pipeline/JigsawPipeline.h"
#include "RootSignatureObject.h"

namespace Jigsaw {
	namespace Pipeline {
		/// <summary>
		/// Interface for PipelineObjects. The RootSignature is also associated
		/// </summary>
		class PipelineObject {
		public:
			PipelineObject(Pipeline::JIGSAW_PIPELINE pipeline, const Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject>& rs_object, JGSW_TOPOLOGY_TYPE topology_type)
				: m_pipeline(pipeline), m_rsObject(rs_object), m_topologyType(topology_type) {}

			/// <summary>
			/// Returns the JGSW_TOPOLOGY_TYPE associated with the pipeline
			/// </summary>
			/// <returns></returns>
			JGSW_TOPOLOGY_TYPE GetTopologyType() const;

			/// <summary>
			/// Returns the JGSW_PIPELINE designation associated with the pipeline object
			/// </summary>
			/// <returns></returns>
			Pipeline::JIGSAW_PIPELINE GetPipeline() const;

			const Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject>& GetRootSignature() const;

		protected:

			Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject> m_rsObject;
			JGSW_TOPOLOGY_TYPE m_topologyType;
			Pipeline::JIGSAW_PIPELINE m_pipeline;

		};


		inline JGSW_TOPOLOGY_TYPE PipelineObject::GetTopologyType() const {
			return m_topologyType;
		}

		inline const Ref<RootSignatureObject>& PipelineObject::GetRootSignature() const {
			return m_rsObject;
		}

		inline Pipeline::JIGSAW_PIPELINE PipelineObject::GetPipeline() const {
			return m_pipeline;
		}
	}
}

#endif