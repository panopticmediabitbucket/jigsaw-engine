/*********************************************************************
 * PipelineService.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2020
 *********************************************************************/
#ifndef _PIPELINE_SERVICE_H_
#define _PIPELINE_SERVICE_H_

// Jigsaw_Engine
#include "Graphics/GraphicsContext.h"
#include "RootSignatureBuilder.h"
#include "Assets/DataAssets.h"
#include "JigsawPipeline.h"
#include "Injection/JigsawInjection.h"
#include "Graphics/Pipeline/PipelineObject.h"

#define JGSW_PIPELINE_DRAW_UID_STRING "f3a844dd-07bb-4791-8542-693efcad11d0"
#define JGSW_PIPELINE_DRAW_UID UIDFromString(JGSW_PIPELINE_DRAW_UID_STRING)

namespace Jigsaw {
	namespace Orchestration {
		class orOrchestrator;
	}
}

namespace Jigsaw {
	namespace Pipeline {

		/// <summary>
		/// The PipelineServiceManager is in charge of delivering PipelineStateResources, Shaders, Root signature objects,
		/// and any other graphics-pipeline-related tools needed by the orOrchestrator or a renderer to execute a draw function on the GPU
		///
		/// This class requires resources allocated by the DX_Context and passed by the orOrchestrator to initialize PipelineStateResources, ShaderResources, 
		/// Input layouts, rasterization stages, stenciling pipelines and more.
		/// </summary>
		class JGSW_API PipelineServiceManager : public jsSlots<PipelineServiceManager> {
		public:
			/// <summary>
			/// Creates the PipelineServiceManager instance. An error will be generated if it already exists.
			/// </summary>
			/// <returns></returns>
			static PipelineServiceManager* InitializePipelineService(GraphicsContext* render_context);

			/// <summary>
			/// The 'GetPipeline' function is publicly available. It is assumed the PiplineServiceManager is initialized.
			/// This will return a PipelineStateResource that can be used to initialize the PS/RS-related steps of the GraphicsCommandList
			/// </summary>
			/// <param name="pipeline"></param>
			/// <returns></returns>
			static const Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& GetPipeline(JIGSAW_PIPELINE pipeline);

		private:
			/// <summary>
			/// Retrieves the PipelineServiceManager instance. Creates one if it does not exist.
			/// </summary>
			/// <returns></returns>
			static const PipelineServiceManager* GetPipelineService();

			/// <summary>
			/// Initializes any predefined pipelines using the RootSignatureBuilders and PipelineObjectBuilders.
			/// </summary>
			bool InitializePipelines(Jigsaw::GraphicsContext* render_context);

			PipelineServiceManager();

			std::map<JIGSAW_PIPELINE, Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>> pipelines;
			std::map<JIGSAW_PIPELINE, Jigsaw::Ref<Jigsaw::ShaderResource>> shaders;

			bool m_initialized = false;

		};
	}
}
#endif // _PIPELINE_SERVICE_H_
