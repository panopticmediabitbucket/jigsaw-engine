#include "PipelineService.h"
#include "Graphics/Pipeline/PipelineObjectBuilder.h"
#include "Graphics/__graphics_api_context.h"
#include "Assets/dlAssetRegistrar.h"
#include "PipelineBuilderProvider.h"
#include "Util/COLOR.h"

namespace Jigsaw
{
	namespace Pipeline
	{

		//////////////////////////////////////////////////////////////////////////

		PipelineServiceManager* PipelineServiceManager::InitializePipelineService(GraphicsContext* render_context) 
		{
			PipelineServiceManager* instance = const_cast<PipelineServiceManager*>(GetPipelineService());

			J_D_ASSERT_LOG_ERROR(!(instance->m_initialized), PipelineServiceManager, "Tried to initialize an already-initialized PipelineService");

			instance->InitializePipelines(render_context);
			instance->m_initialized = true;
			return instance;
		}

		//////////////////////////////////////////////////////////////////////////

		const PipelineServiceManager* PipelineServiceManager::GetPipelineService()
		{
			static PipelineServiceManager instance;
			return &instance;
		}

		//////////////////////////////////////////////////////////////////////////

		const Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& PipelineServiceManager::GetPipeline(JIGSAW_PIPELINE pipeline)
		{
			auto instance = GetPipelineService();

			J_D_ASSERT_LOG_ERROR(instance->m_initialized, PipelineServiceManager, "The PipelineServiceManager has not been initialized before attempting to be used");

			auto iter = instance->pipelines.find(pipeline);
			J_D_ASSERT_LOG_ERROR(iter != GetPipelineService()->pipelines.end(), PipelineServiceManager, "Requested a pipeline {0} that does not exist", pipeline);
			return GetPipelineService()->pipelines.at(pipeline);
		}

		//////////////////////////////////////////////////////////////////////////

		bool PipelineServiceManager::InitializePipelines(Jigsaw::GraphicsContext* render_context)
		{
			using namespace Jigsaw::Assets;

			THREAD_LOCAL_SYSTEM_RESOURCES sys_resources;

			hash_val shader_resource_hash = sysHashString("ShaderResource").Value();
			Unique<dlAssetRegistrar> asset_manager = Jigsaw::MakeUnique<Jigsaw::dlAssetRegistrar>();
			Ref<ShaderResource> vert_shader_resource = std::dynamic_pointer_cast<ShaderResource>(asset_manager->FetchAsset("BasicVertex.cso", shader_resource_hash, sys_resources));
			Ref<ShaderResource> skinning_shader_resource = std::dynamic_pointer_cast<ShaderResource>(asset_manager->FetchAsset("DualLinearBlend_VertexSkinning.cso", shader_resource_hash, sys_resources));
			Ref<ShaderResource>	pix_shader_resource = std::dynamic_pointer_cast<ShaderResource>(asset_manager->FetchAsset("BasicPixel.cso", shader_resource_hash, sys_resources));
			Ref<ShaderResource> position_only_shader_resource = std::dynamic_pointer_cast<ShaderResource>(asset_manager->FetchAsset("PositionOnlyVertex.cso", shader_resource_hash, sys_resources));
			Ref<ShaderResource> color_only_shader_resource = std::dynamic_pointer_cast<ShaderResource>(asset_manager->FetchAsset("ColorOnlyPixel.cso", shader_resource_hash, sys_resources));

			PipelineBuilderProvider provider;

			// building the basic draw pipeline
			{
				Unique<RootSignatureBuilder> rs_builder = provider.GetRootSignatureBuilder();

				rs_builder->SetNextShaderVisiblity(JGSW_SHADER::VERTEX)
					->PushStructAs32BitConstant<MVPMatrixConstant>(0)
					->SetNextShaderVisiblity(JGSW_SHADER::PIXEL)
					->PushStructAs32BitConstant<WorldLight>(0)
					->AddProperty(JGSW_ROOT_SIGNATURE_PROPERTY::STREAM_OUTPUT);
				Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject> rs_object = rs_builder->Build(render_context);

				Unique<PipelineObjectBuilder> ps_builder = provider.GetPipelineBuilder(JIGSAW_PIPELINE_DRAW);
				ps_builder->IAPushPerVertexDesc("POSITION", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("COLOR", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("NORMAL", JGSW_FORMAT::R32G32B32_FLOAT)
					->SOPushOutputParameter("SV_POSITION", 0, 4, 0)
					->RSSetRootSignature(rs_object)
					->AddShader(vert_shader_resource.get(), JGSW_SHADER::VERTEX)->AddShader(pix_shader_resource.get(), JGSW_SHADER::PIXEL)
					->SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE::JGSW_TOPOLOGY_TYPE_TRIANGLE)
					->DSVSetFormat(JGSW_FORMAT::D24_UNORM_S8_UINT)->RTVAddTargetFormat(JGSW_FORMAT::R8G8B8A8_UNORM)
					->SetSamplingParameters(1, 0);
				Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject> ps_resource = ps_builder->Build(render_context);

				pipelines[JIGSAW_PIPELINE_DRAW] = ps_resource;
			}

			// building the solid-color-id pipeline
			{
				Unique<RootSignatureBuilder> rs_builder = provider.GetRootSignatureBuilder();
				rs_builder
					->SetNextShaderVisiblity(JGSW_SHADER::VERTEX)
					->PushStructAs32BitConstant<MVPMatrixConstant>(0)
					->PushStructAs32BitConstant<RGBA>(1);
				Jigsaw::Ref<RootSignatureObject> rs_object = rs_builder->Build(render_context);

				Unique<PipelineObjectBuilder> ps_builder = provider.GetPipelineBuilder(JIGSAW_PIPELINE_COLOR_ID);
				ps_builder->IAPushPerVertexDesc("POSITION", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("COLOR", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("NORMAL", JGSW_FORMAT::R32G32B32_FLOAT)
					->RSSetRootSignature(rs_object)
					->AddShader(position_only_shader_resource.get(), JGSW_SHADER::VERTEX)->AddShader(color_only_shader_resource.get(), JGSW_SHADER::PIXEL)
					->SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE::JGSW_TOPOLOGY_TYPE_TRIANGLE)
					->DSVSetFormat(JGSW_FORMAT::D24_UNORM_S8_UINT)->RTVAddTargetFormat(JGSW_FORMAT::R32G32B32A32_FLOAT);
				Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject> ps_resource = ps_builder->Build(render_context);

				pipelines[JIGSAW_PIPELINE_COLOR_ID] = ps_resource;
			}

			// building the colored-lines pipeline
			{
				Ref<ShaderResource> projection_only_v_shader = std::dynamic_pointer_cast<ShaderResource>(asset_manager->FetchAsset("ProjOnlyColoredPrim_optRadAlpha.cso", shader_resource_hash, sys_resources));

				Unique<RootSignatureBuilder> rs_builder = provider.GetRootSignatureBuilder();
				rs_builder->SetNextShaderVisiblity(JGSW_SHADER::VERTEX)
					->PushStructAs32BitConstant<PrimitiveProjectionDataBuffer>(0);
				Ref<RootSignatureObject> rs_object = rs_builder->Build(render_context);

				Unique<PipelineObjectBuilder> ps_builder = provider.GetPipelineBuilder(JIGSAW_PIPELINE_COLOR_ID);
				ps_builder->IAPushPerVertexDesc("POSITION", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("INDEX", JGSW_FORMAT::R32_SINT)
					->IAPushPerVertexDesc("COLOR", JGSW_FORMAT::R32G32B32A32_FLOAT)
					->RSSetRootSignature(rs_object)
					->AddShader(projection_only_v_shader.get(), JGSW_SHADER::VERTEX)->AddShader(color_only_shader_resource.get(), JGSW_SHADER::PIXEL)
					->SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE::JGSW_TOPOLOGY_TYPE_LINE)
					->DSVSetFormat(JGSW_FORMAT::D24_UNORM_S8_UINT)->RTVAddTargetFormat(JGSW_FORMAT::R8G8B8A8_UNORM)
					->SetSamplingParameters(1, 0)->SetBlendingParameters(JGSW_BLEND::STANDARD);

				pipelines[JIGSAW_PIPELINE_COLORED_LINES] = ps_builder->Build(render_context);
			}

			// building the JIGSAW_PIPELINE_SKINNED_MESH pipeline
			{
				Unique<RootSignatureBuilder> rs_builder = provider.GetRootSignatureBuilder();
				rs_builder->SetNextShaderVisiblity(JGSW_SHADER::VERTEX)
					->PushStructAs32BitConstant<MVPMatrixConstant>(0)
					->PushTextureReferenceArgument(1);
				Ref<RootSignatureObject> rs_object = rs_builder->Build(render_context);

				Unique<PipelineObjectBuilder> ps_builder = provider.GetPipelineBuilder(JIGSAW_PIPELINE_COLOR_ID);
				ps_builder->IAPushPerVertexDesc("POSITION", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("COLOR", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("NORMAL", JGSW_FORMAT::R32G32B32_FLOAT)
					->IAPushPerVertexDesc("INDEX", JGSW_FORMAT::R16_UINT)
					->IAPushPerVertexDesc("INDEX", JGSW_FORMAT::R16_UINT)
					->IAPushPerVertexDesc("INDEX", JGSW_FORMAT::R16_UINT)
					->IAPushPerVertexDesc("INDEX", JGSW_FORMAT::R16_UINT)
					->IAPushPerVertexDesc("WEIGHT", JGSW_FORMAT::R32_FLOAT)
					->IAPushPerVertexDesc("WEIGHT", JGSW_FORMAT::R32_FLOAT)
					->IAPushPerVertexDesc("WEIGHT", JGSW_FORMAT::R32_FLOAT)
					->IAPushPerVertexDesc("WEIGHT", JGSW_FORMAT::R32_FLOAT)
					->RSSetRootSignature(rs_object)
					->AddShader(skinning_shader_resource.get(), JGSW_SHADER::VERTEX)->AddShader(color_only_shader_resource.get(), JGSW_SHADER::PIXEL)
					->SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE::JGSW_TOPOLOGY_TYPE_TRIANGLE)
					->DSVSetFormat(JGSW_FORMAT::D24_UNORM_S8_UINT)->RTVAddTargetFormat(JGSW_FORMAT::R8G8B8A8_UNORM)
					->SetSamplingParameters(1, 0)->SetBlendingParameters(JGSW_BLEND::STANDARD);

				pipelines[JIGSAW_PIPELINE_SKINNED_MESH] = ps_builder->Build(render_context);
			}

			return true;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineServiceManager::PipelineServiceManager()
		{
		}

		//////////////////////////////////////////////////////////////////////////
	}
}
