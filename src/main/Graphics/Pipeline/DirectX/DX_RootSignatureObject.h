#ifndef _DX_ROOT_SIGNATURE_H_
#define _DX_ROOT_SIGNATURE_H_

#include "Graphics/Pipeline/RootSignatureObject.h"
#include "Graphics/__graphics_api.h"

namespace Jigsaw {
	namespace Pipeline {

		/// <summary>
		/// DX_RootSignatureObject houses DX-specific data
		/// </summary>
		class DX_RootSignatureObject : public RootSignatureObject { 
		public:
			DX_RootSignatureObject(Microsoft::WRL::ComPtr<ID3D12RootSignature>& dx_rsObject) : m_rootSignature(dx_rsObject) { };

			/// <summary>
			/// Returns the underlying dx12 root signature
			/// </summary>
			/// <returns></returns>
			Microsoft::WRL::ComPtr<ID3D12RootSignature>& GetRootSignature();

		private:

			Microsoft::WRL::ComPtr<ID3D12RootSignature> m_rootSignature;

		};

		inline Microsoft::WRL::ComPtr<ID3D12RootSignature>& DX_RootSignatureObject::GetRootSignature() {
			return m_rootSignature;
		}
	}
}
#endif