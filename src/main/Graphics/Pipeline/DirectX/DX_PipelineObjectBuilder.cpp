#include "DX_PipelineObjectBuilder.h"
#include "DX_RootSignatureObject.h"
#include "Graphics/__graphics_api.h"
#include "Graphics/DirectX/DX_Context.h"
#include "Graphics/Pipeline/DirectX/DX_PipelineObject.h"

namespace Jigsaw
{
	namespace Pipeline
	{

		//////////////////////////////////////////////////////////////////////////

		DX_PipelineObjectBuilder::DX_PipelineObjectBuilder(JIGSAW_PIPELINE pipeline) : raw_data_stream(new void* [PSO_RAW_STREAM_INITIAL_Bytes]), data_capacity(PSO_RAW_STREAM_INITIAL_Bytes),
			data_size(0), rt_formats({}), PipelineObjectBuilder(pipeline)
		{
			rt_formats.NumRenderTargets = 0;
			ZeroMemory(raw_data_stream, PSO_RAW_STREAM_INITIAL_Bytes);
		}

		//////////////////////////////////////////////////////////////////////////

		DX_PipelineObjectBuilder::~DX_PipelineObjectBuilder()
		{
			delete[] raw_data_stream;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::AddShader(const Jigsaw::ShaderResource* shader, JGSW_SHADER shader_type)
		{

			switch (shader_type)
			{
			case JGSW_SHADER::VERTEX:
				PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_VS(shader->GetShaderBytes()));
				break;
			case JGSW_SHADER::PIXEL:
				PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_PS(shader->GetShaderBytes()));
				break;
			case JGSW_SHADER::GEOMETRY:
			default:
				PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_GS(shader->GetShaderBytes()));
				break;
			}

			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::IAPushPerVertexDesc(LPCSTR semantic_name, const JGSW_FORMAT format)
		{
			auto it = semantic_map.find(semantic_name);
			UINT semantic_index = 0;
			if (it != semantic_map.end())
			{
				it->second += 1;
				semantic_index = it->second;
			}
			else
			{
				semantic_map.insert(std::make_pair(semantic_name, 0));
			}

			D3D12_INPUT_ELEMENT_DESC ia_desc_element =
			{ semantic_name, semantic_index, D3D12_CONVERSIONS::ConvertFormat(format),
				0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
			ia_desc.push_back(ia_desc_element);

			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::RTVAddTargetFormat(JGSW_FORMAT format)
		{
			rt_formats.RTFormats[rt_formats.NumRenderTargets++] = D3D12_CONVERSIONS::ConvertFormat(format);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::DSVSetFormat(JGSW_FORMAT format)
		{
			CD3DX12_PIPELINE_STATE_STREAM_DEPTH_STENCIL_FORMAT DSVFormat = D3D12_CONVERSIONS::ConvertFormat(format);
			PushStreamSubObject(DSVFormat);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::SOPushOutputParameter(LPCSTR semantic_name, const UINT semantic_index, const UINT component_count, const UINT output_slot)
		{
			D3D12_SO_DECLARATION_ENTRY so_decl;
			so_decl.Stream = 0;
			so_decl.SemanticName = semantic_name;
			so_decl.SemanticIndex = semantic_index;
			so_decl.StartComponent = 0;
			so_decl.ComponentCount = component_count;
			so_decl.OutputSlot = output_slot;
			this->so_decl.push_back(so_decl);

			so_decl_strides.push_back(component_count * 4);

			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::RSSetRootSignature(const Ref<RootSignatureObject>& rs_object)
		{
			this->rs_object = rs_object;
			DX_RootSignatureObject* dx_rso = static_cast<DX_RootSignatureObject*>(rs_object.get());
			CD3DX12_PIPELINE_STATE_STREAM_ROOT_SIGNATURE root_sig = dx_rso->GetRootSignature().Get();
			PushStreamSubObject(root_sig);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE topology_type)
		{
			this->topology_type = topology_type;
			PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_PRIMITIVE_TOPOLOGY((D3D12_PRIMITIVE_TOPOLOGY_TYPE)topology_type));
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::SetSamplingParameters(UINT count, UINT quality)
		{
			DXGI_SAMPLE_DESC sample = { count, quality };
			PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_SAMPLE_DESC(sample));
			CD3DX12_PIPELINE_STATE_STREAM_RASTERIZER raster;
			CD3DX12_RASTERIZER_DESC& raster_desc = *raster.Get();
			raster_desc.MultisampleEnable = TRUE;
			raster_desc.AntialiasedLineEnable = TRUE;
			raster_desc.FillMode = D3D12_FILL_MODE_WIREFRAME;

			//raster_desc.ForcedSampleCount = 4;
			PushStreamSubObject(raster);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		PipelineObjectBuilder* DX_PipelineObjectBuilder::SetBlendingParameters(JGSW_BLEND blend_type)
		{
			CD3DX12_PIPELINE_STATE_STREAM_BLEND_DESC blend_obj;
			CD3DX12_BLEND_DESC& blend_desc = *blend_obj.Get();
			blend_desc.AlphaToCoverageEnable = FALSE;

			switch (blend_type)
			{
			case JGSW_BLEND::STANDARD:
			{
				D3D12_RENDER_TARGET_BLEND_DESC rt_blend_desc;
				rt_blend_desc.BlendEnable = TRUE;
				rt_blend_desc.LogicOpEnable = FALSE;
				rt_blend_desc.SrcBlend = D3D12_BLEND_SRC_ALPHA;
				rt_blend_desc.DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
				rt_blend_desc.BlendOp = D3D12_BLEND_OP_ADD;
				rt_blend_desc.SrcBlendAlpha = D3D12_BLEND_SRC_ALPHA;
				rt_blend_desc.DestBlendAlpha = D3D12_BLEND_INV_SRC_ALPHA;
				rt_blend_desc.BlendOpAlpha = D3D12_BLEND_OP_ADD;
				rt_blend_desc.LogicOp = D3D12_LOGIC_OP_NOOP;
				rt_blend_desc.RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_RED | D3D12_COLOR_WRITE_ENABLE_GREEN | D3D12_COLOR_WRITE_ENABLE_BLUE;

				blend_desc.RenderTarget[0] = rt_blend_desc;
			}
			break;
			default:
				J_D_ASSERT_LOG_ERROR(false, DX_PipelineObjectBuilder, "The JGSW_BLEND routine {0} is not implemented with DX12", blend_type);
			}

			PushStreamSubObject(blend_obj);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		Ref<PipelineObject> DX_PipelineObjectBuilder::Build(Jigsaw::GraphicsContext* graphics_context)
		{
			// compiling the Stream Output declarations into the PSO
			if (!so_decl.empty())
			{
				D3D12_SO_DECLARATION_ENTRY* so_decl_array = so_decl.data();
				UINT count = (UINT)so_decl.size();
				UINT* strides = so_decl_strides.data();

				D3D12_STREAM_OUTPUT_DESC so_desc;
				so_desc.NumEntries = count;
				so_desc.pBufferStrides = strides;
				so_desc.NumStrides = count;
				so_desc.pSODeclaration = so_decl_array;
				so_desc.RasterizedStream = 0;
				PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_STREAM_OUTPUT(so_desc));
			}

			// compiling the Input Assembly descriptions into the PSO
			if (!ia_desc.empty())
			{
				D3D12_INPUT_ELEMENT_DESC* desc_array = ia_desc.data();
				UINT count = (UINT)ia_desc.size();
				D3D12_INPUT_LAYOUT_DESC layout_desc = { desc_array, count };

				CD3DX12_PIPELINE_STATE_STREAM_INPUT_LAYOUT input_layout = layout_desc;
				PushStreamSubObject(input_layout);
			}

			// pushing the formats onto the sub object stream
			PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_RENDER_TARGET_FORMATS(rt_formats));

			// fetching the dx12 device object from the command list
			DX_Context* dx_context = static_cast<DX_Context*>(graphics_context);
			ID3D12Device2* device = dx_context->GetDevice();

			D3D12_PIPELINE_STATE_STREAM_DESC stream_desc{
				data_size - 1, raw_data_stream
			};

			Microsoft::WRL::ComPtr<ID3D12PipelineState> dx_plo;
			HRESULT pso_result = device->CreatePipelineState(&stream_desc, __uuidof(ID3D12PipelineState), &dx_plo);
			//Jigsaw::DX_Context::PrintDebugMessages();
			J_D_ASSERT_LOG_ERROR(!FAILED(pso_result), DX_PipelineObjectBuilder, "Failed to create PSO");

			Ref<PipelineObject> pl_object = MakeRef<PipelineObject>(pipeline, rs_object, topology_type);

			return Ref<PipelineObject>(new DX_PipelineObject(pipeline, rs_object, topology_type, dx_plo));
		}

		//////////////////////////////////////////////////////////////////////////
	}
}