#ifndef _DX_ROOT_SIGNATURE_BUILDER_H_
#define _DX_ROOT_SIGNATURE_BUILDER_H_

#include "Graphics/Pipeline/RootSignatureBuilder.h"

namespace Jigsaw {
	namespace Pipeline {

		/// <summary>
		/// Implementation for compiling DX12 Root Signatures. Unlike some other builders, the order in which some of these calls are made is important.
		/// The 'SetNextShaderVisibility' call will have an impact on subsequent 'Push' calls. 
		/// </summary>
		class DX_RootSignatureBuilder final : public RootSignatureBuilder {
		public:

			DX_RootSignatureBuilder();

			CANT_COPY(DX_RootSignatureBuilder);
			CANT_MOVE(DX_RootSignatureBuilder);

			/// <summary>
			/// Indicates the visibility of the subsequent 'Push' calls to the root signature description. 
			/// 
			/// Any encountered Shader visibilities will automatically adjust the Root Signature flags to enable access to the root signature for the corresponding shader type
			/// </summary>
			/// <param name="visibility"></param>
			/// <returns></returns>
			RootSignatureBuilder* SetNextShaderVisiblity(const JGSW_SHADER visibility) override final;

			/// <summary>
			/// Inclusive bit-wise or of the current Root Signature flags with the 'flags' parameter
			/// </summary>
			/// <param name="flags"></param>
			/// <returns></returns>
			RootSignatureBuilder* OrRootFlags(D3D12_ROOT_SIGNATURE_FLAGS flags);

			/// <summary>
			/// Sets the current flag value to that of the bit-wise and of the "flags" parameter and the builders "flags" member
			/// </summary>
			/// <param name="flags"></param>
			/// <returns></returns>
			RootSignatureBuilder* AndRootFlags(D3D12_ROOT_SIGNATURE_FLAGS flags);

			/// <summary>
			/// Includes the specified property in the RootSignature definition
			/// </summary>
			/// <param name="property"></param>
			/// <returns></returns>
			RootSignatureBuilder* AddProperty(const JGSW_ROOT_SIGNATURE_PROPERTY property) override final;

			/// <summary>
			/// Pushes a 32-bit constant buffer description at the specified buffer index. For DX12, this results in a direct upload.
			/// </summary>
			/// <param name="register_number"></param>
			/// <param name="no_32_bit_params"></param>
			/// <returns></returns>
			RootSignatureBuilder* Push32BitConstantArgument(u32 register_number, size_t no_32_bit_params) override final;

			/// <summary>
			/// Pushes a resource-reference argument for the specified register number. This could be a sampler, texture, or a buffer.
			/// </summary>
			/// <param name="register_number"></param>
			/// <returns></returns>
			virtual RootSignatureBuilder* PushTextureReferenceArgument(u32 register_number) override final;

			/// <summary>
			/// Completes the construction and builds a RootSignatureResource for use in the pipeline
			/// </summary>
			/// <param name="cmd_list"></param>
			/// <returns></returns>
			virtual Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject> Build(Jigsaw::GraphicsContext* graphics_context) override final;

		private:

			std::vector<CD3DX12_ROOT_PARAMETER1> m_rootParams;

			D3D12_ROOT_SIGNATURE_FLAGS m_flags = D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS |
				D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS | D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS
				| D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT;

			// mutable instance data
			D3D12_SHADER_VISIBILITY m_visibility;
		};

	}
}

#endif