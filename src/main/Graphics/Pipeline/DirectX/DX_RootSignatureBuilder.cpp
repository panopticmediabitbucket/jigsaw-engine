#include "Debug/j_debug.h"
#include "DX_RootSignatureBuilder.h"
#include "DX_RootSignatureObject.h"
#include "Graphics/DirectX/DX_Context.h"

namespace Jigsaw
{
	namespace Pipeline
	{

		//////////////////////////////////////////////////////////////////////////

		RootSignatureBuilder* DX_RootSignatureBuilder::Push32BitConstantArgument(u32 register_number, size_t no_32_bit_params)
		{
			CD3DX12_ROOT_PARAMETER1 root_param;
			root_param.InitAsConstants((UINT)no_32_bit_params, register_number, 0, m_visibility);
			m_rootParams.push_back(root_param);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		RootSignatureBuilder* DX_RootSignatureBuilder::PushTextureReferenceArgument(u32 register_number)
		{
			CD3DX12_ROOT_PARAMETER1 root_param;
			root_param.InitAsShaderResourceView(0, 0, D3D12_ROOT_DESCRIPTOR_FLAG_NONE, m_visibility);
			m_rootParams.push_back(root_param);
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject> DX_RootSignatureBuilder::Build(Jigsaw::GraphicsContext* render_context)
		{

			DX_Context* dx_rContext = static_cast<DX_Context*>(render_context);
			ID3D12Device2* dx_device = dx_rContext->GetDevice();

			CD3DX12_ROOT_PARAMETER1* p_array = m_rootParams.data();
			UINT size = (UINT)m_rootParams.size();

			CD3DX12_VERSIONED_ROOT_SIGNATURE_DESC root_sig_desc;
			root_sig_desc.Init_1_1(size, p_array, 0, nullptr, m_flags);

			Microsoft::WRL::ComPtr<ID3DBlob> root_sig_blob;
			Microsoft::WRL::ComPtr<ID3DBlob> error_blob;
			if (S_OK != D3DX12SerializeVersionedRootSignature(&root_sig_desc, D3D_ROOT_SIGNATURE_VERSION_1_1, &root_sig_blob, &error_blob))
			{
				const char* buff = (const char*)error_blob->GetBufferPointer();
				J_D_ASSERT_LOG_ERROR(false, DX_RootSignatureBuilder, "Error initializing root sig: {0}", buff);
			}

			Microsoft::WRL::ComPtr<ID3D12RootSignature> dx_rsObject;
			HRESULT rs_creation = dx_device->CreateRootSignature(FRAME_NODE, root_sig_blob->GetBufferPointer(), root_sig_blob->GetBufferSize(), IID_PPV_ARGS(&dx_rsObject));
			J_D_ASSERT_LOG_ERROR(!FAILED(rs_creation), RootSignatureBuilder, "Failed to create Root Signature");

			return Ref<RootSignatureObject>(new DX_RootSignatureObject(dx_rsObject));
		}

		//////////////////////////////////////////////////////////////////////////

		DX_RootSignatureBuilder::DX_RootSignatureBuilder()
		{
		}

		//////////////////////////////////////////////////////////////////////////

		RootSignatureBuilder* DX_RootSignatureBuilder::SetNextShaderVisiblity(const JGSW_SHADER visibility) 
		{
			m_visibility = D3D12_CONVERSIONS::ConvertShaderVisibility(visibility);

			switch (m_visibility)
			{
			case D3D12_SHADER_VISIBILITY_VERTEX:
				AndRootFlags(~D3D12_ROOT_SIGNATURE_FLAG_DENY_VERTEX_SHADER_ROOT_ACCESS);
				break;
			case D3D12_SHADER_VISIBILITY_PIXEL:
				AndRootFlags(~D3D12_ROOT_SIGNATURE_FLAG_DENY_PIXEL_SHADER_ROOT_ACCESS);
				break;
			case D3D12_SHADER_VISIBILITY_GEOMETRY:
				AndRootFlags(~D3D12_ROOT_SIGNATURE_FLAG_DENY_GEOMETRY_SHADER_ROOT_ACCESS);
				break;
			case D3D12_SHADER_VISIBILITY_HULL:
				AndRootFlags(~D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS);
				break;
			case D3D12_SHADER_VISIBILITY_DOMAIN:
				AndRootFlags(~D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS);
				break;
			}
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		RootSignatureBuilder* DX_RootSignatureBuilder::OrRootFlags(D3D12_ROOT_SIGNATURE_FLAGS additional_flags)
		{
			m_flags |= additional_flags;
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		RootSignatureBuilder* DX_RootSignatureBuilder::AndRootFlags(D3D12_ROOT_SIGNATURE_FLAGS additional_flags)
		{
			m_flags &= additional_flags;
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

		RootSignatureBuilder* DX_RootSignatureBuilder::AddProperty(const JGSW_ROOT_SIGNATURE_PROPERTY property)
		{
			switch (property)
			{
			case JGSW_ROOT_SIGNATURE_PROPERTY::STREAM_OUTPUT:
				OrRootFlags(D3D12_ROOT_SIGNATURE_FLAG_ALLOW_STREAM_OUTPUT);
				break;
			}
			return this;
		}

		//////////////////////////////////////////////////////////////////////////

	}
}