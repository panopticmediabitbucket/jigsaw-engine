/*********************************************************************
 * DX_PipelineObjectBuilder.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: March 2021
 *********************************************************************/
#ifndef _DX_PIPELINE_OBJECT_BUILDER_H_
#define _DX_PIPELINE_OBJECT_BUILDER_H_

#include "Graphics/Pipeline/PipelineObjectBuilder.h"

namespace Jigsaw {
	namespace Pipeline {

		/// <summary>
		/// DX_PipelineObjectBuilder implements PipelineObjectBuilder for DirectX 12. The completed PipelineObject will be a DX_PipelineObject.
		/// </summary>
		/// <author>
		///  Luke Randazzo
		/// </author>
		class DX_PipelineObjectBuilder final : public PipelineObjectBuilder {
		public:
			DX_PipelineObjectBuilder(JIGSAW_PIPELINE pipeline); 

			~DX_PipelineObjectBuilder();

			CANT_COPY(DX_PipelineObjectBuilder);
			CANT_MOVE(DX_PipelineObjectBuilder);

			/// <summary>
			/// Adds a shader of any type to the Pipeline State Object
			/// </summary>
			/// <param name="shader"></param>
			/// <returns></returns>
			PipelineObjectBuilder* AddShader(const Jigsaw::ShaderResource* shader, JGSW_SHADER shader_type) override final;

			/// <summary>
			/// Pushes a single element to the list of Input Assembler Vertex data
			/// </summary>
			/// <param name="semantic_name"></param>
			/// <param name="format"></param>
			/// <returns></returns>
			PipelineObjectBuilder* IAPushPerVertexDesc(LPCSTR semantic_name, const JGSW_FORMAT format) override final;

			/// <summary>
			/// Pushes an output parameter to be exposed in Stream Output stage
			/// </summary>
			/// <param name="semantic_name"></param>
			/// <param name="component_count"></param>
			/// <param name="output_slot"></param>
			/// <returns></returns>
			PipelineObjectBuilder* SOPushOutputParameter(LPCSTR semantic_name, const UINT semantic_index, const UINT component_count, const UINT output_slot) override final;

			/// <summary>
			/// Sets the root signature for the pipeline
			/// </summary>
			/// <param name="rs_resource"></param>
			/// <returns></returns>
			PipelineObjectBuilder* RSSetRootSignature(const Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject>& rs_resource) override final;

			/// <summary>
			/// Increments the count of render targets and binds the format to the added target
			/// </summary>
			/// <param name="format"></param>
			/// <returns></returns>
			PipelineObjectBuilder* RTVAddTargetFormat(JGSW_FORMAT format) override final;

			/// <summary>
			/// Sets the format for the Depth stencil view
			/// </summary>
			/// <param name="format"></param>
			/// <returns></returns>
			PipelineObjectBuilder* DSVSetFormat(JGSW_FORMAT format) override final;

			/// <summary>
			/// Primitive topology type for PSO set here
			/// </summary>
			/// <param name="topology_type"></param>
			/// <returns></returns>
			PipelineObjectBuilder* SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE topology_type) override final;

			/// <summary>
			/// Passes the sampling parameters to the PSO
			/// </summary>
			/// <param name="count"></param>
			/// <param name="quality"></param>
			/// <returns></returns>
			PipelineObjectBuilder* SetSamplingParameters(UINT count, UINT quality) override final;

			/// <summary>
			/// Sets the blend state for the Jigsaw Pipeline
			/// </summary>
			/// <param name="blend_type"></param>
			/// <returns></returns>
			PipelineObjectBuilder* SetBlendingParameters(JGSW_BLEND blend_type) override final;

			/// <summary>
			/// Returns a fully-constructed pipeline state object. This is the final step. All of the other methods should be called prior
			/// to the final 'Build' call. 
			/// </summary>
			/// <param name="cmd_list"></param>
			/// <returns></returns>
			virtual Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject> Build(Jigsaw::GraphicsContext* graphics_context) override final;

		private:

			/// <summary>
			/// Internal method for accumulating any Pipeline State Sub-object into the raw data stream. The stream will resize automatically as needed.
			/// </summary>
			/// <typeparam name="D"></typeparam>
			/// <typeparam name="I"></typeparam>
			/// <param name="subobject"></param>
			template <typename I, D3D12_PIPELINE_STATE_SUBOBJECT_TYPE Type, typename D = I>
			void PushStreamSubObject(CD3DX12_PIPELINE_STATE_STREAM_SUBOBJECT<I, Type, D> subobject) {

				size_t size_in_Bytes = sizeof(CD3DX12_PIPELINE_STATE_STREAM_SUBOBJECT<I, Type, D>);
				// check if we need to resize
				if ((data_size + size_in_Bytes) > data_capacity) {

					do {
						data_capacity += PSO_RAW_STREAM_INITIAL_Bytes;
					} while ((data_size + size_in_Bytes) > data_capacity);

					void* old_data_stream = raw_data_stream;
					raw_data_stream = new char[data_capacity];

					memcpy(raw_data_stream, old_data_stream, data_size);
					delete[] old_data_stream;
				}

				size_t s_UINT = sizeof(UINT);
				UINT* start_ptr = static_cast<UINT*>(raw_data_stream) + (data_size / s_UINT);
				//*start_ptr = Type;
				//start_ptr += (sizeof(D3D12_PIPELINE_STATE_SUBOBJECT_TYPE) / s_UINT);

				I sub = (I)subobject;
				size_t s_I = sizeof(I);
				size_t s_TYPE = sizeof(D3D12_PIPELINE_STATE_SUBOBJECT_TYPE);
				memcpy(start_ptr, &subobject, size_in_Bytes);
				data_size += (UINT)size_in_Bytes;
			}

			std::map<LPCSTR, UINT> semantic_map;
			std::vector<D3D12_INPUT_ELEMENT_DESC> ia_desc;
			std::vector<D3D12_SO_DECLARATION_ENTRY> so_decl;
			std::vector<UINT> so_decl_strides;

			Jigsaw::JGSW_TOPOLOGY_TYPE topology_type;

			D3D12_RT_FORMAT_ARRAY rt_formats;
			Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject> rs_object;

			size_t data_capacity;
			UINT data_size;
			void* raw_data_stream;
		};
	}
}

#endif