#ifndef _DX_PIPELINE_OBJECT_H_
#define _DX_PIPELINE_OBJECT_H_

#include "Graphics/Pipeline/PipelineObject.h"

namespace Jigsaw {
	namespace Pipeline {

		/// <summary>
		/// DX_PipelineObject implements pipeline object and holds data relevant to a DX_Pipeline
		/// </summary>
		class DX_PipelineObject final : public PipelineObject {
		public:
			DX_PipelineObject(Pipeline::JIGSAW_PIPELINE pipeline,
				const Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject>& rs_object,
				JGSW_TOPOLOGY_TYPE topology_type, Microsoft::WRL::ComPtr<ID3D12PipelineState>& dx_pipelineState)
				: ps_object(dx_pipelineState), PipelineObject(pipeline, rs_object, topology_type)
			{ };

			/// <summary>
			/// Returns the pipeline state associated with this object
			/// </summary>
			/// <returns></returns>
			Microsoft::WRL::ComPtr<ID3D12PipelineState>& GetPipelineState();

		private:
			Microsoft::WRL::ComPtr<ID3D12PipelineState> ps_object;

		};

		inline Microsoft::WRL::ComPtr<ID3D12PipelineState>& DX_PipelineObject::GetPipelineState() {
			return ps_object;
		}
	}
}

#endif