#include "PipelineBuilderProvider.h"
#include "Graphics/__graphics_api.h"

#if J_WIN_10
#include "Graphics/Pipeline/DirectX/DX_PipelineObjectBuilder.h"
#include "Graphics/Pipeline/DirectX/DX_RootSignatureBuilder.h"
#endif

namespace Jigsaw {
	namespace Pipeline {

		Jigsaw::Unique<Jigsaw::Pipeline::PipelineObjectBuilder> Jigsaw::Pipeline::PipelineBuilderProvider::GetPipelineBuilder(JIGSAW_PIPELINE pipeline) {
#if J_WIN_10
			return Unique<PipelineObjectBuilder>(new DX_PipelineObjectBuilder(pipeline));
#endif
		}

		Jigsaw::Unique<Jigsaw::Pipeline::RootSignatureBuilder> PipelineBuilderProvider::GetRootSignatureBuilder()
		{
#if J_WIN_10
			return Jigsaw::Unique<Jigsaw::Pipeline::RootSignatureBuilder>(new DX_RootSignatureBuilder);
#endif
		}
	}
}