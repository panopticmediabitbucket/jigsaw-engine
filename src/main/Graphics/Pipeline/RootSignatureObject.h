#ifndef _ROOT_SIGNATURE_H_
#define _ROOT_SIGNATURE_H_

namespace Jigsaw {
	namespace Pipeline {
		/// <summary>
		/// Interface for RootSignatureObjects--these can be abstractions used to orchestrate drawing or real objects on the GPU
		/// </summary>
		class RootSignatureObject {
		public:
			RootSignatureObject() {};

		};


	}
}

#endif