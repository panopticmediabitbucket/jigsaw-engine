#pragma once
#include "Assets/DataAssets.h"
#include <map>
#include <vector>
#include "_jgsw_api.h"

#define PSO_RAW_STREAM_INITIAL_Bytes 256 

namespace Jigsaw {
	namespace Pipeline {
		/// <summary>
		/// PipelineObjectBuilder is a simple interface used primarily by the PipelineServiceManager to quickly build pipelines without concerning the user about 
		/// creating custom Structs. All of the Stream Sub objects are placed directly in void-aligned memory
		/// </summary>
		/// <author>
		///  Luke Randazzo
		/// </author>
		class PipelineObjectBuilder {
		public:
			PipelineObjectBuilder(JIGSAW_PIPELINE pipeline); 

			CANT_COPY(PipelineObjectBuilder);
			CANT_MOVE(PipelineObjectBuilder);

			/// <summary>
			/// Adds a shader of any type to the Pipeline State Object
			/// </summary>
			/// <param name="shader"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* AddShader(const Jigsaw::ShaderResource* shader, JGSW_SHADER shader_type) = 0;

			/// <summary>
			/// Pushes a single element to the list of Input Assembler Vertex data
			/// </summary>
			/// <param name="semantic_name"></param>
			/// <param name="format"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* IAPushPerVertexDesc(LPCSTR semantic_name, const JGSW_FORMAT format) = 0;

			/// <summary>
			/// Pushes an output parameter to be exposed in Stream Output stage
			/// </summary>
			/// <param name="semantic_name"></param>
			/// <param name="component_count"></param>
			/// <param name="output_slot"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* SOPushOutputParameter(LPCSTR semantic_name, const UINT semantic_index, const UINT component_count, const UINT output_slot) = 0;

			/// <summary>
			/// Sets the root signature for the pipeline
			/// </summary>
			/// <param name="rs_resource"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* RSSetRootSignature(const Jigsaw::Ref<Jigsaw::Pipeline::RootSignatureObject>& rs_resource) = 0;

			/// <summary>
			/// Increments the count of render targets and binds the format to the added target
			/// </summary>
			/// <param name="format"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* RTVAddTargetFormat(JGSW_FORMAT format) = 0;

			/// <summary>
			/// Sets the format for the Depth stencil view
			/// </summary>
			/// <param name="format"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* DSVSetFormat(JGSW_FORMAT format) = 0;

			/// <summary>
			/// Primitive topology type for PSO set here
			/// </summary>
			/// <param name="topology_type"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* SetPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE topology_type) = 0;

			/// <summary>
			/// Passes the sampling parameters to the PSO
			/// </summary>
			/// <param name="count"></param>
			/// <param name="quality"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* SetSamplingParameters(UINT count, UINT quality) = 0;

			/// <summary>
			/// Sets the blend state for the Jigsaw Pipeline
			/// </summary>
			/// <param name="blend_type"></param>
			/// <returns></returns>
			virtual PipelineObjectBuilder* SetBlendingParameters(JGSW_BLEND blend_type) = 0;

			/// <summary>
			/// Returns a fully-constructed pipeline state object. This is the final step. All of the other methods should be called prior
			/// to the final 'Build' call. 
			/// </summary>
			/// <param name="render_context"></param>
			/// <returns></returns>
			virtual Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject> Build(Jigsaw::GraphicsContext* graphics_context) = 0;

		protected:
			JIGSAW_PIPELINE pipeline;

		};
	}
}
