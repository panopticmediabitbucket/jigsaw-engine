#include "GPUResource.h"

//////////////////////////////////////////////////////////////////////////

Jigsaw::GPUResource::GPUResource(Jigsaw::JGSW_GPU_RESOURCE_DATA resource_args) : resource_args(resource_args)
{
}

Jigsaw::GPUResource::~GPUResource()
{
}

Jigsaw::JGSW_GPU_RESOURCE_DATA Jigsaw::GPUResource::GetResourceData() const
{
	return this->resource_args;
}
