/*********************************************************************
 * J_Viewport.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _J_VIEWPORT_H_
#define _J_VIEWPORT_H_

#include "Graphics/GPU_Objects/GPUResource.h"

#define SWAP_CHAIN_COUNT 2

namespace Jigsaw {

	/// <summary>
	/// 'J_Viewport' is basically a swap-chain. It will hold swapable Rtv and Dsv resources that can be moved in-and-out frame-by-frame. 
	/// It does not just have to be used for the final view, however. Separate J_Viewport instances can be used for other render views that exist within the world. 
	/// </summary>
	class JGSW_API J_Viewport {
	public:
		CANT_COPY(J_Viewport);

		J_Viewport();

		// The default constructor does nothing. If resources needs to be freed, use the 'Destroy' function.
		~J_Viewport() = default;

		/// <summary>
		/// Destroys the J_Viewport and any resources it holds reference to
		/// </summary>
		void Destroy();

		/// <summary>
		/// Gets the current RtvResource on the chain.
		/// </summary>
		/// <returns></returns>
		Jigsaw::GPUResource* GetRtvResource() const;

		/// <summary>
		/// Gets the current Dsv resource on the chain. 
		/// </summary>
		/// <returns></returns>
		Jigsaw::GPUResource* GetDsvResource() const;

		/// <summary>
		/// Gets a reference to the Rtv resource associated with the specific frame. 
		/// </summary>
		/// <param name="frame_alignment"></param>
		/// <returns></returns>
		Jigsaw::GPUResource*& GetRtvResource(size_t frame_alignment);

		/// <summary>
		/// Gets a reference to the Dsv resource associated with the specific frame alignment. 
		/// </summary>
		/// <param name="frame_alignment"></param>
		/// <returns></returns>
		Jigsaw::GPUResource*& GetDsvResource(size_t frame_alignment);

		/// <summary>
		/// Returns the Rtv referenced before the last call to 'Increment_Frame' 
		/// </summary>
		/// <returns></returns>
		const Jigsaw::GPUResource* GetPreviousRtv() const;

		/// <summary>
		/// Returns the Dsv referenced before the last call to 'Increment_Frame' 
		/// </summary>
		/// <returns></returns>
		const Jigsaw::GPUResource* GetPreviousDsv() const;

		/// <summary>
		/// Increments the underlying frame pointer. Subsequent calls will return the new Rtv/Dsv
		/// </summary>
		/// <returns></returns>
		size_t IncrementFrame();

		/// <summary>
		/// Aligns frame back to 0. This should really only be used if the rtv/dsv resources have been rewritten.
		/// </summary>
		void ResetFrame();

	protected:
		Jigsaw::GPUResource* rtv_resources[SWAP_CHAIN_COUNT];
		Jigsaw::GPUResource* dsv_resources[SWAP_CHAIN_COUNT];

		size_t frame_index = 0;

	};
}
#endif