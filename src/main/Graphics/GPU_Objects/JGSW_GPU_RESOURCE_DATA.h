#ifndef _JGSW_GPU_RESOURCE_DATA_H_
#define _JGSW_GPU_RESOURCE_DATA_H_

#include "Math/LinAlg.h"
#include "Debug/j_debug.h"

namespace Jigsaw {

	namespace JGSW_VISIBILITY {
		enum {
			SHADER = 0x0001,
			DEPTH_STENCIL = 0x0002,
			RENDER_TARGET = 0x0004,
			STREAM_OUTPUT = 0x0008,
			CPU_READABLE = 0x0010
		};
	};

	typedef unsigned int JGSW_VISIBILITY_;

#undef DOMAIN
	enum class JGSW_SHADER {
		ALL,
		VERTEX,
		HULL,
		DOMAIN,
		GEOMETRY,
		PIXEL,
		AMPLIFICATION,
		MESH,
		__NUM_VISIBILITIES
	};

	enum class JGSW_GPU_RESOURCE_TYPE {
		TEXTURE,
		BUFFER
	};

	enum class JGSW_ROOT_SIGNATURE_PROPERTY {
		STREAM_OUTPUT
	};

	/// <summary>
	/// Generic Topology type object
	/// </summary>
	enum class JGSW_TOPOLOGY_TYPE {
		JGSW_TOPOLOGY_TYPE_UNDEFINED,
		JGSW_TOPOLOGY_TYPE_POINT,
		JGSW_TOPOLOGY_TYPE_LINE,
		JGSW_TOPOLOGY_TYPE_TRIANGLE,
		JGSW_TOPOLOGY_TYPE_PATCH,
		__NUM_JGSW_TOPOLOGY_TYPE
	};

	enum class JGSW_BLEND {
		STANDARD, // A regular, additive blend
	};

	struct JGSW_SAMPLE {
		unsigned short count = 1;
		unsigned short quality = 0;
	};

	// DO NOT CHANGE ANY OF THESE ENUM VALUES. YOU CAN ONLY ADD.
	// These are used in resource descriptions. If for some odd reason they need to change, be prepared to re-export resources
	enum class JGSW_FORMAT : unsigned char {
		UNDEFINED = 0,
		R8G8B8A8_UNORM = 1,

		R16_UINT = 2,
		R32_UINT = 3,
		R32_SINT = 4,
		R32_FLOAT = 5,
		R16G16_UINT = 6,
		R32G32_FLOAT = 7,
		R32G32B32_FLOAT = 8,
		R16G16B16A16_UINT = 9,
		R32G32B32A32_FLOAT = 10,

		D24_UNORM_S8_UINT = 11,
		BC7_SRGB = 12,

		__NUM_JGSW_FORMATS  // always at end
	};

	// DO NOT CHANGE ANY OF THESE ENUM VALUES. YOU CAN ONLY ADD.
	// These are used in resource descriptions. If for some odd reason they need to change, be prepared to re-export resources
	enum class JGSW_VERT_DATA : unsigned char {
		NONE,
		POSITION,
		UV,
		COLOR,
		NORMAL,
		BONE_INDICES,
		BONE_WEIGHTS,
	};

	/// <summary>
	/// For each vertex, we track the source (the semantic name) and the output_type. Again, this is meant to be a tiny structure. It's baked into 
	/// ModelAssets and Meshes. If for some reason we need it to change, a conversion strategy will need to be defined. 
	/// </summary>
	struct JGSW_VERT_TUPLE {
		JGSW_VERT_DATA source;
		JGSW_FORMAT output_type;
	};

	inline size_t GetSize(JGSW_FORMAT format) {
		switch (format) {
		case JGSW_FORMAT::R16_UINT:
			return 2;
		case JGSW_FORMAT::R16G16_UINT:
		case JGSW_FORMAT::R8G8B8A8_UNORM:
		case JGSW_FORMAT::R32_UINT:
		case JGSW_FORMAT::R32_FLOAT:
		case JGSW_FORMAT::D24_UNORM_S8_UINT:
			return 4;
		case JGSW_FORMAT::R16G16B16A16_UINT:
		case JGSW_FORMAT::R32G32_FLOAT:
			return 8;
		case JGSW_FORMAT::R32G32B32_FLOAT:
			return 12;
		case JGSW_FORMAT::R32G32B32A32_FLOAT:
			return 16;
		default:
			J_D_ASSERT_LOG_ERROR(false, JGSW_FORMAT, "The requested JGSW_FORMAT");
			return 0;
		}
	}

	enum JGSW_GPU_RESOURCE_FLAGS {
		JGSW_GPU_RESOURCE_FLAG_32_BIT_FLOAT = 0x0001,

		// For DX12, this flag will reserve a buffer that can be used to update the resource
		JGSW_GPU_RESOURCE_FLAG_UPDATABLE = 0x0002,

		// For DX12, this flag will reserve a buffer that can update the resource. After the first update, the update buffer will be released. 
		JGSW_GPU_RESOURCE_FLAG_SINGLE_UPLOAD = 0x0004
	};

	typedef unsigned int JGSW_GPU_RESOURCE_FLAGS_;

	struct depth_stencil {
		float depth = 1;
		unsigned int stencil = 0;
	};

	struct JGSW_GPU_RESOURCE_DATA {
		JGSW_GPU_RESOURCE_DATA() {}

		// mix of JGSW_VISIBILITY flags
		JGSW_VISIBILITY_ visibility = 0;
		JGSW_FORMAT format = JGSW_FORMAT::UNDEFINED;
		JGSW_GPU_RESOURCE_TYPE resource_type = JGSW_GPU_RESOURCE_TYPE::BUFFER;
		JGSW_GPU_RESOURCE_FLAGS_ flags = 0;

		u32 width = 0;
		u32 height = 0;

		union {
			float default_color[4];
			depth_stencil ds;
		};

		bool cpu_mappable = false;

		JGSW_SAMPLE sample_desc;

#ifdef _JGSW_NONFINAL
		etype_info* type_desc;
#endif
	};
}

#endif