/*********************************************************************
 * GPUResource.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: March 2021
 *********************************************************************/
#ifndef _GPU_RESOURCE_H_
#define _GPU_RESOURCE_H_

// Jigsaw_Engine
#include "JGSW_GPU_RESOURCE_DATA.h"
#include "Ref.h"
#include "Util/Primitives.h"

namespace Jigsaw 
{

	typedef u64 GpuPtr;

	/// <summary>
	/// The core GPUResource class encapsulates data relevant for buffers, textures, multi-textures, and more.
	/// </summary>
	class JGSW_API GPUResource {
	public:
		CANT_COPY(GPUResource);
		CANT_MOVE(GPUResource);

		// Basic constructor
		GPUResource(JGSW_GPU_RESOURCE_DATA resource_args);

		virtual ~GPUResource();

		/// <summary>
		/// Samples the underlying resource with the 'JGSW_BOX' parameters and outputs to 'dest' in row-column-depth order.
		/// </summary>
		/// <param name="dest"></param>
		/// <param name="src_subresource"></param>
		/// <param name="box"></param>
		virtual void SampleSubresource(void* dest, UINT src_subresource, JGSW_BOX box) = 0;

		/// <returns>A GPU-visible pointer for the specified visibility (generally SHADER)</returns>
		virtual GpuPtr GetGpuPointer(JGSW_VISIBILITY_ visibility = JGSW_VISIBILITY::SHADER) const = 0;

		/// <summary>
		/// Returns the resource_args associated with the GPUResource. May or may not have been modified internally through the 
		/// lifetime of the GPUResource. 
		/// </summary>
		/// <returns></returns>
		JGSW_GPU_RESOURCE_DATA GetResourceData() const;

	protected:
		JGSW_GPU_RESOURCE_DATA resource_args;

	};
}

#endif
