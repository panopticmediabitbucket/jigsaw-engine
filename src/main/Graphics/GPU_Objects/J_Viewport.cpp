#include "J_Viewport.h"
#include "Debug/j_debug.h"

namespace Jigsaw {

	//////////////////////////////////////////////////////////////////////////

	J_Viewport::J_Viewport()
	{
		memset(dsv_resources, 0, sizeof(GPUResource*) * SWAP_CHAIN_COUNT);
		memset(rtv_resources, 0, sizeof(GPUResource*) * SWAP_CHAIN_COUNT);
	}

	//////////////////////////////////////////////////////////////////////////

	void J_Viewport::Destroy()
	{
		for (u32 i = 0; i < SWAP_CHAIN_COUNT; i++)
		{
			if (rtv_resources[i]) delete rtv_resources[i];
			if (dsv_resources[i]) delete dsv_resources[i];
		}
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::GPUResource* J_Viewport::GetRtvResource() const 
	{
		return rtv_resources[this->frame_index];
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::GPUResource* J_Viewport::GetDsvResource() const 
	{
		return dsv_resources[this->frame_index];
	}

	//////////////////////////////////////////////////////////////////////////

	GPUResource*& J_Viewport::GetRtvResource(size_t frame_alignment)
	{
		J_D_ASSERT_LOG_ERROR((frame_alignment < SWAP_CHAIN_COUNT), J_Viewport, "Requested a Rtv resource on an out of bounds frame");
		return rtv_resources[frame_alignment];
	}

	//////////////////////////////////////////////////////////////////////////

	GPUResource*& J_Viewport::GetDsvResource(size_t frame_alignment)
	{
		J_D_ASSERT_LOG_ERROR((frame_alignment < SWAP_CHAIN_COUNT), J_Viewport, "Requested a Dsv resource on an out of bounds frame");
		return dsv_resources[frame_alignment];
	}

	//////////////////////////////////////////////////////////////////////////

	const Jigsaw::GPUResource* J_Viewport::GetPreviousRtv() const
	{
		size_t temp_index = frame_index == 0 ? SWAP_CHAIN_COUNT - 1 : frame_index - 1;
		return this->rtv_resources[temp_index];
	}

	//////////////////////////////////////////////////////////////////////////

	const Jigsaw::GPUResource* J_Viewport::GetPreviousDsv() const {
		size_t temp_index = frame_index == 0 ? SWAP_CHAIN_COUNT - 1 : frame_index - 1;
		return this->dsv_resources[temp_index];
	}

	//////////////////////////////////////////////////////////////////////////

	size_t J_Viewport::IncrementFrame()
	{
		++(this->frame_index);
		if (this->frame_index >= SWAP_CHAIN_COUNT)
		{
			this->frame_index = 0;
		}
		return this->frame_index;
	}

	//////////////////////////////////////////////////////////////////////////

	void J_Viewport::ResetFrame()
	{
		frame_index = 0;
	}

	//////////////////////////////////////////////////////////////////////////

}

