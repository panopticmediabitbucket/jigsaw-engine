/*********************************************************************
 * CommandList.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _LOAD_COMMAND_LIST_
#define _LOAD_COMMAND_LIST_

// Jigsaw_Engine
#include "Graphics/__graphics_api.h"
#include "Assets/RuntimeAssets.h"
#include "Graphics/GPU_Objects/GPUResource.h"
#include "Graphics/VERTEX_INDEXED_DATA.h"
#include "Graphics/Pipeline/PipelineObject.h"
#include "Graphics/Pipeline/RootSignatureObject.h"
#include "Ref.h"

namespace Jigsaw {
	/// <summary>
	/// The enum values are modeled after DirectX12 to support direct casting to proprietary enums. 
	/// </summary>
	enum JGSW_COMMAND_TYPE 
	{
		JGSW_COMMAND_TYPE_RENDER,
		JGSW_COMMAND_TYPE_LOAD,

		__JGSW_COMMAND_TYPE_SIZE
	};

	class GraphicsContext;

	/// <summary>
	/// The api-agnostic representation of a CommandList that operates on different types of render instructions. Fed to a CommandListExecutor to execute all instructions recorded to the list.
	/// </summary>
	class JGSW_API CommandList {
	public:
		CommandList();

		virtual void DrawIndexed(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_INDEXED_DATA& render_data) = 0;

		/// <summary>
		/// Directly (not with indexing) draws the specified vertex_data using the specified pl_object
		/// </summary>
		/// <param name="pl_object"></param>
		/// <param name="render_data"></param>
		virtual void DrawDirect(Jigsaw::Ref<Jigsaw::Pipeline::PipelineObject>& pl_object, const VERTEX_DIRECT_DATA& vertex_data) = 0;

		virtual void LoadBuffer(const void* t_arr, size_t t_size, size_t t_count, Jigsaw::GPUResource& buffer_dest) = 0;

		virtual void LoadTexture(const u8* data, size_t bytes_per_row, Jigsaw::GPUResource& texture_dest) = 0;

		virtual std::vector<Jigsaw::GPUResource*>& GetLoadedBuffers();

		virtual bool HasCommands() = 0;

		/// <summary>
		/// Updates the resource with the 'update_data' passed in
		/// </summary>
		/// <param name="gpu_resource"></param>
		/// <param name="update_data"></param>
		virtual void UpdateResource(GPUResource& gpu_resource, const UPDATE_DATA_RAW& update_data) = 0;

		/// <summary>
		/// Sets the passed in GPUResources as the Render Target for upcoming command list instructions.
		/// <param name="rtv_resource"></param>
		/// <param name="dsv_resource"></param>
		/// </summary>
		virtual void SetRenderViews(Jigsaw::GPUResource& rtv_resource, Jigsaw::GPUResource& dsv_resource) = 0;

		/// <summary>
		/// Designates the buffer as an output target for the following draw operation.
		/// <param name="gpu_resource"></param>
		/// </summary>
		virtual void SetBufferOutput(Jigsaw::GPUResource& gpu_resource) = 0;

		/// <summary>
		/// Copies the contents of the source buffer into the destination buffer. 
		/// </summary>
		/// <param name="source"></param>
		/// <param name="dest"></param>
		virtual void CopyBuffer(Jigsaw::GPUResource& source, Jigsaw::GPUResource& dest) = 0;

		/// <summary>
		/// Resets the passed in gpu_resource to its original, clear value.
		/// <param name="gpu_resource"></param>
		/// </summary>
		virtual void ClearResource(Jigsaw::GPUResource& gpu_resource) = 0;

		/// <summary>
		/// Resolves the resource data from the ms_resource into a single-sampled dest_resource
		/// </summary>
		/// <param name="ms_resource"></param>
		/// <param name="dest_resource"></param>
		virtual void ResolveMultiSampledResource(GPUResource& ms_resource, GPUResource& dest_resource) = 0;

		/// <summary>
		/// Prepares the gpu_resource for displaying to an output
		/// </summary>
		/// <param name="gpu_resource"></param>
		virtual void SetToDisplay(Jigsaw::GPUResource& gpu_resource) = 0;

		/// <summary>
		///  Transitions the resource for use in Shaders
		/// </summary>
		/// <param name="gpu_resource"></param>
		virtual void SetToShaderView(Jigsaw::GPUResource& gpu_resource) = 0;

	protected:
		std::vector<Jigsaw::GPUResource*> loading_buffers;

	};
}
#endif // !_LOAD_COMMAND_LIST_

