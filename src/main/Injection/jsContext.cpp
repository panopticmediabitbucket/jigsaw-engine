#include "jsContext.h"
#include <map>
#include <algorithm>
#include <sstream>
#include "Util/VectorUtils.h"
#include "Debug/j_debug.h"
#include "Debug/ScopedLoggingTimer.h"
#include "Assembly/JGSW_ASSEMBLY.h"
#include "jsKnobGraphDefinitionBuilder.h"
#include "jsKnobGraph.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	// jsContext Implementations

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<jsContext> jsContext::Create(const jsContextConfig& config) 
	{
		Ref<jsContext> context = Ref<jsContext>(new jsContext);
		context->config = config;
		context->Init();
		return context;
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<jsContext*> jsContext::GetContextDependencies() 
	{
		return std::vector<jsContext*>();
	}

	//////////////////////////////////////////////////////////////////////////

	jsContext::jsContext() { }

	//////////////////////////////////////////////////////////////////////////

	void jsContext::Init() 
	{
		J_LOG_TRACE(jsContext, "Initializing a JigsawDependentContext");
		jsContext** context_ref_holder = new jsContext*(this);
		jsKnobGraphDefinitionBuilder<jsContext> builder("context");
		builder.SetReference(context_ref_holder);
		knob_definition context_knob_definition = builder.CompileKnob();

		std::vector<Ref<jsKnobProvider>> knob_providers = this->GetKnobProviders();
		std::vector<jsContext*> context_dependencies = this->GetContextDependencies();
		std::vector<jsKnobGraph*> graph_dependencies = std::map_to<jsContext*, jsKnobGraph*>
			(context_dependencies, [](jsContext* dependent_context) { return &dependent_context->knob_graph; });

		START_TIMER_SCOPE(jsContext, "Resolving the jsContext's jsKnobGraph")
			knob_graph = jsKnobGraph(std::move(context_knob_definition), std::move(knob_providers), std::move(graph_dependencies));
			knobs = knob_graph.Resolve();
		END_TIMER_SCOPE

		std::vector<jsContext*> dependent_context_knobs = knob_graph.GetJigsawDependentContextKnobs();
		for (jsContext* dependent_context : dependent_context_knobs) 
		{
			if (dependent_context != this) 
			{
				dependent_context->Init();
			}
		}

		PostGraphResolution();
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<Jigsaw::Ref<jsKnobProvider>> jsContext::GetKnobProviders()
	{
		std::vector<namespace_import>& namespaces = config.m_packageNames;

		// pulling in all of the namespaces
		for (int i = 0; i < namespaces.size(); i++) 
		{
			std::vector<knob_provider_definition> knob_provider_defs = Jigsaw::Assembly::JigsawStaticContext::GetNamespaceProviders(namespaces.at(i).m_namespace);
			for (knob_provider_definition& provider_def : knob_provider_defs) 
			{

				for (namespace_import& import : provider_def.properties.m_namespaceImports) 
				{
					if (std::find_if(namespaces.begin(), namespaces.end(), [=](namespace_import& imp) { return strcmp(imp.m_namespace, import.m_namespace) == 0; }) == namespaces.end()) 
					{
						namespaces.push_back(import);
					}
				}
			}
		}

		std::vector<Jigsaw::Ref<jsKnobProvider>> knob_providers;

		// instantiating all of the knob providers in all of the namespaces
		for (int i = 0; i < namespaces.size(); i++) 
		{
			namespace_import& import = namespaces.at(i);
			std::vector<knob_provider_definition> knob_provider_defs = Jigsaw::Assembly::JigsawStaticContext::GetNamespaceProviders(import.m_namespace);

			J_LOG_INFO(jsContext, "Instantiating the Knob Providers for namespace {0}", namespaces.at(i).m_namespace);
			for (knob_provider_definition& definition : knob_provider_defs) 
			{
				J_LOG_INFO(jsContext, "Instantiating a KnobProvider: " + definition.name);

				Ref<jsKnobProvider> kp = Jigsaw::Ref<jsKnobProvider>(definition.build_provider());

				// removing the filtered types from the imported knob provider
				auto new_end = std::remove_if(kp->knobs.begin(), kp->knobs.end(),
					[&](knob_definition& knob) -> bool
					{
						return std::find(import.m_filterTypes.begin(), import.m_filterTypes.end(), knob.typeInfo) != import.m_filterTypes.end();
					});
				kp->knobs.erase(new_end, kp->knobs.end());

				knob_providers.push_back(kp);
			}
		}

		return knob_providers;
	}

	//////////////////////////////////////////////////////////////////////////

	bool jsContext::FillSlot(const slot_definition& definition) const
	{
		return knob_graph.FillExternalSlot(definition).success;
	}

	//////////////////////////////////////////////////////////////////////////

	void jsContext::FillSlots(const std::vector<slot_definition>& slot_definitions, const std::string& receiver_name) const
	{
		for (const slot_definition& slot_def : slot_definitions) 
		{
			if (!FillSlot(slot_def))
			{
				J_D_ASSERT_LOG_ERROR(false, jsContext, "No matching knob of type {0} was found while filling the slots of knob {1}", slot_def.typeInfo->GetQualifiedName(), receiver_name);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jsContext::FillSlots(jsSlotsBase& slots_class) const
	{
		const std::vector<slot_definition>& slots = GetSlots(slots_class);
		FillSlots(slots, "Anonymous jsSlotsBase object");
	}

	//////////////////////////////////////////////////////////////////////////

	void jsContext::PostGraphResolution() { }

	//////////////////////////////////////////////////////////////////////////
}

