#include "JigsawInjection.h"

constexpr bool strings_equal(char const* a, char const* b) {
	return *a == *b && (*a == '\0' || strings_equal(a + 1, b + 1));
}

constexpr bool ValidateSlotNumber(const char* slot_number) {
	return strings_equal(slot_number, "slot_1") ||
		strings_equal(slot_number, "slot_2") ||
		strings_equal(slot_number, "slot_3") ||
		strings_equal(slot_number, "slot_4") ||
		strings_equal(slot_number, "slot_5") ||
		strings_equal(slot_number, "slot_6") ||
		strings_equal(slot_number, "slot_7") ||
		strings_equal(slot_number, "slot_8") ||
		strings_equal(slot_number, "slot_9") ||
		strings_equal(slot_number, "slot_10") ||
		strings_equal(slot_number, "slot_11") ||
		strings_equal(slot_number, "slot_12") ||
		strings_equal(slot_number, "slot_13") ||
		strings_equal(slot_number, "slot_14") ||
		strings_equal(slot_number, "slot_15");
}
