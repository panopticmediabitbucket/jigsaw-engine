/*********************************************************************
 * jsSlots.h
 *
 * A templated implementation of knob_definition and slot_definition construction designed to disambiguate
 * the construction process of those data types.
 *
 * Author: jl_ra
 * Originally created: April 2021
 *********************************************************************/
#ifndef _JIGSAW_SLOTS_H_
#define _JIGSAW_SLOTS_H_

#include "Injection/injection_definitions.h"
#include "Util/DTO.h"
#include "_jgsw_api.h"
#include <vector>

namespace Jigsaw 
{

	/// <summary>
	/// jsSlotsBase is the base class for any component or Knob that is meant to be filled
	/// with the JigsawContext at time of initialization
	/// </summary>
	class JGSW_API jsSlotsBase {
	public:
		friend std::vector<Jigsaw::slot_definition>& GetSlots(Jigsaw::jsSlotsBase& slots);
		friend std::vector<Jigsaw::slot_definition>& GetSlots(Jigsaw::jsSlotsBase* slots);

		virtual ~jsSlotsBase();

	protected:

		std::vector<slot_definition> slots;

	};

	/// <summary>
	/// This template is attached to every implementation of jsSlotsBase.
	/// It uses some meta programming, but the general idea is simple: a static method needs to be exposed for every knob that uses the jsSlotsBase interface.
	/// That method returns static information about the slots attached to any constructed version of the object. 
	/// 
	/// 15 slots are available to each jsSlotsBase implementation. Using constexpr functions, the GetReceiverDefinition call checks which of these slots are in use
	/// (a struct will be built in the base class for this using the Macros laid out in JigsawInjection.h).
	///
	/// Upon calling the GetReceiverDefinition function, the slots can be retrieved. 
	///
	/// See the 'JigsawInjection.h' file and the _COMPLETE_SLOT function as well as 'KnobGraphNodeBuilder.h'.
	/// </summary>
	template <typename T>
	class JGSW_API jsSlots : virtual public jsSlotsBase {
	private:
		typedef char yes;
		typedef int no;
		template<typename U> static yes HasSlot_1(typename U::slot_1_def*);
		template<typename U> static no HasSlot_1(...);

		template<typename U> static yes HasSlot_2(typename U::slot_2_def*);
		template<typename U> static no HasSlot_2(...);

		template<typename U> static yes HasSlot_3(typename U::slot_3_def*);
		template<typename U> static no HasSlot_3(...);

		template<typename U> static yes HasSlot_4(typename U::slot_4_def*);
		template<typename U> static no HasSlot_4(...);

		template<typename U> static yes HasSlot_5(typename U::slot_5_def*);
		template<typename U> static no HasSlot_5(...);

		template<typename U> static yes HasSlot_6(typename U::slot_6_def*);
		template<typename U> static no HasSlot_6(...);

		template<typename U> static yes HasSlot_7(typename U::slot_7_def*);
		template<typename U> static no HasSlot_7(...);

		template<typename U> static yes HasSlot_8(typename U::slot_8_def*);
		template<typename U> static no HasSlot_8(...);

		template<typename U> static yes HasSlot_9(typename U::slot_9_def*);
		template<typename U> static no HasSlot_9(...);

		template<typename U> static yes HasSlot_10(typename U::slot_10_def*);
		template<typename U> static no HasSlot_10(...);

		template<typename U> static yes HasSlot_11(typename U::slot_11_def*);
		template<typename U> static no HasSlot_11(...);

		template<typename U> static yes HasSlot_12(typename U::slot_12_def*);
		template<typename U> static no HasSlot_12(...);

		template<typename U> static yes HasSlot_13(typename U::slot_13_def*);
		template<typename U> static no HasSlot_13(...);

		template<typename U> static yes HasSlot_14(typename U::slot_14_def*);
		template<typename U> static no HasSlot_14(...);

		template<typename U> static yes HasSlot_15(typename U::slot_15_def*);
		template<typename U> static no HasSlot_15(...);

	public:
		virtual ~jsSlots() = default;

		static Jigsaw::slot_receiver_definition GetReceiverDefinition() {
			Jigsaw::slot_receiver_definition receiver_definition;

			if constexpr (sizeof(HasSlot_1<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_1_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_2<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_2_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_3<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_3_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_4<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_4_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_5<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_5_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_6<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_6_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_7<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_7_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_8<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_8_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_9<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_9_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_10<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_10_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_11<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_11_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_12<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_12_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_13<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_13_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_14<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_14_def::stateless_slot_def());
			}
			if constexpr (sizeof(HasSlot_15<T>(0)) == sizeof(yes)) {
				receiver_definition.slotDefinitions.push_back(T::slot_15_def::stateless_slot_def());
			}

			receiver_definition.receiverType = &Jigsaw::etype_info::Id<T>();
			return receiver_definition;
		}

	protected:
		using _THIS_TYPE = T;

	};

	// fwd
	template <typename T>
	struct get_slot_ext;
	template <typename T>
	struct has_slot_ext;

	/// <summary>
	/// For classes that inherit from a jsSlotsBase instance, an extension can be named that exposes the slots
	/// from the parent and the child. 
	/// </summary>
	/// <typeparam name="Impl">The implementing class, eg. class SomeClass : public jsSlotsExtension<SomeClass, BaseClass></typeparam>
	/// <typeparam name="Base"></typeparam>
	template <typename Impl, typename Base>
	class JGSW_API jsSlotsExtension : virtual public jsSlots<Impl>, public Base {
	public:
		jsSlotsExtension<Impl, Base>(const jsSlotsExtension<Impl, Base>&) = delete;
		jsSlotsExtension<Impl, Base>(jsSlotsExtension<Impl, Base>&&) = delete;

		template<typename...Args>
		jsSlotsExtension(Args&&...args) : Base(std::forward<Args>(args)...) {}

		/// <summary>
		/// Replacement of the GetReceiverDefinition method
		/// </summary>
		/// <returns></returns>
		static Jigsaw::slot_receiver_definition GetReceiverDefinition() 
		{
			static_assert(std::is_base_of_v<Base, Impl>() && !std::is_same_v<Impl, Base>(), "'jsSlotsExtension' must only be used with a class that derives from the specified base.");
			slot_receiver_definition receiver = jsSlots<Impl>::GetReceiverDefinition();
			std::vector<slot_definition> base_defs = jsSlots<Base>::GetReceiverDefinition().slotDefinitions;
			for (slot_definition& def : base_defs) 
			{
				receiver.slotDefinitions.push_back(std::move(def));
			}
			return receiver;
		}

	private:
		friend Impl;
		friend struct has_slot_ext<Impl>;
		friend struct get_slot_ext<Impl>;
		using __slotextbase = Base;
		using _THIS_TYPE = Impl;

	};

	/// <summary>
	/// Utility for extracting the base type from a class that extends SLOT_EXT<T, U>.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template<typename T>
	struct has_slot_ext 
	{
	private:
		template <typename U> static u8 check(typename U::__slotextbase*);
		template <typename U> static u16 check(...);
	public:
		static constexpr bool value = sizeof(u8) == sizeof(check<T>(0));
	};

	/// <summary>
	/// Utility for retrieving the base type from a jsSlotsExtension. Should only be used if 'if constexpr (has_slot_ext<T>::value)' evaluates to true
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template<typename T>
	struct get_slot_ext 
	{
		typedef typename T::__slotextbase base_type;
	};

}
#endif