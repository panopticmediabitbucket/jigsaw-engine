/*********************************************************************
 * jsKnobGraph.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: April 2021
 *********************************************************************/
#ifndef _KNOB_GRAPH_H_
#define _KNOB_GRAPH_H_

#include <vector>
#include "Ref.h"
#include "Injection/injection_definitions.h"
#include "Injection/jsKnobProvider.h"
#include "_jgsw_api.h"

namespace Jigsaw 
{

	struct JGSW_API KNOB_GRAPH_LINK_RESULT 
	{
		bool success = false;
	};

	class jsContext;

	/// <summary>
	/// The KnobGraph takes a series of knob providers, fetches the definitions of all the knobs attached to them, and constructs a free-floating, acyclic graph for them..
	/// After the initial construction of the graph, any errors can be examined. In order to actually resolve all of the dependencies, the graph must be in a valid state, and then.
	/// 'Resolve' is called by the user. 
	/// </summary>
	class JGSW_API jsKnobGraph 
	{
	public:
		jsKnobGraph();

		/// <summary>
		/// The primary constructor requests a knob_definition enumerating the jsContext and the knob_providers containing all of the knob_definitions to be examined.
		/// <param name="context_def"></param>
		/// <param name="knob_providers"></param>
		/// </summary>
		jsKnobGraph(
			knob_definition&& context_def,
			std::vector<Jigsaw::Ref<Jigsaw::jsKnobProvider>>&& knob_providers,
			std::vector<Jigsaw::jsKnobGraph*>&& graph_dependencies
		);

		/// <summary>
		/// Each _node in the 'all_nodes' list will be resolved recursively. If the _node has already been resolved, then it will skip.
		/// <returns></returns>
		/// </summary>
		std::vector<knob_definition> Resolve();

		/// <summary>
		/// Returns any jsContext knobs associated with the graph.
		/// <returns></returns>
		/// </summary>
		std::vector<jsContext*> GetJigsawDependentContextKnobs();

		/// <summary>
		/// Fills a slot provided from an unknown source.
		/// <param name="slot_definition"></param>
		/// </summary>
		KNOB_GRAPH_LINK_RESULT FillExternalSlot(const slot_definition& slot_definition) const;

	private:
		struct _dependency;

		/// <summary>
		/// The _node is the core data struct used to link all the definitions together into a web of dependencies.
		/// </summary>
		struct _node 
		{
			knob_definition knob;
			std::vector<_dependency> constructor_dependencies;
			std::vector<_dependency> slot_dependencies;
			bool examined = false;
		};

		/// <summary>
		/// The _dependency struct is consumed by the _node struct as an indicator of a _node that the given _node is dependent on.
		/// </summary>
		struct _dependency 
		{
			_node* on = nullptr;
			slot_definition slot;
			bool lazy = false;

			static _dependency On(_node* node, const slot_definition& slot_def);
		};

		/// <summary>
		/// LinkDependencies is called in the constructor. It is the initial step where all of the dependencies are wired together and the graph is constructed recursively.
		/// <param name="node"></param>
		/// <param name="visited_node_list"></param>
		/// </summary>
		void LinkDependencies(_node* node, std::vector<_node*> visited_node_list);

		/// <summary>
		/// A second method used to LinkDependencies of dependencies--recursive implementation.
		/// <param name="dependencies"></param>
		/// <param name="visited_node_list"></param>
		/// </summary>
		void LinkDependencies(std::vector<_dependency>& dependencies, std::vector<_node*> visited_node_list);

		/// <summary>
		/// The internal call that happens on the user's call to 'Resolve'. 
		/// <param name="node"></param>
		/// </summary>
		void ResolveDependencies(_node* node);

		/// <summary>
		/// Finds matching knobs for a slot. This should be one item long for non-collection slots.
		/// <param name="nodes"></param>
		/// <param name="slot_definition"></param>
		/// <returns></returns>
		/// </summary>
		std::vector<_dependency> FindMatchingKnobsForSlot(const Jigsaw::slot_definition& slot_definition) const;

		/// <summary>
		/// Searches for a knob whose name matches the slot definition. If a single matching name exists, the cull_list is replaced with a list with only the matching name.
		///  Otherwise, the cull_list is unchanged..
		/// <param name="cull_list"></param>
		/// <param name="definition"></param>
		/// </summary>
		void CullForNameMatch(std::vector<_node*>& cull_list, const Jigsaw::slot_definition& definition) const;

		/// <summary>
		/// If the slot_definition has a qualifier, the cull_list will have any nodes with non-matching qualifiers removed.
		/// <param name="cull_list"></param>
		/// <param name="definition"></param>
		/// </summary>
		void CullForQualifierMatch(std::vector<_node*>& cull_list, const Jigsaw::slot_definition& definition) const;

		/// <summary>
		/// Resolves the _dependency, constructing all of its dependencies (if any) and then the _dependency's node. 'lazy' dependencies will be added to the lazy list and deferred.
		/// <param name="dependency"></param>
		/// </summary>
		void ResolveDependencyOrDeferLazy(_dependency& dependency);

#if JGSW_DEBUG

		/// <summary>
		/// Builds a string indicating all of the different matches. This is used in debug builds to display error information.
		/// <param name="match_list"></param>
		/// <returns></returns>
		/// </summary>
		std::string BuildMatchString(const std::vector<_node*>& match_list) const;

		/// <summary>
		/// Builds a string indicating a cyclic dependency.
		/// <param name="node"></param>
		/// <param name="cycle"></param>
		/// <param name="cycle_end"></param>
		/// <returns></returns>
		/// </summary>
		std::string CyclicDependencyErrorBuilder(_node* node, std::vector<_node*>::iterator cycle, const std::vector<_node*>::iterator& cycle_end);

#endif // JGSW_DEBUG

		/// <summary>
		/// This method locates a corresponding node that can be used to fill the each of defined dependencies and pushes a reference
		/// to that node into the node_dependency_container
		/// </summary>
		/// <param name="dependencies"></param>
		/// <param name="node_dependency_container"></param>
		/// <param name="knob_name"></param>
		void AddDirectedEdgesForDependencies(std::vector<_dependency>& node_dependency_container, std::vector<Jigsaw::slot_definition>& defined_dependencies);

		std::vector<_node*> m_allNodes;
		std::vector<_dependency> m_lazyDependencies;
		std::vector<Jigsaw::Ref<Jigsaw::jsKnobProvider>> m_knobProviders;
		std::vector<Jigsaw::jsKnobGraph*> m_graphDependencies;
	};
}
#endif
