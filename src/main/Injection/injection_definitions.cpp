#include "injection_definitions.h"
#include "Injection/jsSlots.h"
#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw {

	//////////////////////////////////////////////////////////////////////////

	START_REGISTER_SERIALIZABLE_CLASS(namespace_import)
		END_REGISTER_SERIALIZABLE_CLASS(namespace_import)

		std::vector<slot_definition>& GetSlots(Jigsaw::jsSlotsBase& slots) {
		return slots.slots;
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<slot_definition>& GetSlots(Jigsaw::jsSlotsBase* slots)
	{
		return slots->slots;
	}

	//////////////////////////////////////////////////////////////////////////

	JGSW_API bool TypeMatch(const Jigsaw::knob_definition& knob, const Jigsaw::slot_definition& slot)
	{
		bool match = knob.typeInfo == slot.typeInfo;
		if (match)
		{
			return true;
		}
		else
		{
			auto iter = knob.superClassCasters.find(*slot.typeInfo);
			return iter != knob.superClassCasters.end();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	JGSW_API void FillSlotWithKnob(const Jigsaw::slot_definition& slot, const Jigsaw::knob_definition& knob)
	{
		if (slot.typeInfo == knob.typeInfo)
		{
			slot.fillSlotWith(knob.referenceContainer, slot.referenceContainer);
		}
		else
		{
			void* castedKnobContainer = nullptr;
			etype_index idx(*slot.typeInfo);
			knob.superClassCasters.at(idx)(knob.referenceContainer, &castedKnobContainer);
			slot.fillSlotWith(&castedKnobContainer, slot.referenceContainer);
		}
	}

	//////////////////////////////////////////////////////////////////////////
}

