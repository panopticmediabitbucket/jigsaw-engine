/*********************************************************************
 * jsKnobGraphDefinitionBuilder.h
 *
 * A templated implementation of knob_definition and slot_definition construction designed to disambiguate
 * the construction process of those data types.
 *
 * Author: jl_ra
 * Originally created: April 2021
 *********************************************************************/
#ifndef _KNOB_GRAPH_NODE_BUILDER_H_
#define _KNOB_GRAPH_NODE_BUILDER_H_

// Jigsaw_Engine
#include "Debug/j_debug.h"
#include "Injection/injection_definitions.h"
#include "jsContext.h"
#include "jsSlots.h"
#include "Ref.h"

namespace Jigsaw
{
	enum class SLOT_COLLECTION_TYPE
	{
		NONE, VECTOR
	};

	/// <summary>
	/// jsKnobGraphDefinitionBuilder is a templated implementation that returns a knob_definition or slot_definition (depending on what's needed at the time)
	/// for a Ref or collection of Refs of type T. 
	/// </summary>
	template<typename T>
	class JGSW_API jsKnobGraphDefinitionBuilder
	{
	public:

		/// <summary>
		/// Sole constructor. The name is always required.
		/// <param name="name"></param>
		/// </summary>
		jsKnobGraphDefinitionBuilder(const char* name)
		{
			this->m_name = name;
		}

		/// <summary>
		/// Completes the construction of the knob_definition requested by the builder calls at the call site.
		/// <returns></returns>
		/// </summary>
		knob_definition CompileKnob()
		{
			if (!reference)
			{
				reference = new T*(nullptr);
			}

			knob_definition definition;
			definition.referenceContainer = reference;
			definition.name = m_name;
			definition.qualifier = std::move(m_qualifier);

			definition.postConstruct = postConstruct;
			if (m_createFunction)
			{
				definition.customConstruct = m_createFunction;
			}
			else
			{
				definition.construct = &CreateReferenceFunction;
			}
			definition.isConstructed = &IsReferencePresent;
			definition.stateless_slot_definitions = GetStatelessSlotDefinitions<T>();
			definition.destructor = &ReferenceDestructor;
			definition.instanceSlotGetter = &GetInstanceSlotDefinition;

			if constexpr (std::is_base_of_v<jsContext, T>)
			{
				definition.getDependentContext = &GetDependentContext;
			}

			definition.constructor_dependencies = std::move(m_constructorDependencies);
			definition.superClassCasters = std::move(m_superClassCasters);

			definition.typeInfo = m_typeInfo;
			return definition;
		}

		/// <summary>
		/// Completes the construction of the slot_definition requested by the builder calls at the call site.
		/// <returns></returns>
		/// </summary>
		slot_definition CompileSlot()
		{
			slot_definition definition;
			definition.name = m_name;
			definition.qualifier = m_qualifier;
			definition.isCollection = m_collectionType != SLOT_COLLECTION_TYPE::NONE;
			definition.referenceContainer = reference;

			if (m_collectionType == SLOT_COLLECTION_TYPE::NONE)
			{
				definition.isConstructed = &IsReferencePresent;
				definition.destructor = &ReferenceDestructor;
			}

			J_D_ASSERT_LOG_ERROR(m_setterFunction, jsKnobGraphDefinitionBuilder, "m_setterFunction is null! Are you missing a call to 'SetSetterDestination'?");
			definition.fillSlotWith = m_setterFunction;
			definition.typeInfo = &etype_info::Id<T>();
			definition.lazy = lazy;

			bool is_context = std::is_base_of_v<jsContext, T>;
			bool context_compatible = (lazy && is_context) || !is_context;
			J_D_ASSERT_LOG_ERROR(context_compatible, jsKnobGraphDefinitionBuilder<T>,
				"A slot that is a JigsawDependentContext must include the 'LAZY' modifier. It can not be a constructor argument. Error building slot type {0} with name {1}", definition.typeInfo->GetQualifiedName(), definition.name.c_str());

			return definition;
		}

		/// <summary>
		/// A type override. 
		/// </summary>
		/// <param name="typeInfo"></param>
		/// <returns></returns>
		inline jsKnobGraphDefinitionBuilder<T>& OverrideType(const etype_info* typeInfo)
		{
			J_D_ASSERT_LOG_ERROR(typeInfo->DescendsFrom<T>(), jsKnobGraphDefinitionBuilder<T>, "Attempted to use non-descendant type {0} while building a knob for {1}", typeInfo->GetQualifiedName(), m_typeInfo->GetQualifiedName());
			m_typeInfo = typeInfo;
			return *this;
		}

		/// <summary>
		/// Completes the construction of a stateless slot definition.
		/// <returns></returns>
		/// </summary>
		slot_definition CompileStatelessSlot(bool is_collection)
		{
			slot_definition definition;
			definition.name = std::move(m_name);
			definition.qualifier = std::move(m_qualifier);
			definition.isCollection = is_collection;
			definition.typeInfo = &etype_info::Id<T>();
			definition.lazy = lazy;

			return definition;
		}

		/// <summary>
		/// Adds a constructor dependency for the knob_definition that's being built.
		/// <param name="slot"></param>
		/// <returns></returns>
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& AddConstructorDependency(slot_definition slot)
		{
			m_constructorDependencies.push_back(std::move(slot));
			return *this;
		}

		/// <summary>
		/// A second, internal template function. Takes a generic type T_PAR and forms a casting function to convert back and forth. 
		/// <returns></returns>
		/// </summary>
		template<typename T_PAR>
		inline jsKnobGraphDefinitionBuilder<T>& AddSuperType()
		{
			static_assert(std::is_base_of_v<T_PAR, T>);
			static const auto& t_par_info = etype_info::Id<T_PAR>();
			m_superClassCasters.insert(std::make_pair(etype_index(t_par_info), &Cast<T_PAR>));
			return *this;
		}

		/// <summary>
		/// Sets the internal reference that will be written to/read from. If it is not set with this function, the builder will create its own. 
		/// <param name="reference"></param>
		/// <returns></returns>
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetReference(T** reference)
		{
			this->reference = reference;
			return *this;
		}

		/// <summary>
		/// Pass the function that will populate the underlying reference.
		/// <param name="m_createFunction"></param>
		/// <returns></returns>
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetCreateReferenceFunction(knob_definition::CustomConstructor ctor)
		{
			m_createFunction = ctor;
			return *this;
		}

		/// <summary>
		/// Sets the internal qualifier argument for the knob_definition or slot_definition
		/// <param name="qualifier"></param>
		/// <returns></returns>
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetQualifier(const char* qualifier)
		{
			this->m_qualifier = qualifier;
			return *this;
		}

		/// <summary>
		/// Sets the internal qualifier argument for the knob_definition or slot_definition
		/// <param name="qualifier"></param>
		/// <returns></returns>
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetQualifier(hash_val qualifier)
		{
			this->m_qualifier = sysHashString(qualifier);
			return *this;
		}

		/// <summary>
		/// These are highly-advanced functions that should only be used by macros or those who intimately understand the knob graph.
		/// 
		/// A destination of unknown type is specified for the slot_definition to be built. When the corresponding 'injection_type'
		/// This SetterFunction specifies a vector as the destination
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetLazy(bool lazy)
		{
			this->lazy = lazy;
			return *this;
		}

		/// <summary>
		/// These are highly-advanced functions that should only be used by macros or those who intimately understand the knob graph.
		/// 
		/// A destination of unknown type is specified for the slot_definition to be built. When the corresponding 'injection_type'
		/// This SetterFunction specifies a vector as the destination
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetSetterDestination(std::vector<T*>* destination)
		{
			m_collectionType = SLOT_COLLECTION_TYPE::VECTOR;
			reference = destination;
			m_setterFunction = &TransmitReferenceToVector;
			return *this;
		}

		/// <summary>
		/// These are highly-advanced functions that should only be used by macros or those who intimately understand the knob graph.
		/// 
		/// A destination of unknown type is specified for the slot_definition to be built. When the corresponding 'injection_type'
		/// This SetterFunction specifies a single reference that can be populated.
		/// </summary>
		inline jsKnobGraphDefinitionBuilder<T>& SetSetterDestination(T** destination)
		{
			m_collectionType = SLOT_COLLECTION_TYPE::NONE;
			reference = destination;
			m_setterFunction = &TransmitReference;
			return *this;
		}

		/// <summary>
		/// Sets a post-construct routine for after the knob is constructed. 'T_PAR' is the type that owns the function. 'T' must publicly inherit from 'T_PAR'
		/// </summary>
		/// <typeparam name="O"></typeparam>
		/// <returns></returns>
		template <typename T_PAR, void (T_PAR::* func)()>
		inline jsKnobGraphDefinitionBuilder<T>& SetPostConstruct()
		{
			postConstruct = &DynamicCallback<T_PAR, func>;
			return *this;
		}

	private:
		const char* m_name;
		const char* m_qualifier;
		bool lazy = false;
		void* reference = nullptr;
		knob_definition::CustomConstructor m_createFunction;
		const etype_info* m_typeInfo = &etype_info::Id<T>();

		inj_definition::ReferenceHandler postConstruct = nullptr;
		slot_definition::ReferenceTransmitter m_setterFunction = nullptr;

		SLOT_COLLECTION_TYPE m_collectionType;
		void* destination = nullptr;

		std::vector<slot_definition> m_constructorDependencies;
		std::map<etype_index, knob_definition::CastFunction> m_superClassCasters;

		template <typename T_PAR>
		static void Cast(KNOBOBJ* _from, SLOTOBJ* _to)
		{
			T** from = static_cast<T**>(_from);
			T_PAR** to = static_cast<T_PAR**>(_to);
			*to = static_cast<T*>(*from);
		}

		/// <summary>
		/// The function used to fill a reference on a slot
		/// </summary>
		/// <param name="_from"></param>
		/// <param name="_to"></param>
		static void TransmitReference(KNOBOBJ* _from, SLOTOBJ* _to)
		{
			T** from = static_cast<T**>(_from);
			T** to = static_cast<T**>(_to);
			*to = dynamic_cast<T*>(*from);
		}

		/// <summary>
		/// The function used to populate a vector slot with a reference
		/// </summary>
		/// <param name="_from"></param>
		/// <param name="_to"></param>
		static void TransmitReferenceToVector(KNOBOBJ* _from, SLOTOBJ* _toVector)
		{
			T** from = static_cast<T**>(_from);
			std::vector<T*>* toVector = static_cast<std::vector<T*>*>(_toVector);
			toVector->push_back(dynamic_cast<T*>(*from));
		}

		/// <summary>
		/// A template used for creating anonymous member callback functions. Used initially for post-construct scenarios.
		/// </summary>
		/// <param name="_ref"></param>
		template <typename T_PAR, void (T_PAR::* func)()>
		inline static void DynamicCallback(void* _ref)
		{
			static_assert(std::is_base_of_v<T_PAR, T>);
			T& ref = **static_cast<T**>(_ref);
			(ref.*func)();
		}

		/// <summary>
		/// Creates a function that will check if the container holding a copy of the reference exists.
		/// <param name="reference"></param>
		/// <returns></returns>
		/// </summary>
		inline static bool IsReferencePresent(void* _reference)
		{
			T*& reference = *static_cast<T**>(_reference);
			return (bool)(reference);
		}

		/// <summary>
		/// Function for finalizing the node storage and the node itself.
		/// <param name="reference"></param>
		/// <returns></returns>
		/// </summary>
		inline static void ReferenceDestructor(void* _reference)
		{
			T** reference = static_cast<T**>(_reference);
			delete *reference;
			delete reference;
		}

		/// <summary>
		/// Generates a generic 'create reference function'. For types that need data to be passed into the constructor, this will not work.
		/// <param name="reference"></param>
		/// <returns></returns>
		/// </summary>
		inline static void CreateReferenceFunction(void* _reference)
		{
			if constexpr (std::is_default_constructible_v<T>)
			{
				T** reference = static_cast<T**>(_reference);
				(*reference) = new T;
			}
		}

		/// <summary>
		/// Returns a function that will out the attached slots if any exist.
		/// <param name="reference"></param>
		/// <returns></returns>
		/// </summary>
		template<typename U>
		inline static std::vector<slot_definition> GetStatelessSlotDefinitions()
		{
			if constexpr (Jigsaw::has_slot_ext<U>::value)
			{
				std::vector<slot_definition> upper_types = jsSlots<U>::GetReceiverDefinition().slotDefinitions;
				std::vector<slot_definition> lower_types = GetStatelessSlotDefinitions<typename Jigsaw::get_slot_ext<U>::base_type>();
				upper_types.reserve(lower_types.size() + upper_types.size());
				for (slot_definition& lower : lower_types)
				{
					upper_types.push_back(std::move(lower));
				}
				return upper_types;
			}
			else if constexpr (std::is_base_of_v<Jigsaw::jsSlots<U>, U>)
			{
				return jsSlots<U>::GetReceiverDefinition().slotDefinitions;
			}
			else if constexpr (std::is_base_of_v<Jigsaw::jsSlotsBase, U>)
			{
				return U::GetReceiverDefinition().slotDefinitions;
			}
			return std::vector<slot_definition>();
		}

		/// <summary>
		/// Gets a function that retrieves a specific slot from an instance.
		/// <param name="instance"></param>
		/// <returns></returns>
		/// </summary>
		inline static slot_definition& GetInstanceSlotDefinition(void* instance, const sysHashString& hash_name)
		{
			if constexpr (std::is_base_of_v<jsSlotsBase, T>)
			{
				T*& ref = *static_cast<T**>(instance);
				jsSlotsBase* slots_class = dynamic_cast<jsSlotsBase*>(ref);
				J_D_ASSERT_LOG_ERROR(slots_class, jsKnobGraphDefinitionBuilder, "A fatal error has occurred while getting instance slots for type {0}, are you sure the 'jsSlots<>' interface is public?", etype_info::Id<T>().GetQualifiedName());
				std::vector<slot_definition>& slot_defs = GetSlots(slots_class);
				for (slot_definition& slot_def : slot_defs)
				{
					if (slot_def.name == hash_name)
					{
						return slot_def;
					}
				}
			}
			static slot_definition empty;
			J_D_ASSERT_LOG_ERROR(false, jsKnobGraphDefinitionBuilder, "A fatal error has occurred while trying to fetch an instance slot definition with name '{0}'", hash_name.c_str());
			return empty;
		}

		/// <summary>
		/// Returns a function that will fetch a dependent context pointer to be resolved.
		/// <param name="instance"></param>
		/// <returns></returns>
		/// </summary>
		inline static jsContext* GetDependentContext(void* _instance)
		{
			if constexpr (std::is_base_of_v<jsContext, T>)
			{
				T** instance = static_cast<T**>(_instance);
				jsContext* context = dynamic_cast<jsContext*>(*instance);
				return context;
			}
			else
			{
				return nullptr;
			}
		}
	};
}

#endif