/*********************************************************************
 * JigsawInjection.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _JIGSAW_INJECTION_H_
#define _JIGSAW_INJECTION_H_

// Jigsaw_Engine
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Debug/ScopedLoggingTimer.h"
#include "Debug/j_debug.h"
#include "Injection/jsContext.h"
#include "Injection/jsKnobProvider.h"
#include "Injection/jsSlots.h"
#include "Injection/injection_definitions.h"
#include "Injection/jsKnobGraphDefinitionBuilder.h"
#include "Util/DTO.h"
#include "Util/looping_macros.h"

#ifndef _EXPAND
#define _EXPAND(...) __VA_ARGS__
#endif

//////////////////////////////////////////////////////////////////////////

/// JGSW_KNOB_PROVIDER -- Declare a jsKnobProvider.
/// jsKnobProviders house a set of Knob definitions that are constructed during dependency resolution and injection when a jsContext is created.
/// The KnobProvider must also be 'Registered' with the global set of 'namespaced' knob providers. 
///
/// There are other ways of providing Knobs to a jsContext, but they often involve a more specialized implementation. The jsKnobProvider covers
/// the most basic case: we have a set of classes that we need to initialize as a part of jsContext initialization. The jsContext receives a set of namespaces,
/// fetches the jsKnobProviders associated with that namespace, constructs all of their knobs, and then resolves the dependencies between them.  

// EXAMPLE: Put something like this in a .cpp or .ipp file

// // Declaring the name of the KnobProvider
// JGSW_KNOB_PROVIDER(TestKnobProvider)
// {
//		// Declaring a Knob in the knob provider. 
//		KNOB(TestInheritedClass, inheritedClass, 
//			IS_A(TestParentClass), SLOT_ARG(ConstructorArgType, ctorArg), QUALIFIER("PrimaryParentClassInstance")
//		{
//			return new TestInheritedClass(ctorArg);
//		}
//	}
//
// START_JGSW_KNOB_PROVIDER_PROPERTIES(TestKnobProvider)
// PROVIDER_NAMESPACE("Test_Namespace")						// Declaring the namespace of the provider
// END_JGSW_KNOB_PROVIDER_PROPERTIES(TestKnobProvider)
// REGISTER_JGSW_KNOB_PROVIDER(TestKnobProvider);			// Not that we've completed our provider and declared its properties, we are ready to send it off!
// 

//////////////////////////////////////////////////////////////////////////

#define JGSW_KNOB_PROVIDER(classname) class classname : public Jigsaw::jsKnobProvider, virtual protected Jigsaw::DTO<classname>

#define START_JGSW_KNOB_PROVIDER_PROPERTIES(classname)\
namespace ___ {\
	Jigsaw::knob_provider_properties classname##_properties() {\
		Jigsaw::knob_provider_properties properties; 

#define START_JGSW_COMPONENT_PROPERTIES(classname, component)\
namespace ___ {\
	Jigsaw::knob_provider_properties component##_properties() {\
		Jigsaw::knob_provider_properties properties;

#define PROVIDER_NAMESPACE(str) properties.m_namespace = str;

#define IMPORT_NAMESPACE(str) \
{ \
	Jigsaw::namespace_import imp; \
	imp.m_namespace = str; \
	properties.m_namespaceImports.push_back(std::move(imp)); \
}

#define IMPORT_NAMESPACE_FILTER(str, ...) \
{ \
	Jigsaw::namespace_import imp; \
	imp.m_namespace = str; \
	imp.m_filterTypes = Jigsaw::etype_info::GetTypes<__VA_ARGS__>(); \
	properties.m_namespaceImports.push_back(std::move(imp)); \
}

#define END_JGSW_KNOB_PROVIDER_PROPERTIES(classname)\
		return properties;\
	}; \
}

#define END_JGSW_COMPONENT_PROPERTIES(classname, component)\
		return properties;\
	}; \
}

#define REGISTER_JGSW_KNOB_PROVIDER(classname) \
Jigsaw::jsKnobProvider* classname##_constructor() {\
	return new classname;\
}\
struct classname##_register_def { \
	classname##_register_def() {\
		Jigsaw::knob_provider_definition provider_definition; \
		provider_definition.provider_type = &Jigsaw::etype_info::Id<classname>(); \
		provider_definition.properties = ___::##classname##_properties(); \
		provider_definition.build_provider = classname##_constructor;\
		provider_definition.name = #classname; \
		Jigsaw::Assembly::JigsawStaticContext::SubmitProvider(provider_definition); \
	}\
}; \
extern classname##_register_def classname##_register_def_call; \
classname##_register_def classname##_register_def_call;

template <typename T, char const* name>
Jigsaw::jsKnobProvider* ComponentConstructorFunction() {
	using namespace Jigsaw;
	Jigsaw::jsKnobProvider* provider = new Jigsaw::jsKnobProvider;
	jsKnobGraphDefinitionBuilder<T> builder(name);
	provider->knobs.push_back(builder.CompileKnob());
	return provider;
}

#define JGSW_COMPONENT(classname, component) \
char component##_name_global[] = #component; \
struct component##_register_def { \
	component##_register_def() {\
		Jigsaw::knob_provider_definition provider_definition; \
		provider_definition.properties = ___::##component##_properties(); \
		provider_definition.build_provider = &ComponentConstructorFunction<classname, component##_name_global>; \
		Jigsaw::Assembly::JigsawStaticContext::SubmitProvider(provider_definition); \
	}\
}; \
component##_register_def component##_register_def_call;

//////////////////////////////////////////////////////////////////////////

/// KNOB ROUTINES FOR ARGUMENTS, INTERNAL

//////////////////////////////////////////////////////////////////////////

// PUSH - Initial add of data to builder. Often the only step.

#define _PUSH_SLOT(classname, type_name, qual) { \
classname** ref = new classname*; \
jsKnobGraphDefinitionBuilder<classname> slot_builder(#type_name); \
slot_builder.SetQualifier(qual).SetSetterDestination(ref); \
Jigsaw::slot_definition slot_def = slot_builder.CompileSlot(); \
builder.AddConstructorDependency(slot_def); \
constructor_dependencies.push_back(std::move(slot_def)); \
}
#define _PUSH_IS_A(classname, type_name, qual) builder.AddSuperType<type_name>(); 
#define _PUSH_QUAL(qual, b, c) builder.SetQualifier(qual);
#define _PUSH_POST_CONSTRUCT(owner, func, c) builder.SetPostConstruct<owner, &owner::func>();

// HARVEST - Extraction of data during build function

#define _HARVEST_SLOT(classname, type_name, qual, ...) *static_cast<classname**>((*std::find_if(constructor_dependencies.begin(), constructor_dependencies.end(), [&](const Jigsaw::slot_definition& slot_def) { return slot_def.typeInfo == &Jigsaw::etype_info::Id<classname>(); } )).referenceContainer) __VA_ARGS__
#define _HARVEST_IS_A(type_name, classname, qual, ...) 
#define _HARVEST_QUAL(a, b, c, ...)
#define _HARVEST_POST_CONSTRUCT(a, b, c, ...)

// ARG - Conversion of macro into Knob function argument

#define _ARG_SLOT(classname, type_name, qual, ...) classname* type_name __VA_ARGS__
#define _ARG_IS_A(classname, type_name, qual, ...)
#define _ARG_QUAL(classname, type_name, qual, ...)
#define _ARG_POST_CONSTRUCT(classname, type_name, qual, ...)

//////////////////////////////////////////////////////////////////////////

/// SLOT ROUTINES FOR ARGUMENTS, INTERNAL

//////////////////////////////////////////////////////////////////////////

#define _SLOT_QUAL(qual, b, c, ...) builder.SetQualifier(std::move(qual));
#define _SLOT_LAZY(...) builder.SetLazy(true);

//////////////////////////////////////////////////////////////////////////

/// LOOPING ACROSS THE PROVIDED MACRO ARGUMENTS AND EXPANDING THEM WITH A MODIFIER, INTERNAL

//////////////////////////////////////////////////////////////////////////

#define _inj_mac_fe_1(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c)
#define _inj_mac_fe_2(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_1(mod, __VA_ARGS__))
#define _inj_mac_fe_3(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_2(mod, __VA_ARGS__))
#define _inj_mac_fe_4(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_3(mod, __VA_ARGS__))
#define _inj_mac_fe_5(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_4(mod, __VA_ARGS__))
#define _inj_mac_fe_6(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_5(mod, __VA_ARGS__))
#define _inj_mac_fe_7(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_6(mod, __VA_ARGS__))
#define _inj_mac_fe_8(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c) _EXPAND(_inj_mac_fe_7(mod, __VA_ARGS__))

#define _COMMA ,

#define _inj_arg_mac_fe_1(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c)
#define _inj_arg_mac_fe_2(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_1(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_3(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_2(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_4(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_3(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_5(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_4(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_6(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_5(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_7(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_6(mod, __VA_ARGS__))
#define _inj_arg_mac_fe_8(mod, mac, a, b, c, ...) _EXPAND(mod##mac)(a, b, c, _COMMA) _EXPAND(_inj_arg_mac_fe_7(mod, __VA_ARGS__))

#define _INJ_ARG_LOOP(_1, __1, ___1, ____1, _2, __2, ___2, ____2, _3, __3, ___3, ____3, _4, __4, ___4, ____4, _5, __5, ___5, ____5, _6, __6, ___6, ____6, _7, __7, ___7, ____7, _8, __8, ___8, ____8, N, ...)N

//////////////////////////////////////////////////////////////////////////

/// INJECTION ARGUMENTS, EXTERNAL

/// Each of these must expand to exactly 4 arguments, even if that means having placeholder values (i.e. "")

/// This is done so that the _INJ_ARG_LOOP that processes the modifiers will have uniform expansion.

//////////////////////////////////////////////////////////////////////////

// KNOB ONLY 

#define SLOT_ARG(classname, type_name) _SLOT, classname, type_name, ""
#define SLOT_ARG_QUAL(classname, type_name, qualifier) _SLOT, classname, type_name, qualifier
#define IS_A(type_name) _IS_A, _KNOB_TYPE, type_name, ""
#define IS_AN(type_name) _IS_A, _KNOB_TYPE, type_name, ""
#define POST_CONSTRUCT(owner, func) _POST_CONSTRUCT, owner, func, ""

// SHARED

#define QUALIFIER(qualifier) _QUAL, qualifier, "", ""
#define _NONE(...) 

// SLOT ONLY

#define LAZY _LAZY, "", "", ""

//////////////////////////////////////////////////////////////////////////

// KNOB user interface

//////////////////////////////////////////////////////////////////////////

#define KNOB(type_name, knob_name, ...) \
struct knob_name##_knob_def { \
	knob_name##_knob_def(_THIS_TYPE& provider) { \
		using namespace Jigsaw; \
		typedef type_name _KNOB_TYPE; \
		jsKnobGraphDefinitionBuilder<_KNOB_TYPE> builder(#knob_name); \
		std::vector<Jigsaw::slot_definition> constructor_dependencies; \
\
		/* Per-macro argument expansion with _PUSH modifier. */ \
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _NONE)(_PUSH, __VA_ARGS__)) \
		type_name** knob_ref = new type_name*(nullptr); \
		builder.SetReference(knob_ref).SetCreateReferenceFunction(&knob_name##_knob_def##_ctor); \
		provider.knobs.insert(provider.knobs.end(), builder.CompileKnob()); \
	}; \
private: \
static void knob_name##_knob_def##_ctor(Jigsaw::knob_definition& def) \
{ \
	std::vector<Jigsaw::slot_definition>& constructor_dependencies = def.constructor_dependencies; \
	*static_cast<type_name**>(def.referenceContainer) = \
		/* Invoking the 'get' function with an expansion of the macro arguments with the _HARVEST modifier. */ \
_THIS_TYPE::knob_name##_get(\
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _NONE)(_HARVEST, __VA_ARGS__)) \
); \
	for (Jigsaw::slot_definition& def : constructor_dependencies) { delete def.referenceContainer; } \
}; \
}; \
\
/* Creating the signature of the 'get' function using the expansion of the macro arguments with the _ARG modifier. It is the user's responsibility to implement this function.*/ \
knob_name##_knob_def knob_name##_knob = knob_name##_knob_def(*this); \
static type_name* knob_name##_get( \
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_8, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_7, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_6, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_5, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_4, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_3, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_2, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _inj_arg_mac_fe_1, _NONE)(_ARG, __VA_ARGS__)) \
) 

//////////////////////////////////////////////////////////////////////////

// SLOT internal 

//////////////////////////////////////////////////////////////////////////

constexpr bool ValidateSlotNumber(const char* slot_number);

#define _COMPLETE_SLOT(slot_number, collection_bool, type_name, slot_name, ...) \
friend class jsSlots<_THIS_TYPE>; \
struct slot_number##_def { \
	slot_number##_def(_THIS_TYPE& receiver) { \
	static_assert(#slot_number, "Invalid slot number. Please select between slot_(1-15)");\
	using namespace Jigsaw; \
		jsKnobGraphDefinitionBuilder<type_name> builder(#slot_name); \
		builder.SetSetterDestination(&receiver.##slot_name); \
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _NONE)(_SLOT, __VA_ARGS__)) \
		receiver.slots.push_back(builder.CompileSlot()); \
	}; \
\
	static Jigsaw::slot_definition stateless_slot_def() { \
	using namespace Jigsaw; \
		jsKnobGraphDefinitionBuilder<type_name> builder(#slot_name); \
		_EXPAND(_INJ_ARG_LOOP(__VA_ARGS__ _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_8, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_7, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_6, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_5, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_4, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_3, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_2, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _inj_mac_fe_1, _NONE)(_SLOT, __VA_ARGS__)) \
		return builder.CompileStatelessSlot(collection_bool);\
	};\
}; \
slot_number##_def slot_number##_slot = slot_number##_def(*this)

//////////////////////////////////////////////////////////////////////////

// SLOT user interface 

//////////////////////////////////////////////////////////////////////////

#define SLOT(slot_number, type_name, slot_name, ...) \
type_name* slot_name = nullptr; \
_COMPLETE_SLOT(slot_number, false, type_name, slot_name, __VA_ARGS__) 

#define SLOT_COLLECTION(slot_number, collection_type, type_name, slot_name, ...) \
collection_type<type_name*> slot_name; \
_COMPLETE_SLOT(slot_number, true, type_name, slot_name, __VA_ARGS__)

#define SLOT_1(...) _EXPAND(SLOT(slot_1, __VA_ARGS__))
#define SLOT_2(...) _EXPAND(SLOT(slot_2, __VA_ARGS__))
#define SLOT_3(...) _EXPAND(SLOT(slot_3, __VA_ARGS__))
#define SLOT_4(...) _EXPAND(SLOT(slot_4, __VA_ARGS__))
#define SLOT_5(...) _EXPAND(SLOT(slot_5, __VA_ARGS__))
#define SLOT_6(...) _EXPAND(SLOT(slot_6, __VA_ARGS__))
#define SLOT_7(...) _EXPAND(SLOT(slot_7, __VA_ARGS__))
#define SLOT_8(...) _EXPAND(SLOT(slot_8, __VA_ARGS__))
#define SLOT_9(...) _EXPAND(SLOT(slot_9, __VA_ARGS__))
#define SLOT_10(...) _EXPAND(SLOT(slot_10, __VA_ARGS__))
#define SLOT_11(...) _EXPAND(SLOT(slot_11, __VA_ARGS__))
#define SLOT_12(...) _EXPAND(SLOT(slot_12, __VA_ARGS__))
#define SLOT_13(...) _EXPAND(SLOT(slot_13, __VA_ARGS__))
#define SLOT_14(...) _EXPAND(SLOT(slot_14, __VA_ARGS__))
#define SLOT_15(...) _EXPAND(SLOT(slot_15, __VA_ARGS__))

#define INJECT_CONTEXT(slot_number, variable_name) \
SLOT(slot_number, Jigsaw::jsContext, variable_name, LAZY)

#endif