#include "jsKnobGraph.h"
#include "../Debug/ScopedLoggingTimer.h"
#include "Debug/j_debug.h"
#include <sstream>

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	/// jsKnobGraph Implementations

	//////////////////////////////////////////////////////////////////////////

	jsKnobGraph::jsKnobGraph(
		knob_definition&& context_def,
		std::vector<Jigsaw::Ref<Jigsaw::jsKnobProvider>>&& knob_providers,
		std::vector<Jigsaw::jsKnobGraph*>&& graph_dependencies
	) : m_knobProviders(std::move(knob_providers)), m_graphDependencies(std::move(graph_dependencies)) 
	{
		_node* node = new _node;
		node->knob = context_def;
		m_allNodes.push_back(node);

		// loading all of the knobs from the knob providers
		for (Jigsaw::Ref<Jigsaw::jsKnobProvider>& provider : m_knobProviders) 
		{
			// populating the map from the providers
			for (knob_definition& knob_def : provider->knobs) 
			{
				_node* node = new _node;
				node->knob = std::move(knob_def);
				m_allNodes.push_back(node);
			}
		}

		// linking all of the dependencies together
		START_TIMER_SCOPE(jsKnobGraph, "Linking dependencies for each node in the graph")
			std::vector<_node*> visitations;
		for (_node* node : m_allNodes) 
		{
			LinkDependencies(node, visitations);
		}
		END_TIMER_SCOPE
	}

	//////////////////////////////////////////////////////////////////////////

	jsKnobGraph::jsKnobGraph()
	{ }

	//////////////////////////////////////////////////////////////////////////

	std::vector<knob_definition> jsKnobGraph::Resolve() 
	{

		// on the first pass, regular dependencies are resolved
		for (_node* node : m_allNodes) 
		{
			ResolveDependencies(node);
		}

		// lazy dependencies are solved next
		for (_dependency dependency : m_lazyDependencies) 
		{
			FillSlotWithKnob(dependency.slot, dependency.on->knob);
		}

		std::vector<knob_definition> knobs;
		std::for_each(m_allNodes.begin(), m_allNodes.end(), [&knobs](_node* node) -> void { knobs.push_back(node->knob); });
		return knobs;
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<jsContext*> jsKnobGraph::GetJigsawDependentContextKnobs() 
	{
		std::vector<jsContext*> context_knobs;
		for (_node* node : m_allNodes) 
		{
			if (node->knob.getDependentContext) 
			{
				jsContext* dependent_context = node->knob.getDependentContext(node->knob.referenceContainer);
				context_knobs.push_back(dependent_context);
			}
		}
		return context_knobs;
	}

	//////////////////////////////////////////////////////////////////////////

	KNOB_GRAPH_LINK_RESULT jsKnobGraph::FillExternalSlot(const slot_definition& slot_definition) const
	{
		std::vector<_dependency> dependencies = FindMatchingKnobsForSlot(slot_definition);
		for (_dependency& dependency : dependencies) 
		{
			FillSlotWithKnob(slot_definition, dependency.on->knob);
		}
		return { true };
	}

	//////////////////////////////////////////////////////////////////////////

	jsKnobGraph::_dependency jsKnobGraph::_dependency::On(_node* node, const slot_definition& slot_def) 
	{
		return { node, slot_def, slot_def.lazy };
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<jsKnobGraph::_dependency> jsKnobGraph::FindMatchingKnobsForSlot(const Jigsaw::slot_definition& slot_definition) const
	{
		std::vector<_node*> matching_nodes;

		for (_node* node : m_allNodes) 
		{
			if (TypeMatch(node->knob, slot_definition)) 
			{
				matching_nodes.push_back(node);
			}
		}

		if (slot_definition.isCollection) 
		{
			CullForQualifierMatch(matching_nodes, slot_definition);
		}
		else 
		{
			if (matching_nodes.size() > 1) 
			{
				CullForQualifierMatch(matching_nodes, slot_definition);
			}
			CullForNameMatch(matching_nodes, slot_definition);

			DEBUG_SCOPED(std::string matches_string = BuildMatchString(matching_nodes);)
				J_D_ASSERT_LOG_ERROR((matching_nodes.size() == 1 || !this->m_graphDependencies.empty()), jsKnobGraph, "Expected a single match for the slot {0} with type {1} and qualifier {2}, but found {3}--- {4}",
					slot_definition.name, slot_definition.typeInfo->GetQualifiedName(), slot_definition.qualifier, matching_nodes.size(), matches_string);
		}

		// forming the dependency list
		std::vector<_dependency> ret_list;
		for (_node* node : matching_nodes) 
		{
			ret_list.push_back(_dependency::On(node, slot_definition));
		}

		// Examining dependent graphs for knobs that can fill the slot if none are found
		auto iter = this->m_graphDependencies.begin();
		while (ret_list.empty() && iter != this->m_graphDependencies.end()) 
		{
			J_LOG_INFO(jsKnobGraph, "Failed to find corresponding nodes to fill the given slot_definition {0}, examining a dependent graph", slot_definition.name);
			ret_list = (*iter)->FindMatchingKnobsForSlot(slot_definition);
			iter++;
		}

		return ret_list;
	}

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::CullForNameMatch(std::vector<_node*>& cull_list, const Jigsaw::slot_definition& definition) const
	{
		std::vector<_node*> ret_list;
		for (_node* node : cull_list) 
		{
			if (definition.name == node->knob.name) 
			{
				ret_list.push_back(node);
			}
		}

		if (!ret_list.empty()) 
		{
			cull_list = ret_list;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::CullForQualifierMatch(std::vector<_node*>& cull_list, const Jigsaw::slot_definition& definition) const
	{
		if (definition.qualifier.Value()) 
		{
			std::vector<_node*>::iterator iter = std::remove_if(cull_list.begin(), cull_list.end(), [&](_node* node) { return definition.qualifier != node->knob.qualifier; });
			cull_list.erase(iter, cull_list.end());
		}
	}

	//////////////////////////////////////////////////////////////////////////

#if JGSW_DEBUG
	std::string jsKnobGraph::BuildMatchString(const std::vector<_node*>& match_list) const
	{
		std::string ret;
		for (const _node* node : match_list) 
		{
			ret += std::string(node->knob.typeInfo->GetQualifiedName()) + ": '" + node->knob.name.c_str() + "' ";
		}

		return ret;
	}

	//////////////////////////////////////////////////////////////////////////

	std::string jsKnobGraph::CyclicDependencyErrorBuilder(_node* node, std::vector<_node*>::iterator cycle, const std::vector<_node*>::iterator& cycle_end) 
	{
		std::ostringstream out;
		while (cycle != cycle_end) 
		{
			out << "(Name: " << (*cycle)->knob.name.c_str() << ") " << (*cycle)->knob.typeInfo->GetQualifiedName() << " -> ";
			cycle++;
		}

		out << "(Name: " << node->knob.name.c_str() << ") " << node->knob.typeInfo->GetQualifiedName();

		return out.str();
	}

#endif // JGSW_DEBUG

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::ResolveDependencyOrDeferLazy(_dependency& dependency) 
	{
		J_D_ASSERT_LOG_ERROR(dependency.on != nullptr, jsKnobGraph, "Unexpected presence of a nullptr in ResolveDependencyOrDeferLazy");
		if (!dependency.on->knob.isConstructed(dependency.on->knob.referenceContainer) && dependency.lazy) 
		{
			m_lazyDependencies.push_back(dependency);
		}
		else 
		{
			ResolveDependencies(dependency.on);
			FillSlotWithKnob(dependency.slot, dependency.on->knob);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::AddDirectedEdgesForDependencies(std::vector<_dependency>& node_dependency_container, std::vector<Jigsaw::slot_definition>& defined_dependencies) 
	{
		for (Jigsaw::slot_definition& slot : defined_dependencies) 
		{
			std::vector<_dependency> match_list = FindMatchingKnobsForSlot(slot);

			for (_dependency& match : match_list) 
			{
				node_dependency_container.push_back(match);
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::LinkDependencies(_node* node, std::vector<_node*> visited_node_list) 
	{
		if (!node->examined) 
		{
			DEBUG_SCOPED(auto _iter = std::find(visited_node_list.begin(), visited_node_list.end(), node);)
				J_D_ASSERT_LOG_ERROR(_iter == visited_node_list.end(), jsKnobGraph, "Cyclic dependency detected in Knob Graph:::: {0}", CyclicDependencyErrorBuilder(node, _iter, visited_node_list.end()));

			visited_node_list.push_back(node);

			// linking the constructor dependencies
			knob_definition& definition = node->knob;
			J_LOG_INFO(jsKnobGraph, "Building constructor dependency edges for jsKnobGraph node type: {0}, name: {1}, qualifier: {2}", definition.typeInfo->GetQualifiedName(), definition.name.c_str(), definition.qualifier.c_str());
			AddDirectedEdgesForDependencies(node->constructor_dependencies, definition.constructor_dependencies);
			LinkDependencies(node->constructor_dependencies, visited_node_list);

			// linking any field slot dependencies attached to the object definition
			if (!definition.stateless_slot_definitions.empty()) 
			{
				J_LOG_INFO(jsKnobGraph, "Building member stateless slot dependency edges for jsKnobGraph node type: {0}, name: {1}, qualifier: {2}", definition.typeInfo->GetQualifiedName(), definition.name.c_str(), definition.qualifier.c_str());
				AddDirectedEdgesForDependencies(node->slot_dependencies, definition.stateless_slot_definitions);

				std::vector<_dependency> non_lazy_dependencies = node->slot_dependencies;
				auto iter = std::remove_if(non_lazy_dependencies.begin(), non_lazy_dependencies.end(), [](_dependency& dependency) { return dependency.lazy; });
				non_lazy_dependencies.erase(iter, non_lazy_dependencies.end());
				LinkDependencies(non_lazy_dependencies, visited_node_list);
			}

			node->examined = true;
			visited_node_list.erase(visited_node_list.end() - 1);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::LinkDependencies(std::vector<_dependency>& dependencies, std::vector<_node*> visited_node_list) 
	{
		for (_dependency dependency : dependencies) 
		{
			_node* which = dependency.on;
			LinkDependencies(which, visited_node_list);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jsKnobGraph::ResolveDependencies(_node* node) 
	{
		J_D_ASSERT_LOG_ERROR(node != nullptr, jsKnobGraph, "A nullptr was unexpectedly passed to 'ResolveDependencies'");
		knob_definition& knob = node->knob;
		if (!knob.isConstructed(knob.referenceContainer)) 
		{

			// resolving the dependencies needed to construct the object
			for (_dependency& dependency : node->constructor_dependencies) 
			{
				ResolveDependencyOrDeferLazy(dependency);
			}

			// constructing the object
			if (knob.construct)
				knob.construct(knob.referenceContainer);
			else
				knob.customConstruct(knob);

			// resolving the slot dependencies attached to the object
			for (_dependency& dependency : node->slot_dependencies) 
			{

				// once the knob is instantiated, the stateful slot definitions can be validated and retrieved
				dependency.slot = knob.instanceSlotGetter(knob.referenceContainer, dependency.slot.name);

				ResolveDependencyOrDeferLazy(dependency);
			}

			// calling the post-construct callback, if one exists
			if (knob.postConstruct) knob.postConstruct(knob.referenceContainer);
		}
	}

	//////////////////////////////////////////////////////////////////////////
}
