#ifndef _JIGSAW_KNOB_PROVIDER_H_
#define _JIGSAW_KNOB_PROVIDER_H_

#include "Injection/injection_definitions.h"
#include "RTTI/etype_info.h"
#include <functional>
#include "_jgsw_api.h"

namespace Jigsaw {

	/// <summary>
	/// The jsKnobProvider is the base class for any provider/configuration class that instantiates
	/// JigsawKnobs. The 'knobs' field is populated with the help of macros located in JigsawInjection.h.
	/// </summary>
	class JGSW_API jsKnobProvider {
	public:
		virtual ~jsKnobProvider() {}

		std::list<knob_definition> knobs;

	};

}



#endif