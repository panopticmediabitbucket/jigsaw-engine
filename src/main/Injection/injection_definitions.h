/*********************************************************************
 * injection_definitions.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: February 2021
 *********************************************************************/
#ifndef _INJECTION_DEFINITIONS_H_
#define _INJECTION_DEFINITIONS_H_

// std 
#include <algorithm>
#include <functional>
#include <map>
#include <set>
#include <string>

// Jigsaw_Engine
#include "Ref.h"
#include "RTTI/etype_info.h"
#include "System/sysHashString.h"

namespace Jigsaw
{

	class jsContext;

#define KNOBOBJ void
#define SLOTOBJ void
#define INJOBJ void

	struct JGSW_API namespace_import 
	{
		const char* m_namespace;
		std::vector<const etype_info*> m_filterTypes;
	};

	/// <summary>
	/// Provider properties provided
	/// </summary>
	struct JGSW_API knob_provider_properties 
	{
		const char* m_namespace = "GLOBAL";
		std::vector<namespace_import> m_namespaceImports;
	};

	/// <summary>
	/// Base definition struct common between knob_definitions and slot_definitions
	/// </summary>
	struct JGSW_API inj_definition 
	{
		typedef void(*ReferenceHandler)(INJOBJ*);
		typedef bool(*ReferenceChecker)(INJOBJ*);
		typedef jsContext*(*DependentContextGetter)(KNOBOBJ*);

		/// Core data follows. Name, qualifier, the reference itself, and the type information.
		sysHashString name;
		sysHashString qualifier;
		void* referenceContainer = nullptr;
		const Jigsaw::etype_info* typeInfo = nullptr;

		/// Alternative date
		bool isCollection = false;

		/// Base routines, common to knob/slot definitions
		ReferenceChecker isConstructed = nullptr;
		ReferenceHandler destructor = nullptr;
	};

	/// <summary>
	/// Slots need the ability to be filled. Hence the 'fillSlotWith' property.
	/// </summary>
	struct JGSW_API slot_definition : public inj_definition 
	{
		typedef void(*ReferenceTransmitter)(KNOBOBJ* from, SLOTOBJ* to);

		bool lazy = false;
		ReferenceTransmitter fillSlotWith;
	};

	/// <summary>
	/// Knobs need to have a handle to the initial creation of the knob, the destruction of the knob,
	/// any dependencies needed to initially construct the knob, and a function for providing slot definitions that need to be filled 
	/// post-construction of the knob.
	/// </summary>
	struct JGSW_API knob_definition : public inj_definition 
	{
		typedef slot_definition&(*InstanceSlotGetter)(SLOTOBJ*, const sysHashString&);
		typedef void(*CustomConstructor)(knob_definition&);
		typedef void(*CastFunction)(KNOBOBJ*, SLOTOBJ*);

		std::vector<slot_definition> stateless_slot_definitions;
		std::vector<slot_definition> constructor_dependencies;
		std::map<Jigsaw::etype_index, CastFunction> superClassCasters;

		ReferenceHandler construct = nullptr;
		CustomConstructor customConstruct = nullptr;

		ReferenceHandler postConstruct = nullptr;
		InstanceSlotGetter instanceSlotGetter = nullptr;
		DependentContextGetter getDependentContext = nullptr;
	};

	/// <summary>
	/// Defines a slot receiver's stateless slot definitions.
	/// </summary>
	struct JGSW_API slot_receiver_definition 
	{
		std::vector<slot_definition> slotDefinitions;
		const Jigsaw::etype_info* receiverType = nullptr;
	};

	class jsSlotsBase;

	JGSW_API std::vector<slot_definition>& GetSlots(Jigsaw::jsSlotsBase& slots);
	JGSW_API std::vector<slot_definition>& GetSlots(Jigsaw::jsSlotsBase* slots);

	/// <summary>
	/// TypeMatch utility to check if a knob/slot have the same type.
	/// </summary>
	JGSW_API bool TypeMatch(const Jigsaw::knob_definition& knob, const Jigsaw::slot_definition& slot);

	/// <summary>
	/// This method assumes that we've already determined that the knob fills the slot
	/// </summary>
	/// <param name="slot"></param>
	/// <param name="knob"></param>
	JGSW_API void FillSlotWithKnob(const Jigsaw::slot_definition& slot, const Jigsaw::knob_definition& knob);

}
#endif