/*********************************************************************
 * jsContext.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _JIGSAW_CONTEXT_H_
#define _JIGSAW_CONTEXT_H_

// Jigsaw_Engine
#include "Injection/jsKnobGraphDefinitionBuilder.h"
#include "Injection/jsContextConfig.h"
#include "Injection/jsKnobProvider.h"
#include "Injection/jsSlots.h"
#include "Injection/jsKnobGraph.h"
#include "Injection/injection_definitions.h"
#include "Ref.h"

namespace Jigsaw
{

	typedef jsKnobProvider* (*KNOB_PROVIDER_ALLOCATOR)();

	/// <summary>
	/// Final definition of a knob provider must provide a handle to construct the provider as well as 
	/// any associated properties.
	/// </summary>
	struct JGSW_API knob_provider_definition
	{
		KNOB_PROVIDER_ALLOCATOR build_provider = nullptr;
		std::string name;
		const Jigsaw::etype_info* provider_type = nullptr;
		struct knob_provider_properties properties;
	};

	/// <summary>
	/// JigsawContext finds all of the knob_definitions specified using the parameters provided in the jsContextConfig.
	/// 
	/// Dependencies are constructed in order according to their needs at the time of construction. Slots can be filled 
	/// by passing the corresponding slot_definition to the 'FillSlot' method.
	/// </summary>
	class JGSW_API jsContext
	{
	public:
		/// <summary>
		/// The externally-visible function that instantiates a jsContext.
		/// </summary>
		/// <param name="config"></param>
		/// <returns></returns>
		static Jigsaw::Ref<jsContext> Create(const jsContextConfig& config);

		/// <summary>
		/// Returns the first knob of the given type 'T'. Should only be used if there is one knob of the given type
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns></returns>
		template <typename T>
		inline T* GetKnob(const char* name = "") 
		{
			T* ref;
			T** ref_ptr = &ref;

			jsKnobGraphDefinitionBuilder<T> builder(name);
			builder.SetSetterDestination(ref_ptr);
			Jigsaw::slot_definition slot = builder.CompileSlot();

			knob_graph.FillExternalSlot(slot);
			return ref;
		}

		/// <summary>
		/// Searches the jsContext for a knob that matches the requirements of the slot_definition, and proceeds to fill the slot.
		/// 
		/// If the slot is not found, false is returned. If multiple matching slots are found, behavior is undefined. In Debug mode, an exception is thrown. 
		/// </summary>
		/// <param name="definition"></param>
		/// <returns></returns>
		bool FillSlot(const Jigsaw::slot_definition& definition) const;

		/// <summary>
		/// Iterates through the list of slot_definitions, filling each of them along the way with the knobs registered in the
		/// context.
		/// </summary>
		/// <param name="slot_definitions"></param>
		void FillSlots(const std::vector<Jigsaw::slot_definition>& slot_definitions, const std::string& receiver_name) const;

		/// <summary>
		/// Fills the slots of the passed in jsSlotsBase class
		/// </summary>
		/// <param name="slots_class"></param>
		void FillSlots(Jigsaw::jsSlotsBase& slots_class) const;

	protected:
		/// <summary>
		/// This internal function is called during the Create flow, and it's responsible for importing all of the namespaces,
		/// instantiating all of the knobs, and placing them in their appropriate slots. 
		/// </summary>
		void Init();

		/// <summary>
		/// Returns the KnobProviders associated to any of the underlying data structures. This method is virtual, as different Contexts may have different means
		/// of acquiring their providers.
		/// <returns></returns>
		/// </summary>
		virtual std::vector<Jigsaw::Ref<Jigsaw::jsKnobProvider>> GetKnobProviders();

		/// <summary>
		/// An optional override that specifies post-resolution behavior for the Dependent context. This could involve filling some additional slots, initializing some dependent behavior, etc.
		/// </summary>
		virtual void PostGraphResolution();

		/// <summary>
		/// An optional override that returns a list of dependent contexts to be used when initializing the jsKnobGraph.
		/// <returns></returns>
		/// </summary>
		virtual std::vector<Jigsaw::jsContext*> GetContextDependencies();

		/// <summary>
		/// The internal constructor just creates a blank context
		/// </summary>
		jsContext();

		std::vector<Jigsaw::knob_definition> knobs;
		jsKnobGraph knob_graph;

	private:
		jsContextConfig config;

	};

}
#endif