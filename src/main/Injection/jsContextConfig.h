#ifndef _JIGSAW_CONTEXT_CONFIG_H_
#define _JIGSAW_CONTEXT_CONFIG_H_

#include <vector>
#include <string>
#include "_jgsw_api.h"
#include "injection_definitions.h"

namespace Jigsaw {

	struct JGSW_API jsContextConfig {
		std::vector<namespace_import> m_packageNames;

	};
}

#endif