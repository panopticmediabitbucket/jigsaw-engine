//*********************************************************
//
// These extension methods, classes, and utilities were developed
// by Luke Randazzo to streamline some of directX's processes
//
//*********************************************************

#ifndef _X_DX_D12_H_
#define _X_DX_D12_H_

#include "Debug/j_debug.h"
#include "d3d12.h"
#include <wrl.h>
#include "Graphics/GPU_Objects/JGSW_GPU_RESOURCE_DATA.h"

class XD3D12_CPU_DESCRIPTOR_HANDLE : public D3D12_CPU_DESCRIPTOR_HANDLE {
public:
	XD3D12_CPU_DESCRIPTOR_HANDLE(D3D12_CPU_DESCRIPTOR_HANDLE desc_handle, Microsoft::WRL::ComPtr<ID3D12Device2> dx_device, D3D12_DESCRIPTOR_HEAP_TYPE desc_type)
		: D3D12_CPU_DESCRIPTOR_HANDLE(desc_handle),
		increment_size(dx_device->GetDescriptorHandleIncrementSize(desc_type)),
		orig_ptr(desc_handle.ptr),
		desc_type(desc_type) { }

	inline XD3D12_CPU_DESCRIPTOR_HANDLE& operator ++(int a) {
		ptr = SIZE_T(INT64(ptr) + increment_size);
		return *this;
	}

	inline XD3D12_CPU_DESCRIPTOR_HANDLE& operator --(int a) {
		ptr = SIZE_T(INT64(ptr) - increment_size);
		return *this;
	}

	inline XD3D12_CPU_DESCRIPTOR_HANDLE& rewind() {
		ptr = orig_ptr;
		return *this;
	}

	inline XD3D12_CPU_DESCRIPTOR_HANDLE& get(const UINT i) {
		ptr = SIZE_T(INT64(orig_ptr) + (increment_size * i));
		return *this;
	}

	inline D3D12_DESCRIPTOR_HEAP_TYPE GetHeapType() const {
		return desc_type;
	}

private:

	const INT64 increment_size;
	const INT64 orig_ptr;
	const D3D12_DESCRIPTOR_HEAP_TYPE desc_type;

};

class XD3D12_GPU_DESCRIPTOR_HANDLE : public D3D12_GPU_DESCRIPTOR_HANDLE {
public:
	XD3D12_GPU_DESCRIPTOR_HANDLE(D3D12_GPU_DESCRIPTOR_HANDLE desc_handle, Microsoft::WRL::ComPtr<ID3D12Device2> dx_device, D3D12_DESCRIPTOR_HEAP_TYPE desc_type)
		: D3D12_GPU_DESCRIPTOR_HANDLE(desc_handle), increment_size(dx_device->GetDescriptorHandleIncrementSize(desc_type)), orig_ptr(desc_handle.ptr) {
	}

	inline XD3D12_GPU_DESCRIPTOR_HANDLE& operator ++(int a) {
		ptr = SIZE_T(INT64(ptr) + increment_size);
		return *this;
	}

	inline XD3D12_GPU_DESCRIPTOR_HANDLE& operator --(int a) {
		ptr = SIZE_T(INT64(ptr) - increment_size);
		return *this;
	}

	inline XD3D12_GPU_DESCRIPTOR_HANDLE& rewind() {
		ptr = orig_ptr;
		return *this;
	}

	inline XD3D12_GPU_DESCRIPTOR_HANDLE& get(const size_t i) {
		ptr = SIZE_T(INT64(orig_ptr) + (increment_size * i));
		return *this;
	}

private:

	const INT64 increment_size;
	const INT64 orig_ptr;
};

class XD3D12_COMMAND_QUEUE_DESC : public D3D12_COMMAND_QUEUE_DESC {
private:
	XD3D12_COMMAND_QUEUE_DESC(D3D12_COMMAND_LIST_TYPE type, UINT node_mask) {
		Type = type;
		Priority = D3D12_COMMAND_QUEUE_PRIORITY_NORMAL;
		NodeMask = node_mask;
		Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	}
public:
	static XD3D12_COMMAND_QUEUE_DESC LoadQueue(UINT node_mask) {
		return XD3D12_COMMAND_QUEUE_DESC(D3D12_COMMAND_LIST_TYPE_COPY, node_mask);
	};

	static XD3D12_COMMAND_QUEUE_DESC FrameQueue(UINT node_mask) {
		return XD3D12_COMMAND_QUEUE_DESC(D3D12_COMMAND_LIST_TYPE_DIRECT, node_mask);
	};

	static XD3D12_COMMAND_QUEUE_DESC EditorQueue(UINT node_mask) {
		return XD3D12_COMMAND_QUEUE_DESC(D3D12_COMMAND_LIST_TYPE_DIRECT, node_mask);
	};
};

struct XD3D12_VIEW_DESC {
	union {
		D3D12_SHADER_RESOURCE_VIEW_DESC* srv_desc;
		D3D12_RENDER_TARGET_VIEW_DESC* rtv_desc;
		D3D12_DEPTH_STENCIL_VIEW_DESC* dsv_desc;
	};
};

/// <summary>
/// Struct wrapper is just there for static registration in the .cpp file. Converts between any number of JGSW formats and their D3D12 counterparts
/// </summary>
class D3D12_CONVERSIONS {
public:
	static void __PopulateD3D12Conversions();

	D3D12_PRIMITIVE_TOPOLOGY primitive_topology[(int)Jigsaw::JGSW_TOPOLOGY_TYPE::__NUM_JGSW_TOPOLOGY_TYPE];
	DXGI_FORMAT formats[(int)Jigsaw::JGSW_FORMAT::__NUM_JGSW_FORMATS];
	D3D12_SHADER_VISIBILITY shader_visibilities[(int)Jigsaw::JGSW_SHADER::__NUM_VISIBILITIES];

	static D3D12_PRIMITIVE_TOPOLOGY ConvertPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE type);
	static DXGI_FORMAT ConvertFormat(Jigsaw::JGSW_FORMAT format);
	static D3D12_SHADER_VISIBILITY ConvertShaderVisibility(Jigsaw::JGSW_SHADER visibility);

private:
	static D3D12_CONVERSIONS& Instance();

};

inline D3D12_PRIMITIVE_TOPOLOGY D3D12_CONVERSIONS::ConvertPrimitiveTopology(Jigsaw::JGSW_TOPOLOGY_TYPE type) {
	D3D12_PRIMITIVE_TOPOLOGY topology = Instance().primitive_topology[(int)type];
	J_D_ASSERT_LOG_ERROR(topology != D3D12_PRIMITIVE_TOPOLOGY_TYPE_UNDEFINED, D3D12_CONVERSIONS, "The specified JGSW_TOPOLOGY_TYPE has no equivalent D3D_PRIMITIVE_TOPOLOGY");
	return topology;
}

inline DXGI_FORMAT D3D12_CONVERSIONS::ConvertFormat(Jigsaw::JGSW_FORMAT format) {
	return Instance().formats[(int)format];
}

inline D3D12_SHADER_VISIBILITY D3D12_CONVERSIONS::ConvertShaderVisibility(Jigsaw::JGSW_SHADER visibility) {
	return Instance().shader_visibilities[(int)visibility];
}

inline D3D12_CONVERSIONS& D3D12_CONVERSIONS::Instance() {
	static D3D12_CONVERSIONS conversions;
	return conversions;
}

JGSW_API D3D12_RESOURCE_BARRIER DX_ResourceTransition(ID3D12Resource* resource, D3D12_RESOURCE_STATES before, D3D12_RESOURCE_STATES after);

#endif
