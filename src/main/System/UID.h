/*********************************************************************
 * UID.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _UID_H_
#define _UID_H_

// std
#include <string>

namespace Jigsaw
{
	/// <summary>
	/// UID is a locally-unique identifier.
	/// </summary>
	class JGSW_API UID
	{
	public:
		/// <summary>
		/// A direct constructor. Generally it's better to call 'Create()' than to manually set a UID.
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		UID(long long a, long long b) : data_a(a), data_b(b) { }

		/// <summary>
		/// Default constructor
		/// </summary>
		UID() : data_a(0), data_b(0) { }

		/// <summary>
		/// Copy constructor.
		/// </summary>
		/// <param name="other"></param>
		UID(const UID& other) : data_a(other.data_a), data_b(other.data_b) { }

		/// <summary>
		/// Move constructor
		/// </summary>
		/// <param name="other"></param>
		UID(UID&& other) noexcept : data_a(other.data_a), data_b(other.data_b) { }

		/// <summary>
		/// Stack assignment operator.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		UID& operator=(const UID& other);

		/// <summary>
		/// Creates a UID from anywhere in the program that can be used freely.
		/// </summary>
		/// <returns></returns>
		static UID Create();

		/// <summary>
		/// Returns a UID with the values reserved for an uninitialized object
		/// </summary>
		/// <returns></returns>
		inline static UID Empty() { return UID(0, 0); }

		/// <summary>
		/// Returns the value of the first data element
		/// </summary>
		/// <returns></returns>
		inline s64 GetData_a() const { return data_a; }

		/// <summary>
		/// Returns the value of the second data element
		/// </summary>
		/// <returns></returns>
		inline s64 GetData_b() const { return data_b; }

		/// <summary>
		/// General equality override
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		inline bool operator==(const UID& other) const { return data_a == other.data_a && data_b == other.data_b; }

		/// <summary>
		/// General less than override
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		inline bool operator<(const UID& other) const { return data_a < other.data_a || (data_a == other.data_a && data_b < other.data_b); }

		/// <summary>
		/// Basic check to ensure the UID is populated
		/// </summary>
		inline operator bool() const { return data_a || data_b; }

		s64 data_a;
		s64 data_b;
	};

#define UID_STR_SIZE 37
#define UID_STR(name) char name[UID_STR_SIZE]
#define NEW_UID_STR(name) char* name = new char[UID_STR_SIZE]

	JGSW_API UID UIDFromString(const UID_STR(name));
	JGSW_API void UIDFromString(UID* node, std::string* string);
	JGSW_API UID UIDFromString(std::string&& str);
	JGSW_API std::string* UIDToString(const UID* uid);
	JGSW_API std::string UIDToString(const UID& uid);

	JGSW_API void UIDToString(const UID& uid, UID_STR(dest));
}

namespace std 
{
	template <> struct hash<Jigsaw::UID>
	{
		size_t operator()(const Jigsaw::UID& uid) const 
		{
			return (size_t)uid.GetData_a();
		}
	};
}
#endif