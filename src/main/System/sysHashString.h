/*********************************************************************
 * sysHashString.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _SYS_HASH_STRING_H_
#define _SYS_HASH_STRING_H_

 // External
#include <string>
#include <map>
#include "spdlog/fmt/fmt.h"

namespace Jigsaw
{
	typedef u32 hash_val;

	/// <summary>
	/// Provides a utility for hashing strings. In non-release builds, the string will still be available for logging/documenting purposes.
	/// The goal of the hash is not to be secure. It's meant to be a fast hash that's highly sensitive to small changes. It should also be unique enough 
	/// to reliably produce a different value for each in a reasonably sized set of strings (say a couple million).
	/// </summary>
	struct sysHashString
	{

		constexpr sysHashString() : m_value(0) JGSW_DEV_ONLY(, m_str(nullptr), m_length(0)) {}
		constexpr sysHashString(hash_val val) : m_value(val) JGSW_DEV_ONLY(, m_str(nullptr), m_length(0)) {}
		constexpr sysHashString(const char* str) : JGSW_DEV_ONLY(m_length(0),) m_value(Hash(str, &m_length)) JGSW_DEV_ONLY(, m_str(str)) {}

		constexpr sysHashString& operator=(const sysHashString& other)
		{
			m_value = other.m_value;
			JGSW_DEV_ONLY(m_str = other.m_str;)
				return *this;
		}

		inline sysHashString& operator=(const char* str)
		{
			return *this = Create(str);
		}

		/// <summary>
		/// Creates the hash with an explicit 'length' specification. This is useful in cases where a smaller piece of a longer
		/// string needs to be converted to a hash. 
		/// </summary>
		/// <param name="str"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		static constexpr sysHashString Create(const char* str, size_t length)
		{
			return { Hash(str, length) JGSW_DEV_ONLY(, str, length) };
		}

		static constexpr sysHashString Create(const char* str)
		{
			size_t length = 0;
			u32 hash = str ? Hash(str, &length) : 0;
			return { hash JGSW_DEV_ONLY(, str, length) };
		}

		constexpr bool operator ==(const sysHashString& other) const noexcept
		{
			return other.m_value == m_value 
				// If one of these hashes does not have a string value, we bypass this part of the comparison.
				// Otherwise, we make sure that the number of characters specified matches. This is a smoke test.
				JGSW_DEV_ONLY(&& (!m_str || !other.m_str || (!strncmp(other.m_str, m_str, m_length) && other.m_length == m_length)));
		}

		constexpr bool operator !=(const sysHashString& other) const noexcept
		{
			return !(*this == other);
		}

		constexpr bool operator <(const sysHashString& other) const noexcept
		{
			return other.m_value < m_value;
		}

		constexpr hash_val Value() const
		{
			return m_value;
		}

#if JGSW_DEV
		const char* c_str() const
		{
			return m_str;
		}
#endif // JGSW_DEV

	private:
#if JGSW_DEV
		constexpr sysHashString(hash_val val, const char* str, size_t length = 0) : m_value(val), m_str(str), m_length(length) {}
#endif

		hash_val m_value;
#if JGSW_DEV
		const char* m_str;
		size_t m_length;
#endif // JGSW_DEV

		/// <summary>
		/// Hashes the string for a fixed length, disregarding null termination.
		/// </summary>
		/// <param name="str"></param>
		/// <param name="length"></param>
		/// <returns></returns>
		static constexpr u32 Hash(const char* str, size_t length)
		{
			const u32 basis = 2166136261;
			const u32 prime = 16777619;

			unsigned int hash = basis;
			for (unsigned int i = 0; i < length; i++)
			{
				hash = hash ^ str[i];
				hash = hash * prime;
			}
			return hash;
		}

		/// <summary>
		/// Hashes the string until a null terminator is reached. The length is output to the provided arg.
		/// </summary>
		/// <param name="str"></param>
		/// <returns></returns>
		static constexpr u32 Hash(const char* str, size_t* _length)
		{
			size_t mock_length = 0;
			size_t& length = _length ? *_length : mock_length;
			length = 0;
			const u32 basis = 2166136261;
			const u32 prime = 16777619;

			unsigned int hash = basis;
			while ((*str) != '\0')
			{
				hash = hash ^ *str++;
				hash = hash * prime;
				length++;
			}
			return hash == basis ? 0 : hash;
		}
	};

	/// <summary>
	/// Utility for calculating the hash value of a type name at compile time. 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template<typename T>
	struct sysTypeHash
	{
	private:
		static const unsigned int FRONT_SIZE_CLASS_STRUCT = sizeof("Jigsaw::sysTypeHash<class");
		static const unsigned int FRONT_SIZE_ENUM = sizeof("Jigsaw::sysTypeHash<enum");
		static const unsigned int FRONT_SIZE_PRIMITIVE = sizeof("Jigsaw::sysTypeHash<") - 1u;

		static const unsigned int BACK_SIZE = sizeof(">::Get");

	public:
		/// <summary>
		/// Retrieves via some variable manipulation the name of the class at compile time. 
		/// Then proceeds to hash that name and return the associated value. 
		/// </summary>
		/// <returns></returns>
		static constexpr sysHashString Get()
		{
			if constexpr (std::is_const_v<T>)
			{
				return sysTypeHash<std::remove_const_t<T>>::Get();
			}
			unsigned int FRONT_SIZE = 0;
			if constexpr (std::is_class_v<T>)
			{
				FRONT_SIZE = FRONT_SIZE_CLASS_STRUCT;
			}
			else if constexpr (std::is_enum_v<T>)
			{
				FRONT_SIZE = FRONT_SIZE_ENUM;
			}
			else
			{
				FRONT_SIZE = FRONT_SIZE_PRIMITIVE;
			}

			const char* func_name = __FUNCTION__;
			const char* ptr = func_name + FRONT_SIZE;
			ptr += *ptr == ' ' ? 1 : 0; // This correction is in place to handle the 1-character size difference between 'class' and 'struct'
			const char* end = func_name + sizeof(__FUNCTION__) - BACK_SIZE;
			unsigned int length = (unsigned int)(end - ptr);

			return sysHashString::Create(ptr, length);
		}
	};

}

_STD_BEGIN
template <>
struct JGSW_API hash<Jigsaw::sysHashString>
{
	_NODISCARD size_t operator()(const Jigsaw::sysHashString& _Keyval) const noexcept
	{
		return (size_t)_Keyval.Value();
	}
};
_STD_END

/// <summary>
/// Template specialization for formatting hash strings
/// </summary>
template <> struct fmt::formatter<Jigsaw::sysHashString> 
{
	constexpr auto parse(fmt::format_parse_context& ctx) -> decltype(ctx.begin()) 
	{
		// Return an iterator past the end of the parsed range:
		// We don't parse anything aside from the index
		return ctx.begin() + 1;
	}

	// We format the sysHashString to display the string as well as the hash value
	template <typename FormatContext>
	auto format(const Jigsaw::sysHashString& hash, FormatContext& ctx) -> decltype(ctx.out()) 
	{
		// ctx.out() is an output iterator to write to.
		return fmt::format_to(
			ctx.out(),
			"({0}->0x{1:x})",
			hash.c_str(), hash.Value());
	}
};


#endif // _SYS_HASH_STRING_H_