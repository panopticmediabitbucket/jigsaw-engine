#include "sysSignature.h"

using namespace Jigsaw::Util;

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	inline size_t SumSizes(const std::vector<Jigsaw::etype_index> aligned_types)
	{
		size_t size = 0;
		for (Jigsaw::etype_index et_i : aligned_types)
		{
			size += et_i.Get().size;
		}

		return size;
	}

	//////////////////////////////////////////////////////////////////////////

	// sysSignature Implementations

	//////////////////////////////////////////////////////////////////////////

	sysSignature::sysSignature(const std::vector<Jigsaw::etype_index>& aligned_types) : m_alignedTypes(aligned_types), m_sigMap(aligned_types)
	{
		size_t hash = 0;
		for (const Jigsaw::etype_info& aligned_type : aligned_types)
		{
			hash += aligned_type.GetProperties().hashName.Value();
		}
		this->m_hash = { hash, nullptr };
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::sysSignature::sysSignature(sysSignature&& other) noexcept : m_alignedTypes(std::move(other.m_alignedTypes)), m_sigMap(std::move(other.m_sigMap)), m_hash(other.m_hash)
	{
	}

	//////////////////////////////////////////////////////////////////////////

	sysSignature::sysSignature(const sysSignature& other) noexcept : m_alignedTypes(other.m_alignedTypes), m_sigMap(other.m_alignedTypes), m_hash(other.m_hash) { }

	//////////////////////////////////////////////////////////////////////////

	const std::vector<Jigsaw::etype_index>& sysSignature::GetAlignedTypes() const
	{
		return m_alignedTypes;
	}

	//////////////////////////////////////////////////////////////////////////

	const bool sysSignature::ContainsType(Jigsaw::etype_index type) const
	{
		auto iterator = std::find(std::begin(m_alignedTypes), std::end(m_alignedTypes), type);
		return std::end(m_alignedTypes) != iterator;
	}

	//////////////////////////////////////////////////////////////////////////

	bool sysSignature::HasCompleteConversionMap(const sysSignature& convert_to, Unique<sysSignatureMap>* out_conversion_map) const
	{
		sysSignatureMap::CONVERSION conversion = sysSignatureMap::GetConversionMap(*this, convert_to, out_conversion_map);
		return conversion == sysSignatureMap::CONVERSION::FULL;
	}

	//////////////////////////////////////////////////////////////////////////

	bool Jigsaw::sysSignature::operator==(const sysSignature& other) const
	{
		const std::vector<etype_index>& o_aligned_types = other.GetAlignedTypes();

		bool match = m_alignedTypes.size() == o_aligned_types.size();

		auto local_iter = m_alignedTypes.begin();
		auto other_iter = o_aligned_types.begin();
		while (match && local_iter != m_alignedTypes.end())
		{
			match &= *local_iter++ == *other_iter++;
		}
		return match;
	}

	//////////////////////////////////////////////////////////////////////////

	bool Jigsaw::sysSignature::operator<(const sysSignature& other) const
	{
		const std::vector<etype_index>& o_aligned_types = other.GetAlignedTypes();

		bool size_match = m_alignedTypes.size() == o_aligned_types.size();

		if (size_match)
		{
			bool less_than = false;

			auto local_iter = m_alignedTypes.begin();
			auto other_iter = o_aligned_types.begin();
			while (!less_than && local_iter != m_alignedTypes.end())
			{
				less_than |= (*local_iter) < (*other_iter);
				local_iter++;
				other_iter++;
			}
			return less_than;
		}
		else
		{
			return m_alignedTypes.size() < o_aligned_types.size();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	SignatureBuilder& Jigsaw::SignatureBuilder::operator=(SignatureBuilder& other)
	{
		types = other.types;
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	// SignatureBuilder implementations

	//////////////////////////////////////////////////////////////////////////

	SignatureBuilder& SignatureBuilder::AddType(const Jigsaw::etype_info& type)
	{
		if (!HasType(type))
		{
			types.push_back(type);
		}
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	SignatureBuilder& SignatureBuilder::AddTypes(const sysSignature& signature)
	{
		const std::vector<etype_index>& aligned_types = signature.GetAlignedTypes();
		std::for_each(std::begin(aligned_types), std::end(aligned_types), [&](etype_index index) -> void
			{
				AddType(index.Get());
			});
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	SignatureBuilder& Jigsaw::SignatureBuilder::AddTypes(const std::vector<Jigsaw::etype_index>& indices)
	{
		for (const Jigsaw::etype_index& index : indices)
		{
			AddType(index.Get());
		}
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	SignatureBuilder& Jigsaw::SignatureBuilder::SortMode(SORT_MODE sort_mode)
	{
		this->sort_mode = sort_mode;
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	bool SignatureBuilder::HasType(Jigsaw::etype_index type) const
	{
		return std::find(std::begin(types), std::end(types), type) != std::end(types);
	}

	//////////////////////////////////////////////////////////////////////////

	Unique<sysSignature> SignatureBuilder::Build()
	{
		if (sort_mode == SORT_MODE::SORT_MODE_ON)
		{
			std::sort(types.begin(), types.end());
		}
		sysSignature* sig = new sysSignature(types);
		return Unique<sysSignature>(sig);
	}

	//////////////////////////////////////////////////////////////////////////

}
