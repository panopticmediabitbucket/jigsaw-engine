#include "sysSignatureMap.h"
#include "sysSignature.h"
#include "Debug/j_debug.h"

using namespace Jigsaw::Util;

namespace Jigsaw
{
	//////////////////////////////////////////////////////////////////////////

	// utility method for initializing the size map
	std::map<Jigsaw::etype_index, u32> BuildSizeMapAndOffsets(const std::vector<etype_index> etype_indices, u32* index_offset, size_t& size)
	{
		std::map<Jigsaw::etype_index, u32> etype_info_offset_map;
		u32 offset = 0;
		u32 index = 0;
		size_t max_align = 1;
		for (etype_index et_i : etype_indices)
		{
			size_t align = et_i.Get().align;
			max_align = align > max_align ? align : max_align;

			J_D_ASSERT_LOG_ERROR((align <= sizeof(max_align_t)), sysSignatureMap, "sysSignatureMap attempted to be built with an object whose maximum alignment exceeds 'max_align_t' value");

			u32 misalignment = offset % align;
			if (misalignment != 0)
			{
				// padding needed to realign for the target type
				offset += ((u32)et_i.Get().align - misalignment);
			}
			etype_info_offset_map.insert(std::make_pair(et_i, offset));
			index_offset[index++] = offset;
			offset += (u32)et_i.Get().size;
		}
		size_t pad = max_align - (offset % max_align);
		// include some padding to align to the highest alignment requirement for the given types
		size = offset + (pad == max_align ? 0 : pad);
		return etype_info_offset_map;
	}

	//////////////////////////////////////////////////////////////////////////

	// utility method for initializing the size map without the index offsets
	std::map<Jigsaw::etype_index, u32> BuildSizeMap(const std::vector<etype_index> etype_indices)
	{
		std::map<Jigsaw::etype_index, u32> etype_info_offset_map;
		u32 offset = 0;
		for (etype_index et_i : etype_indices)
		{
			etype_info_offset_map.insert(std::make_pair(et_i, offset));
			offset += (u32)et_i.Get().size;
		}
		return etype_info_offset_map;
	}

	//////////////////////////////////////////////////////////////////////////

	sysSignatureMap::sysSignatureMap(const std::vector<etype_index>& etype_indices) : m_indexedOffsets(new u32[etype_indices.size()]),
		m_typeOffsetMap(BuildSizeMapAndOffsets(etype_indices, this->m_indexedOffsets.get(), size)) { }

	//////////////////////////////////////////////////////////////////////////

	// this constructor is used to override the index offsets
	Jigsaw::sysSignatureMap::sysSignatureMap(u32* index_offset, std::map<Jigsaw::etype_index, u32>&& type_offset_map) : m_indexedOffsets(index_offset),
		m_typeOffsetMap(std::move(type_offset_map)) { }

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::sysSignatureMap::sysSignatureMap(sysSignatureMap&& other) noexcept
		: m_indexedOffsets(std::move(other.m_indexedOffsets)), m_typeOffsetMap(std::move(other.m_typeOffsetMap)), size(other.size) { }

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::sysSignatureMap::sysSignatureMap() : size(0)
	{
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::sysSignatureMap::CONVERSION Jigsaw::sysSignatureMap::GetConversionMap(const Jigsaw::sysSignature& from, const Jigsaw::sysSignature& to, Unique<sysSignatureMap>* out_conversion_map)
	{
		const std::vector<etype_index>& conv_aligned_types = to.GetAlignedTypes();
		u32* aligned_offsets = new u32[conv_aligned_types.size()];
		std::map<Jigsaw::etype_index, u32> type_offset_map;
		u32 i = 0;
		CONVERSION conversion = CONVERSION::_INIT;

		for (etype_index type : conv_aligned_types)
		{
			bool is_valid = from.ContainsType(type);
			u32 offset = sm_invalidIndex;
			if (is_valid)
			{
				switch (conversion)
				{
				case CONVERSION::_INIT:
					conversion = CONVERSION::FULL;
					break;
				case CONVERSION::NONE:
					conversion = CONVERSION::PARTIAL;
					break;
				default:
					break;
				}
				offset = from.GetMap()[type];
			}
			else
			{
				switch (conversion)
				{
				case CONVERSION::_INIT:
					conversion = CONVERSION::NONE;
					break;
				case CONVERSION::FULL:
					conversion = CONVERSION::PARTIAL;
					break;
				default:
					break;
				}
			}

			aligned_offsets[i++] = offset;
			type_offset_map.insert(std::make_pair(type, offset));
		}

		// only create the conversion_map if there is a valid conversion map that can be created
		if (conversion != CONVERSION::NONE)
		{
			if (out_conversion_map)
			{
				*out_conversion_map = Unique<sysSignatureMap>(new sysSignatureMap(aligned_offsets, std::move(type_offset_map)));
			}
		}
		else
		{
			delete[] aligned_offsets;
		}

		return conversion;
	}

	//////////////////////////////////////////////////////////////////////////
}
