/*********************************************************************
 * sysSignature.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _SIGNATURE_H_
#define _SIGNATURE_H_

 // Jigsaw_Engine
#include "Ref.h"
#include "sysSignatureMap.h"
#include "RTTI/etype_info.h"

// std
#include <algorithm>
#include <string>
#include <vector>

namespace Jigsaw
{
	struct signature_hash
	{
		u64 hash;
		const char* descriptor = nullptr;

		inline bool operator==(const signature_hash& other) const noexcept { return hash == other.hash; };
		inline bool operator<(const signature_hash& other) const noexcept { return hash < other.hash; };
	};

	/// <summary>
	/// sysSignature is the common implementation for the SystemSignature and the EntitySignature. Member types will
	/// be collected in the aligned_argument_types.
	/// </summary>
	class JGSW_API sysSignature
	{
		friend class SignatureBuilder;
	public:
		/// <summary>
		/// Move constructor.
		/// <param name="other"></param>
		/// <returns></returns>
		/// </summary>
		sysSignature(sysSignature&& other) noexcept;

		/// <summary>
		/// Copy constructor.
		/// <param name="other"></param>
		/// <returns></returns>
		/// </summary>
		sysSignature(const sysSignature& other) noexcept;

		/// <summary>
		/// Returns an immutable vector of the types in order
		/// </summary>
		const std::vector<Jigsaw::etype_index>& GetAlignedTypes() const;

		/// <summary>
		/// Checks if the signature contains the given type
		/// </summary>
		const bool ContainsType(Jigsaw::etype_index) const;

		/// <summary>
		/// Returns the total size of the m_alignedTypes for this signature
		/// </summary>
		inline size_t GetSize() const { return m_sigMap.GetSize(); }

		/// <summary>
		/// Returns an immutable map that associates type_infos with their byte offset into the data
		/// or associates indexes of the aligned types into their offset in the data
		/// </summary>
		inline const sysSignatureMap& GetMap() const { return m_sigMap; };

		/// <summary>
		/// Returns the hash value of the sysSignature.
		/// <returns></returns>
		/// </summary>
		inline signature_hash GetHash() const { return m_hash; }

		/// <summary>
		/// Checks if a complete conversion exists to the target signature. If so, it's populated in the provided destination.
		///
		/// </summary>
		/// <param name="convert_to">The signature that the out_conversion_map's indices will map to</param>
		/// <param name="out_conversion_map">A pointer to a sysSignatureMap unique_ptr that will receive the completed conversion sysSignatureMap</param>
		/// <returns>true <==> there is a complete conversion between the signatures, and the out_conversion_map will be populated.</returns>
		bool HasCompleteConversionMap(const sysSignature& convert_to, Jigsaw::Unique<sysSignatureMap>* out_conversion_map) const;

		/// <summary>
		/// Equality operator ensures that there is a matching number of types and the exact order matches.
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		bool operator==(const sysSignature& other) const;

		/// <summary>
		/// Less than operator for use with map objects
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		bool operator<(const sysSignature& other) const;

	protected:
		std::vector<Jigsaw::etype_index> m_alignedTypes;
		sysSignatureMap m_sigMap;
		signature_hash m_hash;

	private:
		/// <summary>
		/// The primary constructor for sysSignature takes a vector of etype_index objects. They are assumed to be sorted at this point. 
		/// It is private to enforce construction of a sysSignature with an approved interface. The SignatureBuilder is a good candidate.
		/// </summary>
		/// <param name="aligned_types"></param>
		sysSignature(const std::vector<Jigsaw::etype_index>& aligned_types);
	};

	/// <summary>
	/// sysSignature is the common implementation for the SystemSignature and the EntitySignature. Member types will
	/// be collected in the aligned_argument_types.
	/// </summary>
	class JGSW_API SignatureBuilder
	{
	public:
		enum class SORT_MODE
		{
			SORT_MODE_ON, SORT_MODE_OFF
		};

		/// <summary>
		/// Constructor
		/// </summary>
		SignatureBuilder() {};

		/// <summary>
		/// Copy constructor is supported with non-const argument
		/// </summary>
		/// <param name="other"></param>
		SignatureBuilder(SignatureBuilder& other) : types(other.types) { }

		/// <summary>
		/// Move constructor
		/// </summary>
		/// <param name="other"></param>
		SignatureBuilder(SignatureBuilder&& other) noexcept : types(other.types) { }

		/// <summary>
		/// Assignment operator is supported for SignatureBuilders because the construction has not been commited. 
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		SignatureBuilder& operator=(SignatureBuilder& other);

		/// <summary>
		/// If it is not already there, adds the type to be registered for the sysSignature
		/// </summary>
		/// <param name="type"></param>
		/// <returns></returns>
		SignatureBuilder& AddType(const Jigsaw::etype_info& type);

		/// <summary>
		/// Pull in all the types from the signature parameter that are not already present in the SignatureBuilder's types registry
		/// </summary>
		/// <param name="signature"></param>
		/// <returns></returns>
		SignatureBuilder& AddTypes(const sysSignature& signature);

		/// <summary>
		/// Adds the array of types to the current SignatureBuilder
		/// </summary>
		/// <param name="indices"></param>
		/// <returns></returns>
		SignatureBuilder& AddTypes(const std::vector<Jigsaw::etype_index>& indices);

		/// <summary>
		/// Sets the sort mode for the aligned types on build
		/// </summary>
		/// <param name="sort_mode"></param>
		/// <returns></returns>
		SignatureBuilder& SortMode(SORT_MODE sort_mode);

		bool HasType(Jigsaw::etype_index type) const;

		/// <summary>
		/// Sorts the types and then builds a sysSignature with them
		/// </summary>
		/// <returns></returns>
		Jigsaw::Unique<sysSignature> Build();

	private:

		SORT_MODE sort_mode = SORT_MODE::SORT_MODE_ON;
		std::vector<Jigsaw::etype_index> types;
	};
}

namespace std
{
	template <> struct JGSW_API hash<Jigsaw::sysSignature>
	{
		size_t operator()(const Jigsaw::sysSignature& sig) const
		{
			return (size_t)sig.GetHash().hash;
		}
	};
}

namespace std
{
	template <> struct JGSW_API hash<Jigsaw::signature_hash>
	{
		size_t operator()(const Jigsaw::signature_hash& sig_hash) const
		{
			return (size_t)sig_hash.hash;
		}
	};
}
#endif
