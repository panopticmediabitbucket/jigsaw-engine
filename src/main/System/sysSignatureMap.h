/*********************************************************************
 * sysSignatureMap.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _SIGNATURE_MAP_H_
#define _SIGNATURE_MAP_H_

 // Jigsaw_Engine
#include "RTTI/etype_info.h"

// std
#include <map>
#include <vector>

namespace Jigsaw
{

	class sysSignature;

	/// <summary>
	/// All completed Signatures have a SignatureMapping that is used to index either by integer
	/// for aligned data types or by type_info to return the byte offset to the corresponding data type.
	/// </summary>
	class JGSW_API sysSignatureMap
	{
	public:
		/// <summary>
		/// Primary constructor takes an immutable vector of type_indices
		/// </summary>
		sysSignatureMap(const std::vector<Jigsaw::etype_index>& aligned_types);

		/// <summary>
		/// Move constructor
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		sysSignatureMap(sysSignatureMap&& other) noexcept;

		/// <summary>
		/// Empty constructor. Without initializing or assigning to, the sysSignatureMap will be useless
		/// </summary>
		sysSignatureMap();

		/// <param name="type_info"></param>
		/// <returns>The offset in Bytes to the type_info</returns>
		inline u32 GetTypeOffset(const Jigsaw::etype_index& type_info) const { return m_typeOffsetMap.at(type_info); };

		inline u32 operator[](const Jigsaw::etype_index& type_info) const { return m_typeOffsetMap.at(type_info); };

		/// <returns>The offset in Bytes to the aligned_type index i</returns>
		inline u32 GetOffsetOfTypeIndex(u32 i) const { return m_indexedOffsets[i]; }

		/// <returns>The offset in Bytes to the aligned_type index i</returns>
		inline int operator[](const u32 i) const { return (m_indexedOffsets.get())[i]; }

		/// <returns>The total, aligned size of the given object</returns>
		inline size_t GetSize() const { return size; }

		enum class CONVERSION 
		{
			NONE = 0,
			PARTIAL = 1,
			FULL = 2,
			_INIT = 3,		// used internally to specify an initial, ambiguous state
		};

		/// <summary>
		/// Builds a new sysSignatureMap that has indices that are aligned to the types in the convert_to signature.
		///
		/// Example: 
		/// sysSignatureMap sig_map built with 4 types : Quaternion, Vector2, Vector3, DataStruct
		/// sysSignature 'convert_to' built with 2 m_alignedTypes: DataStruct, Vector3
		/// 
		/// sysSignatureMap conversion_map;
		/// if(sig_map.HasConversionMap(convert_to, conversion_map)){
		///		// Do stuff
		/// }
		/// 
		/// conversion_map[0] will return an offset that maps to 'DataStruct' in the first structure. 
		/// conversion_map[1] will return the offset to Vector3 in the original structure. 
		///
		/// 
		/// </summary>
		/// <param name="from"></param>
		/// <param name="to"></param>
		/// <param name="out_conversion_map"></param>
		/// <returns>True if a perfect conversion exists</returns>
		static CONVERSION GetConversionMap(const sysSignature& from, const sysSignature& to, Unique<sysSignatureMap>* out_conversion_map);

		static const u32 sm_invalidIndex = 0xFFFFFFFF;

	private:
		/// <summary>
		/// A constructor that provides an override for directly passing the index and type offsets. Used internally.
		/// </summary>
		/// <param name="index_offset"></param>
		/// <param name="aligned_types"></param>
		sysSignatureMap(u32* index_offset, std::map<Jigsaw::etype_index, u32>&& type_offset_map);

		size_t size;
		Jigsaw::Unique<u32[]> m_indexedOffsets;
		std::map<Jigsaw::etype_index, u32> m_typeOffsetMap;

	};
}

#endif // !_SIGNATURE_MAP_H_
