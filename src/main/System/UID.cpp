#include "UID.h"

// Jigsaw_Engine
#include "Debug/j_debug.h"
#include "Marshalling/JigsawMarshalling.h"
#include "Util/BYTE.h"

// std
#include <algorithm>
#include "combaseapi.h"

using namespace Jigsaw::Util;

namespace Jigsaw
{
	START_REGISTER_SERIALIZABLE_CLASS(UID);
	REGISTER_CUSTOM_FIELD_MAPPER(UID, std::string, value, UIDToString, UIDFromString);
	END_REGISTER_SERIALIZABLE_CLASS(UID);

	//////////////////////////////////////////////////////////////////////////

	void UIDFromString(UID* node, std::string* string)
	{
		size_t index = 0;
		while ((index = string->find_first_of('-')) != std::string::npos)
		{
			string = &string->erase(index, 1);
		}
		Unique<u8[]> Bytes = Util::HexStrToBytes(string->c_str(), (int)string->size());
		long long* long_Bytes = (long long*)(Bytes.get());

		node->data_a = long_Bytes[0];
		node->data_b = long_Bytes[1];
	}

	//////////////////////////////////////////////////////////////////////////

	JGSW_API UID UIDFromString(const UID_STR(name))
	{
		UID id;
		J_D_ASSERT_LOG_ERROR((strlen(name) == (UID_STR_SIZE - 1)), UID, "Did the UID size change? The hex implementation needs to then as well");
		u8* output = reinterpret_cast<u8*>(&id.data_a);
		HexStrToBytes(name, name + 8, output);
		name += 9; // skipping the hyphen
		HexStrToBytes(name, name + 4, output + 4);
		name += 5; // skipping the hyphen
		HexStrToBytes(name, name + 4, output + 6);
		name += 5; // skipping the hyphen
		HexStrToBytes(name, name + 4, output + 8);
		name += 5; // skipping the hyphen
		HexStrToBytes(name, name + 12, output + 10);
		return id;
	}

	//////////////////////////////////////////////////////////////////////////

	UID UIDFromString(std::string&& str)
	{
		UID ret_val;
		UIDFromString(&ret_val, &str);
		return ret_val;
	}

	//////////////////////////////////////////////////////////////////////////

	std::string* UIDToString(const UID* uid)
	{
		// converting the uid to hex
		long long data_[2];
		data_[0] = uid->GetData_a();
		data_[1] = uid->GetData_b();
		Unique<char[]> hex_str = Util::BytesToHexStr((u8*)&data_, 16);
		std::string i_str = std::string(hex_str.get());
		return new std::string(i_str.substr(0, 8) + '-' + i_str.substr(8, 4) + '-' + i_str.substr(12, 4) + '-' + i_str.substr(16, 4) + '-' + i_str.substr(20));
	}

	//////////////////////////////////////////////////////////////////////////

	std::string UIDToString(const UID& uid)
	{
		std::string* str = UIDToString(&uid);
		std::string ret_val = *str;
		delete str;
		return ret_val;
	}

	//////////////////////////////////////////////////////////////////////////

	void UIDToString(const UID& uid, UID_STR(dest))
	{
		using namespace Jigsaw::Util;
		long long data_[2];
		data_[0] = uid.GetData_a();
		data_[1] = uid.GetData_b();
		u8* data = (u8*)&data_;

		sprintf_s(dest, UID_STR_SIZE, "%02X%02X%02X%02X-%02X%02X-%02X%02X-%02X%02X-%02X%02X%02X%02X%02X%02X",
			data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8], data[9], data[10], data[11], data[12], data[13], data[14], data[15]);

		dest[UID_STR_SIZE - 1] = '\0';
	}

	//////////////////////////////////////////////////////////////////////////

	UID& UID::operator=(const UID& other)
	{
		data_a = other.GetData_a();
		data_b = other.GetData_b();
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	UID UID::Create()
	{
		GUID id;
		HRESULT res = CoCreateGuid(&id);
		(void)res;
		long long data_a = static_cast<long long>(id.Data1) << 32;
		data_a |= static_cast<long long>(id.Data2) << 16;
		data_a |= static_cast<long long>(id.Data3);
		long long data_b = *(long long*)id.Data4;
		return UID(data_a, data_b);
	}

	//////////////////////////////////////////////////////////////////////////


}
