/*********************************************************************
 * sysDelegate.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: October 2021
 *********************************************************************/
#ifndef _EV_HANDLER_H_
#define _EV_HANDLER_H_

 // Jigsaw_Engine
#include "Util/DTO.h"

// std
#include <vector>

namespace Jigsaw
{
	/// <summary>
	/// An sysDelegate template specifies an event signature that listeners can subscribe to or unsubscribe from.
	/// The event owner dispatches 'Notify' calls when the event happens. In 
	/// </summary>
	/// <typeparam name="EventArgs"></typeparam>
	template <typename...EventArgs>
	class sysDelegate
	{
	public:
		using evMemberFunc = MemberFunctor<void, EventArgs...>;

		template <typename T, void(T::*Func)(EventArgs...)>
		void Subscribe(T* obj)
		{
			evMemberFunc tempFunc = evMemberFunc::template Create<T, Func>(obj);
			m_objListeners.push_back(tempFunc);
		}

		template <typename T, void(T::* Func)(EventArgs...)>
		void Unsubscribe(T* obj)
		{
			evMemberFunc tempFunc = evMemberFunc::template Create<T, Func>(obj);
			auto iter = std::remove_if(m_objListeners.begin(), m_objListeners.end(), [=](evMemberFunc& func) { return func == tempFunc; });
			if (iter != m_objListeners.end())
			{
				m_objListeners.erase(iter);
			}
		}

		void Notify(EventArgs... args)
		{
			// We make a copy here in case any of he invoked listeners unsubscribe from the event.
			std::vector<evMemberFunc> fns = m_objListeners;
			for (evMemberFunc& listener : m_objListeners)
			{
				listener(args...);
			}
		}

	private:
		std::vector<evMemberFunc> m_objListeners;

	};

}

#endif // _EV_HANDLER_H_