/*********************************************************************
 * FileUtil.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _JIGSAW_FILE_UTIL_H_
#define _JIGSAW_FILE_UTIL_H_

// Jigsaw_Engine
#include "System/UID.h"

// std
#include <sstream>
#include <fstream>

namespace Jigsaw
{

	/// <summary>
	/// A simple struct that encapsulates some basic information about a file and an active file descriptor. It is used with the 'load'
	/// functions to provide all information that's needed to properly load the given resource.
	/// </summary>
	struct FILE_DATA
	{
		size_t file_size;
		wchar_t* file_name;
		FILE* file;
	};

	class JGSW_API FileUtil
	{
	public:
#define JGSW_MAX_PATH 512

		static const char* ProjectRoot();

		/// <summary>
		/// Gets a pointer to the file name, if it's included, from the input path. The last token delimited by a slash (/ or \\)
		/// is determined to be the file name, with or without an extension.
		/// </summary>
		/// <param name="path">The input path that may have a filename at the end.</param>
		/// <param name="outPath">Optional buffer to output the path up to the file name</param>
		/// <returns></returns>
		static const char* GetNameFromPath(const char* path, char* outPath);

#if JGSW_DEV
		static std::ofstream OutputFileAtRelativePath(const char* rel_path, const char* file_name, const char* optional_ext, char** outPath);
#endif

		static void GetSceneMachineFabricationPath(const UID& sceneId, const UID& machineId, char* output);

		static FILE_DATA GetFileWrite(const char* file_path);

		static FILE_DATA GetFileRead(const char* file_path);

		static std::istringstream GetStringStreamFromFile(const FILE_DATA& file_data);

		[[deprecated]]static bool GetFileReadIfExists(const std::string& file_path, FILE_DATA* out_data);

		static bool GetFileReadIfExists(const char* file_path, FILE_DATA* out_data);

	};
}
#endif

