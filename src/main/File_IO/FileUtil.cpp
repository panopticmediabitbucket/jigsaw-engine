#include "FileUtil.h"

#include "Debug/j_debug.h"
#include "Application/ApplicationRootProperties.h"

// std
#include <algorithm>
#include <string>
#include <filesystem>

namespace Jigsaw
{
	//////////////////////////////////////////////////////////////////////////

	const char* _ProjectRoot()
	{
		static char projdir[256];
		_getcwd(projdir, 256);
		return projdir;
	}

	//////////////////////////////////////////////////////////////////////////

	const char* FileUtil::ProjectRoot()
	{
		static const char* projdir = _ProjectRoot();
		return projdir;
	}

	//////////////////////////////////////////////////////////////////////////

	const char* FileUtil::GetNameFromPath(const char* path, char* outPath)
	{
		if (path)
		{
			const char* last_slash = std::max(strrchr(path, '\\'), strrchr(path, '/'));
			if (last_slash)
			{
				size_t path_l = last_slash - path;
				// The outPath needs to include everything up to the last slash
				if (outPath)
				{
					strcpy_s(outPath, path_l, path);
				}
				if (path_l < strlen(path))
				{
					return path + path_l;
				}
			}
			else
			{
				// In this case, there is no path, and the input 'path' itself is the file name
				if (outPath)
				{
					outPath[0] = '\0';
				}
				return path;
			}
		}

		// base case. If the path is null or finishes with a slash
		return nullptr;
	}

	//////////////////////////////////////////////////////////////////////////

	std::ofstream FileUtil::OutputFileAtRelativePath(const char* rel_path, const char* filename, const char* optional_ext, char** outPath)
	{
		char filedir[JGSW_MAX_PATH];
		const char* projdir = ProjectRoot();
		sprintf_s(filedir, JGSW_MAX_PATH, "%s\\%s", projdir, rel_path);
		if (!std::filesystem::exists(filedir))
		{
			std::filesystem::create_directories(filedir);
		}

		char filepath[JGSW_MAX_PATH];
		if (optional_ext)
		{
			sprintf_s(filepath, JGSW_MAX_PATH, "%s\\%s.%s", filedir, filename, optional_ext);
		}
		else
		{
			sprintf_s(filepath, JGSW_MAX_PATH, "%s\\%s", filedir, filename);
		}

		std::ofstream stream(filepath, std::ios::out);
		if (outPath && stream)
		{
			size_t length = strlen(filepath);
			*outPath = new char[length];
			memcpy(*outPath, filepath, length);
		}

		return stream;
	}

	//////////////////////////////////////////////////////////////////////////

	void FileUtil::GetSceneMachineFabricationPath(const UID& sceneId, const UID& machineId, char* output)
	{
		// building the file path using the ApplicationRootProperties
		UID_STR(sceneIdStr);
		UID_STR(machineIdStr);
		UIDToString(sceneId, sceneIdStr);
		UIDToString(machineId, machineIdStr);

		sprintf(output, "%s\\%s\\%2.2s\\%s\\%s", ProjectRoot(), ApplicationRootProperties::Get().lib_path.c_str(), sceneIdStr, sceneIdStr, machineIdStr);
	}

	//////////////////////////////////////////////////////////////////////////

	FILE_DATA FileUtil::GetFileWrite(const char* file_path)
	{
		// load the file needed to initialize the resource
		FILE* resource_file;
		errno_t error = fopen_s(&resource_file, file_path, "wb");

		FILE_DATA file;
		file.file = resource_file;

		return file;
	}

	//////////////////////////////////////////////////////////////////////////

	FILE_DATA FileUtil::GetFileRead(const char* file_path)
	{
		// load the file needed to initialize the resource
		FILE* resource_file;
		errno_t error = fopen_s(&resource_file, file_path, "rb");

		J_LOG_INFO(FILE_DATA, "Attempting to open file at {0}", file_path);
		fseek(resource_file, 0, SEEK_END);
		size_t size = ftell(resource_file);
		fseek(resource_file, 0, SEEK_SET);

		// copying wstr of path for use in FILE_DATA
		std::filesystem::path path(file_path);
		size_t size_wstr = path.wstring().size();
		wchar_t* c_str = new wchar_t[path.wstring().size() + 1];
		path.wstring().copy(c_str, size_wstr, 0);
		c_str[size_wstr] = 0;

		return { size, c_str, resource_file };
	}

	//////////////////////////////////////////////////////////////////////////

	std::istringstream FileUtil::GetStringStreamFromFile(const FILE_DATA& file_data)
	{
		char* file_read = new char[file_data.file_size + 1];
		file_read[file_data.file_size] = '\0';
		fread_s(file_read, file_data.file_size, file_data.file_size, 1, file_data.file);
		std::string str(std::move(file_read));

		return std::move(std::istringstream(str));
	}

	//////////////////////////////////////////////////////////////////////////

	bool FileUtil::GetFileReadIfExists(const std::string& file_path, FILE_DATA* out_data)
	{
		bool exists = std::filesystem::exists(file_path);
		if (exists)
		{
			*out_data = GetFileRead(file_path.c_str());
		}
		return exists;
	}

	//////////////////////////////////////////////////////////////////////////

	bool FileUtil::GetFileReadIfExists(const char* file_path, FILE_DATA* out_data)
	{
		bool exists = std::filesystem::exists(file_path);
		if (exists)
		{
			*out_data = GetFileRead(file_path);
		}
		return exists;

	}

	//////////////////////////////////////////////////////////////////////////

}