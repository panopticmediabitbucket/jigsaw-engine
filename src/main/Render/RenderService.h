/*********************************************************************
 * RenderService.h
 * The RenderService is the main interface for sending Renderables to the queue for the upcoming frame.
 *
 * Author: jl_ra
 * Originally created: April 2021
 *********************************************************************/
#ifndef _RENDER_SERVICE_H_
#define _RENDER_SERVICE_H_

// Jigsaw_Engine
#include "Graphics/VERTEX_INDEXED_DATA.h"
#include "Graphics/Pipeline/JigsawPipeline.h"
#include "Graphics/GraphicsContext.h"
#include "Graphics/CommandList.h"
#include "Graphics/CommandListExecutor.h"
#include "Injection/JigsawInjection.h"
#include "RenderJobContainer.h"

namespace Jigsaw
{
	typedef std::function<void()> RENDER_SERVICE_CALLBACK;

	/// <summary>
	/// RenderService is a graphics-api independent interface used by the MainApplicationOrchestrator and any dependent systems.
	/// </summary>
	class JGSW_API RenderService : public jsSlots<RenderService>, public Jigsaw::RenderJobContainer
	{
	public:

		/// <summary>
		/// RenderService has no constructor dependencies
		/// </summary>
		RenderService();

		/// <summary>
		/// No move construction
		/// </summary>
		/// <param name=""></param>
		RenderService(RenderService&&) = delete;

		/// <summary>
		/// No copy construction
		/// </summary>
		/// <param name=""></param>
		RenderService(const RenderService&) = delete;

		/// <summary>
		/// Sets the RenderTarget to which the RenderService will display. Any transition commands are handled. Must be called before 'Execute'
		/// </summary>
		/// <param name="gpu_resource"></param>
		void DisplayOn(Jigsaw::GPUResource& rtv_resource);

		/// <summary>
		/// Executes the RenderJobs stored in the underlying RenderJobContainer.
		/// </summary>
		virtual void Execute();

		void SetStartFrameCallback(RENDER_SERVICE_CALLBACK&& callback);

		void SetEndFrameCallback(RENDER_SERVICE_CALLBACK&& callback);

	protected:
		/// <summary>
		/// Provides a RenderJob with a matching type of this service.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		Jigsaw::Ref<Jigsaw::RenderJob> BuildJob(const std::string& name) override;

		RENDER_SERVICE_CALLBACK start_frame_callback;
		RENDER_SERVICE_CALLBACK end_frame_callback;

		// mutable variables
		Jigsaw::GPUResource* render_target;

		// slots
		SLOT(slot_1, Jigsaw::GraphicsContext, s_graphicsContext);

	};
}
#endif