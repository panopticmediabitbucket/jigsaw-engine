#include "RenderJobContainer.h"
#include "Debug/j_debug.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	bool RenderJobContainer::HasJob(const std::string& name, Jigsaw::Ref<Jigsaw::RenderJob>* out_render_job)
	{
		auto iter = job_map.find(name);
		if (out_render_job && iter != job_map.end())
		{
			*out_render_job = iter->second->render_job;
			return true;
		}
		return iter != job_map.end();
	}

	//////////////////////////////////////////////////////////////////////////

	Ref<RenderJob> RenderJobContainer::GetJob(const std::string& name)
	{
		auto iter = job_map.find(name);
		J_D_ASSERT_LOG_ERROR(iter != job_map.end(), RenderJobContainer, "Attempted to retrieve job named '{0}' which is not contained in the target container.", name);
		return iter->second->render_job;
	}

	//////////////////////////////////////////////////////////////////////////

	Ref<RenderJob> RenderJobContainer::CreateJob(const std::string& name, const std::string* dependency)
	{
		Ref<RenderJob> new_job = this->BuildJob(name);

		J_D_ASSERT_LOG_ERROR(pool_index < 30, RenderJob, "The RenderJobContainer pool has reached capacity and overflown");
		render_node* node = &node_pool[pool_index++];
		node->render_job = new_job;
		render_trees.push_back(node);

		if (dependency != nullptr)
		{
			render_node* dependency_node = job_map.at(*dependency);
			node->dependency = dependency_node;
			auto iter = std::find_if(render_trees.begin(), render_trees.end(), [=](const render_node* root) { return root->render_job->name == *dependency; });
			if (iter != render_trees.end())
			{
				render_trees.erase(iter);
			}
		}

		job_map.insert(std::make_pair(name, node));
		return new_job;
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJobContainer::AddDependency(const std::string& name, const std::string& depends_on)
	{
		auto parent_iter = job_map.find(name);
		auto dependency_iter = job_map.find(depends_on);

		J_D_ASSERT_LOG_ERROR(parent_iter != job_map.end() && dependency_iter != job_map.end(), RenderJobContainer, "Dependency from {0} to {1} could not be linked because the parent or the dependency do not exist in the container", name, depends_on);
		render_node* parent = parent_iter->second;
		render_node* dependency = dependency_iter->second;

		J_D_ASSERT_LOG_ERROR(!parent->dependency, RenderJobContainer, "Adding the dependency to job '{0}' on '{1}' could not be completed because it's already dependent on {2}", name, depends_on, parent->dependency->render_job->name);

		parent->dependency = dependency;
		auto iter = std::find_if(render_trees.begin(), render_trees.end(), [=](const render_node* root)
			{ return root->render_job->name == dependency->render_job->name; });
		if (iter != render_trees.end())
		{
			render_trees.erase(iter);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJobContainer::AddJob(const Jigsaw::Ref<Jigsaw::RenderJob>& render_job)
	{
		J_D_ASSERT_LOG_ERROR(pool_index < 30, RenderJob, "The RenderJobContainer pool has reached capacity and overflown");
		render_node* node = &node_pool[pool_index++];
		node->render_job = render_job;
		render_trees.push_back(node);

		job_map.insert(std::make_pair(render_job->name, node));
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJobContainer::ClearContainer()
	{
		for (int i = 0; i < pool_index; i++)
		{
			render_node& node = node_pool[i];
			node.render_job = Ref<RenderJob>();
			node.dependency = nullptr;
			node.dependency_executing = false;
		}
		pool_index = 0;

		job_map.clear();
		render_trees.clear();
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<Jigsaw::Ref<Jigsaw::RenderJob>> RenderJobContainer::GetLeaves()
	{
		std::vector<Jigsaw::Ref<Jigsaw::RenderJob>> leaves;
		std::vector<render_node*>::iterator root_iterator = render_trees.begin();
		while (root_iterator != render_trees.end())
		{
			render_node* root = *root_iterator;
			render_node* leaf = ExtractLeaf(root);

			leaves.push_back(leaf->render_job);

			if (leaf == root)
			{
				root_iterator = render_trees.erase(root_iterator);
			}
			else
			{
				root_iterator++;
			}
		}

		return leaves;
	}

	//////////////////////////////////////////////////////////////////////////

	RenderJobContainer::render_node* RenderJobContainer::ExtractLeaf(render_node* node)
	{
		if (node->dependency && !node->dependency_executing)
		{
			render_node* leaf = ExtractLeaf(node->dependency);
			if (leaf == node->dependency)
			{
				node->dependency_executing = true;
			}
			return leaf;
		}
		else
		{
			return node;
		}
	}

	//////////////////////////////////////////////////////////////////////////
}