#include "MeshRendererMachinePiece.h"
#include "Marshalling/JigsawMarshalling.h"


namespace Jigsaw {
	namespace Render {
		START_REGISTER_SERIALIZABLE_CLASS(MeshRendererMachinePiece);
		REGISTER_SERIALIZABLE_CHILD_OF(MeshRendererMachinePiece, jsPiece);
		REGISTER_SERIALIZABLE_FIELD(MeshRendererMachinePiece, MeshRendererComponent, mesh);
		END_REGISTER_SERIALIZABLE_CLASS(MeshRendererMachinePiece);
	}
}
