#ifndef _MESH_RENDERER_MACHINE_PIECE_H_
#define _MESH_RENDERER_MACHINE_PIECE_H_

#include "Machines/jsPiece.h"
#include "Graphics/GPU_Objects/GPUResource.h"

namespace Jigsaw {
	namespace Render {

		struct MeshRendererComponent 
		{
			GPUResource* vertices;
			GPUResource* indices;
		};

		JGSW_MACHINE_PIECE(MeshRendererMachinePiece) {
		public:
			JGSW_COMPONENT_DATA(MeshRendererComponent, mesh);
		};
	}
}

#endif
