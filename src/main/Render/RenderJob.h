/*********************************************************************
 * RenderJob.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: April 2021
 *********************************************************************/
#ifndef _RENDER_JOB_H_
#define _RENDER_JOB_H_

 // Jigsaw_Engine
#include "Graphics/GraphicsContext.h"
#include "Graphics/CommandListExecutor.h"
#include "Graphics/Pipeline/JigsawPipeline.h"
#include "Graphics/Pipeline/PipelineService.h"
#include "Ref.h"

// stl
#include <vector>

namespace Jigsaw
{
	/// <summary>
	/// RenderJobs are used to specify all of the different operations that need to happen in a user-specified pipeline phase. 
	///
	/// Guarantees are generally not made about execution order, except in cases where a resource is updated (say a constant buffer)
	/// prior to the use of that resource in a draw command, or vice-versa. Any resource dependencies that exist *between* jobs must be
	/// managed by external systems. 
	/// </summary>
	class JGSW_API RenderJob
	{
	public:
		/// <summary>
		/// The primary constructor for the RenderJob requires a name and a m_graphicsContext to use
		/// </summary>
		/// <param name="name"></param>
		/// <param name="m_graphicsContext"></param>
		RenderJob(const std::string& name, Jigsaw::GraphicsContext* graphics_context);

		/// <summary>
		/// Virtual destructor
		/// </summary>
		virtual ~RenderJob();

		/// <summary>
		/// Takes two GPUResources and sets them as the Render Targets for the upcoming draw commands.
		/// </summary>
		void SetRenderTarget(
			Jigsaw::GPUResource& render_target,
			Jigsaw::GPUResource& dsv_resource,
			bool clear = false
		);

		/// <summary>
		/// Updates the 
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="update_data"></param>
		template <typename T>
		void UpdateResource(GPUResource& update_resource, UPDATE_DATA<T> update_data);

		/// <summary>
		/// .
		/// </summary>
		void DrawIndexed(Jigsaw::Pipeline::JIGSAW_PIPELINE pipeline, Jigsaw::VERTEX_INDEXED_DATA& render_data);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="vertex_data"></param>
		void DrawLines(const Jigsaw::VERTEX_DIRECT_DATA& vertex_data);

		/// <summary>
		/// Sends all of the commands recorded to lists in the render job to a corresponding executor backed by the GraphicsContext
		/// </summary>
		void Execute();

		/// <summary>
		/// Takes the multi-sampled contents of one resource and merges them to the destination resource, possibly in preparation for the dest_resource
		/// being sampled or displayed
		/// </summary>
		/// <param name="ms_resource"></param>
		/// <param name="dest_resource"></param>
		void ResolveMultiSampledResource(GPUResource& ms_resource, GPUResource& dest_resource);

		/// <summary>
		/// Adds a dependency reference to an external job
		/// </summary>
		/// <param name="dependency"></param>
		void SetDependency(const Jigsaw::Ref<RenderJob>& dependency);

		const std::string name;

	protected:
		Jigsaw::GraphicsContext* m_graphicsContext;
		Jigsaw::Ref<RenderJob> job_dependency;

		// mutable variables
		Jigsaw::GPUResource* m_renderTarget;
		Jigsaw::GPUResource* m_dsvTarget;
		Jigsaw::Ref<Jigsaw::CommandList> active_cmd_list;
		Jigsaw::Ref<Jigsaw::CommandListExecutor> active_executor;

	};

	//////////////////////////////////////////////////////////////////////////

	template <typename T>
	inline void RenderJob::UpdateResource(GPUResource& update_resource, UPDATE_DATA<T> update_data)
	{
		J_D_ASSERT_LOG_ERROR(update_data.data != nullptr && update_data.size != 0, RenderJob, "Can not call 'UpdateResource' with no data");
		update_data.size *= sizeof(T);

		active_cmd_list->UpdateResource(update_resource, reinterpret_cast<UPDATE_DATA_RAW&>(update_data));
	}

	//////////////////////////////////////////////////////////////////////////

	inline void RenderJob::DrawLines(const Jigsaw::VERTEX_DIRECT_DATA& vertex_data)
	{
		Ref<Pipeline::PipelineObject> pipeline = Pipeline::PipelineServiceManager::GetPipeline(Jigsaw::Pipeline::JIGSAW_PIPELINE_COLORED_LINES);
		active_cmd_list->DrawDirect(pipeline, vertex_data);
	}

	//////////////////////////////////////////////////////////////////////////

	inline void RenderJob::ResolveMultiSampledResource(GPUResource& ms_resource, GPUResource& dest_resource)
	{
		active_cmd_list->ResolveMultiSampledResource(ms_resource, dest_resource);
	}

	//////////////////////////////////////////////////////////////////////////
}

#endif
