#include "RenderService.h"
#include "Graphics/Pipeline/PipelineService.h"

#define _CALLBACK(callback) if (callback) callback()

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	RenderService::RenderService() {}

	//////////////////////////////////////////////////////////////////////////

	void RenderService::Execute()
	{
		_CALLBACK(start_frame_callback);

		/// <summary>
		/// Simple initial implementation that just collects all of the leaves iteratively and executes them
		/// </summary>
		std::vector<Ref<RenderJob>> leaves = GetLeaves();
		while (!leaves.empty())
		{
			for (Ref<RenderJob>& render_job : leaves)
			{
				render_job->Execute();
			}
			s_graphicsContext->GetCommandListExecutor(JGSW_COMMAND_TYPE_RENDER)->SignalAndWait();
			leaves = GetLeaves();
		}

		_CALLBACK(end_frame_callback);

		ClearContainer();

		this->s_graphicsContext->DisplayFrame();
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::RenderJob> RenderService::BuildJob(const std::string& name)
	{
		return MakeRef<Jigsaw::RenderJob>(name, s_graphicsContext);
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderService::SetStartFrameCallback(RENDER_SERVICE_CALLBACK&& callback)
	{
		start_frame_callback = std::move(callback);
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderService::SetEndFrameCallback(RENDER_SERVICE_CALLBACK&& callback)
	{
		end_frame_callback = std::move(callback);
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderService::DisplayOn(Jigsaw::GPUResource& rtv_resource)
	{
		this->render_target = &rtv_resource;
	}

	//////////////////////////////////////////////////////////////////////////

}

