#include "Physics/Transform.h"
#include "Render/Machine/MeshRendererMachinePiece.h"
#include "Render/RenderService.h"
#include "Systems/jsSystem.h"
#include "Camera.h"

namespace Jigsaw {
	namespace Render {

		/// <summary>
		/// </summary>
		/// <param name=""></param>
		JGSW_SYSTEM(MeshRenderSystem) {
			JGSW_SYSTEM_METHOD(MeshRender, const MeshRendererComponent, const Transform);

			SLOT_1(RenderService, s_renderService);
			SLOT_2(Camera, camera);
		};

		START_REGISTER_SYSTEM(MeshRenderSystem);
		END_REGISTER_SYSTEM(MeshRenderSystem);

		///////////////////////////////////////////////////////////////////////

		void MeshRenderSystem::MeshRender(const MeshRendererComponent& mesh, const Transform& transform) {
			Ref<RenderJob> job = s_renderService->GetJob("frame_game_job");

			VERTEX_INDEXED_DATA data;
			data.pvMat = camera->GetProjectionMatrix(1.77777778f) * camera->GetViewMatrix();
			data.t = transform;
			data.vertex_element_size = sizeof(PositionColorNormal);
			data.vertices_resource = mesh.vertices;
			data.indices_resource = mesh.indices;

			job->DrawIndexed(Jigsaw::Pipeline::JIGSAW_PIPELINE_DRAW, data);
		}
	}
}
