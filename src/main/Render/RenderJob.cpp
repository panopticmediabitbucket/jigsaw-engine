#include "RenderJob.h"
#include "Graphics/Pipeline/PipelineService.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	RenderJob::RenderJob(const std::string& name, Jigsaw::GraphicsContext* graphics_context)
		: name(name), m_graphicsContext(graphics_context) {
		this->active_cmd_list = m_graphicsContext->GetCommandList(JGSW_COMMAND_TYPE::JGSW_COMMAND_TYPE_RENDER);
		this->active_executor = m_graphicsContext->GetCommandListExecutor(JGSW_COMMAND_TYPE::JGSW_COMMAND_TYPE_RENDER);
	}

	//////////////////////////////////////////////////////////////////////////

	RenderJob::~RenderJob()
	{
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJob::SetRenderTarget(
		Jigsaw::GPUResource& render_target,
		Jigsaw::GPUResource& dsv_resource,
		bool clear)
	{

		active_cmd_list->SetRenderViews(render_target, dsv_resource);
		if (clear)
		{
			active_cmd_list->ClearResource(render_target);
			active_cmd_list->ClearResource(dsv_resource);
		}

		this->m_renderTarget = &render_target;
		this->m_dsvTarget = &dsv_resource;
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJob::DrawIndexed(
		Jigsaw::Pipeline::JIGSAW_PIPELINE pipeline,
		Jigsaw::VERTEX_INDEXED_DATA& render_data)
	{
		auto pl = Jigsaw::Pipeline::PipelineServiceManager::GetPipeline(pipeline);
		active_cmd_list->DrawIndexed(pl, render_data);
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJob::Execute()
	{
		if (active_cmd_list)
		{
			this->active_executor->SubmitCommandList(this->active_cmd_list);
		}
		active_cmd_list.reset();
	}

	//////////////////////////////////////////////////////////////////////////

	void RenderJob::SetDependency(const Jigsaw::Ref<RenderJob>& dependency)
	{
		this->job_dependency = dependency;
	}

	//////////////////////////////////////////////////////////////////////////
}
