/*********************************************************************
 * RenderJobContainer.h
 *
 * Author: jl_ra
 * Originally created: April 2021
 *********************************************************************/
#ifndef _RENDER_JOB_CONTAINER_H_
#define _RENDER_JOB_CONTAINER_H_

#include <map>
#include <vector>
#include <string>

#include "RenderJob.h"
#include "Graphics/GraphicsContext.h"
#include "Ref.h"
#include "_jgsw_api.h"

#undef GetJob
#undef AddJob

namespace Jigsaw 
{
		/// <summary>
		/// The RenderJobContainer is an interface for any item that stores and returns RenderJobs.
		/// </summary>
		class JGSW_API RenderJobContainer
		{
		public:

			/// <summary>
			/// Checks if a job with 'name' exists in the job map. If it does, the 'out_render_job' will be populated with it, (or not if it is null)
			/// </summary>
			/// <param name="name"></param>
			/// <param name="out_render_job"></param>
			/// <returns></returns>
			bool HasJob(const std::string& name, Jigsaw::Ref<Jigsaw::RenderJob>* out_render_job);

			/// <summary>
			/// Returns the job associated with the name. 
			/// </summary>
			/// <param name="name"></param>
			/// <returns></returns>
			Jigsaw::Ref<Jigsaw::RenderJob> GetJob(const std::string& name);

			/// <summary>
			/// Creates a new job associated with 'name'
			/// </summary>
			/// <param name="name"></param>
			/// <returns></returns>
			Jigsaw::Ref<Jigsaw::RenderJob> CreateJob(const std::string& name, const std::string* dependency = nullptr);

			/// <summary>
			/// Registers a dependency from job 'name' to 'depends_on'
			/// </summary>
			/// <param name="name"></param>
			/// <param name="depends_on"></param>
			void AddDependency(const std::string& name, const std::string& depends_on);

		protected:

			/// <summary>
			/// An internal method that allows the inheriting class to pass an already created job into the data structure.
			/// </summary>
			/// <param name="render_job"></param>
			void AddJob(const Jigsaw::Ref<Jigsaw::RenderJob>& render_job);

			/// <summary>
			/// Implementations of the RenderJobContainer abstract class must provide a mechanism for building a new job. This will be called internally by 'CreateJob,' which
			/// files the built job into the underlying data structures for reference later. 
			/// </summary>
			/// <param name="name"></param>
			/// <returns></returns>
			virtual Jigsaw::Ref<Jigsaw::RenderJob> BuildJob(const std::string& name) = 0;

			/// <summary>
			/// Reinitializes the container.
			/// </summary>
			void ClearContainer();

			/// <summary>
			/// Returns the jobs with no outstanding dependencies and clears them in the process.
			/// </summary>
			/// <returns></returns>
			std::vector<Jigsaw::Ref<Jigsaw::RenderJob>> GetLeaves();

		private:
			/// <summary>
			/// Each render node is allowed exactly one dependency. This is done to manage complexity
			/// </summary>
			struct render_node
			{
				Jigsaw::Ref<Jigsaw::RenderJob> render_job;
				render_node* dependency = nullptr;
				bool dependency_executing = false;
			};

			render_node* ExtractLeaf(render_node* node);

			/// <summary>
			/// Each render_node is the root of its own render tree
			/// </summary>
			size_t pool_index = 0;
			render_node node_pool[10];
			std::vector<render_node*> render_trees;
			std::map<std::string, render_node*> job_map;

		};
}
#endif