#include "SystemArguments.h"
#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw
{
	START_REGISTER_SERIALIZABLE_CLASS(SystemInformation)
	REGISTER_SERIALIZABLE_FIELD(SystemInformation, std::string, qualified_system_name)
	END_REGISTER_SERIALIZABLE_CLASS(SystemInformation)

	START_REGISTER_SERIALIZABLE_CLASS(PriorityNode)
	REGISTER_SERIALIZABLE_VECTOR(PriorityNode, SystemInformation, concurrent_systems)
	END_REGISTER_SERIALIZABLE_CLASS(PriorityNode)

	START_REGISTER_SERIALIZABLE_CLASS(SystemManagerArguments)
	REGISTER_SERIALIZABLE_VECTOR(SystemManagerArguments, PriorityNode, system_priority_levels)
	END_REGISTER_SERIALIZABLE_CLASS(SystemManagerArguments)
}