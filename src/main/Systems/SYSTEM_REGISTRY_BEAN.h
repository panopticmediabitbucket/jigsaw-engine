/*********************************************************************
 * SYSTEM_REGISTRY_BEAN.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _SYSTEM_REGISTRY_BEAN_H_
#define _SYSTEM_REGISTRY_BEAN_H_

// Jigsaw_Engine
#include "Ref.h"
#include "System/sysSignature.h"

namespace Jigsaw
{
	enum class JGSW_API JGSW_SYSTEM_ATTRIBUTES
	{
		CONCURRENT				= 1,
		RUN_IN_EDITOR			= (CONCURRENT			<< 1),
		FILTERABLE				= (RUN_IN_EDITOR		<< 1),
		RENDER_SYSTEM			= (FILTERABLE			<< 1),
	};

	enum class JGSW_API JGSW_SYSTEM_TYPE
	{
		USER,
		RENDER,
	};

	class JGSW_API jsSystem;

	typedef Jigsaw::Ref<Jigsaw::jsKnobProvider>(*SYSTEM_KNOB_PROVIDER_CONSTRUCTOR)(const char* name, const char* qualifier);

	/// <summary>
	/// The SYSTEM_REGISTRY_BEAN contains all registry information needed for the SystemRegistry to keep track of
	/// all of the systems and how to instantiate them.
	/// </summary>
	struct JGSW_API SYSTEM_REGISTRY_BEAN
	{
		Jigsaw::Ref<Jigsaw::sysSignature> signature;
		SYSTEM_KNOB_PROVIDER_CONSTRUCTOR systemKnobProviderConstructor = nullptr;
		u32 systemAttributeFlags = 0;
		JGSW_SYSTEM_TYPE systemType = JGSW_SYSTEM_TYPE::USER;
		const Jigsaw::etype_info* typeInfo = nullptr;
	};
}
#endif
