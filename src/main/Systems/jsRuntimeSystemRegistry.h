/*********************************************************************
 * RuntimeSystemRegistry.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _RUNTIME_SYSTEM_DATA_REGISTRY_H_
#define _RUNTIME_SYSTEM_DATA_REGISTRY_H_

 // Jigsaw_Engine
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Injection/JigsawInjection.h"
#include "Injection/jsContext.h"
#include "Systems/SystemArguments.h"
#include "Systems/jsSystem.h"
#include "RUNTIME_SYSTEM_DATA.h"

// std
#include <vector>

namespace Jigsaw
{
	/// <summary>
	/// jsRuntimeSystemRegistry is responsible for maintaining a list of active and inactive systems for a specific 'world'
	/// </summary>
	class JGSW_API jsRuntimeSystemRegistry : public jsSlots<jsRuntimeSystemRegistry>, public Jigsaw::jsContext
	{
	public:
		jsRuntimeSystemRegistry(
			const Jigsaw::SystemManagerArguments& arguments,
			const std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>& system_beans
		);

		/// <summary>
		/// Returns the RUNTIME_PRIORITY_LEVEL at the given priority index for processing.
		/// </summary>
		const Jigsaw::RUNTIME_PRIORITY_LEVEL& GetPriorityLevel(size_t priority_index);

		/// <summary>
		/// Indicates the number of priority levels for the registry.
		/// </summary>
		size_t GetPriorityLevels() const;

	protected:

		/// <summary>
		/// This override retrieves the JigsawKnobProviders from the SYSTEM_REGISTRY_BEANs.  
		/// <returns></returns>
		/// </summary>
		std::vector<Jigsaw::Ref<Jigsaw::jsKnobProvider>> GetKnobProviders() override;

		/// <summary>
		/// Returns the injected context.
		/// </summary>
		std::vector<Jigsaw::jsContext*> GetContextDependencies() override;

		/// <summary>
		/// For the system registry, the RUNTIME_PRIORITY_LEVELS have slots that need to be filled. This is important to ensure the RootSystems are populated.
		/// </summary>
		void PostGraphResolution() override;

		std::vector<Jigsaw::RUNTIME_PRIORITY_LEVEL> m_priorityLevels;

		std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN> m_systemBeans;

		Jigsaw::SystemManagerArguments m_activeArguments;

		INJECT_CONTEXT(slot_1, root_context);

	};
}
#endif