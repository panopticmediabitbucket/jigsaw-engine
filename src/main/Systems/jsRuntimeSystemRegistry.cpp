#include "jsRuntimeSystemRegistry.h"
#include "Debug/j_debug.h"
#include "Injection/jsKnobGraphDefinitionBuilder.h"

#ifdef __JIGSAW_EDITOR
#define IN_EDITOR true
#else
#define IN_EDITOR false
#endif

namespace Jigsaw
{
	//////////////////////////////////////////////////////////////////////////

	static const char* instancesQualArray[10] =
	{
		"instance_0",
		"instance_1",
		"instance_2",
		"instance_3",
		"instance_4",
		"instance_5",
		"instance_6",
		"instance_7",
		"instance_8",
		"instance_9"
	};

	//////////////////////////////////////////////////////////////////////////

	jsRuntimeSystemRegistry::jsRuntimeSystemRegistry(
		const Jigsaw::SystemManagerArguments& arguments,
		const std::vector<Jigsaw::SYSTEM_REGISTRY_BEAN>& system_beans
	) : m_activeArguments(arguments), m_systemBeans(system_beans) { }

	//////////////////////////////////////////////////////////////////////////

	std::vector<Jigsaw::Ref<Jigsaw::jsKnobProvider>> jsRuntimeSystemRegistry::GetKnobProviders()
	{
		using namespace Jigsaw;
		J_LOG_INFO(jsRuntimeSystemRegistry, "Collecting knobs from jsRuntimeSystemRegistry systems");

		std::vector<Jigsaw::Ref<jsKnobProvider>> knob_providers;

		for (auto node : this->m_activeArguments.system_priority_levels)
		{
			J_LOG_INFO(jsRuntimeSystemRegistry, "Processing PriorityNode with {0} systems", node.concurrent_systems.size());
			RUNTIME_PRIORITY_LEVEL runtime_node;
			runtime_node.system_count = node.concurrent_systems.size();
			runtime_node.systems = new RUNTIME_SYSTEM_DATA[runtime_node.system_count];

			for (int i = 0; i < runtime_node.system_count; i++)
			{
				RUNTIME_SYSTEM_DATA& runtime_system = runtime_node.systems[i];
				runtime_system.instance_count = 1;
				runtime_system.system_instances = new jsSystem * [1];

				const Jigsaw::SystemInformation& sys_info = node.concurrent_systems.at(i);
				auto search_iter = std::find_if(this->m_systemBeans.begin(), this->m_systemBeans.end(),
					[&](const Jigsaw::SYSTEM_REGISTRY_BEAN& bean)
					{
						J_D_ASSERT_LOG_ERROR(bean.typeInfo != nullptr, jsRuntimeSystemRegistry, "type_info of a SYSTEM_REGISTRY_BEAN was null.");
						return bean.typeInfo->GetQualifiedName() == sys_info.qualified_system_name;
					}
				);

				J_D_ASSERT_LOG_ERROR(search_iter != this->m_systemBeans.end(), jsRuntimeSystemRegistry, "Failed to find a corresponding system whose name matches the fully-qualified name '{0}'", sys_info.qualified_system_name);
				SYSTEM_REGISTRY_BEAN& bean = *search_iter;
				runtime_system.registry_data = bean;
				runtime_system.active = !IN_EDITOR
					|| (bean.systemAttributeFlags & ((int)JGSW_SYSTEM_ATTRIBUTES::RENDER_SYSTEM | (int)JGSW_SYSTEM_ATTRIBUTES::RUN_IN_EDITOR));

				J_D_ASSERT_LOG_ERROR(bean.systemKnobProviderConstructor != nullptr, jsRuntimeSystemRegistry, "Unexpected knob provider constructor function being null");
				J_D_ASSERT_LOG_ERROR(runtime_system.instance_count <= ARRAYSIZE(instancesQualArray), jsRuntimeSystemRegistry, "Can not have more than 10 requested instances");
				for (int j = 0; j < runtime_system.instance_count; j++)
				{
					const char* name = bean.typeInfo->GetQualifiedName();
					Jigsaw::Ref<Jigsaw::jsKnobProvider> system_knob_provider =
						bean.systemKnobProviderConstructor(name, instancesQualArray[j]);
					knob_providers.push_back(system_knob_provider);
				}
			}

			this->m_priorityLevels.push_back(runtime_node);
		}

		return knob_providers;
	}

	//////////////////////////////////////////////////////////////////////////

	void jsRuntimeSystemRegistry::PostGraphResolution()
	{
		using namespace Jigsaw;

		for (RUNTIME_PRIORITY_LEVEL& PRIORITY_LEVEL : this->m_priorityLevels)
		{

			for (int i = 0; i < PRIORITY_LEVEL.system_count; i++)
			{
				RUNTIME_SYSTEM_DATA& runtime_system = PRIORITY_LEVEL.systems[i];

				for (int j = 0; j < runtime_system.instance_count; j++)
				{
					const char* system_name = runtime_system.registry_data.typeInfo->GetQualifiedName();
					jsKnobGraphDefinitionBuilder<jsSystem> builder(system_name);
					jsSystem** destination = runtime_system.system_instances + j;
					builder.SetSetterDestination(destination).SetQualifier(instancesQualArray[j]);
					FillSlot(builder.CompileSlot());
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	const RUNTIME_PRIORITY_LEVEL& jsRuntimeSystemRegistry::GetPriorityLevel(size_t priority_index)
	{
		return m_priorityLevels[priority_index];
	}

	//////////////////////////////////////////////////////////////////////////

	size_t jsRuntimeSystemRegistry::GetPriorityLevels() const
	{
		return m_priorityLevels.size();
	}

	//////////////////////////////////////////////////////////////////////////

	std::vector<Jigsaw::jsContext*> jsRuntimeSystemRegistry::GetContextDependencies()
	{
		std::vector<Jigsaw::jsContext*> dependencies;
		if (root_context)
		{
			dependencies.push_back(this->root_context);
		}
		return dependencies;
	}

	//////////////////////////////////////////////////////////////////////////

}
