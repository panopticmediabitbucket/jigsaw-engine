/*********************************************************************
 * SystemArguments.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _SYSTEM_ARGUMENTS_H_
#define _SYSTEM_ARGUMENTS_H_

// std
#include <vector>
#include <string>

namespace Jigsaw 
{
	///
	/// 
	/// 
	struct JGSW_API SystemInformation
	{
		std::string qualified_system_name;
	};

	///
	/// PriorityNodes indicate groups of systems that can be run concurrently. These are organized by name. 
	/// 
	struct JGSW_API PriorityNode
	{
		std::vector<SystemInformation> concurrent_systems;
	};

	///
	/// SystemArguments are consumed by the ApplicationRootProperties, among other things, to designate the ordering of Systems,
	/// among other things, for the engine runtime. 
	///
	struct JGSW_API SystemManagerArguments
	{
		std::vector<PriorityNode> system_priority_levels;
		bool in_editor = false;
	};
}
#endif