#ifndef _RUNTIME_SYSTEM_DATA_H_
#define _RUNTIME_SYSTEM_DATA_H_

#include "SYSTEM_REGISTRY_BEAN.h"
#include "Ref.h"
#include <vector>

namespace Jigsaw
{
	///
	/// RUNTIME_SYSTEM_DATA houses all relevant data for a single system.
	/// 
	struct RUNTIME_SYSTEM_DATA {
		SYSTEM_REGISTRY_BEAN registry_data;
		bool active = true;
		Jigsaw::jsSystem** system_instances = nullptr;
		size_t instance_count = 0;
	};

	///
	/// 
	/// 
	struct RUNTIME_PRIORITY_LEVEL {
		RUNTIME_SYSTEM_DATA* systems = nullptr;
		size_t system_count = 0;
	};

	/*
	void DestructNode(RUNTIME_PRIORITY_LEVEL& node) {
		for (int i = 0; i < node.system_count; i++) {
			delete[] node.systems[i].system_instances;
		}
		delete[] node.systems;
	}
	*/
}

#endif

