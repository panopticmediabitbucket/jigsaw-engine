/*********************************************************************
 * jsSystem.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _ROOT_SYS_H_
#define _ROOT_SYS_H_

 // std
#include <vector>
#include <string>
#include <typeinfo>

// Jigsaw Engine
#include "Assembly/JGSW_ASSEMBLY.h"
#include "Entities/jsEntity.h"
#include "Injection/JigsawInjection.h"
#include "Injection/jsKnobProvider.h"
#include "Injection/jsKnobGraphDefinitionBuilder.h"
#include "System/sysSignatureMap.h"
#include "Util/looping_macros.h"
#include "Systems/SYSTEM_REGISTRY_BEAN.h"
#include "System/sysSignature.h"
#include "Util/DTO.h"
#include "Math/LinAlg.h"

namespace Jigsaw
{
	/// <summary>
	/// Every system, multi-threaded or not, must inherit from the jsSystem class
	/// 
	/// New systems are registered using the macros.
	/// </summary>
	class JGSW_API jsSystem 
	{
	public:
		RTTI_LINK_CHILDREN(jsSystem);

		virtual ~jsSystem() = default;

		void Update(Jigsaw::jsEntity& entity, const Jigsaw::sysSignatureMap& conversion_map);

	protected:
		virtual void InternalUpdate(Jigsaw::jsEntity& entity, const Jigsaw::sysSignatureMap& conversion_map) = 0;

		// Internally, systems can retrieve the target entity that they are operating on. 
		const Jigsaw::jsEntity& GetEntity();

	private:
		Jigsaw::jsEntity* this_entity;

		static std::vector<Jigsaw::sysSignature> system_signatures;

	};
}

///
/// Defined below are the macros that define the general semantics for creating new systems

#define THIS_ENTITY GetEntity()

#define _type_entity_mapping_cast(i, type_name) *static_cast<_EXPAND(type_name)*>((void*)(entity_loc + conversion_map[_EXPAND(i)_]))
#define _ref_type_arg(arg, type_name) type_name& arg
#define _sys_sig_member_type(arg, type_name) const Jigsaw::etype_info* _EXPAND(arg)_ = &Jigsaw::etype_info::Id<type_name>(); builder.AddType(*_EXPAND(arg)_);

#define JGSW_SYSTEM(sys_name) class sys_name : public Jigsaw::jsSystem, public Jigsaw::jsSlots<sys_name>

#define JGSW_SYSTEM_METHOD(sys_name, ...) \
public:\
	void InternalUpdate(Jigsaw::jsEntity& entity, const Jigsaw::sysSignatureMap& conversion_map) override {\
		u8* entity_loc = entity.GetRawData(); \
		_EXPAND(sys_name)(_EXPAND(_COUNT_TYPE_ARGS(from __VA_ARGS__, _mac_arg_fe_8, _mac_arg_fe_7, _mac_arg_fe_6, _mac_arg_fe_5, _mac_arg_fe_4, _mac_arg_fe_3, _mac_arg_fe_2, _mac_arg_fe_1)(_type_entity_mapping_cast, zero, __VA_ARGS__)));\
	};\
 static Jigsaw::SYSTEM_REGISTRY_BEAN _Get_Registry_Bean() {\
	Jigsaw::SYSTEM_REGISTRY_BEAN bean; \
	Jigsaw::SignatureBuilder builder; \
	_EXPAND(_COUNT_TYPE_ARGS(__VA_ARGS__, _mac_fe_8, _mac_fe_7, _mac_fe_6, _mac_fe_5, _mac_fe_4, _mac_fe_3, _mac_fe_2, _mac_fe_1)(_sys_sig_member_type, a, __VA_ARGS__))\
	bean.signature = Jigsaw::MakeRef<Jigsaw::sysSignature>(builder.SortMode(Jigsaw::SignatureBuilder::SORT_MODE::SORT_MODE_OFF).Build());\
	bean.typeInfo = &Jigsaw::etype_info::Id<_THIS_TYPE>(); \
	return bean; \
}; \
void _EXPAND(sys_name)(_EXPAND(_COUNT_TYPE_ARGS(__VA_ARGS__, _mac_arg_fe_8, _mac_arg_fe_7, _mac_arg_fe_6, _mac_arg_fe_5, _mac_arg_fe_4, _mac_arg_fe_3, _mac_arg_fe_2, _mac_arg_fe_1)(_ref_type_arg, a, __VA_ARGS__))) 

//template<typename T>


#define START_REGISTER_SYSTEM(sys_name) \
class sys_name##KnobProvider : public Jigsaw::jsKnobProvider \
{ \
public: \
sys_name##KnobProvider(const char* name, const char* qualifier) \
{\
	using namespace Jigsaw; \
	jsKnobGraphDefinitionBuilder<sys_name> builder(name); \
	builder.SetQualifier(qualifier); \
	builder.AddSuperType<Jigsaw::jsSystem>(); \
	knobs.insert(knobs.end(), builder.CompileKnob()); \
}; \
}; \
Jigsaw::Ref<Jigsaw::jsKnobProvider> sys_name##KnobProvider_Builder(const char* name, const char* qualifier) { \
	return Jigsaw::Ref<Jigsaw::jsKnobProvider>(new sys_name##KnobProvider(name, qualifier)); \
};\
struct sys_name##_Bean_Register { \
	sys_name##_Bean_Register() {\
		Jigsaw::SYSTEM_REGISTRY_BEAN bean = sys_name::_Get_Registry_Bean(); \
		bean.systemKnobProviderConstructor = &sys_name##KnobProvider_Builder; \

#define ADD_SYSTEM_ATTRIBUTE(attribute) bean.systemAttributeFlags |= (int)attribute;

#define END_REGISTER_SYSTEM(sys_name) \
		Jigsaw::Assembly::SystemRegistry::GetBeans().push_back(bean); \
	}; \
}; \
extern _EXPAND(sys_name)_Bean_Register _EXPAND(sys_name)_Bean_Register_def; \
_EXPAND(sys_name)_Bean_Register _EXPAND(sys_name)_Bean_Register_def; 


#endif 
