/*********************************************************************
 * jsEntity.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _JIGSAW_ENTITY_H_
#define _JIGSAW_ENTITY_H_

 // Jigsaw_Engine
#include "RTTI/etype_info.h"
#include "System/sysSignature.h"
#include "System/UID.h"
#include "Util/BYTE.h"

// std
#include <memory>

namespace Jigsaw
{
	/// <summary>
	/// The jsEntityCluster is friended so that the raw, underlying data stream can be retrieved from the jsEntity
	/// </summary>
	class JGSW_API jsEntityCluster;

	/// <summary>
	/// A jsEntity is an instantiation of an EntitySignature's type layout. JigsawEntities are packed tightly into an
	/// EntityCluster, which are managed by the EntityService. 
	/// </summary>
	class JGSW_API jsEntity
	{
		friend JGSW_API jsEntityCluster;
	public:
		/// <summary>
		/// Defines the scope of a given entity
		/// </summary>
		enum SCOPE
		{
			SCOPE_EPHEMERAL,
			SCOPE_STATIC,
			SCOPE_PERSISTENT
		};

		/// <summary>
		/// Copy constructor just takes data directly from the other jsEntity; it does not copy it because the jsEntity object
		/// does not own the underlying object
		/// </summary>
		/// <param name="other"></param>
		jsEntity(const jsEntity& other);

		/// <summary>
		/// Rvalue move constructor just takes data directly from the other jsEntity. The reason is that the jsEntity
		/// does not own the underlying data, so it can't transfer it. 
		/// </summary>
		/// <param name="other"></param>
		jsEntity(jsEntity&& other) noexcept;

		/// <summary>
		/// Returns a non-reference copy of the member data of type 'T' in this particular entity.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <returns>The components current data, not by reference</returns>
		template <typename T>
		T GetMemberData() const
		{
			static const Jigsaw::etype_info& type = Jigsaw::etype_info::Id<T>();
			u8 offset = m_map.GetTypeOffset(type);
			u8* loc = m_rawData + offset;
			return *Jigsaw::Util::CastToTypeObject<T>(loc);
		}

		template <typename T>
		T& GetMemberDataRef()
		{
			static const Jigsaw::etype_info& type = Jigsaw::etype_info::Id<T>();
			u8 offset = m_map.GetTypeOffset(type);
			u8* loc = m_rawData + offset;
			return *Jigsaw::Util::CastToTypeObject<T>(loc);
		}

		/// <summary>
		/// Assigns the value of 'data' to the the Entity's component of the same type 'T'
		/// </summary>
		/// <typeparam name="T"></typeparam>
		template <typename T>
		inline void SetMemberData(const T& data)
		{
			static const Jigsaw::etype_info& type = Jigsaw::etype_info::Id<T>();
			u8 offset = m_map.GetTypeOffset(type);
			u8* loc = m_rawData + offset;
			*static_cast<T*>((void*)loc) = data;
		}

		/// <summary>
		/// Assigns the value of 'data' to the the Entity's component of the same type 'T'
		/// </summary>
		/// <typeparam name="T"></typeparam>
		template <typename T>
		inline void SetMemberData(T&& data)
		{
			static const Jigsaw::etype_info& type = Jigsaw::etype_info::Id<T>();
			u8 offset = m_map.GetTypeOffset(type);
			u8* loc = m_rawData + offset;
			*static_cast<T*>((void*)loc) = std::move(data);
		}

		/// <summary>
		/// Returns a reference to this Entity's unique identifier
		/// </summary>
		/// <returns></returns>
		const Jigsaw::UID& GetUID() const;

		/// <summary>
		/// Fetches the direct raw_data located in the cluster
		/// </summary>
		/// <returns></returns>
		u8* GetRawData() { return m_rawData; }

		/// <summary>
		/// Returns the scope of the current entity
		/// </summary>
		/// <returns></returns>
		SCOPE GetScope() const;

	private:

		jsEntity(u8* raw_data, const Jigsaw::sysSignatureMap& map, Jigsaw::UID& uid, SCOPE scope);

		SCOPE m_scope;
		const Jigsaw::sysSignatureMap& m_map;
		Jigsaw::UID m_id;

		/// <summary>
		/// The jsEntity itself has no responsibility for the lifetime of the underlying raw_data. 
		/// The deletion of the raw_data is the responsibility of the jsEntityCluster which is interfaced with
		/// by the JigsawMachineObserver
		/// </summary>
		u8* m_rawData;
	};
}
#endif
