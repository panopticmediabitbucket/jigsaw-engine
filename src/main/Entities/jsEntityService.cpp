#include "jsEntityService.h"

using namespace Jigsaw;

//////////////////////////////////////////////////////////////////////////

Jigsaw::jsEntityService::jsEntityService() {
}

//////////////////////////////////////////////////////////////////////////

jsEntityCluster& Jigsaw::jsEntityService::operator[](const Jigsaw::sysSignature& signature)
{
	auto find = m_signatureClusterMap.find(signature.GetHash());
	if (find != m_signatureClusterMap.end()) 
	{
		return *(find->second);
	} else 
	{
		jsEntityCluster* new_service = new jsEntityCluster(MakeRef<sysSignature>(signature));
		m_signatureClusterMap.insert(std::make_pair(signature.GetHash(), new_service));
		return *new_service;
	}
}

//////////////////////////////////////////////////////////////////////////

