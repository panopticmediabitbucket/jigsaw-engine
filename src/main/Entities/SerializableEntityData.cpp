#include "SerializableEntityData.h"
#include "Marshalling/JigsawMarshalling.h"

using namespace Jigsaw::Util;

namespace Jigsaw
{
	START_REGISTER_SERIALIZABLE_CLASS(SerializableEntityData)
	REGISTER_SERIALIZABLE_FIELD(SerializableEntityData, UID, entity_id)
	REGISTER_SERIALIZABLE_FIELD(SerializableEntityData, jsEntity::SCOPE, scope)
	REGISTER_SERIALIZABLE_VECTOR(SerializableEntityData, etype_index, aligned_types)
	REGISTER_SERIALIZABLE_FIELD(SerializableEntityData, StringVector, string_vector)
	END_REGISTER_SERIALIZABLE_CLASS(SerializableEntityData)
}