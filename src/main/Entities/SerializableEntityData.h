#ifndef _SERIALIZABLE_ENTITY_DATA_H_
#define _SERIALIZABLE_ENTITY_DATA_H_

#include "jsEntity.h"
#include "System/UID.h"
#include "Util/StringVector.h"

namespace Jigsaw 
{
	class SerializableEntityData
	{
	public:
		Jigsaw::UID entity_id;
		Jigsaw::jsEntity::SCOPE scope;
		std::vector<Jigsaw::etype_index> aligned_types;
		Jigsaw::Util::StringVector string_vector;

	};
}
#endif