/*********************************************************************
 * jsEntityCluster.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _JIGSAW_ENTITY_CLUSTER_H_
#define _JIGSAW_ENTITY_CLUSTER_H_

 // Jigsaw_Engine
#include "Debug/j_debug.h"
#include "Entities/jsEntity.h"
#include "Ref.h"
#include "System/UID.h"
#include "System/sysSignature.h"
#include "Util/BYTE.h"

// External
#include <stdlib.h>

#define CLUSTER_SIZE 60

namespace Jigsaw
{
	/// <summary>
	/// JigsawEntityClusters maintain all of the JigsawEntityClusterNodes for a unique signature. 
	/// </summary>
	class JGSW_API jsEntityCluster
	{
	private:
		/// <summary>
		/// A private class that encapsulates the interface into any of the Entity Data Clusters
		/// </summary>
		class jsEntityClusterNode
		{
		public:
			/// <summary>
			/// Primary constructor takes a signature reference as an argument.
			/// </summary>
			/// <param name="signature"></param>
			jsEntityClusterNode(const Jigsaw::Ref<Jigsaw::sysSignature>& signature);

			/// <summary>
			/// Returns an Entity representing the data at the specified index 'x'
			/// </summary>
			/// <param name="x"></param>
			/// <returns></returns>
			jsEntity operator[](u32 x);

			/// <summary>
			/// Destroys the jsEntity at the given index, calling the destructor for each of its components in the sysSignature
			/// </summary>
			/// <param name="index"></param>
			void Destroy(int index);

			/// <summary>
			/// Fabricates a brand new jsEntity with the local sysSignature
			/// </summary>
			/// <param name="scope">The lifetime of the entity</param>
			/// <param name="id_ovrd">An optional parameter specifying the id of the fabricated entity</param>
			/// <returns></returns>
			jsEntity FabricateEntity(jsEntity::SCOPE scope, const UID* id_ovrd);

			/// <summary>
			/// Pops the last jsEntity off the end of the ClusterNode
			/// </summary>
			/// <returns></returns>
			jsEntity PopLast();

			/// <summary>
			/// Replaces the empty spot at 'index' with an already-existing 'entity' that is being moved from somewhere else,
			/// usually the result of a 'PopLast' call
			/// </summary>
			/// <param name="entity"></param>
			/// <param name="index"></param>
			void ReplaceEmptyIndex(const jsEntity& entity, u32 index);

			bool Contains(const Jigsaw::UID& id, u32* index);

			inline u32 GetCount() const { return count; }

		private:
			Jigsaw::Ref<u8[]> raw_data;
			Jigsaw::UID cluster_ids[CLUSTER_SIZE];
			Jigsaw::jsEntity::SCOPE scopes[CLUSTER_SIZE];
			unsigned int count;
			const Jigsaw::Ref<Jigsaw::sysSignature>& signature;
		};

	public:

#if JGSW_DEV
		static void ConvertEntities(jsEntityCluster& from, jsEntityCluster& to, const UID* entity_ids, size_t num_ids);
#endif // JGSW_DEV

		/// <summary>
		/// This public class allows external classes to iterate through the JigsawEntities of a given node without having
		/// direct access to the Node definition itself
		/// </summary>
		class ENTITY_ITERATOR
		{
		public:
			ENTITY_ITERATOR(jsEntityClusterNode& cluster);

			inline ENTITY_ITERATOR& operator++(int) { i++; return *this; }

			inline jsEntity operator*() { return node[i]; };

			inline operator bool() { return i < node.GetCount(); };

		private:
			jsEntityClusterNode& node;

			u32 i = 0;

		};

		/// <summary>
		/// The primary constructor needs only to know about the sysSignature for all of the JigsawEntities managed within. 
		/// This sysSignature will never change for the entire lifetime of the JigsawEntityClusterRoot.
		/// </summary>
		/// <param name="signature"></param>
		jsEntityCluster(const Jigsaw::Ref<Jigsaw::sysSignature>& signature) : signature(signature) {}

		/// <summary>
		/// Returns the number of ClusterNodes in the Cluster
		/// </summary>
		/// <returns></returns>
		size_t GetClusterCount() const;

		/// <summary>
		/// Returns the total number of Entities present in this cluster, across all of its nodes
		/// </summary>
		/// <returns></returns>
		size_t GetTotalCount() const;

		/// <summary>
		/// Returns the jsEntity associated with the UID. It is assumed that the Entity exists in the Cluster
		/// </summary>
		/// <param name="entity_id"></param>
		/// <returns></returns>
		jsEntity FetchEntity(const Jigsaw::UID& entity_id);

		/// <summary>
		/// Creates a new entity associated with this jsEntityCluster and its signature
		/// </summary>
		/// <returns></returns>
		inline jsEntity FabricateEntity(jsEntity::SCOPE scope) { return FabricateEntity(scope, nullptr); };

		/// <summary>
		/// Remove the entity with the specified UID from the jsEntityCluster
		/// </summary>
		/// <param name="entity_id"></param>
		void RemoveEntity(const Jigsaw::UID& entity_id);

		/// <summary>
		/// Returns a constant reference to the signature housed in the Cluster
		/// </summary>
		/// <returns></returns>
		const Jigsaw::sysSignature& GetSignature() const;

		ENTITY_ITERATOR GetNodeIterator(unsigned int i);

	private:
		/// <summary>
		/// The internal fabricate call builds an entity with the id assigned manually. If the argument is null, a random Id is assigned
		/// </summary>
		/// <returns></returns>
		jsEntity FabricateEntity(jsEntity::SCOPE scope, const UID* id);

		std::unordered_map<Jigsaw::UID, Jigsaw::Ref<jsEntityClusterNode>> entity_node_map;
		std::vector<Jigsaw::Ref<jsEntityClusterNode>> cluster_nodes;
		const Jigsaw::Ref<Jigsaw::sysSignature> signature;

	};
}

#endif
