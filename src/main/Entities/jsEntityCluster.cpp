#include "jsEntityCluster.h"
#include "Debug/j_debug.h"

using namespace Jigsaw;

////////////////////////////////////////////////////////////////////////////////

inline jsEntity Jigsaw::jsEntityCluster::jsEntityClusterNode::operator[](u32 x)
{
	u32 offset = x * (u32)signature->GetSize();
	return jsEntity(raw_data.get() + offset, signature->GetMap(), cluster_ids[x], scopes[x]);
}

////////////////////////////////////////////////////////////////////////////////

jsEntityCluster::jsEntityClusterNode::jsEntityClusterNode(const Jigsaw::Ref<Jigsaw::sysSignature>& signature) : signature(signature),
raw_data(Jigsaw::Ref<u8[]>((u8*)_aligned_malloc(signature->GetSize()* CLUSTER_SIZE, sizeof(max_align_t)), [](u8* u8) { _aligned_free(u8); })), count(0) {}

////////////////////////////////////////////////////////////////////////////////

void Jigsaw::jsEntityCluster::jsEntityClusterNode::Destroy(int index)
{
	jsEntity entity = this->operator[](index);
	u8* entity_address = entity.GetRawData();
	const Jigsaw::sysSignatureMap& map = signature->GetMap();

	u32 i = 0;
	for (const Jigsaw::etype_info& t_info : signature->GetAlignedTypes())
	{
		u8* type_address = map[i++] + entity_address;
		bool was_destructed = t_info.GetUnsafeFunctions().DestructInPlace(type_address);
		J_D_ASSERT_LOG_ERROR((was_destructed), jsEntity, "The Component type {0} of the entity cannot be instantiated--it is either not move assignable or not default destructible. It cannot be a component for a jsEntity", t_info.GetQualifiedName());
	}
}

////////////////////////////////////////////////////////////////////////////////

jsEntity Jigsaw::jsEntityCluster::jsEntityClusterNode::FabricateEntity(jsEntity::SCOPE scope, const UID* id_ovrd)
{
	u8* entity_address = &raw_data[count * signature->GetSize()];
	const Jigsaw::sysSignatureMap& map = signature->GetMap();

	u32 i = 0;
	for (const Jigsaw::etype_info& t_info : signature->GetAlignedTypes())
	{
		u8* type_address = map[i++] + entity_address;
		bool was_constructed = t_info.GetUnsafeFunctions().ConstructInPlace(type_address);
		J_D_ASSERT_LOG_ERROR((was_constructed), jsEntity, "The Component type {0} of the entity cannot be instantiated--it is either not move assignable or not default constructible", t_info.GetQualifiedName());
	}

	cluster_ids[count] = id_ovrd ? *id_ovrd : Jigsaw::UID::Create();
	scopes[count] = scope;
	return jsEntity(entity_address, signature->GetMap(), cluster_ids[count++], scope);
}

////////////////////////////////////////////////////////////////////////////////

jsEntity Jigsaw::jsEntityCluster::jsEntityClusterNode::PopLast()
{
	return (*this)[--count];
}

////////////////////////////////////////////////////////////////////////////////

void Jigsaw::jsEntityCluster::jsEntityClusterNode::ReplaceEmptyIndex(const jsEntity& entity, u32 index)
{
	J_D_ASSERT_LOG_ERROR((index < count), jsEntityClusterNode, "Insert was called with an index beyond the jsEntityClusterNode's capacity");
	const u8* const data = entity.m_rawData;
	memcpy(&raw_data[index * signature->GetSize()], data, signature->GetSize());
	cluster_ids[index] = entity.GetUID();
}

////////////////////////////////////////////////////////////////////////////////

bool Jigsaw::jsEntityCluster::jsEntityClusterNode::Contains(const Jigsaw::UID& id, u32* index)
{
	for (u32 i = 0; i < count; i++)
	{
		if (cluster_ids[i] == id)
		{
			*index = i;
			return true;
		}
	}
	return false;
}

////////////////////////////////////////////////////////////////////////////////

#if JGSW_DEV
void Jigsaw::jsEntityCluster::ConvertEntities(jsEntityCluster& from, jsEntityCluster& to, const UID* entity_ids, size_t num_ids)
{
	using namespace Jigsaw::Util;

	Unique<sysSignatureMap> conversion_map;
	sysSignatureMap::CONVERSION conv_type = sysSignatureMap::GetConversionMap(from.GetSignature(), to.GetSignature(), &conversion_map);

	for (size_t i = 0; i < num_ids; i++)
	{
		const UID& e_id = entity_ids[i];

		// locating the entities
		jsEntity from_entity = from.FetchEntity(e_id);
		jsEntity to_entity = to.FabricateEntity(from_entity.GetScope(), &e_id);
		u8* from_data = from_entity.GetRawData();
		u8* to_data = to_entity.GetRawData();

		const std::vector<etype_index>& types = to.GetSignature().GetAlignedTypes();
		for (u32 j = 0; j < types.size(); j++)
		{
			u32 source_offset = (*conversion_map)[j];
			u32 dest_offset = to.GetSignature().GetMap()[j];

			// Copying the data from one entity to another
			if (source_offset != sysSignatureMap::sm_invalidIndex)
			{
				types[j].Get().GetUnsafeFunctions().Copy(to_data + dest_offset, from_data + source_offset);
			}
		}

		from.RemoveEntity(e_id);
	}
}
#endif //JGSW_DEV

////////////////////////////////////////////////////////////////////////////////

size_t Jigsaw::jsEntityCluster::GetClusterCount() const
{
	return cluster_nodes.size();
}

size_t Jigsaw::jsEntityCluster::GetTotalCount() const
{
	return entity_node_map.size();
}

jsEntity Jigsaw::jsEntityCluster::FetchEntity(const Jigsaw::UID& entity_id)
{
	u32 index = 0;
	Ref<jsEntityClusterNode>& node = entity_node_map[entity_id];
	node->Contains(entity_id, &index);
	return (*node)[index];
}

jsEntity Jigsaw::jsEntityCluster::FabricateEntity(jsEntity::SCOPE scope, const UID* id_ovrd)
{
	if (cluster_nodes.size() == 0 || cluster_nodes.back()->GetCount() >= CLUSTER_SIZE)
	{
		Ref<jsEntityClusterNode> node = MakeRef<jsEntityClusterNode>(signature);
		cluster_nodes.push_back(node);
	}
	jsEntity e = cluster_nodes.back()->FabricateEntity(scope, id_ovrd);
	entity_node_map.insert(std::make_pair(e.GetUID(), cluster_nodes.back()));
	return e;
}

void Jigsaw::jsEntityCluster::RemoveEntity(const Jigsaw::UID& entity_id)
{
	Ref<jsEntityClusterNode>& last_node = cluster_nodes.back();
	auto iter = entity_node_map.find(entity_id);
	if (iter != entity_node_map.end())
	{
		Ref<jsEntityClusterNode>& node = iter->second;
		u32 index = 0;
		if (node->Contains(entity_id, &index))
		{
			node->Destroy(index);
			if (node == last_node && index == node->GetCount() - 1)
			{
				node->PopLast();
			}
			else
			{
				jsEntity entity = last_node->PopLast();
				node->ReplaceEmptyIndex(entity, index);
				entity_node_map[entity.GetUID()] = node;
			}

			if (last_node->GetCount() == 0)
			{
				cluster_nodes.pop_back();
			}
			entity_node_map.erase(entity_id);
		}
	}
}

const Jigsaw::sysSignature& Jigsaw::jsEntityCluster::GetSignature() const
{
	return *signature;
}

Jigsaw::jsEntityCluster::ENTITY_ITERATOR Jigsaw::jsEntityCluster::GetNodeIterator(u32 i)
{
	return ENTITY_ITERATOR(*cluster_nodes[i].get());
}

Jigsaw::jsEntityCluster::ENTITY_ITERATOR::ENTITY_ITERATOR(jsEntityClusterNode& node) : node(node) { }
