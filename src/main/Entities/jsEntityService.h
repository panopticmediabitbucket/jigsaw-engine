/*********************************************************************
 * jsEntityService.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: March 2021
 *********************************************************************/
#ifndef _JIGSAW_ENTITY_CLUSTER_SERVICE_H_
#define _JIGSAW_ENTITY_CLUSTER_SERVICE_H_

 // Jigsaw_Engine
#include "System/sysSignature.h"
#include "jsEntityCluster.h"

// std
#include <map>

namespace Jigsaw
{
	/// <summary>
	/// The JigsawEntityClusterService is the primary interface for retrieving JigsawEntityClusters that are active
	/// in the world. 
	/// </summary>
	class JGSW_API jsEntityService {
	public:
		jsEntityService();

		/// <summary>
		/// Fetches the cluster associated with the sysSignature
		/// </summary>
		/// <param name="signature"></param>
		/// <returns></returns>
		jsEntityCluster& operator[](const Jigsaw::sysSignature& signature);

		/// <summary>
		/// Fetches the cluster associated with the signature_hash. Assumes that the signature exists.
		/// </summary>
		/// <param name="signature"></param>
		/// <returns></returns>
		inline jsEntityCluster& operator[](const Jigsaw::signature_hash& signature) { return *m_signatureClusterMap[signature]; };

		/// <summary>
		/// begin override enables use with foreach semantics
		/// </summary>
		/// <returns></returns>
		inline std::map<Jigsaw::signature_hash, jsEntityCluster*>::const_iterator begin() const { return m_signatureClusterMap.begin(); };

		/// <summary>
		/// end override enables use with foreach semantics
		/// </summary>
		/// <returns></returns>
		inline std::map<Jigsaw::signature_hash, jsEntityCluster*>::const_iterator end() const { return m_signatureClusterMap.end(); };

	private:
		std::map<Jigsaw::signature_hash, jsEntityCluster*> m_signatureClusterMap;

	};
}
#endif