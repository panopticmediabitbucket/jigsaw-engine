#ifndef _J_DEBUG_H_
#define _J_DEBUG_H_
#include "j_log.h"
#include <cassert>

#if JGSW_DEBUG

#define DEBUG_SCOPED(inst) inst

#define J_D_ASSERT_LOG_ERROR(expr, classname, text, ...)\
if (!(expr)) { \
		J_LOG_ERROR(classname, text, __VA_ARGS__); \
		__debugbreak(); \
		assert(false); \
}

#else
#define DEBUG_SCOPED(inst)
#define J_D_ASSERT_LOG_ERROR(expr, classname, text, ...)
#endif
#endif
