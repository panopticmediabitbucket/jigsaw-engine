#ifndef _SCOPED_LOGGING_TIMER_
#define _SCOPED_LOGGING_TIMER_

#include <chrono>
#include <string>
#include "Debug/j_log.h"

namespace Jigsaw {
	namespace Debug {
		template <typename T>
		class ScopedLoggingTimer {
		public:
			ScopedLoggingTimer(const std::string& task_name) : task_name(task_name), _start(std::chrono::high_resolution_clock::now()) {}

			~ScopedLoggingTimer() {
				std::chrono::high_resolution_clock::time_point _end = std::chrono::high_resolution_clock::now();
				std::chrono::duration<float> s_dur = _end - _start;
				J_LOG_INFO(T, "Exit duration of task '{0}': {1:f}", task_name, s_dur.count());
			}

		private:
			std::chrono::high_resolution_clock::time_point _start;
			std::string task_name;

		};
	}
}

#define START_TIMER_SCOPE(_typename, task_name) { Jigsaw::Debug::ScopedLoggingTimer<_typename> _slt(task_name);
#define END_TIMER_SCOPE }
#endif