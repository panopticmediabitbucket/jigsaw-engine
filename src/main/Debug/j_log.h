#ifndef _J_LOG_H_
#define _J_LOG_H_

#ifndef SPDLOG_ACTIVE_LEVEL
#define SPDLOG_ACTIVE_LEVEL SPDLOG_LEVEL_TRACE
#endif

/// Sets the log level if it was not specified in the preprocessor
#ifndef _LOG_LEVEL_
#define _LOG_LEVEL_ spdlog::level::trace
#endif

#include <stdlib.h>
#include "Ref.h"
#include "RTTI/etype_info.h"

#include <spdlog/async_logger.h>
#include <spdlog/logger.h>
#include <spdlog/spdlog.h>
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Debug {
		class JGSW_API JLogger {
		public:
			/// <summary>
			/// Primary logger get call. It returns nothing, but a call is issue always to make sure the logger is initialized
			/// </summary>
			static void GetLogger();

		private:
			/// <summary>
			/// JLogger private constructor used for the logging implementation
			/// </summary>
			JLogger();

			Jigsaw::Ref<spdlog::async_logger> logger; 

		};
	}
}

#ifdef __JIGSAW_LOG_ENABLED
#define LOG_INFO(info) std::cout << info << std::endl;
#define LOG_ERR(err) std::cout << info << std::endl;
#else
#define LOG_INFO(info) 
#define LOG_ERR(err) 
#endif
#endif

#define _EXPAND(...) __VA_ARGS__
#define _VA_EXP(...) __VA_ARGS__
#define _VA_REM(...)
#define __PRE_STR_GET(format_string, ...) format_string 
#define __PST_STR_GET(message, format_string, ...) message, __VA_ARGS__
#define __LOG_EXPANDER(classname, ...) { \
std::string __log_msg = Jigsaw::etype_info::Id<classname>().GetQualifiedName() + ": " + _EXPAND(__PRE_STR_GET(__VA_ARGS__))

#define J_LOG_DEBUG(classname, ...) \
Jigsaw::Debug::JLogger::GetLogger();\
SPDLOG_DEBUG(__VA_ARGS__);

#define J_LOG_ERROR(classname, ...) \
Jigsaw::Debug::JLogger::GetLogger();\
SPDLOG_ERROR(__VA_ARGS__);

#define J_LOG_TRACE(classname, ...) \
Jigsaw::Debug::JLogger::GetLogger();\
SPDLOG_TRACE(__VA_ARGS__);

#define J_LOG_WARN(classname, ...) \
Jigsaw::Debug::JLogger::GetLogger();\
SPDLOG_WARN(__VA_ARGS__);

#define J_LOG_INFO(classname, ...) \
Jigsaw::Debug::JLogger::GetLogger(); \
SPDLOG_INFO(__VA_ARGS__);

