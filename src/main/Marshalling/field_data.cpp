#include "Marshalling/field_data.h"

namespace Jigsaw {
	namespace Marshalling {

		field_data::field_data(std::string&& name, const Jigsaw::etype_info& type, bool is_array, int array_size, bool is_reference)
			: name(name), field_type(type), is_array(is_array), array_size(array_size), is_reference(is_reference), is_pointer(false) { }

		field_data::field_data(const field_data& other) : name(other.name), field_type(other.field_type), getter(other.getter), setter(other.setter),
			is_array(other.is_array), array_size(other.array_size), is_reference(other.is_reference), is_pointer(other.is_pointer) { }

		bool field_data::IsBoundedArray(size_t* bound_size) const {
			if (is_array && array_size > 0) {
				*bound_size = array_size;
				return true;
			}
			return false;
		}

		bool field_data::IsArray() const {
			return is_array;
		}

		const Jigsaw::etype_info& field_data::GetType() const {
			return field_type;
		}

	};
}
