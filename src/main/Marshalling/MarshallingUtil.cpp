#include "MarshallingUtil.h"
#include "Assembly/JGSW_ASSEMBLY.h"

JGSW_API const Jigsaw::Marshalling::MarshallingMap& Jigsaw::Marshalling::E_GetMarshallingMap(const Jigsaw::etype_info& type_info) {
	return Jigsaw::Assembly::MarshallingRegistry::GetMarshallingMap(type_info);
}
