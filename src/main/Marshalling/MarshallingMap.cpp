#include "Marshalling/MarshallingMap.h"
#include "Assembly/JGSW_ASSEMBLY.h"
#include "RTTI/etype_info.h"
#include <algorithm>

namespace Jigsaw {
	namespace Marshalling {
		MarshallingMap::MarshallingMap(const Jigsaw::etype_info& type, std::function<void* ()> manufacturer) : type(&type), manufacturer(manufacturer), field_map() {}

		MarshallingMap::MarshallingMap(const Jigsaw::etype_info& type, std::function<void* (void*)> context_root_manufacturer)
			: type(&type), manufacturer(), context_root_manufacturer(context_root_manufacturer), field_map() {}

		MarshallingMap::MarshallingMap(const MarshallingMap& other) : type(other.type), field_map(other.field_map), manufacturer(other.manufacturer),
			context_root_manufacturer(other.context_root_manufacturer), parent_classes(other.parent_classes) {}

		MarshallingMap& MarshallingMap::operator=(const MarshallingMap& other)
		{
			type = other.type;
			parent_classes = other.parent_classes;
			field_map = other.field_map;
			manufacturer = other.manufacturer;
			context_root_manufacturer = other.context_root_manufacturer;
			return *this;
		}

		void MarshallingMap::RegisterFieldData(const std::string&& name, const field_data* f_data) {
			field_map.insert(std::make_pair(name, f_data));
		}

		void MarshallingMap::RegisterParentClass(const Jigsaw::etype_info& parent) {
			Jigsaw::etype_index index(parent);
			auto found = std::find(parent_classes.begin(), parent_classes.end(), index);
			if (found == parent_classes.end()) {
				parent_classes.push_back(index);
			}
		}

		const field_data* MarshallingMap::GetFieldDataByName(const std::string&& name) const {
			const field_data* f_data = nullptr;
			GetFieldDataByName(name, &f_data);
			return f_data;
		}

		const std::vector<const field_data*> Jigsaw::Marshalling::MarshallingMap::GetFields() const {
			std::vector<const field_data*> ret_val;
			GetFields(ret_val);
			std::unique(ret_val.begin(), ret_val.end(), [](const field_data* a, const field_data* b) { return a->name == b->name; });
			return ret_val;
		}

		void* MarshallingMap::Manufacture(void* arg) const {
			if (context_root_manufacturer && arg) {
				return context_root_manufacturer(arg);
			}
			else {
				return manufacturer();
			}
		}

		const Jigsaw::etype_info& MarshallingMap::GetType() const {
			return *type;
		}

		const bool MarshallingMap::InheritsFrom(const Jigsaw::etype_info& parent) const {
			bool found = parent == GetType();
			auto iterator = parent_classes.begin();

			while (iterator != parent_classes.end() && !found) {
				found |= *iterator == parent || Jigsaw::Assembly::MarshallingRegistry::GetMarshallingMap(*iterator).InheritsFrom(parent);
				iterator++;
			}

			return found;
		}

		const bool Jigsaw::Marshalling::MarshallingMap::IsContextInitialized() const {
			return (bool)context_root_manufacturer;
		}

		void Jigsaw::Marshalling::MarshallingMap::GetFields(std::vector<const field_data*>& return_vector) const {
			for (auto field_data_pair : field_map) {
				const field_data* f_data = field_data_pair.second;
				return_vector.push_back(f_data);
			}

			for (auto parent_type : parent_classes) {
				Jigsaw::Assembly::MarshallingRegistry::GetMarshallingMap(parent_type).GetFields(return_vector);
			}
		}

		const void MarshallingMap::GetFieldDataByName(const std::string& name, const field_data** f_data) const {
			auto iter = field_map.find(name);
			if (iter == field_map.end()) {
				for (const Jigsaw::etype_info& parent : parent_classes) {
					const MarshallingMap& map = Jigsaw::Assembly::MarshallingRegistry::GetMarshallingMap(parent);
					map.GetFieldDataByName(name, f_data);
					if (*f_data != nullptr) {
						return;
					}
				}
			}
			else {
				*f_data = (*iter).second;
			}
		}

	}
}
