#ifndef _UNMARSHALLING_RESULTS_H_
#define _UNMARSHALLING_RESULTS_H_

#include <vector>
#include "Assets/ASSET_LOAD_RESULT.h"

namespace Jigsaw {
	namespace Marshalling {
		/// <summary>
		/// Basic struct for describing the results of an Unmarshalling operation
		/// </summary>
		struct UNMARSHALLING_RESULT {
			void* raw_data = nullptr;
			Jigsaw::RESULT result = Jigsaw::RESULT::INCOMPLETE;
			Jigsaw::RES_FLAGS_ flags = Jigsaw::RES_FLAGS_NONE;
			std::vector<Jigsaw::UNRESOLVED_REFERENCE> unresolved_references;
		};

		/// <summary>
		/// Basic struct for describing the results of an marshalling operation
		/// </summary>
		struct MARSHALLING_RESULT {
			std::string marshalled_object;
			Jigsaw::RESULT result = Jigsaw::RESULT::INCOMPLETE;
		};
	}
}
#endif
