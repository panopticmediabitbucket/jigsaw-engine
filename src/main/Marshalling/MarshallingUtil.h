/*********************************************************************
 * MarshallingUtil.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: December 2020
 *********************************************************************/

#ifndef _MARSHALLING_UTIL_H_
#define _MARSHALLING_UTIL_H_

// Jigsaw_Engine
#include "File_IO/FileUtil.h"
#include "JSONNode.h"

namespace Jigsaw {
	namespace Marshalling {
		JGSW_API const MarshallingMap& E_GetMarshallingMap(const Jigsaw::etype_info& type_info);

		class MarshallingUtil 
		{
		public:

			//////////////////////////////////////////////////////////////////////////

			template<typename T>
			static Jigsaw::Ref<T> Unmarshal(Jigsaw::FILE_DATA& file_data) 
			{
				const Jigsaw::etype_info& type_id = Jigsaw::etype_info::Id<T>();
				std::istringstream json_stream(FileUtil::GetStringStreamFromFile(file_data));

				Jigsaw::Marshalling::JSONNodeReader root(json_stream, E_GetMarshallingMap(type_id));
				UNMARSHALLING_RESULT result = root.BuildNode();
				return result.result != RESULT::FAILURE ? Jigsaw::Ref<T>(static_cast<T*>(result.raw_data)) : Jigsaw::Ref<T>();
			};

			//////////////////////////////////////////////////////////////////////////

			template<typename T>
			static Jigsaw::Ref<T> Unmarshal(const std::string& str) 
			{
				const Jigsaw::etype_info& type_id = Jigsaw::etype_info::Id<T>();
				std::istringstream json_stream(str);

				Jigsaw::Marshalling::JSONNodeReader root(json_stream, E_GetMarshallingMap(type_id));
				UNMARSHALLING_RESULT result = root.BuildNode();
				return result.result != RESULT::FAILURE ? Jigsaw::Ref<T>(static_cast<T*>(result.raw_data)) : Jigsaw::Ref<T>();
			};

			//////////////////////////////////////////////////////////////////////////

			template<typename T>
			static std::string Marshal(T* raw_object) 
			{
				const Jigsaw::etype_info& type_id = Jigsaw::etype_info::Id<T>();

				Jigsaw::Marshalling::JSONNodeWriter root(static_cast<void*>(raw_object), E_GetMarshallingMap(type_id));
				MARSHALLING_RESULT res = root.ReadNode();

				return res.marshalled_object;
			}

			//////////////////////////////////////////////////////////////////////////
		};
	}
}
#endif
