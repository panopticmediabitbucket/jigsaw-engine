/*********************************************************************
 * ViewportWindow.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: April 2021
 *********************************************************************/
#ifndef _VIEWPORT_WINDOW_H_
#define _VIEWPORT_WINDOW_H_

#include "window.h"

// Jigsaw_Engine
#include "Ref.h"
#include "Graphics/GraphicsContext.h"
#include "Orchestration/orOrchestrator.h"
#include "System/sysDelegate.h"

#define KEYDOWN_TRANSITION_FLAG 0b1 << 30 

/**
 * This class exists to encapsulate low-level data for the window and program context. 
*/
class JGSW_API ViewportWindow : public Window 
{
public:
	// Constructors
	ViewportWindow(float wh_ratio, Jigsaw::GraphicsContext* render_context, Jigsaw::orOrchestrator* orchestrator);

	ViewportWindow(const ViewportWindow& other, float wh_ratio, Jigsaw::GraphicsContext* render_context);

	float GetWHRatio() const;

	void PopulateWindowClass(WNDCLASS *wndClass, HINSTANCE hInstance);

	void SetSizeChangeEventHandler(Jigsaw::sysDelegate<u32, u32>* windowSizeChangeHandler);

protected:
	virtual LRESULT Awake(const HWND hWnd) noexcept(false) override;
	virtual LRESULT Resize(unsigned long width, unsigned long height, HWND hWnd) override;
	virtual LRESULT TerminateOrchestrator(HWND hWnd) override;
	virtual LRESULT KeyboardInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) override;
	virtual LRESULT MouseInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) override;

	Jigsaw::orOrchestrator* orchestrator;
	Jigsaw::GraphicsContext* m_graphicsContext;
	
private:
	// private variables
	float wh_ratio;

	Jigsaw::sysDelegate<u32, u32>* m_windowSizeChangeHandler = nullptr;

	// Inherited via Window
};
#endif