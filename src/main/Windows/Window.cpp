#include "window.h"

LRESULT Window::Terminate(HWND hWnd, HRESULT error) {
	char class_name[256];
	GetClassName(hWnd, class_name, 255);
	HINSTANCE main_programInstance = (HINSTANCE)GetModuleHandle(NULL);

	DestroyWindow(hWnd);
	WNDCLASS wndClass;

	GetClassInfo(main_programInstance, class_name, &wndClass);
	if (hWnd) {
		UnregisterClass(wndClass.lpszClassName, main_programInstance);
	}
	return error;
}

/**
* Signal handler for generic Window class
*/
LRESULT Window::HandleMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	switch (uMsg) {
	case WM_SHOWWINDOW:
		try {
			return Awake(hWnd);
		}
		catch (HRESULT e) {
			return Terminate(hWnd, e);
		}
	case WM_SIZE:
		return Resize(LOWORD(lParam), HIWORD(lParam), hWnd);
	case WM_CLOSE:
		TerminateOrchestrator(hWnd);
		return Terminate(hWnd, 0);
	case WM_KEYDOWN:
	case WM_KEYUP:
		return KeyboardInput(hWnd, uMsg, wParam, lParam);
    case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
    case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
    case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
    case WM_XBUTTONDOWN: case WM_XBUTTONDBLCLK:
    case WM_LBUTTONUP:
    case WM_RBUTTONUP:
    case WM_MBUTTONUP:
    case WM_XBUTTONUP:
	case WM_MOUSEWHEEL:
    case WM_MOUSEMOVE:
    //case WM_SETCURSOR:
		return MouseInput(hWnd, uMsg, wParam, lParam);
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(hWnd, uMsg, wParam, lParam);
}
