#include "ViewportWindow.h"
#include <cstdlib>
#include "Debug/j_debug.h"
#include <chrono>

#include "windowsx.h"

using namespace Microsoft::WRL;

//////////////////////////////////////////////////////////////////////////

ViewportWindow::ViewportWindow(float wh_ratio, Jigsaw::GraphicsContext* graphics_context, Jigsaw::orOrchestrator* orchestrator)
	: wh_ratio(wh_ratio), m_graphicsContext(graphics_context), orchestrator(orchestrator) { }

//////////////////////////////////////////////////////////////////////////

ViewportWindow::ViewportWindow(const ViewportWindow& other, float wh_ratio, Jigsaw::GraphicsContext* graphics_context)
	: wh_ratio(wh_ratio), m_graphicsContext(graphics_context) { }

//////////////////////////////////////////////////////////////////////////

float ViewportWindow::GetWHRatio() const
{
	return wh_ratio;
};

//////////////////////////////////////////////////////////////////////////

/**
 * Fills the main window class with any default details. This function is isolated to eventually support loading and other features.
 */
void ViewportWindow::PopulateWindowClass(WNDCLASS* wndClass, HINSTANCE hInstance) 
{
	wndClass->style = CS_DBLCLKS;
	wndClass->lpszMenuName = NULL;
	wndClass->lpszClassName = "Viewport_window_class";
	wndClass->hInstance = hInstance;
	wndClass->hCursor = LoadCursor(NULL, IDC_ARROW);
	wndClass->hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
}

//////////////////////////////////////////////////////////////////////////

void ViewportWindow::SetSizeChangeEventHandler(Jigsaw::sysDelegate<u32, u32>* windowSizeChangeHandler)
{
	m_windowSizeChangeHandler = windowSizeChangeHandler;
}

//////////////////////////////////////////////////////////////////////////

LRESULT ViewportWindow::Awake(const HWND hWnd)
{
	m_graphicsContext->CreateWindowSwapchain(hWnd, 0, 0);
	orchestrator->StartApplication();
	return 0;
}

//////////////////////////////////////////////////////////////////////////

LRESULT ViewportWindow::Resize(unsigned long width, unsigned long height, HWND hWnd)
{
	static UINT size_change_event_index = 0;
	std::string resize_name = "WINDOW_SIZE_CHANGE_" + size_change_event_index++;
	Jigsaw::Ref<Jigsaw::ASYNC_EVENT> block_event = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>(resize_name.c_str());
	Jigsaw::Ref<Jigsaw::EVENT_LISTENER> block_listener = Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(block_event);
	Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> message_received = orchestrator->GetMessageQueue()->BlockUntil(block_listener);

	J_LOG_DEBUG(ViewportWindow, "Waiting for the orchestrator to block so that we can resize the window");
	message_received->Await();
	m_graphicsContext->CreateWindowSwapchain(hWnd, width, height);
	if (m_windowSizeChangeHandler) m_windowSizeChangeHandler->Notify((u32)width, (u32)height);
	block_event->Notify();
	return 0;
}

//////////////////////////////////////////////////////////////////////////

LRESULT ViewportWindow::TerminateOrchestrator(HWND hWnd) 
{
	orchestrator->EndApplication();
	return 0;
}

//////////////////////////////////////////////////////////////////////////

LRESULT ViewportWindow::KeyboardInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{
	auto message_queue = orchestrator->GetMessageQueue();

	Jigsaw::QWORD word;
	Jigsaw::QINPUT& input = reinterpret_cast<Jigsaw::QINPUT&>(word);
	switch (uMsg)
	{
	case WM_KEYDOWN:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QKEY_DOWN);
		if ((lParam & KEYDOWN_TRANSITION_FLAG))
		{
			J_LOG_INFO(ViewportWindow, "Non transition");
			return 0;
		}
		break;
	case WM_KEYUP:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QKEY_UP);
		break;
	}

	J_LOG_INFO(ViewportWindow, "Input event detected with value {0}", wParam);
	input.SetInputChar((short)wParam);
	message_queue->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, word);
	return 0;
}

//////////////////////////////////////////////////////////////////////////

LRESULT ViewportWindow::MouseInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) 
{
	auto message_queue = orchestrator->GetMessageQueue();

	Jigsaw::QWORD word;
	Jigsaw::QINPUT& input = reinterpret_cast<Jigsaw::QINPUT&>(word);
	input.SetProcedureId((u64)hWnd);
	switch (uMsg)
	{
    case WM_LBUTTONDOWN: case WM_LBUTTONDBLCLK:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QLEFT_MOUSE_DOWN);
		break;
    case WM_RBUTTONDOWN: case WM_RBUTTONDBLCLK:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QRIGHT_MOUSE_DOWN);
		break;
    case WM_MBUTTONDOWN: case WM_MBUTTONDBLCLK:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QMIDDLE_MOUSE_DOWN);
		break;
    case WM_XBUTTONDOWN: case WM_XBUTTONDBLCLK:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QX_MOUSE_DOWN);
		input.SetLongParam(wParam);
		break;
    case WM_LBUTTONUP:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QLEFT_MOUSE_UP);
		break;
    case WM_RBUTTONUP:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QRIGHT_MOUSE_UP);
		break;
    case WM_MBUTTONUP:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QMIDDLE_MOUSE_UP);
		break;
    case WM_XBUTTONUP:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QX_MOUSE_UP);
		input.SetLongParam(wParam);
		break;
    case WM_MOUSEWHEEL:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QMOUSE_WHEEL);
		input.SetFloatParam((float)GET_WHEEL_DELTA_WPARAM(wParam) / (float)WHEEL_DELTA);
		break;
    case WM_MOUSEMOVE:
		input.SetInputType(Jigsaw::QINPUT_TYPE::QMOUSE_POSITION);
		input.SetFloatParam((float)GET_X_LPARAM(lParam), 0);
		input.SetFloatParam((float)GET_Y_LPARAM(lParam), 1);
		break;
	}

	input.SetInputChar((short)wParam);
	message_queue->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT, word);
	return 0;
}

//////////////////////////////////////////////////////////////////////////

