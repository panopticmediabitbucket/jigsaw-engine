#pragma once
#include <windows.h>
#include <stdio.h>
#include <WinUser.h>
#include <iostream>
#include "window.h"
#include "_jgsw_api.h"

class JGSW_API Window {
public:
	virtual LRESULT HandleMessage(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
	virtual void PopulateWindowClass(WNDCLASS *wndClass, HINSTANCE hInstance) = 0;

protected:
	/**
	* Abstract method--the routine for initial creation will be different depending on the type of window in question.
	* Event handlers may need to be created, 
	*/
	virtual LRESULT Awake(const HWND hWnd) noexcept(false) = 0;

	virtual LRESULT Resize(unsigned long width, unsigned long height, HWND hWnd) = 0;

	virtual LRESULT KeyboardInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

	virtual LRESULT MouseInput(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam) = 0;

	virtual LRESULT TerminateOrchestrator(HWND hWnd) = 0;

	LRESULT Terminate(HWND window, HRESULT error);
};
