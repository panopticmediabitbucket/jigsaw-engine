#ifndef _TIME_STEP_H_
#define _TIME_STEP_H_

#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Time {
		struct JGSW_API TimeStep {
			float s;
		};
	}
}
#endif