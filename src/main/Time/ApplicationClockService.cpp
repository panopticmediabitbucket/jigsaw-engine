#include "ApplicationClockService.h"

namespace Jigsaw {
	namespace Time {

		ApplicationClockReader::ApplicationClockReader() : delta_time(0.0f) {}

		// ApplicationClockService implementations

		Jigsaw::Time::ApplicationClockService::ApplicationClockService() : previous_time(std::chrono::high_resolution_clock::now()) {}

		void ApplicationClockService::Update() {
			std::chrono::high_resolution_clock::time_point now = std::chrono::high_resolution_clock::now();
			std::chrono::duration<float> dur = now - previous_time;
			delta_time = dur.count();
			previous_time = now;
		}
	}
}

