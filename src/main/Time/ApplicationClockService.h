#ifndef _APPLICATION_CLOCK_SERVICE_H_
#define _APPLICATION_CLOCK_SERVICE_H_

#include <chrono>
#include "_jgsw_api.h"

namespace Jigsaw {
	namespace Time {
		/// <summary>
		/// The ApplicationClockReader can be injected into any JigsawSlots class using the 
		/// SLOT macro. It provides a window into the time elapsed since the last frame rendered.
		/// </summary>
		class JGSW_API ApplicationClockReader {
		public:
			ApplicationClockReader();

			/// <summary>
			/// Returns the time in seconds since the last frame rendered
			/// </summary>
			/// <returns></returns>
			inline float DeltaTime() const {
				return delta_time;
			}

		protected:
			float delta_time;

		};

		/// <summary>
		/// The ApplicationClockService is not a publicly-visible class. It is used only by the ApplicationOrchestrator.
		/// </summary>
		class INTERNAL ApplicationClockService : public ApplicationClockReader {
		public:
			ApplicationClockService();

			/// <summary>
			/// Updates the internal clock with the difference between the previous and the current time
			/// </summary>
			void Update();

		private:
			std::chrono::high_resolution_clock::time_point previous_time;

		};
	}
}
#endif
