#include "SkeletalAnimationSystem.h"

namespace Jigsaw
{
	namespace Animation
	{
		using namespace Jigsaw::Render;

		//////////////////////////////////////////////////////////////////////////

		void SkeletalAnimationSystem::Skin(MeshAnimationComponent& component, const MeshRendererComponent& render_component)
		{


			component.m_sampler.Advance(s_clockReader->DeltaTime());
			Ref<jSkeleton> sample_skel = component.m_skeleton->Clone();
			component.m_sampler.Sample(*sample_skel);
			sample_skel->ConvertToObjectSpace();

			// if we haven't allocated a skeleton buffer yet, we need to do that
			if (!component.m_skelBuffer) 
			{
				JGSW_GPU_RESOURCE_DATA resource_args;
				resource_args.flags |= JGSW_GPU_RESOURCE_FLAG_UPDATABLE;
				resource_args.visibility |= (u32)JGSW_SHADER::VERTEX;
				resource_args.resource_type = JGSW_GPU_RESOURCE_TYPE::BUFFER;
				resource_args.width = sizeof(jBone) * component.m_skeleton->GetDeformerCount();

				component.m_skelBuffer = s_renderContext->CreateGPUResource(resource_args);
			}

			Ref<RenderJob> job = s_renderService->GetJob("frame_job");
			UPDATE_DATA<jBone> bone_update;
			bone_update.size = component.m_skeleton->GetDeformerCount();
			bone_update.data = &component.m_skeleton->GetBone(0);

			job->UpdateResource<jBone>(*component.m_skelBuffer, bone_update);

			VERTEX_INDEXED_DATA indexed_data;
			indexed_data.vertices_resource = render_component.vertices;
			indexed_data.indices_resource = render_component.indices;
			indexed_data.buffer_refs[0] = component.m_skelBuffer;

			job->DrawIndexed(Pipeline::JIGSAW_PIPELINE_SKINNED_MESH, indexed_data);
		}

		//////////////////////////////////////////////////////////////////////////

	}
}