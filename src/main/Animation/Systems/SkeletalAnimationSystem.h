#ifndef _SKELETAL_ANIMATION_SYSTEM_H_
#define _SKELETAL_ANIMATION_SYSTEM_H_

// Jigsaw Engine
#include "Animation/Components/MeshAnimationComponent.h"
#include "Graphics/GraphicsContext.h"
#include "Injection/JigsawInjection.h"
#include "Render/RenderService.h"
#include "Render/Machine/MeshRendererMachinePiece.h"
#include "Systems/jsSystem.h"
#include "Time/ApplicationClockService.h"

namespace Jigsaw {
	namespace Animation {

		JGSW_SYSTEM(SkeletalAnimationSystem) {
		public:

			JGSW_SYSTEM_METHOD(Skin, MeshAnimationComponent, const Render::MeshRendererComponent);

		private:
			SLOT_1(Jigsaw::Time::ApplicationClockReader, s_clockReader);
			SLOT_2(Jigsaw::GraphicsContext, s_renderContext);
			SLOT_3(Jigsaw::RenderService, s_renderService);

		};
	}
}

#endif