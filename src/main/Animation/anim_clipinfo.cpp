#include "anim_clipinfo.h"

namespace Jigsaw {
	namespace Animation {

		//////////////////////////////////////////////////////////////////////////

		/// CHANNEL_TYPE_INFO Implementations and registry

		//////////////////////////////////////////////////////////////////////////

		size_t CHANNEL_TYPE_INFO::GetChannelValueSize(CHANNEL_TYPE channel) {
			return sm_channelValueSizes[(u32)channel];
		}

		//////////////////////////////////////////////////////////////////////////

		size_t CHANNEL_TYPE_INFO::GetChannelAlign(CHANNEL_TYPE channel) {
			return sm_channelValueAlignments[(u32)channel];
		}

		//////////////////////////////////////////////////////////////////////////

		size_t CHANNEL_TYPE_INFO::sm_channelValueSizes[(u32)CHANNEL_TYPE::__NUM_CHANNELS]{};
		size_t CHANNEL_TYPE_INFO::sm_channelValueAlignments[(u32)CHANNEL_TYPE::__NUM_CHANNELS]{};

		REGISTER_CHANNEL_VALUE_TYPE(LCLROTATION_QUAT);

		// Vector3-valued CHANNEL_TYPEs
		REGISTER_CHANNEL_VALUE_TYPE(LCLROTATION_XYZ);
		REGISTER_CHANNEL_VALUE_TYPE(LCLPOSITION_XYZ);
		REGISTER_CHANNEL_VALUE_TYPE(LCLSCALE_XYZ);

		// float-valued CHANNEL_TYPEs
		REGISTER_CHANNEL_VALUE_TYPE(LCLEULER_X);
		REGISTER_CHANNEL_VALUE_TYPE(LCLEULER_Y);
		REGISTER_CHANNEL_VALUE_TYPE(LCLEULER_Z);
		REGISTER_CHANNEL_VALUE_TYPE(GBLEULER_X);
		REGISTER_CHANNEL_VALUE_TYPE(GBLEULER_Y);
		REGISTER_CHANNEL_VALUE_TYPE(GBLEULER_Z);
		REGISTER_CHANNEL_VALUE_TYPE(LCLSCALE_X);
		REGISTER_CHANNEL_VALUE_TYPE(LCLSCALE_Y);
		REGISTER_CHANNEL_VALUE_TYPE(LCLSCALE_Z);
		REGISTER_CHANNEL_VALUE_TYPE(LCLTRANSLATION_X);
		REGISTER_CHANNEL_VALUE_TYPE(LCLTRANSLATION_Y);
		REGISTER_CHANNEL_VALUE_TYPE(LCLTRANSLATION_Z);

	}
}