#ifndef _MESH_ANIMATION_COMPONENT_H_
#define _MESH_ANIMATION_COMPONENT_H_

#include "Animation/Clip.h"
#include "Animation/Sample/AnimClipSampler.h"
#include "Graphics/GPU_Objects/GPUResource.h"
#include "Ref.h"

namespace Jigsaw {
	namespace Animation {
		struct MeshAnimationComponent {
			AnimClipSampler m_sampler;
			Ref<AnimClip> m_clip;
			Ref<jSkeleton> m_skeleton;
			GPUResource* m_skelBuffer = nullptr;
		};
	}
}
#endif // _MESH_ANIMATION_COMPONENT_H_