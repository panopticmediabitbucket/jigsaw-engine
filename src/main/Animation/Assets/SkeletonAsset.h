/*********************************************************************
 * SkeletonAsset.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _SKELETON_ASSET_H_
#define _SKELETON_ASSET_H_

#include "Assets/DataAssets.h"
#include "Animation/Skeleton.h"

namespace Jigsaw 
{
	/// <summary>
	/// SkeletonAsset holds reference to a jsSkeleton that can be used at runtime. 
	/// </summary>
	class JGSW_API SkeletonAsset final : public DataAsset
	{
	public:
		SkeletonAsset(const AssetDescriptor& descriptor);

		DECLARE_DATA_ASSET(SkeletonAsset);

		~SkeletonAsset();

		// <returns>The underlying skeleton</returns>
		inline const jSkeleton& GetSkeleton() const { return *m_skeleton; }

#if JGSW_DEV
		// <summary>A packaging function that takes the provided skeleton and converts it to a packaged that can be saved in the filesystem</summary>
		static ASSET_PACKAGE_RESULT Package(jSkeleton* skel);
#endif

	protected:
		/// <summary>
		/// Main load function for use with the dlAssetRegistrar. Loads the jSkeleton from a file. 
		/// </summary>
		/// <param name="file_data"></param>
		/// <param name="sys_resources"></param>
		/// <returns></returns>
		ASSET_LOAD_RESULT Load(const FILE_DATA& file_data, THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) override;

		jSkeleton* m_skeleton;

	};
}

#endif // _SKELETON_ASSET_H_