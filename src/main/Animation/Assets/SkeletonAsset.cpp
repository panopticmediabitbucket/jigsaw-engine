#include "SkeletonAsset.h"

using namespace Jigsaw::Assets;

namespace Jigsaw 
{

//////////////////////////////////////////////////////////////////////////

// SkeletonAsset Implementations

//////////////////////////////////////////////////////////////////////////

IMPL_DATA_ASSET(SkeletonAsset);

//////////////////////////////////////////////////////////////////////////

Jigsaw::SkeletonAsset::SkeletonAsset(const AssetDescriptor& descriptor) : DataAsset(descriptor), m_skeleton(nullptr) { }

//////////////////////////////////////////////////////////////////////////

SkeletonAsset::~SkeletonAsset()
{
	_aligned_free(m_skeleton);
}

//////////////////////////////////////////////////////////////////////////

#if JGSW_DEV

Jigsaw::ASSET_PACKAGE_RESULT SkeletonAsset::Package(jSkeleton* skel)
{
	jSkeleton* new_skel = skel->CloneRaw();
	new_skel->NormalizePointers((Util::ptr)skel);

	ASSET_PACKAGE_RESULT result;
	result.m_begin = reinterpret_cast<u8*>(new_skel);
	result.m_byteCount = new_skel->m_header.sizeInBytes;

	result.m_shouldFreeMemory = true;
	result.result = RESULT::COMPLETE;

	return result;
}

#endif // JGSW_DEV

//////////////////////////////////////////////////////////////////////////

Jigsaw::ASSET_LOAD_RESULT SkeletonAsset::Load(const FILE_DATA& file_data, THREAD_LOCAL_SYSTEM_RESOURCES sys_resources)
{
	ASSET_LOAD_RESULT result;
	u8* skel_buff = reinterpret_cast<u8*>(_aligned_malloc(file_data.file_size, 0x10));
	assert(skel_buff);
	HRESULT res = fread_s(skel_buff, file_data.file_size, file_data.file_size, 1, file_data.file);
	if (res == 0)
	{
		result.result = RESULT::FAILURE;
		return result;
	}

	// resolving the internal pointers of the skeleton
	m_skeleton = reinterpret_cast<jSkeleton*>(skel_buff);
	m_skeleton->ResolvePointers((Util::ptr)skel_buff);

	result.result = RESULT::COMPLETE;

	return result;
}

//////////////////////////////////////////////////////////////////////////

}

