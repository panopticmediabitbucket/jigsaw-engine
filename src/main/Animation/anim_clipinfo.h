#ifndef _ANIM_CLIPINFO_H_
#define _ANIM_CLIPINFO_H_

#include "Compression/anim_compressed_types.h"
#include "System/sysHashString.h"
#include "Util/DTO.h"
#include "Math/LinAlg.h"

namespace Jigsaw {
	namespace Animation {

		enum class TRACK_TYPE : s8 {
			INVALID = -1,		// Unusable track. Returned in cases where no actual track exists
			BONE,				// Track for a bone. 
		};

		enum class CHANNEL_TYPE : u8 {
			LCLROTATION_QUAT,

			__START_XYZ_CHANNELS = 0x10,
			LCLROTATION_XYZ = __START_XYZ_CHANNELS,
			LCLPOSITION_XYZ,
			LCLSCALE_XYZ,
			__END_XYZ_CHANNELS = 0x20,

			__START_LCLEULER_CHANNELS = 0x20,
			LCLEULER_X = __START_LCLEULER_CHANNELS,
			LCLEULER_Y,
			LCLEULER_Z,
			__END_LCLEULER_CHANNELS,

			__START_GBLEULER_CHANNELS = __END_LCLEULER_CHANNELS,
			GBLEULER_X = __START_GBLEULER_CHANNELS,
			GBLEULER_Y,
			GBLEULER_Z,
			__END_GBLEULER_CHANNELS,

			__START_LCLTRANSLATION_CHANNELS = __END_GBLEULER_CHANNELS,
			LCLTRANSLATION_X = __START_LCLTRANSLATION_CHANNELS,
			LCLTRANSLATION_Y,
			LCLTRANSLATION_Z,
			__END_LCLTRANSLATION_CHANNELS,

			__START_LCLSCALE_CHANNELS = __END_LCLTRANSLATION_CHANNELS,
			LCLSCALE_X = __START_LCLSCALE_CHANNELS,
			LCLSCALE_Y,
			LCLSCALE_Z,
			__END_LCLSCALE_CHANNELS,

			__NUM_CHANNELS,
		};

		class JGSW_API CHANNEL_TYPE_INFO {
		public:
			static size_t GetChannelValueSize(CHANNEL_TYPE channel);
			static size_t GetChannelAlign(CHANNEL_TYPE channel);

			static size_t sm_channelValueSizes[(u32)CHANNEL_TYPE::__NUM_CHANNELS];
			static size_t sm_channelValueAlignments[(u32)CHANNEL_TYPE::__NUM_CHANNELS];
		};

		/// <summary>
		/// Default template is invalid
		/// </summary>
		template <CHANNEL_TYPE CH>
		struct channel_value { };

		/// Specialization macros clarify the stored value type

#define CHANNEL_VALUE_TYPE(channel_type, value_type) \
template <> struct channel_value<CHANNEL_TYPE::channel_type> { \
	static void ReportProperties() { \
		const_cast<size_t*>(CHANNEL_TYPE_INFO::sm_channelValueSizes)[(u32)CHANNEL_TYPE::channel_type] = sizeof(value_type); \
		const_cast<size_t*>(CHANNEL_TYPE_INFO::sm_channelValueAlignments)[(u32)CHANNEL_TYPE::channel_type] = alignof(value_type); \
		static_assert(alignof(value_type) <= 0x10); \
	}; \
	typedef value_type type; \
}; 

#define REGISTER_CHANNEL_VALUE_TYPE(channel_type) \
static const AutoRegister channel_type##_size_register(channel_value<CHANNEL_TYPE::channel_type>::ReportProperties)

		// Quaternion-valued CHANNEL_TYPES
		CHANNEL_VALUE_TYPE(LCLROTATION_QUAT, Quat3Float);

		// Vector3-valued CHANNEL_TYPEs
		CHANNEL_VALUE_TYPE(LCLROTATION_XYZ, Vector3);
		CHANNEL_VALUE_TYPE(LCLPOSITION_XYZ, Vector3);
		CHANNEL_VALUE_TYPE(LCLSCALE_XYZ, Vector3);

		// float-valued CHANNEL_TYPEs
		CHANNEL_VALUE_TYPE(LCLEULER_X, float);
		CHANNEL_VALUE_TYPE(LCLEULER_Y, float);
		CHANNEL_VALUE_TYPE(LCLEULER_Z, float);
		CHANNEL_VALUE_TYPE(GBLEULER_X, float);
		CHANNEL_VALUE_TYPE(GBLEULER_Y, float);
		CHANNEL_VALUE_TYPE(GBLEULER_Z, float);
		CHANNEL_VALUE_TYPE(LCLSCALE_X, float);
		CHANNEL_VALUE_TYPE(LCLSCALE_Y, float);
		CHANNEL_VALUE_TYPE(LCLSCALE_Z, float);
		CHANNEL_VALUE_TYPE(LCLTRANSLATION_X, float);
		CHANNEL_VALUE_TYPE(LCLTRANSLATION_Y, float);
		CHANNEL_VALUE_TYPE(LCLTRANSLATION_Z, float);

		enum class INTERP : u8 {
			STATIC,		// The value of the key does not interpolate
			LINEAR,		// The value of the key linearly interpolates between one value and the next
			BEZIER_2,	
			BEZIER_3,
		};

		inline bool IsXYZChannel(CHANNEL_TYPE channel_type) {
			return CHANNEL_TYPE::__START_XYZ_CHANNELS <= channel_type && channel_type < CHANNEL_TYPE::__END_XYZ_CHANNELS;
		}

		struct track_info {
			hash_val trackId;
			TRACK_TYPE trackType;
			u8 numChannels;
			u16 channelStartIndex;
		};

		struct key_info {
			float time;
			u16 valueStartIndex;
			INTERP interpolationMode;
		};

	}
}

#endif