#ifndef _ANIM_COMPRESSED_TYPES_H_
#define _ANIM_COMPRESSED_TYPES_H_

#include "Math/LinAlg.h"

namespace Jigsaw {
	namespace Animation {

		/// <summary>
		/// Quat3Float leverages the property that any Identity quaternion rotation can be represented by its inverse.
		/// Taking that into consideration, a full Quaternion can be represented with only three components. The implicit 'w' component.
		/// is always taken to be non-negative. This needs to be guaranteed by the Quaternion implementation.
		/// </summary>
		struct Quat3Float {
			inline Quat3Float() : ijk() {}
			inline Quat3Float(Quaternion q) : ijk(q.w < 0 ? -q.ijk : q.ijk) { }

			inline Quaternion ToQuaternion() const {
				return Quaternion(sqrtf(1 - ijk.Dot(ijk)), ijk.x, ijk.y, ijk.z);
			}

			Vector3 ijk;

		};

	}
}

#endif