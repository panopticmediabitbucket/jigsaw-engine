#include "AnimClipSampler.h"

namespace Jigsaw {
	namespace Animation {

		//////////////////////////////////////////////////////////////////////////

		AnimClipSampler::AnimClipSampler(AnimClip* clip, bool loop) : m_clip(clip), m_loop(loop) {
		}

		//////////////////////////////////////////////////////////////////////////

		bool AnimClipSampler::Advance(float t) {
			m_elapsedTime += t;
			if (m_elapsedTime > m_clip->GetClipLength()) {
				m_elapsedTime = m_loop ? m_elapsedTime - m_clip->GetClipLength() : m_clip->GetClipLength();
				return true;
			}
			return false;
		}

		//////////////////////////////////////////////////////////////////////////

		void AnimClipSampler::Sample(jSkeleton& skeleton) {
			for (u32 i = 0; i < skeleton.GetBoneCount(); i++) {
				jBone& bone = skeleton[i];
				hash_val bone_hash = skeleton.GetBoneHash(i);

				Track bone_track = m_clip->GetTrackByHashId(bone_hash);

				// If there is any information for this bone in this clip, we sample
				if (bone_track.IsValid()) {
					Vector3 position = SampleTranslation(bone_track, bone);
					Vector3 scale = SampleScale(bone_track, bone);
					Quaternion rotation = SampleRotation(bone_track, bone);

					bone.m_rigidTransform = Math::DualQuaternion(rotation, position);
					bone.m_scale = scale;
				}
			}
		}

		//////////////////////////////////////////////////////////////////////////

		Vector3 AnimClipSampler::SampleTranslation(const Track& bone_track, const jBone& bone)
		{
			Vector3 ret = bone.m_rigidTransform.GetTranslation();
			if (const AnimChannel* x_channel = bone_track[CHANNEL_TYPE::LCLTRANSLATION_X]) {
				ret.x = x_channel->Sample<float>(m_elapsedTime);
			}
			if (const AnimChannel* y_channel = bone_track[CHANNEL_TYPE::LCLTRANSLATION_Y]) {
				ret.y = y_channel->Sample<float>(m_elapsedTime);
			}
			if (const AnimChannel* z_channel = bone_track[CHANNEL_TYPE::LCLTRANSLATION_Z]) {
				ret.z = z_channel->Sample<float>(m_elapsedTime);
			}
			return ret;
		}

		//////////////////////////////////////////////////////////////////////////

		Vector3 AnimClipSampler::SampleScale(const Track& bone_track, const jBone& bone)
		{
			Vector3 ret = bone.m_scale;
			if (const AnimChannel* x_channel = bone_track[CHANNEL_TYPE::LCLSCALE_X]) {
				ret.x = x_channel->Sample<float>(m_elapsedTime);
			}
			if (const AnimChannel* y_channel = bone_track[CHANNEL_TYPE::LCLSCALE_Y]) {
				ret.y = y_channel->Sample<float>(m_elapsedTime);
			}
			if (const AnimChannel* z_channel = bone_track[CHANNEL_TYPE::LCLSCALE_Z]) {
				ret.z = z_channel->Sample<float>(m_elapsedTime);
			}
			return ret;
		}

		//////////////////////////////////////////////////////////////////////////

		Quaternion AnimClipSampler::SampleRotation(const Track& bone_track, const jBone& bone)
		{
			if (const AnimChannel* quat_channel = bone_track[CHANNEL_TYPE::LCLROTATION_QUAT]) {
				return quat_channel->Sample<Quaternion>(m_elapsedTime);
			}
			return bone.m_rigidTransform.r;
		}

		//////////////////////////////////////////////////////////////////////////
	}
}