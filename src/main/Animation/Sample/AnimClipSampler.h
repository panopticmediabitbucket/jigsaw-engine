#ifndef _ANIM_CLIP_SAMPLER_H_
#define _ANIM_CLIP_SAMPLER_H_

#include "Animation/Skeleton.h"
#include "Animation/Clip.h"

namespace Jigsaw {
	namespace Animation {

		/// <summary>
		/// AnimClipSampler holds reference to a clip and maintains the elapsed time
		/// </summary>
		class AnimClipSampler {
		public:
			AnimClipSampler(AnimClip* clip, bool loop);

			/// <summary>
			/// Advances the elapsed time in the clip by 't'
			/// </summary>
			/// <param name="t"></param>
			/// <returns>True if advancing reaches the end of the clip</returns>
			bool Advance(float t);

			/// <summary>
			/// Samples the AnimClip at the current elapsed time with the skeleton
			/// </summary>
			/// <param name="skelton"></param>
			void Sample(jSkeleton& skelton);

		private:
			/// <summary>
			/// Extracts the merged translation from the bone and the bone_track given the m_elapsedTime
			/// </summary>
			/// <param name="bone_track"></param>
			/// <param name="bone"></param>
			/// <returns></returns>
			Vector3 SampleTranslation(const Track& bone_track, const jBone& bone);

			/// <summary>
			/// Extracts the merged scale from the bone and bone_track given the m_elapsedTime
			/// </summary>
			/// <param name="bone_track"></param>
			/// <param name="bone"></param>
			/// <returns></returns>
			Vector3 SampleScale(const Track& bone_track, const jBone& bone);

			/// <summary>
			/// Extracts the resolved rotation from the bone and bone_track given the m_elapsedTime
			/// </summary>
			/// <param name="bone_track"></param>
			/// <param name="bone"></param>
			/// <returns></returns>
			Quaternion SampleRotation(const Track& bone_track, const jBone& bone);

			AnimClip* m_clip;
			float m_elapsedTime = 0.0f;
			bool m_loop;

		};
	}
}

#endif // _ANIM_CLIP_SAMPLER_H_
