#ifndef _SKELETON_H_
#define _SKELETON_H_

#include "Util/BYTE.h"
#include "Math/DualQuaternion.h"
#include "System/sysHashString.h"
#include "Ref.h"

namespace Jigsaw {

	struct JGSW_API jBone {
		Math::DualQuaternion m_rigidTransform;
		Vector3 m_scale;
	};

	struct bone_children {
		inline u16* GetChildIndices() { return (u16*)(this + 1); }

		inline const u16* GetChildIndices() const { return (const u16*)(this + 1); }

		u16 numChildren;
		// u16[] -- immediately followed by array of size 'numChildren'
	};

	struct bone_hash_index {
		hash_val hashName;
		u16 boneIndex;
		bool deformationBone;
	};

	struct skeleton_header {
		// The hash name of the skeleton
		hash_val skeletonName;

		// The total size of the jSkeleton asset in Bytes
		size_t sizeInBytes;

		// The bone count of the skeleton
		u16 boneCount;

		// The number of roots bones (bones without parents) in the skeleton
		u16 rootCount;

		// The number of bones that deform in the skeleton
		u16 deformerCount;
	};

	struct skeleton_data {
		// Bones are in order of their indices. Indices are largely determined by their assignment when the skeleton was first created.
		jBone* bones = nullptr;

		// The m_boneHashIndices is sorted on the hash name of the bone. It serves as a quick log n lookup for indices by hash name
		bone_hash_index* boneHashIndices = nullptr;

		// The sort indices convert the actual bone index to its index in the hash-name-sorted boneData array. 
		u16* sortIndices = nullptr;

		// An array of indices for the root bone(s)
		u16* rootIndices;

		// An array of pointers to the bone_children structures that specify the children indices for each bone
		bone_children** children = nullptr;

		// An array of pointers to full names
		const char** names = nullptr;
	};

	/// <summary>
	/// jSkeleton provides a window into reading and interfacing with the articulated blob of bone data. 
	/// m_data members' refer to data appended on to the end of this structure, if the jSkeleton is a blob.
	/// </summary>
	class JGSW_API jSkeleton {
	public:
		friend class SkeletonAsset;

		jSkeleton() = delete;

		~jSkeleton();

		/// <summary>
		/// Creates a mutable clone of this Skeleton that can be used for sampling AnimClips, among other things.
		/// </summary>
		/// <returns></returns>
		Ref<jSkeleton> Clone() const;

		/// <summary>
		/// Converts all the bone transforms to object space
		/// </summary>
		void ConvertToObjectSpace();

		/// <returns>The number of bones in the skeleton</returns>
		inline u32 GetBoneCount() const { return m_header.boneCount; };

		/// <returns>The number of bones in the skeleton</returns>
		inline u32 GetDeformerCount() const { return m_header.deformerCount; };

		/// <returns>The number of roots in the skeleton</returns>
		inline u32 GetRootCount() const { return m_header.rootCount; };

		/// <returns>The bone index of the root at the specified root index</returns>
		inline u32 GetRootBoneIndex(u32 rootIdx) const { return m_data.rootIndices[rootIdx]; };

		/// <returns>The bone at the specified index</returns>
		inline const jBone& GetBone(u32 idx) const { return m_data.bones[idx]; };
		inline const jBone& operator[](u32 idx) const { return m_data.bones[idx]; };

		/// <returns>The bone at the specified index, modifiable</returns>
		inline jBone& GetBone(u32 idx) { return m_data.bones[idx]; };
		inline jBone& operator[](u32 idx) { return m_data.bones[idx]; };

		/// <returns>The hash id of the bone at the specified index</returns>
		inline hash_val GetBoneHash(u32 idx) const { return m_data.boneHashIndices[m_data.sortIndices[idx]].hashName; }

		/// <returns>The bone associated with the specified hash name</returns>
		const jBone& GetBoneByHash(hash_val hash) const;

		/// <returns>The bone associated with the specified hash name, modifiable</returns>
		jBone& GetBoneByHash(hash_val hash);

		/// <returns>The name of the bone at the specified index</returns>
		inline const char* GetName(u32 idx) { return m_data.names[idx]; };

	private:
		/// <summary>
		/// Creates a mutable clone of this Skeleton that can be used for sampling AnimClips, among other things.
		/// </summary>
		/// <returns>A raw pointer to the cloned skeleton. Used internally for creating the reference.</returns>
		jSkeleton* CloneRaw() const;

		/// <summary>
		/// Applies the transformation of the parent to its children, recursively.
		/// </summary>
		/// <param name="root"></param>
		/// <param name="children"></param>
		void ConvertChildrenToObjectSpace(jBone& parent, const bone_children* children);

		/// <summary>
		/// Normalizes the pointers in the skeleton against a base address. 
		/// This needs to be done before saving a Skeleton for use later.
		/// </summary>
		/// <param name="base"></param>
		void NormalizePointers(Util::ptr base);

		/// <summary>
		/// Resolves the pointers in the skeleton against a base address. This needs to be called right after asset load to fix the internal referencing.
		/// </summary>
		/// <param name="base"></param>
		void ResolvePointers(Util::ptr base);

		skeleton_header m_header;
		skeleton_data m_data;

	};
}
#endif // _SKELETON_H_