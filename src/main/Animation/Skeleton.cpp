#include "Skeleton.h"
#include <algorithm>

namespace Jigsaw {

	//////////////////////////////////////////////////////////////////////////

	jSkeleton::~jSkeleton() { }

	//////////////////////////////////////////////////////////////////////////

	void _FreeSkel(jSkeleton* skel) 
	{
		_aligned_free(skel);
	}

	//////////////////////////////////////////////////////////////////////////

	Ref<jSkeleton> jSkeleton::Clone() const
	{
		return Ref<jSkeleton>(CloneRaw());
	}

	//////////////////////////////////////////////////////////////////////////

	void jSkeleton::ConvertToObjectSpace()
	{
		for (u32 i = 0; i < m_header.rootCount; i++)
		{
			u32 root_idx = m_data.rootIndices[i];
			jBone& root = m_data.bones[root_idx];
			bone_children* children = m_data.children[root_idx];
			ConvertChildrenToObjectSpace(root, children);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	// Basic function for use with the binary search function
	inline bool _TrackSearchPredicate(const bone_hash_index& hash_index, const hash_val& hash)
	{
		return hash_index.hashName < hash;
	}

	//////////////////////////////////////////////////////////////////////////

	const jBone& jSkeleton::GetBoneByHash(hash_val hash) const {
		bone_hash_index* hash_index = std::lower_bound(m_data.boneHashIndices, m_data.boneHashIndices + m_header.boneCount, hash, _TrackSearchPredicate);
		return GetBone(hash_index->boneIndex);
	}

	//////////////////////////////////////////////////////////////////////////

	jBone& jSkeleton::GetBoneByHash(hash_val hash)
	{
		bone_hash_index* hash_index = std::lower_bound(m_data.boneHashIndices, m_data.boneHashIndices + m_header.boneCount, hash, _TrackSearchPredicate);
		return GetBone(hash_index->boneIndex);
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::jSkeleton* jSkeleton::CloneRaw() const
	{
		size_t allocation_size = (size_t)(m_data.names) - (size_t)(this);
		void* buffer = _aligned_malloc(allocation_size, 0x10);
		assert(buffer);
		memcpy(buffer, this, allocation_size);
		jSkeleton* clone = reinterpret_cast<jSkeleton*>(buffer);

		// Fixing all of the internal referencing. We don't use the normalize/resolve calls here because 
		// the cloned arrays of pointers (e.g. m_data.children) still point to the old array, so we need to be more surgical.
		Util::ptr old_skel = (Util::ptr)this;
		Util::ptr new_skel = (Util::ptr)buffer;

#define _CORRECT(elem) \
			reinterpret_cast<Util::ptr&>(elem) -= old_skel; \
			reinterpret_cast<Util::ptr&>(elem) += new_skel

		_CORRECT(clone->m_data.bones);
		_CORRECT(clone->m_data.boneHashIndices);
		_CORRECT(clone->m_data.rootIndices);
		_CORRECT(clone->m_data.sortIndices);
		_CORRECT(clone->m_data.children);
		for (u32 i = 0; i < clone->m_header.boneCount; i++)
		{
			_CORRECT(clone->m_data.children[i]);
		}
#undef _CORRECT

		// We did not copy the names, because they're just there for utility anyway and they take up some space, so just copy the pointer
		clone->m_data.names = m_data.names;

		return clone;
	}

	//////////////////////////////////////////////////////////////////////////

	void jSkeleton::ConvertChildrenToObjectSpace(jBone& parent, const bone_children* children)
	{
		for (u32 i = 0; i < children->numChildren; i++)
		{
			u32 child_idx = children->GetChildIndices()[i];
			jBone& child = GetBone(child_idx);

			// The child's scale needs to be put into object space--the translation is also scaled according to the parent
			child.m_scale = parent.m_rigidTransform.r.Rotate(child.m_scale);
			child.m_rigidTransform.SetTranslation(parent.m_scale.ComponentMult(child.m_rigidTransform.GetTranslation()));

			// The parent's scale is applied to the child
			child.m_scale = parent.m_scale.ComponentMult(child.m_scale);

			// The rigid transformation is passed
			child.m_rigidTransform = parent.m_rigidTransform * child.m_rigidTransform;

			const bone_children* child_children = m_data.children[child_idx];
			ConvertChildrenToObjectSpace(child, child_children);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jSkeleton::NormalizePointers(Util::ptr base)
	{
		static_assert(sizeof(jSkeleton) == 72, "Members have been added to the jSkeleton definition. Have corresponding normalization calls been added?");
		reinterpret_cast<Util::ptr&>(m_data.boneHashIndices) -= base;
		reinterpret_cast<Util::ptr&>(m_data.bones) -= base;
		for (u32 i = 0; i < m_header.boneCount; i++)
		{
			reinterpret_cast<Util::ptr&>(*m_data.children) -= base;
		}
		reinterpret_cast<Util::ptr&>(m_data.children) -= base;
		reinterpret_cast<Util::ptr&>(m_data.rootIndices) -= base;
		reinterpret_cast<Util::ptr&>(m_data.sortIndices) -= base;
		if (m_data.names)
		{
			for (u32 i = 0; i < m_header.boneCount; i++)
			{
				reinterpret_cast<Util::ptr&>(*m_data.names) -= base;
			}
			reinterpret_cast<Util::ptr&>(m_data.names) -= base;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void jSkeleton::ResolvePointers(Util::ptr base)
	{
		static_assert(sizeof(jSkeleton) == 72, "Members have been added to the jSkeleton definition. Have corresponding resolution calls been added?");
		reinterpret_cast<Util::ptr&>(m_data.boneHashIndices) += base;
		reinterpret_cast<Util::ptr&>(m_data.bones) += base;
		reinterpret_cast<Util::ptr&>(m_data.children) += base;
		for (u32 i = 0; i < m_header.boneCount; i++)
		{
			reinterpret_cast<Util::ptr&>(*m_data.children) += base;
		}
		reinterpret_cast<Util::ptr&>(m_data.rootIndices) += base;
		reinterpret_cast<Util::ptr&>(m_data.sortIndices) += base;
		if (m_data.names)
		{
			reinterpret_cast<Util::ptr&>(m_data.names) += base;
			for (u32 i = 0; i < m_header.boneCount; i++)
			{
				reinterpret_cast<Util::ptr&>(*m_data.names) += base;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////
}