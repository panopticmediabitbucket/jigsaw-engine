#ifndef _ANIM_INTERPOLATE_H_
#define _ANIM_INTERPOLATE_H_

#include "anim_clipinfo.h"
#include "Compression/anim_compressed_types.h"
#include "Debug/j_debug.h"
#include "Math/LinAlg.h"

namespace Jigsaw {
	namespace Animation {

		//////////////////////////////////////////////////////////////////////////

		// Interpolation functions for anim primitives

		//////////////////////////////////////////////////////////////////////////
		template <typename T>
		inline T Interpolate(INTERP mode, T a, T b, float t);


		// Floating point interpolation

		template<> inline float Interpolate(INTERP mode, float a, float b, float t) {
			switch (mode) {
			case INTERP::STATIC:
				return a;
			case INTERP::LINEAR:
			{
				float val_range = b - a;
				return a + (t * val_range);
			}
			default:
				J_D_ASSERT_LOG_ERROR(false, INTERP, "Unsupported interpolation mode {0}", mode);
				return 0;
			}
		}

		// Quaternion interpolation

		template<> inline Quaternion Interpolate(INTERP mode, Quaternion a, Quaternion b, float t) {
			switch (mode) {
			case INTERP::STATIC:
				return a;
			case INTERP::LINEAR:
			{
				return Quaternion::NLerp(a, b, t);
			}
			default:
				J_D_ASSERT_LOG_ERROR(false, INTERP, "Unsupported interpolation mode {0}", mode);
				return Quaternion();
			}
		}

		//////////////////////////////////////////////////////////////////////////

		// Conversion functions for anim primitives

		//////////////////////////////////////////////////////////////////////////

		// There is a default convert function which just directly casts the data

		template <typename T>
		inline T Decompress(CHANNEL_TYPE channel, const u8* data) { return *reinterpret_cast<const T*>(data); };

		template <> inline Quaternion Decompress<Quaternion>(CHANNEL_TYPE channel, const u8* data) {
			switch (channel) {
			case CHANNEL_TYPE::LCLROTATION_QUAT:
				return reinterpret_cast<const Quat3Float*>(data)->ToQuaternion();
			default:
				J_D_ASSERT_LOG_ERROR(false, CHANNEL_TYPE, "Unsupported channel type {0}", channel);
			}
			return Quaternion();
		}

	}
}

#endif