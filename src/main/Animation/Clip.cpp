#include "Clip.h"
#include "Debug/j_debug.h"

namespace Jigsaw {
	namespace Animation {

		//////////////////////////////////////////////////////////////////////////

		AnimChannel::AnimChannel(u32 numKeys, CHANNEL_TYPE channelType, key_info* keys, u8* raw_data)
			: m_numKeys(numKeys), m_channelType(channelType), m_keys(keys), m_data(raw_data)
		{
		}

		//////////////////////////////////////////////////////////////////////////

		// For use with the binary search
		inline bool _KeySearchPredicate(const key_info& k_info, float time) {
			return k_info.time < time;
		}

		//////////////////////////////////////////////////////////////////////////

		const key_info* AnimChannel::KeySample(float t) const {
			const key_info* key = std::lower_bound(m_keys, m_keys + m_numKeys, t, _KeySearchPredicate);
			if (key != (m_keys + m_numKeys)) {
				// We only stay on the current key if it exactly matches the time value, otherwise we walk back one
				key -= key->time == t ? 0 : 1;
			}
			return key;
		}

		//////////////////////////////////////////////////////////////////////////

		// For use with the binary search
		inline bool _TrackSearchPredicate(const track_info& t_info, const hash_val& hash) {
			return t_info.trackId < hash;
		}

		//////////////////////////////////////////////////////////////////////////


		Track AnimClip::GetTrackByHashId(hash_val trackId)
		{
			const track_info* track = std::lower_bound(m_tracks, m_tracks + m_numTracks, trackId, _TrackSearchPredicate);
			return track != (m_tracks + m_numTracks)
				? Track(track->trackId, track->trackType, track->numChannels, m_channels + track->channelStartIndex)
				: Track();
		}

		//////////////////////////////////////////////////////////////////////////

		Track AnimClip::GetTrackAt(u32 idx)
		{
			const track_info& track = m_tracks[idx];
			return Track(track.trackId, track.trackType, track.numChannels, m_channels + track.channelStartIndex);
		}

		//////////////////////////////////////////////////////////////////////////

		/// AnimClip Implementations

		//////////////////////////////////////////////////////////////////////////

		AnimClip::AnimClip(u32 num_tracks, float clip_length, track_info* tracks, AnimChannel* channels) 
			: m_numTracks(num_tracks), m_clipLength(clip_length), m_tracks(tracks), m_channels(channels)
		{
		}

		//////////////////////////////////////////////////////////////////////////

		Track::Track() : m_trackId(0), m_channels(nullptr), m_trackType(TRACK_TYPE::INVALID), m_numChannels(0)
		{
		}

		//////////////////////////////////////////////////////////////////////////

		Track::Track(hash_val track_id, TRACK_TYPE trackType, u8 numChannels, const AnimChannel* channels) : m_trackId(track_id), m_trackType(trackType), m_numChannels(numChannels), m_channels(channels)
		{
		}

		//////////////////////////////////////////////////////////////////////////
	}
}

