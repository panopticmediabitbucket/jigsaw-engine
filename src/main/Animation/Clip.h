#ifndef _CLIP_H_
#define _CLIP_H_

#include "anim_interpolate.h"
#include "anim_clipinfo.h"
#include "System/sysHashString.h"

namespace Jigsaw {
	namespace Animation {
#define CHANNEL_DATA u8

		class JGSW_API AnimChannel {
		public:
			AnimChannel(u32 numKeys, CHANNEL_TYPE channelType, key_info* keys, u8* raw_data);

			/// <summary>
			/// Samples the AnimChannel with the value type 'T.' The key(s) for the time value are located, any necessary per-sample decompression is applied,
			/// and then an interpolated value is produced for the channel. 
			/// </summary>
			/// <typeparam name="T"></typeparam>
			/// <param name="t"></param>
			/// <returns></returns>
			template <typename T>
			inline T Sample(float t) const { 
				const key_info* key = KeySample(t);
				size_t value_size = CHANNEL_TYPE_INFO::GetChannelValueSize(m_channelType);
				CHANNEL_DATA* raw_sample = m_data + (key->valueStartIndex * value_size);

				T sample = Decompress<T>(m_channelType, raw_sample);

				const key_info* next = key + 1;
				if (next != (m_keys + m_numKeys)) {
					float time_range = next->time - key->time;
					float t_in_range = t - key->time;
					float t_norm = t_in_range / time_range;

					CHANNEL_DATA* next_raw_sample = m_data + (next->valueStartIndex * value_size);
					T next_sample = Decompress<T>(m_channelType, next_raw_sample);
					return Interpolate<T>(key->interpolationMode, sample, next_sample, t_norm);
				}

				return sample;
			}

			/// <returns>A pointer to the data buffer used by this channel</returns>
			inline const CHANNEL_DATA* GetChannelDataBuffer() const { return m_data; }
			
			/// <returns>The channel type</returns>
			inline CHANNEL_TYPE GetChannelType() const { return m_channelType; }

			/// <returns>A pointer to a const array of key info</returns>
			inline const key_info* GetKeyInfoArray() const { return m_keys; }

			/// <returns>The number of keys in this channel</returns>
			inline u16 GetNumKeys() const { return m_numKeys; };

		private:

			/// <returns>The most-recent key associated with the time value</returns>
			const key_info* KeySample(float t) const;

			u16	m_numKeys;
			CHANNEL_TYPE m_channelType;
			key_info* m_keys;
			CHANNEL_DATA* m_data;

		};

		class JGSW_API Track {
		public:
			
			Track();
			Track(hash_val track_id, TRACK_TYPE trackType, u8 numChannels, const AnimChannel* channels);

			inline bool IsValid() const { return m_trackType != TRACK_TYPE::INVALID; }

			/// <returns>The channel at the specified index</returns>
			inline const AnimChannel* operator[](u32 idx) const { return &m_channels[idx]; }
			inline const AnimChannel* operator[](CHANNEL_TYPE channel_type) const { return GetAnimChannel(channel_type); }
			inline const AnimChannel* GetAnimChannel(u32 idx) const { return &m_channels[idx]; }
			inline const AnimChannel* GetAnimChannel(CHANNEL_TYPE channel_type) const {
				auto iter = std::find_if(m_channels, m_channels + m_numChannels, [=](const AnimChannel& channel) { return channel_type == channel.GetChannelType(); });
				if (iter == m_channels + m_numChannels) {
					return nullptr;
				}
				return iter;
			}

			/// <returns>The number of channels in the track</returns>
			inline u32 GetNumChannels() const { return m_numChannels; }

			/// <returns>The hash id of the track</returns>
			inline hash_val GetTrackId() const { return m_trackId; }

			/// <summary>
			/// An iterator implementation for Track AnimChannels
			/// </summary>
			class channel_iter {
			public:
				inline channel_iter(const Track& track, u32 i = 0) : m_track(&track), m_i(i) {}
				inline channel_iter& operator=(const channel_iter& other) {
					m_i = other.m_i;
					m_track = other.m_track;
					return *this;
				}

				inline channel_iter& operator++() {
					m_i++;
					return *this;
				}

				inline channel_iter& operator++(int i) {
					m_i++;
					return *this;
				}

				inline const AnimChannel* operator->() const { return (*m_track)[m_i]; };
				inline const AnimChannel& operator*() const { return *(*m_track)[m_i]; }
				inline bool operator==(const channel_iter& other) const { return m_track == other.m_track && m_i == other.m_i; }
				inline bool operator!=(const channel_iter& other) const { return !(*this == other); }
				inline u32 GetIdx() const { return m_i; }

			private:
				u32 m_i = 0;
				const Track* m_track;
			};

			// C++ iterable expressions
			inline channel_iter begin() const { return channel_iter(*this); };
			inline channel_iter end() const { return channel_iter(*this, m_numChannels); };

		private:
			hash_val m_trackId;
			TRACK_TYPE m_trackType;
			u8 m_numChannels;
			const AnimChannel* m_channels;

		};

		/// <summary>
		/// AnimClip contains all of the unique tracks and channels needed to sample a frame of animation against a Skeleton.
		/// </summary>
		class JGSW_API AnimClip {
		public:
			/// <param name="numTracks"></param>
			/// <param name="tracks"></param>
			/// <param name="channels"></param>
			AnimClip(u32 num_tracks, float clip_length, track_info* tracks, AnimChannel* channels);

			/// <summary>
			/// Returns a track associated with the trackId
			/// </summary>
			/// <param name="trackId"></param>
			/// <returns></returns>
			Track GetTrackByHashId(hash_val trackId);

			/// <summary>
			/// Return the track at the given index
			/// </summary>
			/// <param name="idx"></param>
			/// <returns></returns>
			Track GetTrackAt(u32 idx);

			/// <returns>The number of tracks in the clip</returns>
			inline u32 GetNumTracks() const { return m_numTracks; }

			/// <returns>The time length of the clip</returns>
			inline float GetClipLength() const { return m_clipLength; }

		private:

			const u32 m_numTracks;
			const float m_clipLength;
			const track_info* m_tracks;
			const AnimChannel* m_channels;

		};
	}
}
#endif // _CLIP_H_