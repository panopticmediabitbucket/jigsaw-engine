/*********************************************************************
 * jsSceneLoader.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: February 2021
 *********************************************************************/
#ifndef _JIGSAW_SCENE_LOADER_H_
#define _JIGSAW_SCENE_LOADER_H_

// Jigsaw_Engine
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Graphics/CommandListExecutor.h"
#include "jsScene.h"
#include "Ref.h"
#include "System/ASYNC.h"

// std
#include <future>

namespace Jigsaw
{
	typedef std::map<UID, MULTI_FABRICATION_ARGS> FabricationMap;

	/// <summary>
	/// Required arguments for a scene load operation. The THREAD_LOCAL_RESOURCE_POOL must not be empty
	/// </summary>
	struct SCENE_LOAD_ARGS 
	{
		Jigsaw::UID sceneId;
		Jigsaw::THREAD_LOCAL_RESOURCE_POOL resourcePool;
	};

	/// <summary>
	/// A JigsawSceneLoader produces a ASYNC_JOB that delivers a SCENE_LOAD_RESULT
	/// </summary>
	struct SCENE_LOAD_RESULT 
	{
		AssetRef<jsScene> loadedScene;
		FabricationMap machineFabricationArgs;
		Jigsaw::THREAD_LOCAL_RESOURCE_POOL recycledResources;
	};

	/// <summary>
	/// The jsSceneLoader manufactures a ASYNC_JOB that will eventually return a 'SCENE_LOAD_RESULT' 
	/// </summary>
	class jsSceneLoader
	{
	public:
		jsSceneLoader(SCENE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager);

		/// <summary>
		/// This method can only be called one time. If you want to prepare a job for execution later, you may, but as soon as
		/// 'LoadScene' is called, the underlying jsSceneLoader will no longer be valid
		/// </summary>
		/// <returns></returns>
		Jigsaw::Ref<Jigsaw::ASYNC_JOB<SCENE_LOAD_RESULT>> LoadScene();

	private:
		/// <summary>
		/// Private class manufactures a ASYNC_JOB object that produces a SCENE_LOAD_RESULT
		/// </summary>
		class _SCENE_ASYNC_JOB : public Jigsaw::ASYNC_JOB<SCENE_LOAD_RESULT>
		{
		public:
			/// <summary>
			/// Constructor has the load args moved in
			/// </summary>
			/// <param name="args"></param>
			_SCENE_ASYNC_JOB(SCENE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager);

		protected:
			/// <summary>
			/// Overriden method from base class that is called internally to launch the asynchronous task
			/// </summary>
			/// <returns></returns>
			SCENE_LOAD_RESULT Execute() override;

		private:
			SCENE_LOAD_ARGS m_args;
			Jigsaw::dlAssetRegistrar* m_assetManager;

		};

		SCENE_LOAD_ARGS m_args;
		Jigsaw::dlAssetRegistrar* m_assetManager;
	};
}
#endif // !_JIGSAW_SCENE_FACTORY_H_
