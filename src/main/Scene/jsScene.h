/*********************************************************************
 * jsScene.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: February 2021
 *********************************************************************/
#ifndef _JIGSAW_SCENE_H_
#define _JIGSAW_SCENE_H_

// Jigsaw_Engine
#include "Assets/DataAssets.h"
#include "Machines/jsMachine.h"
#include "System/UID.h"

// std
#include <map>
#include <vector>

namespace Jigsaw
{

	/// <summary>
	/// The building block that makes up the entire streaming system. jsScenes are loaded each in their entirety. Every scene contains reference to a variety
	/// of jsMachines, which of course have their own jsPieces and DataAsset references. Once all of the relevant assets are loaded, each scene has a unique set of
	/// serialized data used to fabricate Persistent and Static entities with the referenced machines. 
	/// </summary>
	class JGSW_API jsScene
	{
	public:
		jsScene() = default;

		/// <returns>The machines this jsScene depends on</returns>
		inline const std::vector<AssetRef<jsMachine>>& GetMachines() const { return machines; };

		/// <returns>The machines ids this jsScene can load.</returns>
		inline const std::vector<UID>& GetMachineIds() const { return machineIds; };

		// Get the scene id
		inline const UID& GetUID() const { return sceneId; };

#if JGSW_DEBUG || JGSW_DEV
		inline const char* GetName() const { return name.c_str(); };
#endif

		UID sceneId;
		std::string name;
		std::vector<UID> machineIds;

		// This is ignored by the Marshalling context. The jsSceneLoader will call the jsMachineLoader to load the machine Ids.
		// Keeping the loading of machines more modular is preferable. This way we can load new machines with the jsMachineLoader and then add them
		// to a scene definition in the Editor.
		std::vector<AssetRef<jsMachine>> machines;	

	};
}

#endif // !_JIGSAW_SCENE_H_
