#include "jsScene.h"

#include "Marshalling/JigsawMarshalling.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	START_REGISTER_SERIALIZABLE_CLASS(jsScene)
		REGISTER_SERIALIZABLE_FIELD(jsScene, std::string, name)
		REGISTER_SERIALIZABLE_FIELD(jsScene, Jigsaw::UID, sceneId)
	REGISTER_SERIALIZABLE_VECTOR(jsScene, Jigsaw::UID, machineIds);
	END_REGISTER_SERIALIZABLE_CLASS(jsScene);

	//////////////////////////////////////////////////////////////////////////
}

