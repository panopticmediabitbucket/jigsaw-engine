/*********************************************************************
 * jsSceneObserver.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _JIGSAW_SCENES_OBSERVER_H_
#define _JIGSAW_SCENES_OBSERVER_H_

 // Jigsaw_Engine
#include "Entities/jsEntityService.h"
#include "Injection/JigsawInjection.h"
#include "Machines/jsMachineObserver.h"
#include "Ref.h"
#include "Scene/jsScene.h"
#include "Scene/jsSceneLoader.h"
#include "System/UID.h"

// std
#include <map>

namespace Jigsaw
{
	/// <summary>
	/// jsSceneObserver maintains a single scene in the world hierarchy and provides an interface for activating it, 
	/// deactivating it, and accessing jsMachineObservers active in the scene .
	/// </summary>
	class JGSW_API jsSceneObserver
	{
	public:
		jsSceneObserver(Jigsaw::jsEntityService* cluster_service, const AssetRef<Jigsaw::jsScene>& scene, FabricationMap&& fabricationArgs) noexcept;

		jsSceneObserver() = default;

		/// <summary>
		/// Get the machine observer, const or non-const
		/// </summary>
		/// <param name="machine_id"></param>
		/// <returns></returns>
		inline const Jigsaw::jsMachineObserver* GetMachineObserver(const Jigsaw::UID& machine_id) const { return m_jgswMachines.at(machine_id); };
		inline Jigsaw::jsMachineObserver* GetMachineObserver(const Jigsaw::UID& machine_id) { return m_jgswMachines.at(machine_id); };

		// Iterable
		inline auto begin() const
		{
			return m_jgswMachines.begin();
		}

		// Iterable
		inline auto end() const
		{
			return m_jgswMachines.end();
		}

		/// <summary>
		/// Return the UID associated with the scene
		/// </summary>
		/// <returns></returns>
		inline const Jigsaw::UID& GetUID() const { return m_sceneId; };

		/// <summary>
		/// Return the number of machines in the scene
		/// </summary>
		/// <returns></returns>
		inline const u32 GetNumMachines() const { return (u32)m_jgswMachines.size(); };

		/// <param name="machine_id"></param>
		/// <returns>True if a machine with the specified machine_id is in the scene</returns>
		inline bool Contains(const UID& machine_id) const { return m_jgswMachines.find(machine_id) != m_jgswMachines.end(); }

		/// <summary>
		/// Awakens the underlying jsScene object, activating all of the JigsawMachines in the World hierarchy,
		/// and Fabricating all of their serialized JigsawEntities 
		/// </summary>
		void Awake();

		/// <summary>
		/// Removes the underlying jsScene object from the World hierarchy, serializing all of the active, non-ephemeral JigsawEntities
		/// in the process and 
		/// </summary>
		void Destroy();

	protected:

		JGSW_DEV_ONLY(virtual) jsMachineObserver* CreateMachineObserver(const AssetRef<jsMachine>& machine, jsEntityCluster& cluster)
		{
			return new jsMachineObserver(machine, cluster, this);
		}

		Jigsaw::jsEntityService* m_clusterService;

		Jigsaw::UID m_sceneId;
		AssetRef<Jigsaw::jsScene> m_scene;
		FabricationMap m_fabricationArgs;
		std::map<const Jigsaw::UID, Jigsaw::jsMachineObserver*> m_jgswMachines;

	};

}
#endif