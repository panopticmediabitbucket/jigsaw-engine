#include "jsSceneLoader.h"

// Jigsaw_Engine
#include "Assets/dlAssetRegistrar.h"
#include "Machines/jsMachineLoader.h"
#include "Debug/j_log.h"
#include "Application/ApplicationRootProperties.h"
#include "Marshalling/MarshallingUtil.h"

using namespace Jigsaw::Assets;

namespace Jigsaw
{
	//////////////////////////////////////////////////////////////////////////

	Jigsaw::jsSceneLoader::jsSceneLoader(SCENE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager) : m_args(std::move(args)), m_assetManager(asset_manager) { }

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::ASYNC_JOB<SCENE_LOAD_RESULT>> Jigsaw::jsSceneLoader::LoadScene()
	{
		return Jigsaw::Ref<ASYNC_JOB<SCENE_LOAD_RESULT>>(new _SCENE_ASYNC_JOB(std::move(m_args), m_assetManager));
	}

	//////////////////////////////////////////////////////////////////////////

	static unsigned int sm_loadIdx = 0;
	Jigsaw::jsSceneLoader::_SCENE_ASYNC_JOB::_SCENE_ASYNC_JOB(SCENE_LOAD_ARGS&& args, Jigsaw::dlAssetRegistrar* asset_manager) : ASYNC_JOB("SCENE_LOAD_" + sm_loadIdx++, OWNER_NOTIFY), m_args(std::move(args)), m_assetManager(asset_manager) { }

	//////////////////////////////////////////////////////////////////////////

	std::map<Jigsaw::UID, Jigsaw::MULTI_FABRICATION_ARGS> LoadFabricationArgs(const std::vector<UID>& machines, const UID& sceneId)
	{

		std::map<Jigsaw::UID, Jigsaw::MULTI_FABRICATION_ARGS> fabrication_args;
		for (int i = 0; i < machines.size(); i++)
		{
			UID_STR(machineStr);
			JGSW_DEBUG_ONLY(UIDToString(machines[i], machineStr));
			FILE_DATA file_data;

			// building the file path using the ApplicationRootProperties
			char fabPath[512];
			FileUtil::GetSceneMachineFabricationPath(sceneId, machines[i], fabPath);

			if (FileUtil::GetFileReadIfExists(fabPath, &file_data))
			{
				J_LOG_INFO(jsSceneLoader, "Fabrication args found for machine id {0}", machineStr);

				// building the fabrication args associated with this machine
				MULTI_FABRICATION_ARGS fab_args;
				Ref<SerializableMachineData> machine_data = Marshalling::MarshallingUtil::Unmarshal<SerializableMachineData>(file_data);
				std::fclose(file_data.file);
				if (machine_data)
				{
					// loading all of the fabrication args for these entities
					for (SerializableEntityData& e_data : machine_data->entity_data)
					{
						fab_args.entity_fabrication_args.push_back(FABRICATION_ARGS().WithSerializedData(e_data));
					}

					fabrication_args.insert(std::make_pair(machines[i], fab_args));
				}
				else
				{
					J_LOG_ERROR(jsSceneLoader, "There were unmarshalling errors while attempting to load fabrication data for machine id: {0}", machineStr);
				}
			}
			else
			{
				J_LOG_INFO(jsSceneLoader, "No fabrication args found for machine id {0}", machineStr);
			}
		}

		return fabrication_args;
	}

	//////////////////////////////////////////////////////////////////////////

	SCENE_LOAD_RESULT jsSceneLoader::_SCENE_ASYNC_JOB::Execute()
	{
		using namespace Jigsaw::Assets;

		J_LOG_INFO(jsSceneLoader, "Thread dispatched to load jigsaw scene");

		THREAD_LOCAL_SYSTEM_RESOURCES resources = m_args.resourcePool.Get();
		Unique<Ref<DataAsset>[]> sceneLoad = m_assetManager->FetchAssets(&m_args.sceneId, 1, resources);
		m_args.resourcePool.Enqueue(std::move(resources));

		SCENE_LOAD_RESULT result;
		if (Ref<DataAsset>* scene_ = sceneLoad.get())
		{
			AssetRef<jsScene> scene(std::dynamic_pointer_cast<ObjectAsset>(scene_[0]));
			if (resources.cmd_list->HasCommands())
			{
				m_args.resourcePool.cmd_list_exec->SubmitCommandList(resources.cmd_list);
			}

			result.loadedScene = scene;
		}
		else
		{
			UID_STR(uid);
			UIDToString(m_args.sceneId, uid);
			J_LOG_ERROR("Failed to Load the jsScene {0}", uid);
			return result;
		}

		AssetRef<jsScene>& scene = result.loadedScene;
		std::vector<UID>& machine_ids = scene->machineIds;
		J_LOG_INFO(jsSceneLoader, "Loading {0:d} machines", machine_ids.size());

		scene->machines.reserve(scene->machineIds.size());

		size_t pool_c = m_args.resourcePool.PoolSize();
		size_t machines_per_tsr = machine_ids.size() / pool_c;
		size_t remainder = machine_ids.size() % pool_c;

		// initiate the load jobs
		std::vector<Ref<ASYNC_JOB<MACHINE_LOAD_RESULT>>> load_jobs;
		u32 running_count = 0;
		for (u32 i = 0; i < pool_c && running_count < machine_ids.size(); i++)
		{
			u32 c = i < remainder ? (u32)machines_per_tsr + (u32)1 : (u32)machines_per_tsr;

			auto start = machine_ids.begin() + running_count;
			MACHINE_LOAD_ARGS load_args = { std::vector<UID>(start, start + c), m_args.resourcePool.Split(1) };
			jsMachineLoader machine_loader(std::move(load_args), m_assetManager);
			running_count += c;
			load_jobs.push_back(machine_loader.LoadMachine());
		}

		// Fetching the fabrication args while the load jobs execute.
		result.machineFabricationArgs = LoadFabricationArgs(scene->GetMachineIds(), m_args.sceneId);

		// collect the completed load jobs
		for (unsigned int i = 0; i < load_jobs.size(); i++)
		{
			while (!load_jobs.at(i)->Ready()) {}
			MACHINE_LOAD_RESULT complete = load_jobs.at(i)->Get();

			for (unsigned int j = 0; j < complete.jgswMachines.size(); j++)
			{
				const AssetRef<jsMachine>& machine = complete.jgswMachines[j];
				scene->machines.push_back(machine);
			}

			result.recycledResources.Merge(std::move(complete.recycledResources));
		}

		// recycle resources and await command list execution
		{
			m_args.resourcePool.cmd_list_exec->SignalAndWait();
		}

		// populated the loaded scene in the result
		result.recycledResources.Merge(std::move(m_args.resourcePool));

		return result;
	}

	//////////////////////////////////////////////////////////////////////////

}

