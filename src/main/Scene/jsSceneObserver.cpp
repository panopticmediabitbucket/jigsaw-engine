#include "jsSceneObserver.h"
#include "Machines/FABRICATION_ARGS.h"

#include "Assets/DataAssets.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	// jsSceneObserver Implementations

	//////////////////////////////////////////////////////////////////////////

	void jsSceneObserver::Awake()
	{
		const std::vector<AssetRef<jsMachine>>& machines = m_scene->GetMachines();
		for (const AssetRef<jsMachine>& machine : machines)
		{
			Jigsaw::jsEntityCluster& cluster = (*m_clusterService)[*machine->GetSignature().get()];
			m_jgswMachines.insert(std::make_pair(machine->machineId, CreateMachineObserver(machine, cluster)));
		}

		for (const std::pair<Jigsaw::UID, Jigsaw::jsMachineObserver*>& machine_pair : m_jgswMachines)
		{
			const Jigsaw::UID& machine_id = machine_pair.first;
			jsMachineObserver* machine_observer = static_cast<jsMachineObserver*>(machine_pair.second);

			auto iter = m_fabricationArgs.find(machine_id);

			if (iter != m_fabricationArgs.end())
			{
				machine_observer->Init(iter->second);
			}
		}

		// We're done with these now
		m_fabricationArgs.clear();
	}

	//////////////////////////////////////////////////////////////////////////

	void jsSceneObserver::Destroy()
	{
		for (std::pair<const Jigsaw::UID, Jigsaw::jsMachineObserver*>& pair : m_jgswMachines)
		{
			jsMachineObserver* machine_observer = static_cast<jsMachineObserver*>(pair.second);
			machine_observer->Destroy();
			delete machine_observer;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	jsSceneObserver::jsSceneObserver(Jigsaw::jsEntityService* cluster_service, const AssetRef<jsScene>& scene, FabricationMap&& fabricationArgs) noexcept
		: m_clusterService(cluster_service), m_sceneId(scene->GetUID()), m_scene(scene), m_fabricationArgs(std::move(fabricationArgs))
	{
	}

	//////////////////////////////////////////////////////////////////////////

}

