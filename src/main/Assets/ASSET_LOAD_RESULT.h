/*********************************************************************
 * ModelAsset.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: December 2020
 *********************************************************************/
#ifndef _ASSET_LOAD_RESULT_H_
#define _ASSET_LOAD_RESULT_H_
#include <vector>
#include "System/UID.h"
#include <functional>
#include <future>

namespace Jigsaw {

	enum class RESULT {
		INCOMPLETE,			// Indicates that some failure happened, but we were able to partially complete the task
		COMPLETE,			// Indicates completion with no errors. There may still be unresolved references though. Be sure to query flags too
		FAILURE				// Indicates complete failure--nothing was salvaged from the operation.
	};

	enum RES_FLAGS : u32
	{
		RES_FLAGS_NONE = 0,				// No flags
		RES_FLAGS_UNRESOLVED_REFS = 1,	// Unresolved references. 
	};

	typedef u32 RES_FLAGS_;

	struct UNRESOLVED_REFERENCE {
		Jigsaw::UID ref_id;

		// function that takes the resolved asset reference and injects it back into the object in question
		std::function<void(void*)> injector;
	};

	struct ASSET_LOAD_RESULT {

		RESULT result = RESULT::INCOMPLETE;
		RES_FLAGS_ flags;
		std::vector<UNRESOLVED_REFERENCE> unresolved_references;
	};

#if JGSW_DEV
	/// <summary>
	/// Dev-only packaging parameters. Static 'Package' functions on DataAssets allow an underlying data source to be 
	/// packaged into a contiguous memory region (m_begin through m_begin + m_byteCount) that the Asset system can then compress and save
	/// to an output file. If the buffer at m_begin was allocated as a result of the packaging function, m_shouldFreeMemory will be true,
	/// and the Asset system will need to free the memory after completing the task. 
	/// </summary>
	struct ASSET_PACKAGE_RESULT {
		RESULT result = RESULT::INCOMPLETE;
		u8* m_begin = nullptr;
		size_t m_byteCount = 0;
		bool m_shouldFreeMemory = false;
	};
#endif // JGSW_DEV
}

#endif // !_ASSET_LOAD_RESULT_H_

