#include "THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Application/ApplicationRootProperties.h"

namespace Jigsaw {

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_RESOURCE_POOL& Jigsaw::THREAD_LOCAL_RESOURCE_POOL::operator=(THREAD_LOCAL_RESOURCE_POOL&& other) noexcept {
		resource_queue = std::move(other.resource_queue);
		this->cmd_list_exec = other.cmd_list_exec;
		return *this;
	}

	//////////////////////////////////////////////////////////////////////////

	unsigned int Jigsaw::THREAD_LOCAL_RESOURCE_POOL::PoolSize() {
		return (u32)this->resource_queue.size();
	}

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_RESOURCE_POOL Jigsaw::THREAD_LOCAL_RESOURCE_POOL::Split(unsigned int n)
	{
		std::queue<THREAD_LOCAL_SYSTEM_RESOURCES> r_queue;
		for (UINT i = 0; i < n; i++) {
			r_queue.push(resource_queue.front());
			resource_queue.pop();
		}

		return THREAD_LOCAL_RESOURCE_POOL(r_queue, cmd_list_exec);
	}

	//////////////////////////////////////////////////////////////////////////

	void Jigsaw::THREAD_LOCAL_RESOURCE_POOL::Merge(THREAD_LOCAL_RESOURCE_POOL&& other) {
		while (other.PoolSize() != 0) {
			this->Enqueue(other.Get());
		}
		auto queue = other.filled_resources;
		while (!queue.empty()) {
			auto front = queue.front();
			filled_resources.push(front);
			queue.pop();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_SYSTEM_RESOURCES Jigsaw::THREAD_LOCAL_RESOURCE_POOL::Get() {
		THREAD_LOCAL_SYSTEM_RESOURCES sys_resources = resource_queue.front();
		resource_queue.pop();
		return sys_resources;
	}

	//////////////////////////////////////////////////////////////////////////

	void Jigsaw::THREAD_LOCAL_RESOURCE_POOL::Enqueue(THREAD_LOCAL_SYSTEM_RESOURCES&& resources) {
		if (resources.cmd_list->HasCommands()) {
			filled_resources.push(resources);
		}
		else {
			resource_queue.push(resources);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void THREAD_LOCAL_RESOURCE_POOL::EnqueueSubmit(THREAD_LOCAL_SYSTEM_RESOURCES&& resources)
	{
		if (resources.cmd_list->HasCommands()) {
			cmd_list_exec->SubmitCommandList(resources.cmd_list);
		}
		Enqueue(std::move(resources));
	}

	//////////////////////////////////////////////////////////////////////////

	std::queue<THREAD_LOCAL_SYSTEM_RESOURCES>& THREAD_LOCAL_RESOURCE_POOL::GetFilledResources() {
		return filled_resources;
	}

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_RESOURCE_POOL::THREAD_LOCAL_RESOURCE_POOL(THREAD_LOCAL_RESOURCE_POOL&& other) noexcept
		: resource_queue(std::move(other.resource_queue)), filled_resources(std::move(other.filled_resources)), cmd_list_exec(std::move(other.cmd_list_exec)) { }

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_RESOURCE_POOL::THREAD_LOCAL_RESOURCE_POOL() { }

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_RESOURCE_POOL::THREAD_LOCAL_RESOURCE_POOL(std::queue<THREAD_LOCAL_SYSTEM_RESOURCES> resource_queue, const Jigsaw::Ref<Jigsaw::CommandListExecutor>& cmd_list_exec) 
		: resource_queue(resource_queue), cmd_list_exec(cmd_list_exec)  {}

	//////////////////////////////////////////////////////////////////////////

	THREAD_LOCAL_RESOURCE_POOL::THREAD_LOCAL_RESOURCE_POOL(THREAD_LOCAL_SYSTEM_RESOURCES&& resources) noexcept
	{
		Enqueue(std::move(resources));
	}

	//////////////////////////////////////////////////////////////////////////

}
