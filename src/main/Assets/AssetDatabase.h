#ifndef _ASSET_DATABASE_H_
#define _ASSET_DATABASE_H_
#include <Windows.h>
#include "sqlite3.h"
#include "fileapi.h"
#include "BaseAsset.h"
#include <sstream>
#include "Ref.h"

namespace Jigsaw {
	enum DB_CONNECTION_TYPE {
		DB_CONNECTION_READONLY, DB_CONNECTION_READWRITE, DB_CONNECTION_CREATE_READWRITE
	};

	/// <summary>
	/// The dlAssetDatabaseReader is used to examine the asset database to collect meta information related to unique
	/// identifiers (UID's) and their relevant file paths, resource types, and more. 
	/// </summary>
	class JGSW_API dlAssetDatabaseReader {
	public:
		dlAssetDatabaseReader(const char* db_connection_string);

		virtual ~dlAssetDatabaseReader();

		/// <summary>
		/// Writes the target 'descriptor' and its associated file_path and metadata to the AssetDatabase
		/// </summary>
		/// <param name="id"></param>
		/// <returns>The completed AssetDescriptor associated with the id</returns>
		AssetDescriptor FetchDescriptor(const Jigsaw::UID& id);

	protected:
		/// <summary>
		/// A protected constructor is available so that inheriting classes can designate the connection type
		/// </summary>
		/// <param name="db_connection_string"></param>
		/// <param name="type"></param>
		dlAssetDatabaseReader(const char* db_connection_string, Jigsaw::DB_CONNECTION_TYPE type);

		sqlite3* db;
	private:
		/// <summary>
		/// Initializes the connection to the database. Called from constructors.
		/// </summary>
		void Init();

		const char* db_connection_string;
		const DB_CONNECTION_TYPE connection_type;
		sqlite3_stmt* desc_select_stmt;

	};

	/// <summary>
	/// The dlAssetDatabaseReadWriter inherits from the reader and also supports writing to the database.
	/// </summary>
	class JGSW_API dlAssetDatabaseReadWriter : public dlAssetDatabaseReader {
	public:
		/// <summary>
		/// Primary constructor for a Read/Writer just needs the connection string
		/// </summary>
		/// <param name="db_connection_string"></param>
		dlAssetDatabaseReadWriter(const char* db_connection_string);

		virtual ~dlAssetDatabaseReadWriter();

		/// <summary>
		/// Writes the target 'descriptor' and its associated file_path and metadata to the AssetDatabase
		/// </summary>
		/// <param name="descriptor"></param>
		void WriteDescriptor(const AssetDescriptor& descriptor);

		/// <summary>
		/// Deletes a descriptor from the table by id. Returns the discarded descriptor.
		/// </summary>
		/// <param name="id"></param>
		AssetDescriptor DeleteDescriptorById(const UID& id);

	private:

		sqlite3_stmt* desc_insert_stmt;
		sqlite3_stmt* m_deleteDescStmt;

	};
}

#endif
