/*********************************************************************
 * DataAsset.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: December 2020
 *********************************************************************/
#ifndef _ASSET_RESOURCES_H_
#define _ASSET_RESOURCES_H_

#include "Assets/dlAssetRegistrar.h"
#include "BaseAsset.h"
#include "Graphics/DirectX/DX_Context.h"
#include "Graphics/DirectX/DirectXDataLayouts.h"
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Ref.h"
#include "RTTI/etype_info.h"
#include "File_IO/FileUtil.h"
#include "Graphics/CommandList.h"
#include "Graphics/Pipeline/JigsawPipeline.h"
#include "Debug/j_debug.h"
#include "Util/DTO.h"

namespace Jigsaw {

	class dlAssetRegistrar; // dlAssetRegistrar reference

// Declares the functions necessary to register a given type as an asset for construction later
// Enables use with dlAssetRegistrar
#define DECLARE_DATA_ASSET(type) \
static constexpr Jigsaw::sysHashString GetHashId() { return Jigsaw::sysHashString(#type); }; \
static Jigsaw::DataAsset* Init(const Jigsaw::AssetDescriptor& desc);

#define IMPL_DATA_ASSET(type) \
DataAsset* type::Init(const AssetDescriptor& desc) { \
	return new type(desc); \
} \
int type##RegisterCall = Jigsaw::AutoRegister::CallWith<Jigsaw::sysHashString, DataAsset*(*)(const AssetDescriptor&)>(Jigsaw::dlAssetRegistrar::RegisterAssetType, type::GetHashId(), type::Init);


		/**
		 * DataAssets are read from the hard drive at runtime. They are shareable. This includes everything from Models to Shaders to Json.
		 */
	class JGSW_API DataAsset : public BaseAsset 
	{
	public:
		friend class dlAssetRegistrar;

		virtual ~DataAsset() { }

	protected:
		DataAsset(const AssetDescriptor& descriptor) : BaseAsset(descriptor) { }

		// these private, virtual implementations are designed to be used by privileged resource management functions
		// the resource management context will query a table for the availability of certain resources in memory or load them if they are absent. 
		virtual ASSET_LOAD_RESULT Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) = 0;

	};

	/// <summary>
	/// For assets that are serialized representations of objects. Could be in json or some other text or binary format.
	/// </summary>
	class JGSW_API ObjectAsset : public DataAsset {
	public:
		inline ObjectAsset(const AssetDescriptor& descriptor) : DataAsset(descriptor), t_info(Jigsaw::etype_info::GetByQualifiedName(descriptor.fully_qualified_type_name.c_str())) {}
		inline ObjectAsset(const AssetDescriptor& descriptor, void* object) : object(object), DataAsset(descriptor), t_info(Jigsaw::etype_info::GetByQualifiedName(descriptor.fully_qualified_type_name.c_str())) {}

		~ObjectAsset();

		DECLARE_DATA_ASSET(ObjectAsset);

		inline void* GetRawData() { return object; };

		inline const Jigsaw::etype_info& GetTypeInfo() { return *t_info; };

	protected:
		ASSET_LOAD_RESULT Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources);
		void* object = nullptr;
		const Jigsaw::etype_info* t_info;

	};

	/// <summary>
	/// Special wrapper class for accessing the underlying data of an ObjectAsset with the type specified.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template<typename T>
	class AssetRef 
	{
	public:
		/// <summary>
		/// Default constructor
		/// </summary>
		AssetRef() : m_typeInfo(&Jigsaw::etype_info::Id<T>()), m_objectAsset() {}

		/// <summary>
		/// Copy constructor
		/// </summary>
		/// <param name="other"></param>
		AssetRef(const AssetRef& other) : m_objectAsset(other.m_objectAsset), m_typeInfo(&Jigsaw::etype_info::Id<T>()) {}

		inline AssetRef& operator=(const AssetRef& other)
		{
			m_objectAsset = other.m_objectAsset;
			m_typeInfo = other.m_typeInfo;
			return *this;
		}

		/// <summary>
		/// Primary constructor used to initially assign an object_asset
		/// </summary>
		/// <param name="object_asset"></param>
		AssetRef(const Jigsaw::Ref<ObjectAsset>& object_asset) : m_objectAsset(object_asset), m_typeInfo(&Jigsaw::etype_info::Id<T>()) {
			bool eq = Jigsaw::etype_index(object_asset->GetTypeInfo()) == Jigsaw::etype_index(*this->m_typeInfo);
			J_D_ASSERT_LOG_ERROR(eq, AssetRef, "A serialized reference was attempted to be initialized with a non-matching type");
		}

		/// <summary>Copy assignment</summary>
		inline AssetRef& operator=(const Ref<ObjectAsset>& asset) {
			m_typeInfo = &asset->GetTypeInfo();
			J_D_ASSERT_LOG_ERROR(m_typeInfo == &Jigsaw::etype_info::Id<T>(), AssetRef<T>, "Trying to assign a non-matching type to an asset ref.");
			m_objectAsset = asset;
			return *this;
		}

		/// <summary>
		/// Move assignment is leveraged by the marshalling context 
		/// </summary>
		/// <param name="other"></param>
		/// <returns></returns>
		AssetRef& operator=(AssetRef<T>&& other) {
			m_objectAsset = std::move(other.m_objectAsset);
			return *this;
		}

		/// <returns>The underlying asset that this template obscures</returns>
		inline const Jigsaw::Ref<ObjectAsset>& GetObjectAsset() {
			return m_objectAsset;
		}

		/// <returns>The underlying object in the json asset.</returns>
		inline T* operator->() const {
			return static_cast<T*>(m_objectAsset->GetRawData());
		}

		/// <returns>A pointer to the underlying object</returns>
		inline T* Get() const {
			return static_cast<T*>(m_objectAsset->GetRawData());
		}

		/// <returns>True if the object is present</returns>
		inline operator bool() const {
			return m_objectAsset.get();
		}

		/// <summary>Releases the underlying asset reference</summary>
		inline void Reset() {
			m_objectAsset.reset();
		}

	private:
		Jigsaw::Ref<ObjectAsset> m_objectAsset;
		const Jigsaw::etype_info* m_typeInfo;

	};

	/**
	* VertexShaders
	*/
	class ShaderResource : public DataAsset {
	public:
		ShaderResource(const AssetDescriptor& descriptor) : DataAsset(descriptor), bytecode_desc({ nullptr, 0 }) { }

		~ShaderResource();

		DECLARE_DATA_ASSET(ShaderResource);

		ID3DBlob* GetShader() const {
			return shader.Get();
		}

		const D3D12_SHADER_BYTECODE GetShaderBytes() const {
			return bytecode_desc;
		}

	protected:
		ASSET_LOAD_RESULT Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources);

		D3D12_SHADER_BYTECODE bytecode_desc;
		Microsoft::WRL::ComPtr<ID3DBlob> shader;
	};

	class JGSW_API Cube : public DataAsset 
	{
	public:
		Cube(const AssetDescriptor& descriptor, size_t s = 1) : DataAsset(descriptor), scale(s), m_indArraySize(0) { }

		~Cube() {
		}

		static constexpr sysHashString GetHashId() { return sysHashString(500000); };

		static DataAsset* Init(const AssetDescriptor& desc);

		inline Jigsaw::GPUResource* GetBuffer() 
		{
			return m_vertBuffer;
		}

		inline Jigsaw::GPUResource* GetIndexBuffer() 
		{
			return m_indBuffer;
		}

	private:
		ASSET_LOAD_RESULT Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources);

		Jigsaw::Pipeline::JIGSAW_PIPELINE m_pipeline;
		Jigsaw::GPUResource* m_vertBuffer = nullptr;
		Jigsaw::GPUResource* m_indBuffer = nullptr;
		size_t m_indArraySize;
		size_t vbuff_size;
		size_t scale;
	};

}
#endif // !_ASSET_RESOURCES_H_
