#include "DataAssets.h"

#include "dlAssetRegistrar.h"
#include "Marshalling/JigsawMarshalling.h"
#include <d3dcompiler.h>
#include "Marshalling/JSONNode.h"

using namespace Jigsaw::Marshalling;

namespace Jigsaw {

	IMPL_DATA_ASSET(ObjectAsset);
	IMPL_DATA_ASSET(ShaderResource);
	IMPL_DATA_ASSET(Cube);

	// ShaderResource implementations


	ASSET_LOAD_RESULT ShaderResource::Load(const FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) {
		HRESULT hresult = D3DReadFileToBlob(file_data.file_name, &shader);

		if (FAILED(hresult)) {
			Jigsaw::DX_Context::PrintDebugMessages();
		}

		bytecode_desc.BytecodeLength = shader->GetBufferSize();
		bytecode_desc.pShaderBytecode = shader->GetBufferPointer();

		ASSET_LOAD_RESULT result;
		result.result = RESULT::COMPLETE;
		return result;
	}

	ShaderResource::~ShaderResource() { }

	ObjectAsset::~ObjectAsset() {
		if (object) {
			bool delete_res = t_info->GetUnsafeFunctions().Delete(object);
			J_D_ASSERT_LOG_ERROR(delete_res, ObjectAsset, "Failed to delete JsonAsset object");
		}
	}

	ASSET_LOAD_RESULT Jigsaw::ObjectAsset::Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) 
	{
		ASSET_LOAD_RESULT result;
		std::istringstream ss(FileUtil::GetStringStreamFromFile(file_data));

		JSONNodeReader json_reader(ss, Jigsaw::Assembly::MarshallingRegistry::GetMarshallingMap(*t_info));
		UNMARSHALLING_RESULT u_result = json_reader.BuildNode();

		object = u_result.raw_data;
		result.unresolved_references = u_result.unresolved_references;
		result.result = u_result.result;

		return result;
	}

	ASSET_LOAD_RESULT Jigsaw::Cube::Load(const Jigsaw::FILE_DATA& file_data, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources)
	{

		PositionColorNormal pos_col_array[]{
			{ Vector3(-1, -1, -1), Vector3(1, 0, 0) , Vector3(0, 0, -1) }, { Vector3(1, -1, -1), Vector3(1, 1, 0), Vector3(0, 0, -1) },
			{ Vector3(-1, 1, -1), Vector3(0, 1, 1), Vector3(0, 0, -1) }, { Vector3(1, 1, -1), Vector3(0, 1, 1), Vector3(0, 0, -1) },
		{ Vector3(1, -1, -1), Vector3(1, 1, 0), Vector3(1, 0, 0) }, { Vector3(1, 1, 1), Vector3(1, 1, 0), Vector3(1, 0, 0) },
		{ Vector3(1, 1, -1), Vector3(0, 1, 1), Vector3(1, 0, 0) }, { Vector3(1, -1, 1), Vector3(1, 1, 0), Vector3(1, 0, 0) },
		{ Vector3(-1, -1, 1), Vector3(1, 0, 0), Vector3(0, 0, 1) }, { Vector3(-1, 1, 1), Vector3(1, 0, 0), Vector3(0, 0, 1), },
		{ Vector3(1, 1, 1), Vector3(1, 1, 0), Vector3(0, 0, 1) }, { Vector3(1, -1, 1), Vector3(1, 1, 0), Vector3(0, 0, 1) },
		{ Vector3(-1, -1, -1), Vector3(1, 0, 0), Vector3(0, -1, 0) }, { Vector3(1, -1, 1), Vector3(1, 1, 0), Vector3(0, -1, 0) },
		{ Vector3(1, -1, -1), Vector3(1, 1, 0), Vector3(0, -1, 0) }, { Vector3(-1, -1, 1), Vector3(1, 0, 0), Vector3(0, -1, 0) },
		{ Vector3(-1, 1, -1), Vector3(0, 1, 1), Vector3(-1, 0, 0) }, { Vector3(-1, -1, 1), Vector3(1, 0, 0), Vector3(-1, 0, 0) },
		{ Vector3(-1, -1, -1), Vector3(1, 0, 0), Vector3(-1, 0, 0) }, { Vector3(-1, 1, 1), Vector3(1, 0, 0), Vector3(-1, 0, 0) },
		{ Vector3(-1, 1, 1), Vector3(1, 0, 0), Vector3(0, 1, 0) }, { Vector3(-1, 1, -1), Vector3(0, 1, 1), Vector3(0, 1, 0) },
		{ Vector3(1, 1, -1), Vector3(0, 1, 1), Vector3(0, 1, 0) }, { Vector3(1, 1, 1), Vector3(1, 1, 0), Vector3(0, 1, 0) }
		};

		UINT ind_array[]
		{
			1, 3, 0, // bottom face
			0, 3, 2,
			4, 5, 6, // back face
			4, 7, 5,
			8, 9, 10, // top face
			8, 10, 11,
			12, 13, 14, // right face
			12, 15, 13,
			16, 17, 18, // front face
			16, 19, 17,
			20, 21, 22, // left face
			20, 22, 23
		};

		// Creating vert buffer
		JGSW_GPU_RESOURCE_DATA resource_data;
		resource_data.flags |= JGSW_GPU_RESOURCE_FLAGS::JGSW_GPU_RESOURCE_FLAG_UPDATABLE;
		resource_data.resource_type = JGSW_GPU_RESOURCE_TYPE::BUFFER;
		resource_data.width = ARRAYSIZE(pos_col_array) * sizeof(PositionColorNormal);
		m_vertBuffer = sys_resources.render_context->CreateGPUResource(resource_data);

		// Creating ind buffer
		JGSW_GPU_RESOURCE_DATA ind_resource_data = resource_data;
		ind_resource_data.width = ARRAYSIZE(ind_array) * sizeof(UINT);
		m_indBuffer = sys_resources.render_context->CreateGPUResource(ind_resource_data);

		// Uploading the data into both buffers
		sys_resources.cmd_list->LoadBuffer((u8*)pos_col_array, sizeof(PositionColorNormal), ARRAYSIZE(pos_col_array), *m_vertBuffer);
		sys_resources.cmd_list->LoadBuffer((u8*)ind_array, sizeof(UINT), ARRAYSIZE(ind_array), *m_indBuffer);

		ASSET_LOAD_RESULT result;
		result.result = RESULT::COMPLETE;

		return result;
	}

}