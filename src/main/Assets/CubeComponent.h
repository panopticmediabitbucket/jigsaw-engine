#ifndef _CUBE_COMPONENT_H_
#define _CUBE_COMPONENT_H_

#include "Ref.h"
#include "Assets/DataAssets.h"
#include "Machines/jsPiece.h"
#include "_jgsw_api.h"

namespace Jigsaw
{
	struct JGSW_API CubeComponent
	{
		Jigsaw::Ref<Cube> cube_model;
	};

	JGSW_MACHINE_PIECE(JGSW_API CubeMachinePiece)
	{
	public:
		JGSW_COMPONENT_DATA(CubeComponent, cube);

	};
}

#endif