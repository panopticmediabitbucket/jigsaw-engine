/*********************************************************************
 * dlAssetRegistrar.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _DL_ASSET_REGISTRAR_H_
#define _DL_ASSET_REGISTRAR_H_

// Jigsaw_Engine
#include "AssetDatabase.h"
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "RuntimeAssets.h"
#include "System/sysHashString.h"

// std
#include <direct.h>
#include <concurrent_unordered_map.h>
#include "windows.h"

/**
 * Public resource management functions that are friended to the base resources.
 */
namespace Jigsaw {

	class DataAsset;

	/// <summary>
	/// dlAssetRegistrar provides a thread-safe view into all of the resources loaded in memory and a mechanism for fetching new ones.
	/// </summary>
	class JGSW_API dlAssetRegistrar {
	public:
		dlAssetRegistrar();

		Jigsaw::Ref<DataAsset> FetchAsset(const char* file_name, hash_val type, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources);

		/// <summary>
		/// Fetches an array of assets associated with the given descriptors in the same order as the descriptors. 
		/// </summary>
		/// <param name="descriptors"></param>
		/// <param name="count"></param>
		/// <param name="sys_resources"></param>
		/// <returns></returns>
		Jigsaw::Unique<Jigsaw::Ref<DataAsset>[]> FetchAssets(const Jigsaw::UID* descriptors, const size_t count, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES sys_resources);

		/// <summary>
		/// Provides a mechanism for resolving unresolved references after the completion of a load operation on an Asset. 
		/// </summary>
		/// <param name="unresolved_references"></param>
		/// <param name="sys_resources"></param>
		void ResolveReferences(std::vector < Jigsaw::UNRESOLVED_REFERENCE>& unresolved_references, Jigsaw::THREAD_LOCAL_SYSTEM_RESOURCES& sys_resources);

		typedef DataAsset* (*AssetFactoryFunction)(const AssetDescriptor&);

		/// <summary>
		/// Used with 'AutoRegister' call in IMPL_DATA_ASSET macro. This submits the Asset type on program initialization to be recalled later on construction, among other things.
		/// </summary>
		/// <param name="hash_id"></param>
		/// <param name="func"></param>
		static void RegisterAssetType(sysHashString hash_id, AssetFactoryFunction func);

#ifdef JIGSAW_DEV
		Jigsaw::Ref<DataAsset> CreateAsset(const char* asset_path, ASSET_TYPE type);
#endif

	private:
		enum class ASSET_STATE {
			LOADING, ACTIVE, INACTIVE
		};

		/// <summary>
		/// Internal struct is used to keep track of the state of a given AssetDescriptor 
		/// </summary>
		struct ASSET_DATA_BLOCK {
			ASSET_STATE state;
			Jigsaw::Weak<Jigsaw::DataAsset> ref;
		};

		std::mutex lock;
		_Guarded_by_(lock) concurrency::concurrent_unordered_map<Jigsaw::UID, ASSET_DATA_BLOCK> resource_map;

		/// <summary>
		/// Internal function used to fetch internal assets and return information about them in the ref_block
		/// </summary>
		_Requires_lock_held_(this->lock) void FindAsset(const Jigsaw::UID& uid, ASSET_DATA_BLOCK& ref_block);

		/// <summary>
		/// Places an indicator that the asset represented by the UID is being loaded into memory and made available
		/// so that concurrent threads do not attempt to load it also. 
		/// </summary>
		_Requires_lock_held_(this->lock) void MarkAssetForLoading(const Jigsaw::UID& uid);

		/// <summary>
		/// The calling thread officially claims the Asset associated with the given UID. If the Asset has already been claimed
		/// by another thread, but the runtime import is incomplete, then the calling thread will block until the Asset is made available.
		/// If the Asset has previously been claimed, but has expired, or if the Asset has never been claimed, this function will return false,
		/// and it is the calling thread's responsibility to fetch the Asset and load it into memory.
		/// </summary>
		/// <param name="uid">The uid of the requested asset</param>
		/// <param name="out_ref">An empty Ref that receives an active reference to the requested Asset</param>
		/// <returns>True if the Resource was available and the out_ref was populated; otherwise, returns false</returns>
		bool ClaimAsset(const Jigsaw::UID& uid, Jigsaw::Ref<Jigsaw::DataAsset>* out_ref);

		/// <summary>
		/// If the calling thread previously issued a call to 'ClaimAsset,' and that call returned false, then the calling thread must subsequently
		/// return to deliver a loaded asset, 'claimed_asset.'
		/// </summary>
		/// <param name="claimed_asset"></param>
		/// <returns></returns>
		bool WriteClaimedAsset(Jigsaw::Ref<Jigsaw::DataAsset>& claimed_asset);

		/// <returns>A mapping between hash_names of Assets types and their Factory functions</returns>
		static std::map<hash_val, AssetFactoryFunction>& GetAssetTypeMap();

		/// <returns>An instantiation of the type requested in the descriptor</returns>
		static Ref<DataAsset> InstantiateAssetType(const AssetDescriptor& descriptor);

	};

};

#endif
