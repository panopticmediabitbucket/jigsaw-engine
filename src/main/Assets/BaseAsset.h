#ifndef _BASE_ASSET_H_
#define _BASE_ASSET_H_
#include "ASSET_LOAD_RESULT.h"
#include <Windows.h>
#include <guiddef.h>
#include <wrl.h>
#include <d3dcompiler.h>
#include "System/UID.h"
#include "_jgsw_api.h"
#include "System/sysHashString.h"

namespace Jigsaw {

	// AssetDescriptors are the primary key used to load, examine, and store references to 
	struct JGSW_API AssetDescriptor {

		Jigsaw::UID id;
		std::string fully_qualified_type_name;
		hash_val type;
		const char* file_path;

		AssetDescriptor() : id(), type(), file_path(nullptr) {}
		AssetDescriptor(Jigsaw::UID id, hash_val type, const char* file_path) : id(id), type(type), file_path(file_path) {}
		bool operator< (const AssetDescriptor& other) {
			return id < other.id;
		}
	};

	class JGSW_API BaseAsset {
	public:
		const AssetDescriptor& GetDescriptor() const {
			return descriptor;
		}

	protected:
		BaseAsset(const AssetDescriptor& descriptor) : descriptor(descriptor) { }
		virtual ~BaseAsset() = 0;

		const AssetDescriptor descriptor;
	};

	// equality operator override for AssetDescriptor
	inline bool operator==(const AssetDescriptor& lhs, const AssetDescriptor& rhs) {
		return lhs.id == rhs.id;
	}
}

namespace std {
	template<>
	struct JGSW_API hash<Jigsaw::AssetDescriptor> {
		size_t operator()(const Jigsaw::AssetDescriptor& descriptor) const {
			return hash<Jigsaw::UID>()(descriptor.id);
		}
	};
}
#endif
