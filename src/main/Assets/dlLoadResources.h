#ifndef _DL_LOAD_RESOURCES_H_
#define _DL_LOAD_RESOURCES_H_

#include "Graphics/GraphicsContext.h"
#include "THREAD_LOCAL_SYSTEM_RESOURCES.h"

namespace Jigsaw {

	/// <summary>
	/// The container that preserves all the THREAD_LOCAL_SYSTEM_RESOURCES instances and dispatches them
	/// safely to the threads that request them. 
	/// </summary>
	class JGSW_API dlLoadResources {
	public:
		/// <summary>
		/// Regular constructor requires the GraphicsContext and the number of resource instances that should be loaded into the pool
		/// </summary>
		dlLoadResources(GraphicsContext* context, u32 number);

		/// <summary>
		/// Get a resource pool with the number of THREAD_LOCAL_SYSTEM_RESOURCES requested. 
		/// ToDo: 'block_until_all' is not implemented.
		/// </summary>
		/// <param name="request_number"></param>
		/// <param name="block_until_all"></param>
		/// <returns></returns>
		[[nodiscard("Upon destruction, a THREAD_LOCAL_RESOURCE_POOL is invalidated, and all the resource instances associated with it are reclaimed")]] 
		THREAD_LOCAL_RESOURCE_POOL GetPool(u32 request_number, bool block_until_all = false);

		void Recycle(THREAD_LOCAL_RESOURCE_POOL&& pool);
		
	protected:

		GraphicsContext* const m_context;

		std::mutex m_mutex;
		THREAD_LOCAL_RESOURCE_POOL m_pool;

	};

}
#endif // _DL_LOAD_RESOURCES_H_