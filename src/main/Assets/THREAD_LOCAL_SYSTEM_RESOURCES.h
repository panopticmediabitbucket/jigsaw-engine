/*********************************************************************
 * MachineCmdHandler.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _THREAD_SAFE_SYSTEM_RESOURCES_H_
#define _THREAD_SAFE_SYSTEM_RESOURCES_H_

#include "Assets/AssetDatabase.h"
#include "Ref.h"
#include "Graphics/CommandListExecutor.h"
#include "Graphics/CommandList.h"
#include "Graphics/GraphicsContext.h"
#include <queue>

namespace Jigsaw {

	/// <summary>
	/// Global Asset Management functions such as FetchAssets require a set of system resource that have been allocated specifically to the calling thread.
	/// The Asset loader can liberally manage the internal resources as needed to resolve the requested Assets. 
	/// </summary>
	struct JGSW_API THREAD_LOCAL_SYSTEM_RESOURCES 
	{
		Jigsaw::GraphicsContext* render_context = nullptr;
		Jigsaw::Ref<Jigsaw::CommandList> cmd_list;
		Jigsaw::Ref<Jigsaw::dlAssetDatabaseReader> db;
	};

	/// <summary>
	/// 
	/// </summary>
	class JGSW_API THREAD_LOCAL_RESOURCE_POOL 
	{
	public:
		CANT_COPY(THREAD_LOCAL_RESOURCE_POOL);

		/// <summary>
		/// Basic constructor
		/// </summary>
		THREAD_LOCAL_RESOURCE_POOL();

		/// <summary>
		/// Move constructor
		/// </summary>
		/// <param name="other"></param>
		THREAD_LOCAL_RESOURCE_POOL(THREAD_LOCAL_RESOURCE_POOL&& other) noexcept;

		/// <summary>
		/// Move constructor. THREAD_SAFE_RESOURCE_POOLS need to be moved, not copied. 
		/// </summary>
		THREAD_LOCAL_RESOURCE_POOL& operator=(THREAD_LOCAL_RESOURCE_POOL&& other) noexcept;

		/// <summary>
		/// Moves a single set of resources into a pool.
		/// </summary>
		/// <param name="resources"></param>
		/// <returns></returns>
		THREAD_LOCAL_RESOURCE_POOL(THREAD_LOCAL_SYSTEM_RESOURCES&& resources) noexcept;

		/// <summary>
		/// Returns the number of elements currently in the resource pool. 
		/// </summary>
		/// <returns></returns>
		unsigned int PoolSize();

		/// <summary>
		/// Generates a new a pool with 'n' resources, if they are available. If they are not available, returns as many
		/// as are available. If none are available, the behavior is undefined.
		/// </summary>
		/// <param name="n"></param>
		/// <returns></returns>
		THREAD_LOCAL_RESOURCE_POOL Split(unsigned int n);

		/// <summary>
		/// Merges the resources of 'other' with this THREAD_LOCAL_RESOURCE_POOL
		/// </summary>
		/// <param name="other"></param>
		void Merge(THREAD_LOCAL_RESOURCE_POOL&& other);

		/// <summary>
		/// Dequeues and returns one THREAD_LOCAL_SYSTEM_RESOURCES instance for use in the calling thread.
		/// </summary>
		/// <returns></returns>
		THREAD_LOCAL_SYSTEM_RESOURCES Get();

		/// <summary>
		/// Enqueues the given 'resources' object. It is assumed that the resources submitted are still in a valid state.
		/// The command list will not be submitted.
		/// </summary>
		void Enqueue(THREAD_LOCAL_SYSTEM_RESOURCES&& resources);

		/// <summary>
		/// Enqueues the given 'resources' object. It is assumed that the resources submitted are still in a valid state.
		/// The command list will be submitted.
		/// </summary>
		void EnqueueSubmit(THREAD_LOCAL_SYSTEM_RESOURCES&& resources);

		/// <summary>
		/// Returns a queue of the resources whose command lists have been populated
		/// </summary>
		/// <returns></returns>
		std::queue<THREAD_LOCAL_SYSTEM_RESOURCES>& GetFilledResources();

		Jigsaw::Ref<Jigsaw::CommandListExecutor> cmd_list_exec;

	private:
		/// <summary>
		/// Internal constructor is used for split operations
		/// </summary>
		THREAD_LOCAL_RESOURCE_POOL(std::queue<THREAD_LOCAL_SYSTEM_RESOURCES> resource_queue, const Jigsaw::Ref<Jigsaw::CommandListExecutor>& cmd_list_exec);

		std::queue<THREAD_LOCAL_SYSTEM_RESOURCES> resource_queue;
		std::queue<THREAD_LOCAL_SYSTEM_RESOURCES> filled_resources;

	};
}

#endif