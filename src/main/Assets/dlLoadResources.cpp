#include "dlLoadResources.h"

#include "Application/ApplicationRootProperties.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	dlLoadResources::dlLoadResources(GraphicsContext* context, u32 number) : m_context(context)
	{
		m_pool.cmd_list_exec = context->GetCommandListExecutor(Jigsaw::JGSW_COMMAND_TYPE_LOAD);

		for (u32 i = 0; i < number; i++) 
		{
			THREAD_LOCAL_SYSTEM_RESOURCES sys_resources = 
			{
				context,
				context->GetCommandList(Jigsaw::JGSW_COMMAND_TYPE_LOAD),
				Jigsaw::MakeRef<Jigsaw::dlAssetDatabaseReader>(ApplicationRootProperties::Get().db_connection.c_str())
			};
			m_pool.Enqueue(std::move(sys_resources));
		}
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::THREAD_LOCAL_RESOURCE_POOL dlLoadResources::GetPool(u32 request_number, bool block_until_all /*= false*/)
	{
		std::scoped_lock(m_mutex);

		return m_pool.PoolSize() >= request_number ? m_pool.Split(request_number) : m_pool.Split(m_pool.PoolSize());
	}

	//////////////////////////////////////////////////////////////////////////

	void dlLoadResources::Recycle(THREAD_LOCAL_RESOURCE_POOL&& pool)
	{
		std::scoped_lock(m_mutex);
		std::queue<THREAD_LOCAL_SYSTEM_RESOURCES> f_resource_queue = pool.GetFilledResources();
		while (!f_resource_queue.empty())
		{
			THREAD_LOCAL_SYSTEM_RESOURCES sys_resources = f_resource_queue.front();
			f_resource_queue.pop();

			sys_resources.cmd_list = m_context->GetCommandList(Jigsaw::JGSW_COMMAND_TYPE_LOAD);
			m_pool.Enqueue(std::move(sys_resources));
		}

		while (pool.PoolSize() > 0)
		{
			m_pool.Enqueue(pool.Get());
		}
	}

	//////////////////////////////////////////////////////////////////////////
}

