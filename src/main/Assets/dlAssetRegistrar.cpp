#include "dlAssetRegistrar.h"
#include <filesystem>
#include <cstdlib>
#include "Debug/j_log.h"
#include "DataAssets.h"

namespace Jigsaw 
{
	//////////////////////////////////////////////////////////////////////////

	dlAssetRegistrar::dlAssetRegistrar() { }

	//////////////////////////////////////////////////////////////////////////

	Ref<DataAsset> dlAssetRegistrar::FetchAsset(const char* file_name, hash_val type, THREAD_LOCAL_SYSTEM_RESOURCES sys_resources) {
		Ref<DataAsset> resource;
		AssetDescriptor descriptor;
		descriptor.type = type;
		descriptor.id = UID::Create();
		descriptor.file_path = file_name;

		// get initial resource
		resource = InstantiateAssetType(descriptor);

		// load the file needed to initialize the resource
		FILE_DATA resource_file_data = FileUtil::GetFileRead(descriptor.file_path);

		resource->Load(resource_file_data, sys_resources);
		fclose(resource_file_data.file);
		delete[] resource_file_data.file_name;

		WriteClaimedAsset(resource);
		return resource;
	}

	//////////////////////////////////////////////////////////////////////////

	void dlAssetRegistrar::ResolveReferences(std::vector<UNRESOLVED_REFERENCE>& unresolved_references, THREAD_LOCAL_SYSTEM_RESOURCES& sys_resources)
	{
		Unique<UID[]> reference_ids(new UID[unresolved_references.size()]);
		for (UINT i = 0; i < unresolved_references.size(); i++)
		{
			reference_ids[i] = unresolved_references[i].ref_id;
			J_LOG_INFO(dlAssetRegistrar, "Fetching unresolved asset reference id {0}", UIDToString(reference_ids[i]));
		}

		Unique<Ref<DataAsset>[]> resolved_references = FetchAssets(reference_ids.get(), unresolved_references.size(), sys_resources);

		for (UINT i = 0; i < unresolved_references.size(); i++)
		{
			unresolved_references[i].injector(&resolved_references[i]);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void dlAssetRegistrar::RegisterAssetType(sysHashString hash_id, AssetFactoryFunction func)
	{
		std::map<hash_val, AssetFactoryFunction>& map = GetAssetTypeMap();
#if JGSW_DEBUG
		auto iter = map.find(hash_id.Value());
		J_D_ASSERT_LOG_ERROR(iter == map.end(), dlAssetRegistrar, "There is a collision in registering asset type with hash_id {0}. This is fatal--make sure all Asset type names are unique.", hash_id.Value());
#endif
		map.insert(std::make_pair(hash_id.Value(), func));
	}

	//////////////////////////////////////////////////////////////////////////

	Unique<Ref<DataAsset>[]> dlAssetRegistrar::FetchAssets(const UID* uids, const size_t count, THREAD_LOCAL_SYSTEM_RESOURCES sys_resources)
	{
		Ref<DataAsset>* resources = new Ref<DataAsset>[count];

		for (int i = 0; i < count; i++)
		{
			// check if the resource is already available in the map
			if (!ClaimAsset(uids[i], &resources[i]))
			{
				AssetDescriptor descriptor = sys_resources.db->FetchDescriptor(uids[i]);
				FILE_DATA resource_file_data = FileUtil::GetFileRead(descriptor.file_path);
				resources[i] = InstantiateAssetType(descriptor);

				ASSET_LOAD_RESULT result = resources[i]->Load(resource_file_data, sys_resources);
				fclose(resource_file_data.file);

				WriteClaimedAsset(resources[i]);

				if (!result.unresolved_references.empty() && result.result != RESULT::FAILURE)
				{
					ResolveReferences(result.unresolved_references, sys_resources);
				}
			}
		}

		return Unique<Ref<DataAsset>[]>(resources);
	}

	//////////////////////////////////////////////////////////////////////////

	BaseAsset::~BaseAsset() { }

	//////////////////////////////////////////////////////////////////////////

	void dlAssetRegistrar::FindAsset(const UID& uid, ASSET_DATA_BLOCK& ref_block)
	{
		auto iter = resource_map.find(uid);
		if (iter != resource_map.end())
		{
			ref_block = iter->second;
		}
		else
		{
			J_LOG_TRACE(dlAssetRegistrar, "Asset non-existent in map history {0}", UIDToString(uid));
			ref_block.state = ASSET_STATE::INACTIVE;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void dlAssetRegistrar::MarkAssetForLoading(const UID& uid)
	{
		J_LOG_TRACE(dlAssetRegistrar, "Marking asset to be loaded: {0}", UIDToString(uid));
		auto iter = resource_map.find(uid);
		ASSET_DATA_BLOCK block;
		block.state = ASSET_STATE::LOADING;

		if (iter != resource_map.end())
		{
			resource_map[uid] = block;
		}
		else
		{
			resource_map.insert(std::make_pair(uid, block));
		}
	}

	//////////////////////////////////////////////////////////////////////////

	bool dlAssetRegistrar::ClaimAsset(const UID& uid, Ref<DataAsset>* out_ref)
	{
		ASSET_DATA_BLOCK read_block;
		goto SPIN;

		J_LOG_TRACE(dlAssetRegistrar, "Attempting to claim asset {0}", UIDToString(uid));

		while (read_block.state == ASSET_STATE::LOADING)
		{
			J_LOG_TRACE(dlAssetRegistrar, "Spin cycle engaged for loading asset {0}", UIDToString(uid));
			Sleep(1);
		SPIN:
			std::scoped_lock<std::mutex> l(lock);
			FindAsset(uid, read_block);

			if (read_block.state == ASSET_STATE::ACTIVE)
			{
				if (*out_ref = read_block.ref.lock())
				{
					J_LOG_TRACE(dlAssetRegistrar, "Active reference discovered for asset {0}", UIDToString(uid));
					return true;
				}
				else
				{
					J_LOG_TRACE(dlAssetRegistrar, "Inactive reference discovered for asset {0}", UIDToString(uid));
					read_block.state = ASSET_STATE::INACTIVE;
				}
			}

			if (read_block.state == ASSET_STATE::INACTIVE)
			{
				MarkAssetForLoading(uid);
			}
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	bool dlAssetRegistrar::WriteClaimedAsset(Ref<DataAsset>& claimed_asset)
	{
		std::scoped_lock<std::mutex> l(lock);

		ASSET_DATA_BLOCK new_block;
		new_block.ref = claimed_asset;
		new_block.state = ASSET_STATE::ACTIVE;
		resource_map[claimed_asset->GetDescriptor().id] = new_block;

		return true;
	}

	//////////////////////////////////////////////////////////////////////////

	Ref<DataAsset> dlAssetRegistrar::InstantiateAssetType(const AssetDescriptor& descriptor)
	{
		const std::map<hash_val, dlAssetRegistrar::AssetFactoryFunction>& map = GetAssetTypeMap();
		auto iter = map.find(descriptor.type);
		J_D_ASSERT_LOG_ERROR(iter != map.end(), dlAssetRegistrar, "Fatal error! Undiscovered type {0} from descriptor path {1} requested", descriptor.type, descriptor.file_path);
		AssetFactoryFunction factory_function = iter->second;
		return Ref<DataAsset>(factory_function(descriptor));
	}

	//////////////////////////////////////////////////////////////////////////

	std::map<hash_val, dlAssetRegistrar::AssetFactoryFunction>& dlAssetRegistrar::GetAssetTypeMap()
	{
		static std::map<hash_val, dlAssetRegistrar::AssetFactoryFunction> map;
		return map;
	}

	//////////////////////////////////////////////////////////////////////////

}

