#include "AssetDatabase.h"
#include "Debug/j_debug.h"
#include <windows.h>
#include "Util/BYTE.h"

namespace Jigsaw
{
	//////////////////////////////////////////////////////////////////////////

	dlAssetDatabaseReader::dlAssetDatabaseReader(const char* db_connection_string) : db_connection_string(db_connection_string), connection_type(DB_CONNECTION_READONLY)
	{
		Init();
	}

	//////////////////////////////////////////////////////////////////////////

	dlAssetDatabaseReader::dlAssetDatabaseReader(const char* db_connection_string, DB_CONNECTION_TYPE type)
		: db_connection_string(db_connection_string), connection_type(type)
	{
		Init();
	}

	//////////////////////////////////////////////////////////////////////////

	dlAssetDatabaseReader::~dlAssetDatabaseReader()
	{
		sqlite3_finalize(desc_select_stmt);

		sqlite3_close(db);
	}

	//////////////////////////////////////////////////////////////////////////

	AssetDescriptor Jigsaw::dlAssetDatabaseReader::FetchDescriptor(const UID& id)
	{

		// converting the uid to hex
		UID_STR(hex_str);
		UIDToString(id, hex_str);
		size_t size = UID_STR_SIZE - 1; // discarding null terminator
		J_LOG_TRACE(dlAssetDatabaseReader, "Fetching descriptor for UID {0}", hex_str);

		// binding the id blob to the prepared statement
		int bind_res = sqlite3_bind_text(desc_select_stmt, 1, hex_str, (int)size, SQLITE_STATIC);

		//issuing the initial request 
		int res = sqlite3_step(desc_select_stmt);

		J_D_ASSERT_LOG_ERROR((res == SQLITE_ROW), dlAssetDatabaseReader, "No database entry available for descriptor id {0}", hex_str);

		// fetching the returned data from the columns
		const unsigned char* id_blob = sqlite3_column_text(desc_select_stmt, 0);
		u32 type = sqlite3_column_int(desc_select_stmt, 1);
		s32 file_name_size = sqlite3_column_bytes(desc_select_stmt, 2);
		const unsigned char* file_name_read = sqlite3_column_text(desc_select_stmt, 2);
		int qual_type_arg_size = sqlite3_column_bytes(desc_select_stmt, 3);
		const unsigned char* qual_type_arg_read = sqlite3_column_text(desc_select_stmt, 3);

		// converting the id_blob into the long long data elements of the UID type
		UID id_u8_blob = UIDFromString((const char*)id_blob);

		// reading the file name for the asset 
		char* file_name_str = new char[file_name_size + 1];
		file_name_str[file_name_size] = '\0';
		memcpy(file_name_str, file_name_read, file_name_size);

		// reading the file name for the asset 
		char* qual_type_arg_str = new char[qual_type_arg_size + 1];
		qual_type_arg_str[qual_type_arg_size] = '\0';
		memcpy(qual_type_arg_str, qual_type_arg_read, qual_type_arg_size);

		// resetting the statement
		res = sqlite3_step(desc_select_stmt);
		J_D_ASSERT_LOG_ERROR(res == SQLITE_DONE, dlAssetDatabaseReader, "More than one row available for unique data type");
		sqlite3_reset(desc_select_stmt);

		AssetDescriptor descriptor(id_u8_blob, type, file_name_str);
		descriptor.fully_qualified_type_name = qual_type_arg_str;
		delete[] qual_type_arg_str;
		return std::move(descriptor);
	}

	//////////////////////////////////////////////////////////////////////////

	void Jigsaw::dlAssetDatabaseReader::Init()
	{
		int flags = 0;
		switch (connection_type)
		{
		case DB_CONNECTION_READONLY:
			flags |= SQLITE_OPEN_READONLY;
			break;
		case DB_CONNECTION_READWRITE:
			flags |= SQLITE_OPEN_READWRITE;
			break;
		case DB_CONNECTION_CREATE_READWRITE:
			flags |= SQLITE_OPEN_READWRITE | SQLITE_OPEN_CREATE;
			break;
		}

		int res = sqlite3_open_v2(db_connection_string, &db, flags, nullptr);
		// if we are creating a new database, we must prepare the table and the indexing system
		if (connection_type == DB_CONNECTION_CREATE_READWRITE)
		{
			sqlite3_stmt* stmt;
			const char* create_cmd = "CREATE TABLE IF NOT EXISTS DescriptorTable (UID character(36) NOT NULL UNIQUE, type int, file_path varchar(1028), qualified_type_argument varchar(1028));";
			sqlite3_prepare_v2(db, create_cmd, (int)strlen(create_cmd), &stmt, nullptr);
			sqlite3_step(stmt);
			sqlite3_finalize(stmt);

			const char* create_scene_machine_cmd = "CREATE TABLE IF NOT EXISTS SceneToMachineTable (SCENE_UID character(36) NOT NULL, MACHINE_UID character(36) NOT NULL);";
			sqlite3_prepare_v2(db, create_scene_machine_cmd, (int)strlen(create_scene_machine_cmd), &stmt, nullptr);
			sqlite3_step(stmt);
			sqlite3_finalize(stmt);

			const char* index_cmd = "CREATE UNIQUE INDEX IF NOT EXISTS uid ON DescriptorTable(UID)";
			sqlite3_prepare_v2(db, index_cmd, (int)strlen(index_cmd), &stmt, nullptr);
			int res = sqlite3_step(stmt);
			J_D_ASSERT_LOG_ERROR(res == SQLITE_DONE, dlAssetDatabaseReader, "Failed to create unique index on field");
			sqlite3_finalize(stmt);
		}

		const char* select_string = "SELECT * FROM DescriptorTable WHERE UID=?";
		int prep_res = sqlite3_prepare_v2(db, select_string, (int)strlen(select_string), &desc_select_stmt, nullptr);
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::dlAssetDatabaseReadWriter::dlAssetDatabaseReadWriter(const char* db_connection_string) : dlAssetDatabaseReader(db_connection_string, DB_CONNECTION_CREATE_READWRITE), desc_insert_stmt(nullptr)
	{
		const char* insert_str("INSERT INTO DescriptorTable (UID, type, file_path, qualified_type_argument) VALUES (?, ?, ?, ?)");
		int prep_res = sqlite3_prepare_v2(db, insert_str, (int)strlen(insert_str), &desc_insert_stmt, nullptr);

		const char* delete_desc_str("DELETE FROM DescriptorTable WHERE UID=?");
		prep_res = sqlite3_prepare_v2(db, delete_desc_str, (int)strlen(delete_desc_str), &m_deleteDescStmt, nullptr);
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::dlAssetDatabaseReadWriter::~dlAssetDatabaseReadWriter()
	{
		sqlite3_finalize(desc_insert_stmt);
		sqlite3_finalize(m_deleteDescStmt);
	}

	//////////////////////////////////////////////////////////////////////////

	void Jigsaw::dlAssetDatabaseReadWriter::WriteDescriptor(const AssetDescriptor& descriptor)
	{
		// getting the UID in hex string form
		UID_STR(id_str);
		UIDToString(descriptor.id, id_str);
		size_t size = UID_STR_SIZE - 1; // discarding null terminator

		int blob_res = sqlite3_bind_text(desc_insert_stmt, 1, id_str, (int)size, SQLITE_STATIC);
		int int_res = sqlite3_bind_int(desc_insert_stmt, 2, descriptor.type);
		int text_res = sqlite3_bind_text(desc_insert_stmt, 3, descriptor.file_path, (int)strlen(descriptor.file_path), SQLITE_STATIC);
		int type_res = sqlite3_bind_text(desc_insert_stmt, 4, descriptor.fully_qualified_type_name.c_str(), (int)descriptor.fully_qualified_type_name.size(), SQLITE_STATIC);

		int res = sqlite3_step(desc_insert_stmt);
		if (res == SQLITE_CONSTRAINT)
		{
			J_LOG_ERROR(AssetDatabaseReadWriter, "SQL constraint encountered on element input")
		}
		else
		{
			J_D_ASSERT_LOG_ERROR(res == SQLITE_DONE, AssetDatabaseReadWriter, "Error while executing write step {0:d}", res);
		}
		sqlite3_reset(desc_insert_stmt);
	}

	//////////////////////////////////////////////////////////////////////////

	AssetDescriptor dlAssetDatabaseReadWriter::DeleteDescriptorById(const UID& id)
	{
		AssetDescriptor desc = FetchDescriptor(id);
		if (desc.file_path)
		{
			UID_STR(hex);
			// getting the UID in hex string form
			UIDToString(id, hex);
			// discarding the null terminator
			u32 size = ARRAYSIZE(hex) - 1;
			int bind_res = sqlite3_bind_text(m_deleteDescStmt, 1, hex, size, SQLITE_STATIC);
			int res = sqlite3_step(m_deleteDescStmt);
			J_D_ASSERT_LOG_ERROR(res == SQLITE_DONE, dlAssetDatabaseReadWriter, "Error while clearing a record.");

			sqlite3_reset(m_deleteDescStmt);
		}

		return desc;
	}

	//////////////////////////////////////////////////////////////////////////

}

