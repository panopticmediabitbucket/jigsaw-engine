#include "ctInputEvents.h"
#include "Debug/j_debug.h"

namespace Jigsaw 
{
	//////////////////////////////////////////////////////////////////////////

	bool ctInputEvents::Key(short key) const
	{
		auto iter = input_map.find(key);
		return iter == input_map.end() ? false : ((*iter).second.activity_flags & PRESSED);
	}

	//////////////////////////////////////////////////////////////////////////

	bool ctInputEvents::KeyDown(short key) const
	{
		auto iter = input_map.find(key);
		return iter == input_map.end() ? false : ((*iter).second.activity_flags & DOWN);
	}

	//////////////////////////////////////////////////////////////////////////

	bool ctInputEvents::KeyUp(short key) const
	{
		auto iter = input_map.find(key);
		return iter == input_map.end() ? false : (((*iter).second.activity_flags & UP) && (*iter).second.activity_flags != TAPPED);
	}

	//////////////////////////////////////////////////////////////////////////

	bool ctInputEvents::MouseDown(MOUSE_BUTTON mouse_button, Rect* testRect /*= nullptr*/)
	{
		return m_mouseButton[(u32)mouse_button] & ACTIVITY_FLAGS::DOWN && ((testRect) ? testRect->Contains(m_mousePos) : true);
	}

	//////////////////////////////////////////////////////////////////////////

	bool ctInputEvents::MouseUp(MOUSE_BUTTON mouse_button, Rect* testRect /*= nullptr*/)
	{
		return m_mouseButton[(u32)mouse_button] & ACTIVITY_FLAGS::UP && ((testRect) ? testRect->Contains(m_mousePos) : true);
	}

	//////////////////////////////////////////////////////////////////////////

	float ctInputEvents::MouseWheel()
	{
		return m_mouseWheel;
	}

	//////////////////////////////////////////////////////////////////////////
}
