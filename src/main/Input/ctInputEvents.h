/*********************************************************************
 * ctInputEvents.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _CT_INPUT_EVENTS_H_
#define _CT_INPUT_EVENTS_H_

 // Jigsaw_Engine
#include "Math/LinAlg.h"
#include "Util/Primitives.h"

// std
#include <unordered_map>

namespace Jigsaw
{
	enum ACTIVITY_FLAGS : s16
	{
		DOWN			= 0b0001,
		UP				= 0b0010,
		PRESSED			= 0b0100,
		TAPPED			= DOWN | UP | PRESSED,
	};

	typedef u16 ACTIVITY_FLAGS_;

	enum class MOUSE_BUTTON
	{
		LEFT, 
		RIGHT, 
		MIDDLE,
		__NUM,
	};

	enum class KEY
	{
		SHIFT = 16
	};

	/// <summary>
	/// A read-only registry of per-frame input events.
	/// </summary>
	class JGSW_API ctInputEvents
	{
	public:
		/// <summary>
		/// Checks if the key is pressed. It doesn't matter if it was pressed that frame or it has been held.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		virtual bool Key(short key) const;

		/// <summary>
		/// Checks if the key was pushed this frame.
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		virtual bool KeyDown(short key) const;

		/// <summary>
		/// Checks if the key was released during this frame. 
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		virtual bool KeyUp(short key) const;

		/// <param name="mouse_button"></param>
		/// <returns>True if the passed in 'mouse_button' was pressed during that frame</returns>
		virtual bool MouseDown(MOUSE_BUTTON mouse_button, Rect* testRect = nullptr);

		/// <param name="mouse_button"></param>
		/// <returns>True if the passed in 'mouse_button' was released during that frame.</returns>
		virtual bool MouseUp(MOUSE_BUTTON mouse_button, Rect* testRect = nullptr);

		/// <returns>The delta of the MouseWheel during the last Update window (generally a frame).</returns>
		virtual float MouseWheel();

		/// <returns>The current position of the mouse</returns>
		virtual Vector2 GetMousePosition() { return m_mousePos; }

		/// <returns>The delta of the mouse.</returns>
		virtual Vector2 GetMouseDelta() { return m_mousePos; }

	protected:
		struct INPUT_ELEMENT
		{
			short key;
			short activity_flags;
		};

		// Mouse Input
		ACTIVITY_FLAGS_ m_mouseButton[(u32)MOUSE_BUTTON::__NUM];
		float m_mouseWheel;
		Vector2 m_mousePos;
		Vector2 m_mouseDelta;

		// Keyboard Input
		std::unordered_map<short, INPUT_ELEMENT> input_map;

	};
}
#endif // _CT_INPUT_EVENTS_H_