/*********************************************************************
 * ctInputEventWriter.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _INPUT_EVENT_UPDATE_SERVICE_H_
#define _INPUT_EVENT_UPDATE_SERVICE_H_

// Jigsaw_Engine
#include "Input/ctInputEvents.h"
#include "Orchestration/orMessageQueue.h"

// std
#include <queue>

namespace Jigsaw 
{
	/// <summary>
	/// Internal input event service designed to handle the input messages passed down from the OS to the ApplicationOrchestrator.
	/// </summary>
	class JGSW_API ctInputEventWriter : public ctInputEvents
	{
	public:
		/// <summary>
		/// Updates the internal storage logic with the input data.
		/// </summary>
		/// <param name="input"></param>
		virtual void HandleInputEvent(const Jigsaw::QINPUT& input);

		/// <summary>
		/// Clears the events stored for the individual frame. If any of the keys stored for that frame were 'TAPPED,' a 'KEY_UP' message will be stored on that key
		/// for processing during the next frame. This is intended to be called at the very end of an Orchestration loop once all the Systems/Services dependent on the
		/// ctInputEventWriter have been processed. 
		/// </summary>
		virtual void ClearFrameEvents();

		// Update functions for keys
		void PressKey(short key);
		void ReleaseKey(short key);
		void UpdateKey(short key, short value);

	private:
		std::queue<short> frame_events;

	};
}
#endif