#include "ctInputEventWriter.h"
#include "Debug/j_log.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	void ctInputEventWriter::UpdateKey(short key, short value)
	{
		auto iter = input_map.find(key);
		frame_events.push(key);

		if (iter == input_map.end())
		{
			INPUT_ELEMENT element;
			element.key = key;
			element.activity_flags = value;
			input_map.insert(std::make_pair(key, element));
		}
		else
		{
			(*iter).second.activity_flags = value;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void ctInputEventWriter::PressKey(short key)
	{
		J_LOG_INFO(ctInputEventWriter, "Key code pressed: {0}", key);
		UpdateKey(key, DOWN | PRESSED);
	}

	//////////////////////////////////////////////////////////////////////////

	void ctInputEventWriter::ReleaseKey(short key)
	{
		J_LOG_INFO(ctInputEventWriter, "Key code released: {0}", key);
		auto iter = input_map.find(key);
		if (iter == input_map.end())
		{
			INPUT_ELEMENT input = { key, UP };
			input_map.insert(std::make_pair(key, input));
		}
		else if ((*iter).second.activity_flags == (DOWN | PRESSED))
		{
			(*iter).second.activity_flags |= UP;
		}
		else
		{
			(*iter).second.activity_flags = UP;
			frame_events.push(key);
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void ctInputEventWriter::HandleInputEvent(const Jigsaw::QINPUT& input)
	{
		switch (input.GetInputType())
		{
		case QINPUT_TYPE::QKEY_DOWN:
			PressKey(input.GetInputChar());
			break;
		case QINPUT_TYPE::QKEY_UP:
			ReleaseKey(input.GetInputChar());
			break;
		case QINPUT_TYPE::QLEFT_MOUSE_DOWN:
			m_mouseButton[(u32)MOUSE_BUTTON::LEFT] |= ACTIVITY_FLAGS::DOWN | ACTIVITY_FLAGS::PRESSED;
			break;
		case QINPUT_TYPE::QMIDDLE_MOUSE_DOWN:
			m_mouseButton[(u32)MOUSE_BUTTON::MIDDLE] |= ACTIVITY_FLAGS::DOWN | ACTIVITY_FLAGS::PRESSED;
			break;
		case QINPUT_TYPE::QRIGHT_MOUSE_DOWN:
			m_mouseButton[(u32)MOUSE_BUTTON::RIGHT] |= ACTIVITY_FLAGS::DOWN | ACTIVITY_FLAGS::PRESSED;
			break;
		case QINPUT_TYPE::QLEFT_MOUSE_UP:
			m_mouseButton[(u32)MOUSE_BUTTON::LEFT] |= ACTIVITY_FLAGS::UP;
			m_mouseButton[(u32)MOUSE_BUTTON::LEFT] &= ~ACTIVITY_FLAGS::PRESSED;
			break;
		case QINPUT_TYPE::QMIDDLE_MOUSE_UP:
			m_mouseButton[(u32)MOUSE_BUTTON::MIDDLE] |= ACTIVITY_FLAGS::UP;
			m_mouseButton[(u32)MOUSE_BUTTON::MIDDLE] &= ~ACTIVITY_FLAGS::PRESSED;
			break;
		case QINPUT_TYPE::QRIGHT_MOUSE_UP:
			m_mouseButton[(u32)MOUSE_BUTTON::RIGHT] |= ACTIVITY_FLAGS::UP;
			m_mouseButton[(u32)MOUSE_BUTTON::RIGHT] &= ~ACTIVITY_FLAGS::PRESSED;
			break;
		case Jigsaw::QINPUT_TYPE::QMOUSE_POSITION:
		{
			Vector2 lastPos = m_mousePos;
			m_mousePos = Vector2(input.GetFloatParam(0), input.GetFloatParam(1));
			m_mouseDelta = m_mousePos - lastPos;
		}
			break;

		case Jigsaw::QINPUT_TYPE::QMOUSE_WHEEL:
			m_mouseWheel = input.GetFloatParam();
			break;
		default:
			J_LOG_INFO(ctInputEventWriter, "Unhandled input event type {0}", input.GetInputType());
			break;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void ctInputEventWriter::ClearFrameEvents()
	{
		m_mouseDelta = Vector2(0, 0);
		m_mouseWheel = 0;

		std::queue<short> next_frame_events;
		while (!frame_events.empty())
		{
			short key = frame_events.front();
			frame_events.pop();

			auto iter = input_map.find(key);
			if (iter != input_map.end())
			{
				if (iter->second.activity_flags == TAPPED)
				{
					iter->second.activity_flags = UP;
					next_frame_events.push(key);
				}
				else
				{
					iter->second.activity_flags &= PRESSED;
				}
			}
		}

		for (u32 i = 0; i < (u32)MOUSE_BUTTON::__NUM; i++)
		{
			m_mouseButton[i] &= ~(ACTIVITY_FLAGS::DOWN | ACTIVITY_FLAGS::UP);
		}

		frame_events = std::move(next_frame_events);
	}

	//////////////////////////////////////////////////////////////////////////

}

