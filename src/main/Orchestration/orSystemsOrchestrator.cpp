#include "orSystemsOrchestrator.h"
#include "Systems/jsSystem.h"
#include "Application/ApplicationRootProperties.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	orSystemsOrchestrator::orSystemsOrchestrator()
	{
		const ApplicationRootProperties& properties = ApplicationRootProperties::Get();
		Jigsaw::Assembly::SystemRegistry::GetBeans();

	}

	//////////////////////////////////////////////////////////////////////////

	void orSystemsOrchestrator::Update()
	{
		size_t priority_levels = s_systemRegistry->GetPriorityLevels();

		for (size_t priority_level = 0; priority_level < priority_levels; priority_level++)
		{
			const RUNTIME_PRIORITY_LEVEL& level = s_systemRegistry->GetPriorityLevel(priority_level);

			for (size_t system_index = 0; system_index < level.system_count; system_index++)
			{
				if (level.systems[system_index].active)
				{
					const RUNTIME_SYSTEM_DATA& system_data = level.systems[system_index];

					const Jigsaw::Ref<sysSignature> system_signature = system_data.registry_data.signature;

					for (auto cluster_iterator : (*s_clusterService))
					{

						Jigsaw::Unique<Jigsaw::sysSignatureMap> conversion_map;
						Jigsaw::jsEntityCluster& cluster = *cluster_iterator.second;

						if (cluster.GetSignature().HasCompleteConversionMap(*system_signature, &conversion_map))
						{
							SynchronousDispatch(cluster, *conversion_map, *system_data.system_instances[0]);
						}
					}
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void orSystemsOrchestrator::SynchronousDispatch(Jigsaw::jsEntityCluster& cluster, const Jigsaw::sysSignatureMap& conversion_map, Jigsaw::jsSystem& system)
	{
		for (UINT i = 0; i < cluster.GetClusterCount(); i++)
		{
			Jigsaw::jsEntityCluster::ENTITY_ITERATOR e_iterator = cluster.GetNodeIterator(i);
			while (e_iterator)
			{
				Jigsaw::jsEntity entity = *e_iterator;
				system.Update(entity, conversion_map);
				e_iterator++;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

}
