/*********************************************************************
 * orOrchestrator.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: January 2021
 *********************************************************************/
#ifndef _MAIN_APPLICATION_ORCHESTRATION_H_
#define _MAIN_APPLICATION_ORCHESTRATION_H_

// Jigsaw_Engine
#include "Orchestration/orMessageQueue.h"

namespace Jigsaw
{
	/// <summary>
	/// It should also really only be visible at the application root.
	///
	/// The orOrchestrator manages the main thread where all of the system execution takes place. 
	/// </summary>
	class JGSW_API orOrchestrator
	{

	public:
		orOrchestrator();

		virtual ~orOrchestrator();

		/// <summary>
		/// Returns the orMessageQueue object that can be used to send instructions to the ApplicationOrchestrator's
		/// MainLoop.
		/// </summary>
		/// <returns></returns>
		virtual Jigsaw::orMessageQueue* GetMessageQueue() = 0;

		/// <summary>
		/// Notifies the application that the render context and all other dependencies have been initialized 
		/// and execution is ready to proceed.
		/// </summary>
		virtual void StartApplication() = 0;

		/// <summary>
		/// Notifies the application that it is time to exit. Whatever tear-down needs to be completed before exiting
		/// is done here.
		/// </summary>
		virtual void EndApplication() = 0;

		/// <summary>
		/// The main orchestration call that happens once per frame. Responsible for executing all of the systems in the application
		/// and synchronizing with the Render thread. 
		///
		/// Returns true if execution is to continue. Returns false otherwise.
		/// </summary>
		virtual bool Orchestrate() = 0;

	protected:
		/// <summary>
		/// The application thread's main loop. Examines the orMessageQueue for any incoming instructions from the execution
		/// context. Internally calls the 'Orchestrate' method once per frame. 
		/// </summary>
		virtual void MainLoop() = 0;

		/// <summary>
		/// Processes the messages in a shared orMessageQueue. Returns true if execution will continue. Returns false for termination.
		/// </summary>
		virtual bool HandleMessages() = 0;

	};
}

#endif

