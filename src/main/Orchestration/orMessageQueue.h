/*********************************************************************
 * MessageQueue.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _MESSAGE_QUEUE_H_
#define _MESSAGE_QUEUE_H_

// std
#include <queue>
#include <string>
#include <mutex>

// Jigsaw_Engine
#include "System/ASYNC.h"
#include "Ref.h"

namespace Jigsaw
{
	// All of the asynchronous orchestration events.
	enum class ORCHESTRATION_ENUM
	{
		ORCHESTRATION_NO_MESSAGE,
		ORCHESTRATION_CONSOLE_INPUT,
		ORCHESTRATION_TERMINATE,
		ORCHESTRATION_INPUT_EVENT,
		ORCHESTRATION_BLOCK
	};

	// Specifies a timing requirement for handling an orchestration message.
	enum class MESSAGE_TIMING
	{
		NEXT_FRAME,
		CURRENT_FRAME
	};

	/// <summary>
	/// The message packet. Can be clarified into one of many QWORD shadow types.
	/// </summary>
	struct QWORD
	{
#define QDATA u8 data[32];

		QDATA;
	};

	/// <summary>
	/// The input type of the QINPUT QWORD. 
	/// </summary>
	enum class QINPUT_TYPE : u8
	{
		_QMOUSE_INPUT_BEGIN,
		QLEFT_MOUSE_DOWN,
		QLEFT_MOUSE_UP,
		QRIGHT_MOUSE_DOWN,
		QRIGHT_MOUSE_UP,
		QMIDDLE_MOUSE_DOWN,
		QMIDDLE_MOUSE_UP,
		QX_MOUSE_DOWN,
		QX_MOUSE_UP,
		QMOUSE_POSITION,
		QMOUSE_WHEEL,
		_QMOUSE_INPUT_END,

		_QKEYBOARD_INPUT_BEGIN,
		QKEY_DOWN,
		QKEY_UP,
		_QKEYBOARD_INPUT_END,
	};

	/// <summary>
	/// A structure that mirrors QWORD. Used for handling input message packets.
	/// </summary>
	struct QINPUT
	{
	public:
		inline void SetInputType(QINPUT_TYPE type)
		{
			data[0] = (u8)type;
		}

		inline QINPUT_TYPE GetInputType() const
		{
			return (QINPUT_TYPE)data[0];
		}

		inline bool IsKeyType() const
		{
			return QINPUT_TYPE::QKEY_DOWN <= GetInputType() && GetInputType() <= QINPUT_TYPE::QKEY_UP;
		}

		inline void SetInputChar(short val)
		{
			reinterpret_cast<short*>(data)[1] = val;
		}

		inline short GetInputChar() const
		{
			return reinterpret_cast<const short*>(data)[1];
		}

		inline void SetLongParam(long long x)
		{
			long long* l_data = reinterpret_cast<long long*>(data);
			l_data[1] = x;
		}

		inline long long GetLongParam() const
		{
			const long long* l_data = reinterpret_cast<const long long*>(data);
			return l_data[1];
		}

		inline void SetFloatParam(float f, u32 i = 0)
		{
			float* f_data = reinterpret_cast<float*>(data);
			f_data[i + 1] = f;
		}

		inline float GetFloatParam(u32 i = 0) const
		{
			const float* f_data = reinterpret_cast<const float*>(data);
			return f_data[i + 1];
		}

		inline void SetProcedureId(u64 procedure_id)
		{
			u64* f_data = reinterpret_cast<u64*>(data);
			f_data[3] = procedure_id;
		}

		inline u64 GetProcedureId() const
		{
			const u64* f_data = reinterpret_cast<const u64*>(data);
			return f_data[3];
		}

	private:
		QDATA;
	};

	/// <summary>
	/// The ORCHESTRATION_MESSAGE structure that's passed from one thread to another. 
	/// </summary>
	struct ORCHESTRATION_MESSAGE
	{
		ORCHESTRATION_ENUM message_type;
		QWORD message;
		std::string str_input;
		Jigsaw::Ref<Jigsaw::ASYNC_EVENT> message_processed_handshake;
		Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> external_event;
		unsigned long long procedure_id = 0;
	};

	/// <summary>
	/// The orMessageQueue class is the primary interface through which the ApplicationOrchestrator receives messages from the Operating System
	/// The OS players (Windows, etc.) are responsible for translating messages coming from the OS to a form recognized by the orMessageQueue.
	/// </summary>
	class JGSW_API orMessageQueue
	{
	public:
		orMessageQueue();

		/// <summary>
		/// Passes a message to be handled by the owner of the orMessageQueue.
		/// </summary>
		/// <param name="operation"></param>
		/// <param name="message"></param>
		/// <returns>An event listener that will be signaled once the message is processed.</returns>
		Jigsaw::Ref<Jigsaw::EVENT_LISTENER> EnqueueMessage(const ORCHESTRATION_ENUM& operation, const QWORD& message);

		/// <summary>
		/// Passes a message to be handled by the owner of the orMessageQueue.
		/// </summary>
		/// <param name="operation"></param>
		/// <param name="message"></param>
		/// <returns>An event listener that will be signaled once the message is processed.</returns>
		Jigsaw::Ref<Jigsaw::EVENT_LISTENER> EnqueueMessage(std::string&& console_message);

		/// <summary>
		/// Releases the top ORCHESTRATION_MESSAGE to the caller, if one exists.
		/// </summary>
		/// <param name="message"></param>
		/// <returns>True if a message is available.</returns>
		virtual bool DequeueMessage(ORCHESTRATION_MESSAGE* message);

		/// <summary>
		/// Pushes any deferred messages into the primary queue to be handled the next frame.  
		/// </summary>
		virtual void EnqueueDeferredMessages();

		/// <summary>
		/// Enqueues an message instructing the target Orchestrator to block until the passed in event is triggered.
		/// </summary>
		/// <param name="event"></param>
		/// <param name="timing"></param>
		/// <returns>An EVENT_LISTENER that is notified once the target Orchestrator is blocking</returns>
		virtual Jigsaw::Ref<Jigsaw::EVENT_LISTENER> BlockUntil(const Jigsaw::Ref<Jigsaw::EVENT_LISTENER>& event, MESSAGE_TIMING timing = MESSAGE_TIMING::CURRENT_FRAME);

	private:
		std::mutex mut;
		std::queue<ORCHESTRATION_MESSAGE> messages;
		std::queue<ORCHESTRATION_MESSAGE> deferred_messages;

	};
}
#endif