#include "orSceneHierarchy.h"
#include "Application/ApplicationRootProperties.h"
#include "Scene/jsSceneLoader.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	// SceneHierarchy Implementations

	//////////////////////////////////////////////////////////////////////////

	orSceneHierarchy::~orSceneHierarchy()
	{
		for (jsSceneObserver* observer : m_sceneObservers)
		{
			delete observer;
		}
	}

	//////////////////////////////////////////////////////////////////////////

	void orSceneHierarchy::LoadAndActivateScene(const UID& scene_id)
	{
		SCENE_LOAD_ARGS args;
		args.resourcePool = s_loadResources->GetPool(2);
		args.sceneId = scene_id;

		jsSceneLoader scene_loader(std::move(args), s_assetManager);
		Ref<ASYNC_JOB<SCENE_LOAD_RESULT>> job = scene_loader.LoadScene();
		SCENE_LOAD_RESULT result = job->Await();
		job->GetEvent()->Notify();

		s_loadResources->Recycle(std::move(result.recycledResources));
		m_sceneObservers.push_back(CreateSceneObserver(s_entityService, result.loadedScene, std::move(result.machineFabricationArgs)));
		m_sceneObservers.back()->Awake();
	}

	//////////////////////////////////////////////////////////////////////////

	Ref<const EVENT_LISTENER> orSceneHierarchy::LoadScene(const UID& scene_id)
	{
		SCENE_LOAD_ARGS args;
		args.resourcePool = s_loadResources->GetPool(2);
		args.sceneId = scene_id;

		jsSceneLoader scene_loader(std::move(args), s_assetManager);
		Ref<ASYNC_JOB<SCENE_LOAD_RESULT>> job = scene_loader.LoadScene();
		m_activeLoadJobs.push_back(job);
		return job->GetListener();
	}

	//////////////////////////////////////////////////////////////////////////

	orSceneHierarchy::orSceneHierarchy() {}

	//////////////////////////////////////////////////////////////////////////

	void orSceneHierarchy::Update()
	{
		auto iter = m_activeLoadJobs.begin();
		while (iter != m_activeLoadJobs.end())
		{
			ASYNC_JOB<SCENE_LOAD_RESULT>& job = **iter;
			if (job.Ready())
			{
				// activate the resulting scene from the scene load job if it is ready
				SCENE_LOAD_RESULT result = job.Get();
				s_loadResources->Recycle(std::move(result.recycledResources));
				m_sceneObservers.push_back(CreateSceneObserver(s_entityService, result.loadedScene, std::move(result.machineFabricationArgs)));
				m_sceneObservers.back()->Awake();

				job.GetEvent()->Notify();
				iter = m_activeLoadJobs.erase(iter);
			}
			else
			{ // else iterate to next
				iter++;
			}
		}
	}

	//////////////////////////////////////////////////////////////////////////

	Unique<UID[]> orSceneHierarchy::GetActiveSceneIds(size_t& out_size)
	{
		out_size = m_sceneObservers.size();
		Unique<UID[]> ids = Unique<UID[]>(new UID[out_size]);
		int i = 0;
		std::for_each(m_sceneObservers.begin(), m_sceneObservers.end(), [&](const jsSceneObserver* scene_observer) { ids.get()[i++] = scene_observer->GetUID(); });
		return std::move(ids);
	}

	//////////////////////////////////////////////////////////////////////////
}

