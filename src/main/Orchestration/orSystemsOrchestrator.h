/*********************************************************************
 * jsSystemsOrchestrator.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _JIGSAW_SYSTEM_ORCHESTRATOR_H_
#define _JIGSAW_SYSTEM_ORCHESTRATOR_H_

// Jigsaw_Engine
#include "Entities/jsEntityService.h"
#include "Injection/JigsawInjection.h"
#include "Ref.h"
#include "Systems/jsRuntimeSystemRegistry.h"

namespace Jigsaw 
{
	/// <summary>
	/// Owned by the ApplicationOrchestrator, the orSystemsOrchestrator
	/// </summary>
	class JGSW_API orSystemsOrchestrator : public jsSlots<orSystemsOrchestrator>
	{
	public:
		/// <summary>
		/// Default constructor
		/// </summary>
		orSystemsOrchestrator();

		/// <summary>
		/// Updates all of the internal Jigsaw Systems
		/// </summary>
		void Update();

	protected:
		void SynchronousDispatch(Jigsaw::jsEntityCluster& cluster, const Jigsaw::sysSignatureMap& conversion_map, Jigsaw::jsSystem& system);

		SLOT_1(Jigsaw::jsEntityService,				s_clusterService);
		SLOT_2(Jigsaw::jsRuntimeSystemRegistry,		s_systemRegistry, LAZY);

	};

}
#endif