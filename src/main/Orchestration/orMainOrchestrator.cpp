#include "Ref.h"
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Application/ApplicationRootProperties.h"
#include "Marshalling/MarshallingUtil.h"
#include "Debug/j_debug.h"
#include "orMainOrchestrator.h"
#include "Render/RenderService.h"

#define MAIN_LOOP(function) while(function) {}

// initializers 
using namespace Jigsaw::Assets;

namespace Jigsaw 
{

	//////////////////////////////////////////////////////////////////////////

	void orMainOrchestrator::StartApplication() 
	{
		J_LOG_INFO(orMainOrchestrator, "Starting main loop");
		main_thread = new std::thread(&Jigsaw::orMainOrchestrator::MainLoop, this);
	}

	//////////////////////////////////////////////////////////////////////////

	void orMainOrchestrator::EndApplication() 
	{
		Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> _event = s_messageQueue->EnqueueMessage(Jigsaw::ORCHESTRATION_ENUM::ORCHESTRATION_TERMINATE, QWORD());
		auto start_time = std::chrono::system_clock::now();
		_event->Await();
		auto end_time = std::chrono::system_clock::now();
		J_LOG_INFO(orMainOrchestrator, "Main Application Orchestrator terminated in {0} seconds", std::chrono::duration_cast<std::chrono::duration<double>>(end_time - start_time).count());
		main_thread->join();
		delete main_thread;
	}

	//////////////////////////////////////////////////////////////////////////

	void orMainOrchestrator::MainLoop() 
	{
		MAIN_LOOP(Orchestrate());
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::orMessageQueue* orMainOrchestrator::GetMessageQueue() 
	{
		return s_messageQueue;
	}

	//////////////////////////////////////////////////////////////////////////

	bool orMainOrchestrator::HandleMessages() 
	{
		ORCHESTRATION_MESSAGE message;
		while (s_messageQueue->DequeueMessage(&message)) 
		{
			switch (message.message_type) 
			{
			case ORCHESTRATION_ENUM::ORCHESTRATION_TERMINATE:
				message.message_processed_handshake->Notify();
				return false;
			case ORCHESTRATION_ENUM::ORCHESTRATION_BLOCK:
				message.message_processed_handshake->Notify();
				J_LOG_DEBUG(orMainOrchestrator, "Blocked by event");
				message.external_event->Await();
				break;
			case ORCHESTRATION_ENUM::ORCHESTRATION_INPUT_EVENT:
				message.message_processed_handshake->Notify();
				s_inputService->HandleInputEvent(reinterpret_cast<QINPUT&>(message.message));
				break;
			default:
				message.message_processed_handshake->Notify();
				break;
			}
		}

		s_messageQueue->EnqueueDeferredMessages();
		return true;
	}

	//////////////////////////////////////////////////////////////////////////

	static std::string frame_job = "frame_game_job";
	/**
	* The primary call orchestrates calls between all primary systems in the application world.
	*/
	bool orMainOrchestrator::Orchestrate()
	{
		if (!HandleMessages())
			return false;

#if !JGSW_DEV
		Jigsaw::Ref<RenderJob> game_job = render_service->CreateJob(frame_job);
		game_job->SetRenderTarget(j_viewport->GetRtvResource(), j_viewport->GetDsvResource(), true);
#endif

		s_sceneHierarchy->Update();
		s_systemsOrchestrator->Update();

#if !JGSW_DEV
		render_service->DisplayOn(j_viewport->GetRtvResource());
		render_service->Execute();
		j_viewport->IncrementFrame();
#endif

		s_clockService->Update();
		s_inputService->ClearFrameEvents();

		return true;
	}

	//////////////////////////////////////////////////////////////////////////
}
