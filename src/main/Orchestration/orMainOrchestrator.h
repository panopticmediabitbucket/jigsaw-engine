/*********************************************************************
 * orMainOrchestrator.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _MAIN_APPLICATION_ORCHESTRATOR_IMPL_H_
#define _MAIN_APPLICATION_ORCHESTRATOR_IMPL_H_

// Jigsaw_Engine
#include "orOrchestrator.h"
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Entities/jsEntityService.h"
#include "Graphics/GPU_Objects/J_Viewport.h"
#include "Injection/JigsawInjection.h"
#include "Input/ctInputEventWriter.h"
#include "Orchestration/orSystemsOrchestrator.h"
#include "Orchestration/orSceneHierarchy.h"
#include "Render/RenderService.h"
#include "Time/ApplicationClockService.h"

namespace Jigsaw 
{
		/// <summary>
		/// The primary loop point for the Jigsaw Engine. Loops through the primary orchestration services, event handlers, and more.
		/// </summary>
		class orMainOrchestrator : public jsSlots<orMainOrchestrator> , public orOrchestrator
		{
		public:
			/// <summary>
			/// Returns the orMessageQueue object that can be used to send instructions to the orOrchestrator's
			/// MainLoop.
			/// </summary>
			/// <returns></returns>
			Jigsaw::orMessageQueue* GetMessageQueue() override;

			/// <summary>
			/// Notifies the application that the render context and all other dependencies have been initialized 
			/// and execution is ready to proceed.
			/// </summary>
			void StartApplication() override;

			/// <summary>
			/// Notifies the application that it is time to exit. Whatever tear-down needs to be completed before exiting
			/// is done here.
			/// </summary>
			void EndApplication() override;

			/// <summary>
			/// The main orchestration call that happens once per frame. Responsible for executing all of the systems in the application
			/// and synchronizing with the Render thread. 
			/// </summary>
			bool Orchestrate() override;

		private:
			/// <summary>
			/// The application thread's main loop. Examines the orMessageQueue for any incoming instructions from the execution
			/// context. Internally calls the 'Orchestrate' method once per frame. 
			/// </summary>
			void MainLoop() override;

			/// <summary>
			/// Processes the messages in a shared orMessageQueue
			/// </summary>
			bool HandleMessages();

			SLOT_1(Jigsaw::ctInputEventWriter,						s_inputService, QUALIFIER("GameInput"));
			SLOT_2(Jigsaw::Time::ApplicationClockService,			s_clockService);
			SLOT_3(Jigsaw::jsEntityService,							s_clusterService);
			SLOT_4(Jigsaw::orSystemsOrchestrator,					s_systemsOrchestrator);
			SLOT_5(Jigsaw::orMessageQueue,							s_messageQueue, QUALIFIER("MainMessageQueue"));
			SLOT_6(Jigsaw::orSceneHierarchy,						s_sceneHierarchy);
			SLOT_7(Jigsaw::J_Viewport,								s_viewport,	QUALIFIER("GameViewport"));
			SLOT_8(Jigsaw::RenderService,					s_renderService);

			std::thread* main_thread;
		};

}
#endif
