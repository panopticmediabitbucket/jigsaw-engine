/*********************************************************************
 * orSceneHierarchy.h
 * 
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: August 2021
 *********************************************************************/
#ifndef _SCENE_CONTEXT_ORCHESTRATOR_H_
#define _SCENE_CONTEXT_ORCHESTRATOR_H_

// Jigsaw_Engine
#include "Assets/dlAssetRegistrar.h"
#include "Assets/dlLoadResources.h"
#include "Injection/JigsawInjection.h"
#include "Graphics/GraphicsContext.h"
#include "Assets/THREAD_LOCAL_SYSTEM_RESOURCES.h"
#include "Scene/jsSceneObserver.h"
#include "Scene/jsSceneLoader.h"
#include "System/UID.h"

#define POOL_SIZE 3

namespace Jigsaw
{
	/// <summary>
	/// orSceneHierarchy manages all of the active scenes in the world. It streams them in and out, and maintains a list of observers.
	/// </summary>
	class JGSW_API orSceneHierarchy : public jsSlots<orSceneHierarchy>
	{
	public:
		orSceneHierarchy();

		/// <summary>
		/// The orSceneHierarchy is only virtual for test and dev builds where we may want some decorative behavior.
		/// </summary>
		JGSW_DEV_ONLY(virtual) ~orSceneHierarchy();

		/// <summary>
		/// Update should be called once per frame on the actual instance. If there are any completed scene load jobs, they will be processed
		/// and entered into the orSceneHierarchy, spawning their initial entities.
		/// </summary>
		void Update();

		/// <summary>
		/// This is a privileged function only to be called by those who have the instance of the orSceneHierarchy. 
		/// 
		/// As soon as the load job is completed, the jsScene will be activated in the orSceneHierarchy.
		/// </summary>
		/// <param name="scene_id"></param>
		/// <returns></returns>
		void LoadAndActivateScene(const Jigsaw::UID& scene_id);

		/// <summary>
		/// Returns a temporary array of the active scene ids in the scene hierarchy
		/// </summary>
		/// <param name="out_size"></param>
		/// <returns></returns>
		Jigsaw::Unique<Jigsaw::UID[]> GetActiveSceneIds(size_t& out_size);

		/// <summary>
		/// Return the number of active scenes in the scene hierarchy
		/// </summary>
		inline u32 GetNumActiveScenes() const { return (u32)m_sceneObservers.size(); };

		/// <summary>
		/// Returns the scene observer at the specified index
		/// </summary>
		inline Jigsaw::jsSceneObserver& GetSceneObserver(u32 idx) { return *m_sceneObservers.at(idx); };

		/// <summary>
		/// Instructs the orSceneHierarchy to load the jsScene and all associated JigsawMachines
		/// into the orSceneHierarchy. An EVENT_LISTENER is returned that other processes can use to monitor the status
		/// of the load.
		/// 
		/// The contents of the scene will not be activated in the hierarchy until the next call to 'Update'
		/// </summary>
		/// <param name="scene_id"></param>
		/// <returns></returns>
		Jigsaw::Ref<const Jigsaw::EVENT_LISTENER> LoadScene(const Jigsaw::UID& scene_id);

		/// <param name="entity_service"></param>
		/// <param name="scene"></param>
		/// <returns>A new scene observer</returns>
		inline JGSW_DEV_ONLY(virtual) jsSceneObserver* CreateSceneObserver(jsEntityService* entity_service, const AssetRef<jsScene>& scene, FabricationMap&& fabricationArgs) 
		{
			return new jsSceneObserver(entity_service, scene, std::move(fabricationArgs));
		}

	protected:

		SLOT_1(Jigsaw::dlAssetRegistrar,				s_assetManager);
		SLOT_2(Jigsaw::jsEntityService,					s_entityService);
		SLOT_3(Jigsaw::dlLoadResources,					s_loadResources);

		std::vector<Jigsaw::Ref<Jigsaw::ASYNC_JOB<Jigsaw::SCENE_LOAD_RESULT>>> m_activeLoadJobs;
		std::vector<Jigsaw::jsSceneObserver*> m_sceneObservers;

	};
}
#endif