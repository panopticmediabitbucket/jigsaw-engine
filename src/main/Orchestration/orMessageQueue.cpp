#include "orMessageQueue.h"

namespace Jigsaw
{

	//////////////////////////////////////////////////////////////////////////

	orMessageQueue::orMessageQueue() { }

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::EVENT_LISTENER> orMessageQueue::EnqueueMessage(const ORCHESTRATION_ENUM& op, const QWORD& message)
	{
		static u32 _event_index = 0;

		std::scoped_lock<std::mutex> l(mut);
		std::string event_name = "ORCHESTRATION_EVENT_" + _event_index++;
		Jigsaw::Ref<Jigsaw::ASYNC_EVENT> event = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>(event_name.c_str());
		ORCHESTRATION_MESSAGE orch_message{ op, message, std::string(), event };

		messages.push(orch_message);
		return Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(event);
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::EVENT_LISTENER> orMessageQueue::EnqueueMessage(std::string&& console_message)
	{
		static unsigned int _event_index = 0;

		std::scoped_lock<std::mutex> l(mut);
		std::string event_name = "CONSOLE_EVENT_" + _event_index++;
		Jigsaw::Ref<Jigsaw::ASYNC_EVENT> event = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>(event_name.c_str());
		ORCHESTRATION_MESSAGE orch_message{ ORCHESTRATION_ENUM::ORCHESTRATION_CONSOLE_INPUT, QWORD(), std::move(console_message), event };

		messages.push(orch_message);
		return Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(event);
	}

	//////////////////////////////////////////////////////////////////////////

	bool orMessageQueue::DequeueMessage(ORCHESTRATION_MESSAGE* message) 
	{
		std::scoped_lock<std::mutex> l(mut);

		if (!messages.empty())
		{
			*message = messages.front();
			messages.pop();
			return true;
		}
		return false;
	}

	//////////////////////////////////////////////////////////////////////////

	void orMessageQueue::EnqueueDeferredMessages()
	{
		while (!deferred_messages.empty())
		{
			messages.push(deferred_messages.front());
			deferred_messages.pop();
		}
	}

	//////////////////////////////////////////////////////////////////////////

	Jigsaw::Ref<Jigsaw::EVENT_LISTENER> orMessageQueue::BlockUntil(const Jigsaw::Ref<Jigsaw::EVENT_LISTENER>& listener, MESSAGE_TIMING timing)
	{
		static unsigned int _event_index = 0;
		std::string name = "BLOCK_EVENT_" + _event_index++;
		Jigsaw::Ref<Jigsaw::ASYNC_EVENT> event = Jigsaw::MakeRef<Jigsaw::ASYNC_EVENT>(name.c_str());

		ORCHESTRATION_MESSAGE orch_message{ ORCHESTRATION_ENUM::ORCHESTRATION_BLOCK, QWORD(), std::string(), event, listener };
		if (timing == MESSAGE_TIMING::CURRENT_FRAME)
		{
			messages.push(orch_message);
		}
		else
		{
			deferred_messages.push(orch_message);
		}

		return Jigsaw::MakeRef<Jigsaw::EVENT_LISTENER>(event);
	}

	//////////////////////////////////////////////////////////////////////////

}