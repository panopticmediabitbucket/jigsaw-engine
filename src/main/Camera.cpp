#define _USE_MATH_DEFINES
#include "Camera.h"

#define _NEAR .01f
#define _FAR 10000.f

Camera::Camera(Vector3 position, Vector3 viewing_dir) : position(position), viewing_dir(viewing_dir)
{
	near_plane = _NEAR;
	far_plane = _FAR;
	x_fov = 90;
}

Mat4x4 Camera::GetRotationMatrix() const {
	Vector3 up(0, 0, 1);
	Vector3 tangent = viewing_dir.Cross(up).Normalized();
	Vector3 bi_tangent = tangent.Cross(viewing_dir).Normalized();

	return Mat4x4(Vector4::AsVector(tangent), Vector4::AsVector(bi_tangent), (-1) * Vector4::AsVector(viewing_dir), Vector4(0, 0, 0, 1));
}

Mat4x4 Camera::GetTranslationMatrix() const {
	return Mat4x4(Vector4(1, 0, 0, -position.x), Vector4(0, 1, 0, -position.y), Vector4(0, 0, 1, -position.z), Vector4(0, 0, 0, 1));
}

Mat4x4 Camera::GetViewMatrix() const {
	return GetRotationMatrix() * GetTranslationMatrix();
}

Mat4x4 Camera::GetProjectionMatrix(const float wh_ratio) const {
	float x_fov_rad = (float)(x_fov * M_PI / 180);
	float x_fov_2 = (float)(x_fov_rad / 2);

	float tan = sinf(x_fov_2) / cosf(x_fov_2);
	float cot = 1 / (tan * wh_ratio);
	float cot_y = 1 / tan;

	float proj = 2.0f / (16 + 16);
	float proj_y = 2.0f / (10 + 10);

	float orth_a = -2.0f / (far_plane - near_plane);
	float orth_b = -(near_plane + far_plane) / (far_plane - near_plane);

	float persp_a = -(far_plane + near_plane) / (far_plane - near_plane);
	float persp_b = -2 * far_plane * near_plane / (far_plane - near_plane);
	float ortho_persp_interp = 1.0f - persp_ortho_interp;

	// float z, z_b;

	return Mat4x4((cot * persp_ortho_interp) + ortho_persp_interp * proj, 0, 0, 0,
		0, (persp_ortho_interp * cot_y) + ortho_persp_interp * proj_y, 0, 0,
		0, 0, persp_ortho_interp * persp_a + ortho_persp_interp * orth_a, persp_ortho_interp * persp_b + ortho_persp_interp * orth_b,
		0, 0, persp_ortho_interp * -1, ortho_persp_interp * 1);
}