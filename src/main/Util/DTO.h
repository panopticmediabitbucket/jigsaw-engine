/*********************************************************************
 * DTO.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _DTO_H_
#define _DTO_H_

// Jigsaw_Engine
#include "AutoRegister.h"
#include "Functor.h"

namespace Jigsaw
{
	/// <summary>
	/// DTO --- DynamicTypeObject. 'T' is '_THIS_TYPE'. Useful with protected inheritance, especially where functions are defined
	/// with Macros. 
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template <typename T>
	class DTO
	{
	protected:
		typedef T _THIS_TYPE;
	};
}
#endif