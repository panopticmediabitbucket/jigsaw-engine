/*********************************************************************
 * Primitives.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: October 2021
 *********************************************************************/
#ifndef _PRIMITIVES_H_
#define _PRIMITIVES_H_

#include "Math/LinAlg.h"

namespace Jigsaw
{
	/// <summary>
	/// This structure directly mimics the D3D12_BOX struct from DirectX12. It can of course be leveraged for other APIs too. 
	/// </summary>
	struct JGSW_BOX 
	{
		u32 left;
		u32 top;
		u32 front;
		u32 right;
		u32 bottom;
		u32 back;
	};

	/// <summary>
	/// 2D Rect.
	/// </summary>
	struct Rect
	{
		float left;
		float top;
		float right;
		float bottom;

		inline bool Contains(Vector2 point) const { return left <= point.x && point.x <= right && top <= point.y && point.y <= bottom; }
	};

	struct Point
	{
		float x, y;
	};
}

#endif