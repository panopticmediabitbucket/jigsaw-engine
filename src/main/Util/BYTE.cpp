#include "BYTE.h"

void Jigsaw::Util::HexStrToBytes(const char* hex_start, const char* hex_end, u8* out)
{
	for (u32 i = 0; hex_start != hex_end; hex_start += 2) {
		out[i] = 0;
		out[i] |= (ConvertToBits(hex_start[0]) << 4);
		out[i++] |= ConvertToBits(hex_start[1]);
	}
}

Jigsaw::Unique<char[]> Jigsaw::Util::BytesToHexStr(u8* data, int len) {
	char* hex_str = new char[len * 2 + 1];
	hex_str[len * 2] = '\0';
	for (int i = 0; i < len; ++i) {
		hex_str[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
		hex_str[(2 * i) + 1] = hexmap[data[i] & 0x0F];
	}
	return Jigsaw::Unique<char[]>(hex_str);
}

Jigsaw::Unique<char[]> Jigsaw::Util::BytesToSQLHexStr(u8* data, int len) {
	char* hex_str = new char[len * 2 + 3];
	hex_str[0] = '\'';
	hex_str[len * 2 + 1] = '\'';
	hex_str[len * 2 + 2] = '\0';
	char* adj_start = &hex_str[1];
	for (int i = 0; i < len; ++i) {
		adj_start[2 * i] = hexmap[(data[i] & 0xF0) >> 4];
		adj_start[2 * i + 1] = hexmap[data[i] & 0x0F];
	}
	return Jigsaw::Unique<char[]>(hex_str);
}

Jigsaw::Unique<u8[]> Jigsaw::Util::HexStrToBytes(const char* hex, int len)
{
	u8* u8_array = new u8[len / 2];

	for (int i = 0; i < len / 2; i++) {
		u8_array[i] = (u8)0;
		u8_array[i] |= (ConvertToBits(hex[i * 2]) << 4);
		u8_array[i] |= ConvertToBits(hex[i * 2 + 1]);
	}

	return Jigsaw::Unique<u8[]>(u8_array);
}
