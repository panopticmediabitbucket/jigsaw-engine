#ifndef _COLOR_H_
#define _COLOR_H_

#include "BYTE.h"
#include "System/UID.h"

struct RGBA {
	float r = 0.f;
	float b = 0.f;
	float g = 0.f;
	float a = 0.f;
};

struct RGBA_s {
	u8 r;
	u8 b;
	u8 g;
	u8 a;
};

inline RGBA UIDToColor(const Jigsaw::UID& uid) {
	Jigsaw::UID id = uid;
	RGBA rgba;
	rgba.r = *reinterpret_cast<float*>(&id.data_a);
	rgba.b = *(reinterpret_cast<float*>(&id.data_a) + 1);
	rgba.g = *(reinterpret_cast<float*>(&id.data_b));
	rgba.a = *(reinterpret_cast<float*>(&id.data_b) + 1);
	return rgba;
}

#endif