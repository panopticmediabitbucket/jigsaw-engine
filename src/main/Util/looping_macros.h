#ifndef _LOOPING_MACROS_H_
#define _LOOPING_MACROS_H_

#define h_next i
#define g_next h
#define f_next g
#define e_next f
#define d_next e
#define c_next d
#define b_next c
#define a_next b

#define six_next seven
#define five_next six
#define four_next five
#define three_next four
#define two_next three
#define one_next two
#define zero_next one

#define seven_ 7
#define six_ 6
#define five_ 5
#define four_ 4
#define three_ 3
#define two_ 2
#define one_ 1
#define zero_ 0

#define _EXPAND(...)__VA_ARGS__
#define _QUOTE r

#define _mac_arg_fe_1(mac, i, x, ...) _EXPAND(mac)(i, x)
#define _mac_arg_fe_2(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_1(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_arg_fe_3(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_2(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_arg_fe_4(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_3(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_arg_fe_5(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_4(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_arg_fe_6(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_5(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_arg_fe_7(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_6(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_arg_fe_8(mac, i, x, ...) _EXPAND(mac)(i, x), _EXPAND(_mac_arg_fe_7(mac, _EXPAND(i)_next, __VA_ARGS__))

#define _mac_fe_1(mac, i, x, ...) _EXPAND(mac)(i, x)
#define _mac_fe_2(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_1(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_fe_3(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_2(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_fe_4(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_3(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_fe_5(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_4(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_fe_6(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_5(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_fe_7(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_6(mac, _EXPAND(i)_next, __VA_ARGS__))
#define _mac_fe_8(mac, i, x, ...) _EXPAND(mac)(i, x) _EXPAND(_mac_fe_7(mac, _EXPAND(i)_next, __VA_ARGS__))

#define _mac_fe_1_no_arg(mac, x, ...) _EXPAND(mac)(x)
#define _mac_fe_2_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_1_no_arg(mac, __VA_ARGS__))
#define _mac_fe_3_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_2_no_arg(mac, __VA_ARGS__))
#define _mac_fe_4_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_3_no_arg(mac, __VA_ARGS__))
#define _mac_fe_5_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_4_no_arg(mac, __VA_ARGS__))
#define _mac_fe_6_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_5_no_arg(mac, __VA_ARGS__))
#define _mac_fe_7_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_6_no_arg(mac, __VA_ARGS__))
#define _mac_fe_8_no_arg(mac, x, ...) _EXPAND(mac)(x) _EXPAND(_mac_fe_7_no_arg(mac, __VA_ARGS__))

#define _COUNT_TYPE_ARGS(_1, _2, _3, _4, _5, _6, _7, _8, N, ...)N

#endif