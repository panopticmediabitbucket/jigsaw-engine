#ifndef _u8_H_
#define _u8_H_
#include "Ref.h"

 __declspec(align(alignof(max_align_t))) struct _RAW_DATA {};

namespace Jigsaw {
	namespace Util {
		/*THESE ARE DUMB*/
		template <typename T>
		constexpr u8* CastToByteArray(T* element)
		{
			void* raw = static_cast<void*>(element);
			return static_cast<u8*>(raw);
		}

		template <typename T>
		constexpr T* CastToTypeObject(u8* data)
		{
			void* raw = static_cast<void*>(data);
			return static_cast<T*>(raw);
		}
		/*END THESE ARE DUMB*/

		constexpr const char hexmap[] = { '0', '1', '2', '3', '4', '5', '6', '7',
						   '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };


		inline u8 ConvertToBits(char character)
		{
			return character > '9' ? character > 'Z' ? character - 0x57 : character - 0x37 : character - 0x30;
		}

		void HexStrToBytes(const char* hex_start, const char* hex_end, u8* out);

		Jigsaw::Unique<char[]> BytesToHexStr(u8* data, int len);

		Jigsaw::Unique<char[]> BytesToSQLHexStr(u8* data, int len);

		Jigsaw::Unique<u8[]> HexStrToBytes(const char* hex, int len);

		inline void NextAlignN(u32& t, u32 n) { t += (((t ^ (n - 1)) + 0b01) & (n - 1)); };

		inline void NextAlignN(size_t& t, size_t n) { t += (((t ^ (n - 1)) + 0b01) & (n - 1)); };

		template <typename T>
		inline void NextAlign(u32& t) { NextAlignN(t, alignof(T)); };

		template <typename T>
		inline void NextAlign(size_t& t) { NextAlignN(t, alignof(T)); };

		template <typename T> inline T ByteSwap(T old)
		{
			T t = 0;
			T mask = 0xFF;
			s32 minRot = -8 * (sizeof(T) - 1);
			for (u32 i = 0; i < sizeof(T); i++)
			{
				T val = old & mask;
				s32 rot = minRot + (16 * (sizeof(T) - 1 - i));
				val = rot > 0 ? val << rot : val >> -rot;
				T newVal = t | val;
				t = newVal;
				mask <<= 8;
			}
			return t;
		}

		typedef u64 ptr;
	}
}
#endif