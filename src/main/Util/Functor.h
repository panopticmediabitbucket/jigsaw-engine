/*********************************************************************
 * Functor.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _FUNCTOR_H_
#define _FUNCTOR_H_

namespace Jigsaw
{

	template<typename RetType, typename DirectArg, typename ...Args>
	class Functor
	{
	public:
		Functor(RetType(*func)(DirectArg, Args...), DirectArg arg) : m_func(func), m_arg(arg) {}
		Functor() = default;
		inline Functor& operator=(const Functor& other)
		{
			m_func = other.m_func;
			m_arg = other.m_arg;
			return *this;
		}

		inline RetType operator()(Args... args) const
		{
			return m_func(m_arg, std::forward<Args>(args)...);
		}

		inline operator bool() const
		{
			return m_func;
		}

	private:
		RetType(*m_func)(DirectArg, Args...) = nullptr;
		DirectArg m_arg;

	};

	/// <summary>
	/// Helper template for storing instance callbacks.
	/// </summary>
	/// <typeparam name="RetType"></typeparam>
	/// <typeparam name="...Args"></typeparam>
	template <typename RetType, typename...Args>
	class MemberFunctor
	{
	public:
		/// <summary>
		/// A MemberFunctor factory function.
		/// </summary>
		/// <param name="obj"></param>
		/// <param name="func"></param>
		template<typename T, RetType(T::* Func)(Args...)>
		static MemberFunctor<RetType, Args...> Create(T* obj)
		{
			MemberFunctor<RetType, Args...> functor;
			functor.m_function = &FunctionRef<T, Func>;
			functor.m_obj = obj;
			return functor;
		}

		RetType operator()(Args... args)
		{
			if constexpr (std::is_same_v<void, RetType>)
			{
				m_function(m_obj, args...);
			}
			else
			{
				return m_function(m_obj, args...);
			}
		}

	private:
		typedef RetType(*FunctionTemplate)(void*, Args...);

		template<typename T, RetType(T::* Func)(Args...)>
		static RetType FunctionRef(void* _obj, Args... args)
		{
			T* obj = static_cast<T*>(_obj);
			if constexpr (std::is_same_v<void, RetType>)
			{
				(obj->*Func)(args...);
			}
			else
			{
				return (obj->*Func)(args...);
			}
		}

		MemberFunctor() = default;

		FunctionTemplate m_function = nullptr;
		void* m_obj = nullptr;

	};
}

#endif // _FUNCTOR_H_
