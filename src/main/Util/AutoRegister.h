/*********************************************************************
 * AutoRegister.h
 *
 * Author: Luke Randazzo <jl_randazzo@hotmail.com>
 * Originally created: September 2021
 *********************************************************************/
#ifndef _AUTOREGISTER_H_
#define _AUTOREGISTER_H_

namespace Jigsaw
{

	/// <summary>
	/// A utility struct whose only parameter is a function that returns nothing and takes no parameters.
	/// This is particularly useful if some static registration/initialization needs to happen.
	/// </summary>
	struct AutoRegister
	{
		template <typename... Ts>
		AutoRegister(void (*func)(Ts...), Ts&& ... Args)
		{
			func(std::forward<Ts>(Args)...);
		};

		template <typename... Ts>
		static int CallWith(void (*func)(Ts...), Ts ... Args)
		{
			func(std::forward<Ts>(Args)...);
			return 0;
		}
	};
}

#endif // _AUTOREGISTER_H_
