#ifndef _JGSW_API_H_
#define _JGSW_API_H_

#define JGSW_API __declspec(dllexport)
#define INTERNAL 
#define EXPIMP_TEMPLATE
#define IS_ALSO_A ,
#define IS_ALSO_AN ,

#define CANT_COPY(classname) classname(const classname& other) = delete; classname& operator=(const classname& other) = delete
#define CANT_MOVE(classname) classname(classname&& other) = delete; classname& operator=(classname&& other) = delete

#define NUM_DEPENDENCY_THREADS 8

#ifdef JGSW_DEBUG
#define JGSW_NONFINAL
#else
#ifdef JGSW_BANK
#define JGSW_NONFINAL
#endif
#endif

#if JGSW_DEV
#define JGSW_DEV_ONLY(...) __VA_ARGS__
#else
#define JGSW_DEV_ONLY(a)
#endif

#if JGSW_DEBUG
#define JGSW_DEBUG_ONLY(a) a
#else
#define JGSW_DEBUG_ONLY(a)
#endif

typedef unsigned char u8;
typedef unsigned short u16;
typedef unsigned int u32;
typedef unsigned long long u64;
typedef char s8;
typedef short s16;
typedef int s32;
typedef long long s64;

#pragma warning(disable:4834 26812)

#endif