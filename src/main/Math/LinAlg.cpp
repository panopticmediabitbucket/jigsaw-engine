#include "Math/LinAlg.h"
#include "Marshalling/JigsawMarshalling.h"

START_REGISTER_SERIALIZABLE_CLASS(Vector3)
REGISTER_SERIALIZABLE_FIELD(Vector3, float, x)
REGISTER_SERIALIZABLE_FIELD(Vector3, float, y)
REGISTER_SERIALIZABLE_FIELD(Vector3, float, z)
END_REGISTER_SERIALIZABLE_CLASS(Vector3)

START_REGISTER_SERIALIZABLE_CLASS(Quaternion)
REGISTER_SERIALIZABLE_FIELD(Quaternion, Vector3, ijk)
REGISTER_SERIALIZABLE_FIELD(Quaternion, float, w)
END_REGISTER_SERIALIZABLE_CLASS(Quaternion)

Vector3 Vector3::WeightedAverage(const float* w, const Vector3* v, size_t count) {
	Vector3 res;
	for (int i = 0; i < count; i++) {
		res = res + (w[i] * v[i]);
	}
	return res;
}

Mat3x3::Mat3x3(float a00, float a01, float a02, float a10, float a11, float a12, float a20, float a21, float a22) {
	data[0] = a00;
	data[1] = a01;
	data[2] = a02;
	data[3] = a10;
	data[4] = a11;
	data[5] = a12;
	data[6] = a20;
	data[7] = a21;
	data[8] = a22;
}

float& Mat3x3::operator()(const UINT m, const UINT n) {
	return data[(m * 3) + n];
}

Mat3x3 Mat3x3::operator*(const Mat3x3& other) const {
	Vector3 r_0 = GetRow(0);
	Vector3 r_1 = GetRow(1);
	Vector3 r_2 = GetRow(2);
	return Mat3x3::FromRows(r_0 * other, r_1 * other, r_2 * other);
}

Mat3x3 Mat3x3::operator+(const Mat3x3& other) const {
	return Mat3x3(data[0] + other.data[0], data[1] + other.data[1], data[2] + other.data[2], data[3] + other.data[3], data[4] + other.data[4], data[5] + other.data[5], data[6] + other.data[6], data[7] + other.data[7], data[8] + other.data[8]);
}

Mat3x3 Mat3x3::operator-(const Mat3x3& other) const {
	return Mat3x3(data[0] - other.data[0], data[1] - other.data[1], data[2] - other.data[2], data[3] - other.data[3], data[4] - other.data[4], data[5] - other.data[5], data[6] - other.data[6], data[7] - other.data[7], data[8] - other.data[8]);
}

Vector3 Mat3x3::operator*(const Vector3& vec) const {
	return Vector3(GetRow(0).Dot(vec), GetRow(1).Dot(vec), GetRow(2).Dot(vec));
}

Vector3 Mat3x3::GetRow(unsigned int m) const {
	int row_i = m * 3;
	float x = data[row_i];
	float y = data[row_i + 1];
	float z = data[row_i + 2];
	return Vector3(x, y, z);
}

Vector3 Mat3x3::GetColumn(unsigned int n) const {
	return Vector3(data[0 + n], data[3 + n], data[6 + n]);
}

Mat3x3 Mat3x3::FromColumns(Vector3 a, Vector3 b, Vector3 c) {
	return Mat3x3(a.x, b.x, c.x,
		a.y, b.y, c.y,
		a.z, b.z, c.z);
}

Mat3x3 Mat3x3::FromRows(Vector3 a, Vector3 b, Vector3 c) {
	return Mat3x3(a.x, a.y, a.z,
		b.x, b.y, b.z, 
		c.x, c.y, c.z);
}

Mat3x3 Mat3x3::EulerAnglesYXZ(float a, float b, float c) {

	float cos_a = cosf(a);
	float sin_a = sinf(a);
	Mat3x3 Y_rot(cos_a, 0, sin_a,
			0, 1, 0,
			-sin_a, 0, cos_a);

	float sin_b = sinf(b);
	float cos_b = cosf(b);
	Mat3x3 X_rot(1, 0, 0,
		0, cos_b, -sin_b,
		0, sin_b, cos_b);

	float cos_c = cosf(c);
	float sin_c = sinf(c);
	Mat3x3 Z_rot(cos_c, -sin_c, 0,
		sin_c, cos_c, 0,
		0, 0, 1);

	return Z_rot * X_rot * Y_rot;
}

Mat3x3 Mat3x3::SkewSymmetric(const Vector3& v) {
	return Mat3x3(0, -v.z, v.y, v.z, 0, -v.x, -v.y, v.z, 0);
}

Mat3x3 Mat3x3::ProjectionMatrix(const Vector3& v) {
	return Mat3x3(v.x * v.x, v.x * v.y, v.x * v.z, 
		v.y * v.x, v.y * v.y, v.y * v.z,
		v.z * v.x, v.z * v.y, v.z * v.z);
}

Mat3x3 Mat3x3::FromAngleAxis(float angle, const Vector3& axis) {
	Vector3 norm = axis.Normalized();
	Mat3x3 Mc = Mat3x3::SkewSymmetric(norm);
	Mat3x3 Mp = Mat3x3::ProjectionMatrix(norm);

	Mat3x3 perp = cosf(angle)* (Identity() - Mp);
	Mat3x3 o_perp = sinf(angle) * Mc;

	return Mp + (perp) + (o_perp);
}

bool Mat3x3::operator==(const Mat3x3& other) const {
	bool ret = true;
	for (int i = 0; i < 9 && ret; i++) {
		ret &= Approx(data[i], other.data[i]);
	}
	return ret;
}

Mat3x3 Mat3x3::Transpose() const {
	return Mat3x3::FromColumns(GetRow(0), GetRow(1), GetRow(2));
}

Mat3x3 Mat3x3::Identity() {
	return Mat3x3(1, 0, 0, 0, 1, 0, 0, 0, 1);
}

bool Approx(float x, float y)
{
	float c = x - y;
	return c > -.000001f && c < .000001f;
 
}

Quaternion Quaternion::FromAngleAxis(float angle, Vector3 axis) {
	float cos = cosf(angle / 2);
	float sin = sinf(angle / 2);
	Vector3 ijk = sin * axis.Normalized();
	return Quaternion(cos, ijk);
}
