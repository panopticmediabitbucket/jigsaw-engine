#ifndef _DUAL_QUATERNION_H_
#define _DUAL_QUATERNION_H_

#include "LinAlg.h"

namespace Jigsaw {
	namespace Math {

		/// <summary>
		/// DualQuaternions are generally used to encode translation and rotation information, generally. 
		/// </summary>
		class JGSW_API DualQuaternion {
		public:
			DualQuaternion();

			/// <summary>
			/// Creates a dual quaternion with real part r and dual part d
			/// </summary>
			/// <param name="r"></param>
			/// <param name="d"></param>
			DualQuaternion(Quaternion&& r, Quaternion&& d);

			/// <summary>
			/// Creates a dual quaternion with the rotation and translation
			/// </summary>
			/// <param name="quaternion"></param>
			/// <param name="translation"></param>
			DualQuaternion(const Quaternion& rotation, const Vector3& translation);

			/// <summary>
			/// Creates a dual quaternion with real part r and dual part d
			/// </summary>
			/// <param name="r"></param>
			/// <param name="d"></param>
			DualQuaternion(const Quaternion& r, const Quaternion& d);

			/// <summary>
			/// Creates a DualQuaternion from a point
			/// </summary>
			/// <param name="point"></param>
			/// <returns></returns>
			static DualQuaternion FromPoint(Vector3 point);

			/// <summary>
			/// Performs Screw Linear Interpolation between the DualQuaternions d and q
			/// </summary>
			/// <param name="d"></param>
			/// <param name="q"></param>
			/// <param name="t"></param>
			/// <returns></returns>
			static DualQuaternion ScLERP(const DualQuaternion d, const DualQuaternion q, const float t);

			/// <summary>
			/// Creates a DualQuaternion that will perform the given rotation and translation when applied on points
			/// </summary>
			/// <param name="angle"></param>
			/// <param name="axis"></param>
			/// <param name="translation"></param>
			/// <returns></returns>
			static DualQuaternion FromAxisAngleTranslation(const Vector3& angle, float axis, const Vector3 translation);

			/// <summary>
			/// Applies the dual quaternion rotation and translation to a specific point
			/// </summary>
			/// <param name="point"></param>
			/// <returns></returns>
			Vector3 TransformPoint(const Vector3& point) const;

			/// <summary>
			/// Provides the quaternion conjugate and the dual conjugate to the given dual quaternion
			/// </summary>
			/// <returns></returns>
			DualQuaternion DualQuatConjugate() const;

			/// <summary>
			/// Gives the DualConjugate for the Dual Quaternion. Good for undoing a translation.
			/// </summary>
			/// <returns></returns>
			DualQuaternion DualConjugate() const;

			/// <summary>
			/// 
			/// </summary>
			/// <returns></returns>
			DualQuaternion Conjugate() const;

			/// <summary>
			/// Provides the pure translation from the origin embedded in the dual quaternion
			/// </summary>
			/// <returns></returns>
			Vector3 GetTranslation() const;

			inline void SetTranslation(const Vector3& transl) {
				d = (Quaternion(0, transl) * r) * .5f;
			}

			float GetLength() const;

			/// <summary>
			/// Dual Linear Blend function with an array of weights and an array of dual quaternions.
			/// Returns a DualQuaternion that blends 
			/// </summary>
			/// <param name="w"></param>
			/// <param name="quats"></param>
			/// <param name="count"></param>
			/// <returns></returns>
			static DualQuaternion DualLinearBlend(float* w, DualQuaternion* quats, size_t count);

			/// <summary>
			/// Multiply one dual quaternion with another and return the result
			/// </summary>
			/// <param name="other"></param>
			/// <returns></returns>
			DualQuaternion operator*(const DualQuaternion& other) const;

			DualQuaternion operator*(float w) const;

			DualQuaternion operator+(const DualQuaternion& other) const;

			DualQuaternion& operator+=(const DualQuaternion& other);

			Quaternion r, d;

		private:
			DualQuaternion(const Vector3& axis, float angle, const Vector3& translation);

		};
	}
}
#endif