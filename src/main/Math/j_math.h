#ifndef _J_MATH_H_
#define _J_MATH_H_

namespace Jigsaw {
	inline float interp(float a, float b, float i) {
		return ((b - a) * i) + a;
	}
}
#endif