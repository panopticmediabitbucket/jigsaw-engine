#include "DualQuaternion.h"

namespace Jigsaw {
	namespace Math {

		//////////////////////////////////////////////////////////////////////////
		
		DualQuaternion::DualQuaternion(const Vector3& axis, float angle, const Vector3& translation) : r(Quaternion::FromAngleAxis(angle, axis)) {
			SetTranslation(translation);
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion::DualQuaternion() : r(1, 0, 0, 0), d(0, 0, 0, 0)
		{
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion::DualQuaternion(Quaternion&& r, Quaternion&& d) : r(r), d(d)
		{
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion::DualQuaternion(const Quaternion& rotation, const Vector3& translation) : r(rotation) {
			SetTranslation(translation);
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion::DualQuaternion(const Quaternion& r, const Quaternion& d) : r(r), d(d) {
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion DualQuaternion::FromPoint(Vector3 point) {
			return DualQuaternion(Quaternion(1, 0, 0, 0), Quaternion(0, point));
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion DualQuaternion::ScLERP(const DualQuaternion d, const DualQuaternion q, const float t) {
			DualQuaternion d_conj_q = d.DualQuatConjugate() * q;
			float cos_th_2 = d_conj_q.r.w;
			float th = acosf(cos_th_2) * 2;
			float sin_th_2 = sinf(th / 2);
			Vector3 l = (1 / sin_th_2) * d_conj_q.r.ijk;

			float tth_2 = t * th / 2;
			float cos_tth_2 = cosf(tth_2);
			float sin_tth_2 = sinf(tth_2);

			Vector3 transl = d_conj_q.GetTranslation();
			float dist = transl.Dot(l);
			float t_d_2 = t * dist / 2;

			DualQuaternion sclerp(Quaternion(cos_tth_2, sin_tth_2 * l), Quaternion(t_d_2 * -sin_tth_2, t_d_2 * cos_tth_2 * l));

			return d * sclerp;
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion DualQuaternion::FromAxisAngleTranslation(const Vector3& angle, float axis, const Vector3 translation) {
			return DualQuaternion(angle, axis, translation);
		}

		//////////////////////////////////////////////////////////////////////////

		Vector3 DualQuaternion::TransformPoint(const Vector3& point) const {
			DualQuaternion q_point = FromPoint(point);
			return ((*this) * q_point * DualQuatConjugate()).d.ijk;
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion DualQuaternion::DualQuatConjugate() const {
			return DualQuaternion(r.Conjugate(), Quaternion(-d.w, d.ijk) );
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion DualQuaternion::DualConjugate() const {
			return DualQuaternion(r, Quaternion(-d.w, -1 * d.ijk));
		}

		//////////////////////////////////////////////////////////////////////////

		DualQuaternion DualQuaternion::Conjugate() const {
			return DualQuaternion(r.Conjugate(), d.Conjugate());
		}

		//////////////////////////////////////////////////////////////////////////

		Vector3 DualQuaternion::GetTranslation() const {
			return 2 * (d * r.Conjugate()).ijk;
		}

		//////////////////////////////////////////////////////////////////////////

		float DualQuaternion::GetLength() const {
			return 0;
		}

		DualQuaternion DualQuaternion::DualLinearBlend(float* w, DualQuaternion* quats, size_t count)
		{
			Jigsaw::Math::DualQuaternion dlb(Quaternion(0, 0, 0, 0), Quaternion(0, 0, 0, 0));
			for (int i = 0; i < count; i++) {
				dlb += (quats[i] * w[i]);
			}
			float r_mag = dlb.r.GetLength();

			Quaternion ret_r = dlb.r * (1 / r_mag);
			Quaternion ret_d = dlb.d * (1 / (r_mag));

			return DualQuaternion(ret_r, ret_d);
		}

		DualQuaternion DualQuaternion::operator*(const DualQuaternion& other) const {
			return DualQuaternion(r * other.r, (r * other.d) + (d * other.r));
		}

		DualQuaternion DualQuaternion::operator*(float w) const {
			return DualQuaternion(Quaternion(w * r.w, w * r.ijk) , Quaternion(w * d.w, w * d.ijk));
		}

		DualQuaternion DualQuaternion::operator+(const DualQuaternion& other) const {
			return DualQuaternion(r + other.r, d + other.d);
		}

		DualQuaternion& DualQuaternion::operator+=(const DualQuaternion& other) {
			r += other.r;
			d += other.d;
			return *this;
		}

	}
}

