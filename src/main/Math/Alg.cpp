#include "Alg.h"

#include <cmath>

bool Jigsaw::Math::QuadraticFormula(float a, float b, float c, float* out_1, float* out_2) {
    float b_sqrd = b * b;
    float f_a_c = 4 * a * c;
    float inner_val = b_sqrd - f_a_c;
    if (inner_val >= 0) {
        float root_val = inner_val == 0 ? 0 : sqrtf(inner_val);
        float o_o_t_a = 1 / (2 * a);

        if(out_1 != nullptr)
            *out_1 = (-b + root_val) * o_o_t_a;
        if(out_2 != nullptr)
            *out_2 = (-b - root_val) * o_o_t_a;

        return true;
    }

    return false;
}
