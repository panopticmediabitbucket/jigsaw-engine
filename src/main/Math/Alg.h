#ifndef _ALG_H_
#define _ALG_H_

namespace Jigsaw {
	namespace Math {

		bool QuadraticFormula(float a, float b, float c, float* out_1, float* out_2);

	}
}
#endif