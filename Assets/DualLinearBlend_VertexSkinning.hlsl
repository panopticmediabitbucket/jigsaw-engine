#include "quaternion.hlsli"

struct BONE_TRANSFORM
{
    float2x4 dualRigidTransform;
    float3 scale;
};

cbuffer MVP_CONSTANT_BUFFER : register(b0) {
	matrix rotationMatrix;
	matrix mvpMat;
    Buffer<float4> bones;
}

struct VERT_INPUT {
	float3 vPos			    : POSITION;
	float3 vColor	        : COLOR0;
	float3 vNorm			: NORMAL0;

	// bone indices
    min16uint boneIdx_a     : INDEX0;
    min16uint boneIdx_b     : INDEX1;
    min16uint boneIdx_c     : INDEX2;
    min16uint boneIdx_d     : INDEX3;

	// the bone weights for this vertex
    float weight_a			: WEIGHT0;
    float weight_b		    : WEIGHT1;
    float weight_c		    : WEIGHT2;
    float weight_d		    : WEIGHT3;
};

struct PIX_INPUT {
	float4 position	: SV_POSITION;
	float4 color	: COLOR0;
	float4 normal	: NORMAL0;
};

inline BONE_TRANSFORM GetBoneTransformAt(min16uint idx) {
    idx *= 3;
    BONE_TRANSFORM transform;
    transform.dualRigidTransform[0].xyzw = bones[idx];
    transform.dualRigidTransform[1].xyzw = bones[idx + 1];
    transform.scale.xyz = bones[idx + 2].xyz;
    return transform;
}

inline float2x4 DualLinearBlend(BONE_TRANSFORM transforms[4], float weights[4], uint numBlends)
{
    float2x4 output = float2x4(0, 0, 0, 0, 0, 0, 0, 0);

    for (uint i = 0; i < numBlends; i++) {
        
    }

    return output;
}

PIX_INPUT main( VERT_INPUT input ) {
    BONE_TRANSFORM transforms[4];

    // Getting the bone transforms
    uint numBlends = 1;
    transforms[0] = GetBoneTransformAt(input.boneIdx_a);
    if (input.boneIdx_b != 0xFFFF) {

        transforms[1] = GetBoneTransformAt(input.boneIdx_b);
        numBlends++;
        if (input.boneIdx_c != 0xFFFF) {

            transforms[2] = GetBoneTransformAt(input.boneIdx_c);
            numBlends++;
            if (input.boneIdx_d != 0xFFFF) {

                transforms[3] = GetBoneTransformAt(input.boneIdx_d);
                numBlends++;
            }
        }
    }

    float2x4 blendPose = DualLinearBlend(transforms, (float[4])input.weight_a, numBlends);

	PIX_INPUT output;
    float3 skinnedPosition = dquat_trans_point(blendPose, input.vPos);
    float3 skinnedNormal = quat_rotate(blendPose[DQ_R], input.vNorm);

	output.position = mul(mvpMat, float4(skinnedPosition, 1.0f));
	output.normal = mul(mvpMat, float4(skinnedNormal, 0.0f));

	// perspective division
	output.position /= output.position.w;

	// shrinking to clipping range
	output.position.z += 1;
	output.position.z /= 2;

    output.color = float4(1, 1, 1, 1);
	return output;
}
