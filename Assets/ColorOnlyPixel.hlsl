struct PIX_INPUT {
	float4 position	: SV_POSITION;
	float4 color	: COLOR0;
};

float4 main(PIX_INPUT input) : SV_TARGET{
	return input.color;
}