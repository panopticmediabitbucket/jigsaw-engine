#ifndef _QUATERNION_HLSLI_
#define _QUATERNION_HLSLI_

// The Quaternion is defined as having the w (real) component first, and the ijk (non-real) components after
#define Q_IDENTITY float4(1, 0, 0, 0);

#define DQ_IDENTITY float2x4(1, 0, 0, 0, 0, 0, 0, 0);

// Access the real/dual components of the dual quaternion
#define DQ_R 0
#define DQ_D 1
#define W x
#define IJK yzw
#define WIJK xyzw

// Multiply two quaternions
inline float4 quat_mul(float4 q_a, float4 q_b) {
    return float4(q_a.W * q_b.W - dot(q_a.IJK, q_a.IJK), q_a.W * q_b.IJK + q_b.W * q_a.IJK + cross(q_a.IJK, q_b.IJK));
}

// Conjugate of the quaternion
inline float4 quat_conj(float4 q_a) {
    return float4(q_a.W, -q_a.IJK);
}

// Rotates a vector with the quaternion. This is an optimized rotation.
inline float3 quat_rotate(float4 q, float3 v) {
    return 2 * (dot(q.IJK, v) * q.IJK + q.W * cross(q.IJK, v)) + ((q.W * q.W - dot(q.IJK, q.IJK)) * v);
}

// Delivers the dual quaternion conjugate needed to perform rigid transformations 
inline float2x4 dquat_conj(float2x4 dq) {
    return float2x4(quat_conj(dq[DQ_R]), -dq[DQ_D].W, dq[DQ_D].IJK);
}

inline float2x4 dquat_mul(float2x4 dq_a, float2x4 dq_b) {
    return float2x4(quat_mul(dq_a[DQ_R].WIJK, dq_b[DQ_R].WIJK), quat_mul(dq_a[DQ_R].WIJK, dq_b[DQ_D].WIJK) + quat_mul(dq_a[DQ_D].WIJK, dq_b[DQ_R].WIJK));
}

// Performs dual quaternion transformation on a point.
inline float3 dquat_trans_point(float2x4 dq, float3 p) {
    float2x4 mul_a = float2x4(dq[DQ_R].WIJK, quat_mul(dq[DQ_R].WIJK, float4(0, p)) + dq[DQ_D].WIJK);
    return dquat_mul(mul_a, dquat_conj(dq))[DQ_D].IJK;
}

#endif // _QUATERNION_HLSLI_