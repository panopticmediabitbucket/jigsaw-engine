cbuffer MVP_CONSTANT_BUFFER : register(b0) {
	matrix rotationMatrix;
	matrix mvpMat;
}

struct VERT_INPUT {
	float3 vPos		: POSITION;
	float3 vColor	: COLOR0;
	float3 vNorm	: NORMAL0;
};

struct PIX_INPUT {
	float4 position	: SV_POSITION;
	float4 color	: COLOR0;
	float4 normal   : NORMAL0;
};

PIX_INPUT main( VERT_INPUT input ) {
	PIX_INPUT output;
	output.color = float4(input.vColor, 1);
	output.position = mul(mvpMat, float4(input.vPos, 1.0f));

	// perspective division
	output.position /= output.position.w;

	// shrinking to clipping range
	output.position.z += 1;
	output.position.z /= 2;

	// passing normal
	//output.normal = mul(mvpMat, float4(input.vNorm, 0.0f)).xyz;
	output.normal = mul(rotationMatrix, float4(input.vNorm, 0.0f));
	return output;
}